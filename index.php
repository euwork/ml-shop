<?php

ini_set('memory_limit', '256M');
date_default_timezone_set('US/Eastern');
ini_set('display_errors', 1);
error_reporting( E_ALL);
// ini_set('display_errors', 0);
// error_reporting( E_ERROR );
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) { 
	$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"]; 
} elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) { 
	$ip_list = explode(', ', $_SERVER["HTTP_X_FORWARDED_FOR"]);
	$_SERVER['REMOTE_ADDR'] = $ip_list[count($ip_list)-1]; 
}


$installmode = false;
if(trim($_SERVER['REQUEST_URI'], '/') == 'install'){
	$installmode = true;
}
if($installmode){
	$config=dirname(__FILE__).'/protected/config/install.php';
} else {
	$config=dirname(__FILE__).'/protected/config/main.php';
}

$db_config = dirname(__FILE__) . '/protected/config/db.php'; 
$params_config = dirname(__FILE__) . '/protected/config/params.php';

if(!file_exists($db_config)) {
	touch($db_config); chmod($db_config, 0666);
}

if(!file_exists($params_config)) {
	copy( $params_config . '.sample', $params_config); chmod($params_config, 0666);
}

if($installmode == false && ( strlen(file_get_contents($db_config)) == 0 ) ){
	header('Location: /install'."\n");
	exit;
}



// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';

require_once($yii);
Yii::createWebApplication($config)->run();
