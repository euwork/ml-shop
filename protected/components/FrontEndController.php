<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class FrontEndController extends CController
{

	
	public $layout='//layouts/column';
	public $description = '';
    public $keywords = '';
    public $breadcrumbs;
    public $domain;


	public function init()
	{
		parent::init();

		if(trim($_SERVER['REQUEST_URI'], '/') == 'install'){ return false;}
		
		if (!Yii::app()->request->getIsAjaxRequest()) {
			// если переключаемся на DE , чистим корзину. на страны с FR_BASE - тоже чистим корзину
			// Исключаем 404 запросы к статическим файлам из шаблона
			if(!preg_match('@\.(ico|css|js|png|jpg|jpeg|png|gif|svg|swf|webp|ttf|otf)$@i', Yii::app()->request->pathInfo)){
				$this->check_products_base();
			}
			// Yii::app()->end();
		}

		Yii::app()->currencies->check();

		Yii::app()->theme = Yii::app()->config->get('theme');


        if (!Yii::app()->request->getIsAjaxRequest()) {
		    Yii::app()->traffCounter->countMe();
        }

        $this->domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
        if(function_exists('idn_to_utf8')){
	        $this->domain = idn_to_utf8($this->domain);
        }
	}

	public function beforeAction($action) 
	{
	  $uri = Yii::app()->request->requestUri; //$_SERVER['REQUEST_URI'];
	  $len = strlen($uri);

	  if ( $len > 1 && $uri[$len - 1] == '/' ) {
		  $this->redirect(rtrim($uri, '/'), true, 301);
	  }

	  if ( (Yii::app()->urlManager->showScriptName 	=== false) 
			   && ( (strpos($uri,'index.php') !== false) || (strpos($uri, '?r=') !== false) || (strpos($uri, '&r=') !== false)) 
					   ){

  	        header('HTTP/1.0 404 Not Found');
	        // Отображаем стандартное представление ошибки
	        $this->layout = '/clear';
	        $this->render('/'.Yii::app()->errorHandler->errorAction, array(
	            'code' => 404,
	            'message'=> 'The requested page does not exist.',
	        ));
	        Yii::app()->end();
	  }

	  return true;
	}


    private function check_products_base()
    {
    	$positions = Yii::app()->shoppingCart->getPositions();

    	if(count($positions) == 0) return false;
    	$base = Products::BASE_FR;
        foreach($positions as $item)
        {
            if($item->isGerBase()){
                $base = Products::BASE_GER;
            }
            break;
        }

    	$msg = false;
        // if(Yii::app()->language != 'de' && $base == Products::BASE_GER){
        // 	Yii::app()->shoppingCart->clear();
        // }

        // if(Yii::app()->language == 'de' && $base != Products::BASE_GER){
        // 	Yii::app()->shoppingCart->clear();
        // }

        if( $base == Products::BASE_GER){
        	Yii::app()->shoppingCart->clear();
        }

    }
}