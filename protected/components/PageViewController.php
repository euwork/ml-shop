<?php
class PageViewController extends FrontEndController
{

	public function actionView($slug)
	{
		if($slug !== strtolower($slug)){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		if(preg_match('@^_@', $slug)){
			throw new CHttpException(404,'The requested page does not exist.');
		}


		$model = $this->loadModelBySlug($slug);

		$domain = $this->domain;

		$this->pageTitle = $model->title;
		$this->keywords = $model->keywords;
		$this->description = $model->description;

		$model->title = str_replace('{domain}', $domain, $model->title);
		$model->description = str_replace('{domain}', $domain, $model->description);
		$model->header = str_replace('{domain}', $domain, $model->header);
		$model->content = str_replace('{domain}', $domain, $model->content);


		$viewpath = strtolower(get_class($model));

		if(get_class($model) == 'Articles'){
			
			if($model->lang != Yii::app()->language){
				$this->redirect(Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.article')));
			}			

		}

		if(get_class($model) == 'Drugs'){
			
			$model->instruction = str_replace('{domain}', $domain, $model->instruction);		

		}

		$this->render('webroot.themes.'.Yii::app()->theme->name.'.views.'.$viewpath.'.view',array(
			'model'=>$model,
		));

	}


	public function loadModelBySlug($slug)
	{

		$uri = Yii::app()->request->requestUri;
		$lang = Yii::app()->language;
		$model = null;
		//drugs
		$prefix = Yii::app()->config->get('prefix.drug');
		if($model === null && ($prefix  == '' or preg_match('@^/'.$prefix.'/@', $uri))) {

			$id = Drugs::findByLSlug($slug, $lang);
			if($id){
				$model=Drugs::model()->findByPk($id);

				if($model->active == 0){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				if($model->drugname == false){
					throw new CHttpException(404,'The requested page does not exist.');
				}
			} 
		}

		// posts
		$prefix = Yii::app()->config->get('prefix.article');
		if($model === null && ($prefix  == '' or preg_match('@^/'.$prefix.'/@', $uri))) {
			$model=Articles::model()->findByAttributes(array('slug' => $slug));
			// if($model !== null){
			// 	return $model;
			// }
		}	

		//categories
		$prefix = Yii::app()->config->get('prefix.cat');
		if($model === null && ($prefix  == '' or preg_match('@^/'.$prefix.'/@', $uri))) {
			$id = Categories::findByLSlug($slug, $lang);
			if($id){
				$model=Categories::model()->findByPk($id);

				if($model->active == 0){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				if($model->catname == false){
					throw new CHttpException(404,'The requested page does not exist.');
				}
			} 
		}

		//pages
		$prefix = Yii::app()->config->get('prefix.page');
		if($model === null && ($prefix  == '' or preg_match('@^/'.$prefix.'/@', $uri))) {
			$id = Pages::findByLSlug($slug, $lang);
			if($id){
				$model=Pages::model()->findByPk($id);

			} 
		}		


		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;	
	}

}