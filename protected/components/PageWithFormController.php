<?php
class PageWithFormController extends FrontEndController
{
    

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'foreColor'=>'0x000000',
                'height' => 28,
                'width' => 104,
                // 'verifyCode' => 'code',
                'maxLength' => 4,
                'minLength' => 3,
                'offset'=> 5,
                'testLimit'=>1,
            ),
        );
    }

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'captcha'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}