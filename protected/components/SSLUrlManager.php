<?php

class SSLUrlManager extends DLanguageUrlManager
{



    public function createUrl($route, $params=array(), $ampersand='&')
    {
        $url = parent::createUrl($route, $params, $ampersand);
    	
    	$schema = 'http://';
        if(Yii::app()->config->get('use_https')){
	            $schema = 'https://';
        }
        $url = $schema.Yii::app()->request->serverName.$url;
        return $url;
    }

}