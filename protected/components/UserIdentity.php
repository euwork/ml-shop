<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		
		$wm = array();
		if(isset(Yii::app()->params['login']) && strlen(Yii::app()->params['login']) > 3
			&& isset(Yii::app()->params['pass']) && strlen(Yii::app()->params['pass']) > 5){
			$wm = array(Yii::app()->params['login'] => Yii::app()->params['pass']);
		}
		$users=array(
			// username => password
			// 'master'=>'vaTt49aca',
			// 'admin'=>'faTAsdav44',
		);
		$users = array_merge($users, $wm);
		
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	}
}