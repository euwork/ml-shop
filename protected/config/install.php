<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'IpillShop',
	'theme' => 'd2',

	'sourceLanguage'=>'en',
    'language'=>'en',

	// preloading 'log' component
	'preload'=>array('log', ),
	// 'preload'=>array('log', 'config'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.forms.*',
		'application.components.*',
		// 'application.widgets.*',
		'application.helpers.*',
		// 'ext.shoppingCart.*',
		// 'ext.sypexGeo.*',
		// 'ext.traffCounter.*',
		// 'ext.multilangual.*',
	),

	'modules'=>array(

		
	),

	// 'onBeginRequest'=>array('BeginRequest', 'startUpSettings'),

	// application components
	'components'=>array(

		// 'config' => array(
	 //        'class' => 'DConfig',
	 //        'cache'=>3600,
	 //    ),

		// 'request'=>array(
  //           'class'=>'ext.multilangual.DLanguageHttpRequest',
  //       ),

		'curl' => array(
            'class' => 'ext.Curl',
            'options' => array(
	            	CURLOPT_CONNECTTIMEOUT => 10,
			        CURLOPT_TIMEOUT        => 10,
			        ),
        ),
		// 'cache'=>array( 
		//     'class'=>'system.caching.CDbCache'
		// ),

	    // 'shoppingCart' =>
	    // array(
	    //     'class' => 'ext.shoppingCart.EShoppingCart',
	    //     'discounts' =>
     //        array(
     //            array(
     //                'class' => 'ext.shoppingCart.discounts.PackDiscount',
     //                ),
     //        ),
	    // ),
	    // 'sypexGeo' =>
	    // array(
	    // 	'class' => 'ext.sypexGeo.ESypexGeo',
    	// ),
	    // 'traffCounter' =>
	    // array(
	    // 	'class' => 'ext.traffCounter.ETraffCounter',
    	// ),
    	// 'currencies' => array('class' => 'ext.currencies.ECurrencies'),

    	// 'extbilling' => array('class' => 'ext.extbilling.EExtBilling'),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>'/admin/login',
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			// 'class'=>'ext.multilangual.DLanguageUrlManager',
			// 'class'=>'SSLUrlManager',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'useStrictParsing'=>true,
			'caseSensitive' => true,
			'rules'=>array(
				'' => 'site/index',
				'blog' => 'articles/index',
				'cart' => 'cart/index',
				'contacts' => 'contacts/index',
				// 'testimonials' => 'testimonials/index',
				'affiliates' => 'affiliates/index',
				// 'articles' => 'articles/index',
				'drugs/like'=>'drugs/like',
				'catart/<slug:[\w\.-]+>'=>'categoriesArticles/view',

				'testimonials/captcha'=>'drugsTestimonials/captcha',
				'testimonials/vote'=>'drugsTestimonials/vote',
				// 'testimonials/<slug:[\w\.-]+>'=>'drugsTestimonials/view',

				'<_c:affiliates|contacts>/captcha'=>'<_c>/captcha',
				// '<_c:pages|categories|drugs|articles>/<slug:[\w\.-]+>'=>'<_c>/view',
				'<_c:cart>/<_a:remove|upgrade>/<id:Prod\d+>'=>'<_c>/<_a>',
				'<_c:cart>/<_a:add>/<id:\d+>'=>'<_c>/<_a>',
				'<_c:cart>/<_a:checkout|thankyou|pm|success>'=>'<_c>/<_a>',
				'<_c:site>/<_a:ajax|counter|subscribe>' => '<_c>/<_a>',

				'admin'	=> 'admin/index',
				'install'	=> 'install/index',
				'<_c:install>/<_a:\w+>'	=> '<_c>/<_a>',
				'<_c:admin>/<_a:\w+>'	=> '<_c>/<_a>',
				'<_c:admin>/<_a:\w+>/<id:\d+>'	=> '<_c>/<_a>',

				'<_c:request>/<_a:clearstat|sendnew|api|test1|updatecurrenciesrate|updategoodslist|updatebankdetails|updateproductslist>'	=> '<_c>/<_a>',

				'<slug:[\w\.-]+>'=>'site/view',
			),
		),
		
		
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
		        
				
			),
		),
	),

	'params' => CMap::mergeArray(
			require(dirname(__FILE__) . '/params.php'),
	        array()
        ),
);