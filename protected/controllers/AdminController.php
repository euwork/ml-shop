<?php
class AdminController extends BackEndController
{
	public $layout='//layouts/admin_main';
	public $header;

	public $installmode = false;


	public function init()
	{
		parent::init();

		$this->installmode = Yii::app()->request->getParam('install', false);

		$this->pageTitle = Yii::app()->name;

		// if($this->installmode){
		// 	$this->layout = '//layouts/admin_install';
		// }



	}

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'index', 
					'orders', 
					'payments', 
					'traff', 
					'profile', 
					'logout', 
					'cacheflush', 
					'countries', 
					'referers', 
					'trackids', 
					'prices', 
					'changeproductstatus', 
					'drugslist',
					'drugedit',
					'catslist',
					'catedit',
					'pageslist',
					'pageedit',
					'languages',
					'config',
					// 'languageedit',
					// 'languagecreate',
					'categoriesorder',
					'drugsorder',
					'drugstestimonialslist',
					'drugstestimonialsedit',
					'reviewslist',
					'reviewedit',
					'reviewdelete',
					'reviewread',
					'reviewcreate',
					'currencies',
					'updaterating',
					'updatelikes',
					'products',
					'bankdetails',
					'articleslist',
					'articlecreate',
					'articleedit',
					'articledelete',
					'importcontent',
					'importreviews',
					'generatelinks',
					),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{


		$data_traff = TraffStat::getToday();
		$data_order = OrdersStat::getToday();
		$data = array_merge($data_traff, $data_order);
		
		$data['balance'] = Profile::getBalance();
		$data['total_paid'] = Payments::getTotalPaid();

		$this->render('dashboard', array('data'=>$data));
	}

	public function actionOrders()
	{
		$form = new OrdersFilterForm;

		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			$form->{$item} = true;
		}

		$form->currency = 'EUR';
		$form->internal = true;
		$form->external = true;

		$form->period = 'thismonth';
		if(isset($_GET['OrdersFilterForm'])){
			$form->attributes = $_GET['OrdersFilterForm'];
			if(!$form->validate()){
				$form->period = 'thismonth';
			}
		} 

		
		
		switch ($form->period) {
			case 'thismonth':
				$date_from = date('Y-n-1');
				break;
			case 'last30days':
				$date_from = date('Y-n-j', time() - 30 * 3600*24);
				break;
			case 'last7days':
				$date_from = date('Y-n-j', time() - 7 * 3600*24);
				break;		
			case 'today':
				$date_from = date('Y-n-j', time() - 0 * 3600*24);
				break;
			case 'yesterday':
				$date_from = date('Y-n-j', time() - 1 * 3600*24);
				break;
			case 'thisweek':
				$date_from = date('Y-n-j', time() - (date('w') - 1) * 3600*24);
				break;
			case 'period':
				$date_from = implode('-', array($form->year_from, $form->month_from, $form->day_from));
				break;
		}
		if($form->period != 'period'){
			$form->day_to = date('j');
			$form->month_to = date('n');
			$form->year_to = date('Y');
			if($form->period == 'yesterday'){
				$form->day_to = date('j', time() - 1 * 3600*24);
			}
			list($form->year_from, $form->month_from, $form->day_from) = explode('-', $date_from);
		}
		$date_to = implode('-', array($form->year_to, $form->month_to, $form->day_to));
		// print_r($form);

		$date_from .= ' 00:00:00';
		$date_to .= ' 23:59:59';
		$date_condition = 'date_cr BETWEEN :date_from AND :date_to';
		$date_params = array(':date_from'=>$date_from, ':date_to'=>$date_to);

		$payment_filter = array();
		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			if(!$form->{$item}){
				$constant_name = 'Orders::PAYMENT_' . strtoupper($item);
				$payment_filter[] = Constant($constant_name);
			}
		}

		// extb
		$extb_filter = array();
		foreach(array('internal', 'external') as $item)
		{
			if(!$form->{$item}){
				$constant_name = 'Orders::EXTB_' . strtoupper($item);
				$extb_filter[] = Constant($constant_name);
			}
		}


		if($form->currency == 'EUR'){
			$currency_prefix = '';
		} else {
			$currency_prefix = '_usd';
		}

		$criteria = new CDbCriteria;
		$criteria->condition = $date_condition;
		$criteria->params = $date_params;
		$criteria->order = 'date_cr DESC';
		$criteria->addNotInCondition('t.payment_method', $payment_filter);
		$criteria->addNotInCondition('t.extb', $extb_filter);

		$count=Orders::model()->count($criteria);
		$pages=new CPagination($count);
		$pages->pageSize=20;
	    $pages->applyLimit($criteria);
		$model = Orders::model()->with(array('order_products' => array('with'=> 'product')))->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->select='sum(subtotal'.$currency_prefix.'*comission/100) as subtotal';
		$criteria->condition = $date_condition . ' and status = :status';
		$criteria->params = array_merge($date_params, array(':status'=> Orders::STATUS_ACCEPT));
		$total['profit'] = Orders::model()->find($criteria)->getAttribute('subtotal'); 
		$criteria->select = 'order_id';
		$total['accept'] = Orders::model()->count($criteria);

		$criteria = new CDbCriteria;
		$criteria->select='sum(subtotal'.$currency_prefix.'*comission/100) as subtotal';
		$criteria->condition = $date_condition .  ' and (status = :status1 or status = :status2)';
		$criteria->params = array_merge($date_params, array(':status1'=> Orders::STATUS_PENDING, ':status2'=> Orders::STATUS_NEW));
		$total['pending'] = Orders::model()->find($criteria)->getAttribute('subtotal');
		$criteria->select = 'order_id';
		$total['wait'] = Orders::model()->count($criteria);

		$total['decline'] = $count - $total['wait'] - $total['accept'];

		$this->render('orders', array('model'=>$model, 'form'=>$form, 'pages' => $pages, 'total'=>$total));
	}


	public function actionTraff()
	{
		$form = new TraffFilterForm;

		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			$form->{$item} = true;
		}

		$form->period = 'week';
		$form->week = preg_replace('@^0@', '', date('W'));
		$form->year1 = date('Y');
		if($form->week == '1' && date('m') == '12'){
			$form->year1 = date('Y')+1;
		}
		$form->year2 = date('Y');
		$form->year3 = date('Y');
		$form->month = date('n');
		if(isset($_GET['TraffFilterForm'])){
			$form->attributes = $_GET['TraffFilterForm'];
			if(!$form->validate()){
				$form->period = 'week';
			}
		} 

		switch ($form->period) {
			case 'week':
                $max_week = TraffFilterForm::getCountWeek($form->year1);
                if($form->week == date('W') && $form->week > $max_week){
					$form->year1--;
                }
                elseif($form->week > $max_week){
                    $form->week = $max_week;
                }	
				$date_from = TraffFilterForm::getMondayOfWeek($form->week, $form->year1);
				$date_to = $date_from + 3600*24*7;
				$from = date('Y-m-d 00:00:00', $date_from);
				$to = date('Y-m-d 23:59:59', $date_to - 3600*24); // минус день потому шо добавили 23:59:59
				$date_format = "%Y %m %d";
				$rows_array = array();
				$cur_date = $date_from;
				for($i=0;$i<7;$i++)
				{
					$rows_array[$i] = $cur_date;
					$cur_date += 24*3600; 
				}
				break;
			
			case 'month':
				$cal_days = cal_days_in_month(CAL_GREGORIAN, $form->month, $form->year3);
				$date_from = strtotime($form->year3 .'-'.$form->month) + 3600*1; //+1 час - баг с переводом времени;

				if($form->month == date('n')){
					$date_to = strtotime(date('Y-m-d')) + 3600*24;
					$cal_days = date('j');
				} else {
					$date_to = $date_from + 3600*24*$cal_days ;
				}
				$from = date('Y-m-d 00:00:00', $date_from);
				$to = date('Y-m-d 23:59:59', $date_to - 3600*24); // минус день потому шо добавили 23:59:59
				$date_format = "%Y %m %d";
				$rows_array = array();
				$cur_date = $date_from;
				for($i=0;$i<$cal_days ;$i++)
				{
					$rows_array[$i] = $cur_date;
					$cur_date += 24*3600;
				}
				break;
			case 'monthly':
				$date_from = strtotime($form->year2 . '-1-1');
				$date_to = strtotime(($form->year2+1). '-1-1');
				$date_format = "%Y %m";
				$from = date('Y-m-d 00:00:00', $date_from);
				$to = date('Y-m-d 23:59:59', $date_to - 3600*24); // минус день потому шо добавили 23:59:59
				// echo $from;
				// Yii::app()->end();
				$rows_array = array();
				$cur_date = $date_from;
				for($i=0;$i<12 ;$i++)
				{
					$cal_days = cal_days_in_month(CAL_GREGORIAN, $i+1, $form->year2);
					$rows_array[$i] = $cur_date;
					$cur_date += 24*3600 * $cal_days;
				}
				break;
		}

		$payment_filter = array();
		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			if($form->{$item}){
				$constant_name = 'Orders::PAYMENT_' . strtoupper($item);
				$payment_filter[] = Constant($constant_name);
			}
		}
		$payment_filter_string = "'" . implode("', '", $payment_filter) . "'";

		$dataArrayTraff = TraffStat::getByDate($from, $to, $date_format);
		$dataArrayOrder = OrdersStat::getByDate($from, $to, $date_format, $payment_filter_string);

		$data_traff = array();
		$data_order = array();
		
		foreach($dataArrayTraff as $item)
		{
			$data_traff[$item['dat']] = $item;
		}
		foreach($dataArrayOrder as $item)
		{
			$data_order[$item['dat']] = $item;
		}

		$this->render('traff',array('form'=>$form, 'data_traff'=>$data_traff, 'data_order'=>$data_order, 'rows_array'=>$rows_array));
	}

	public function actionPayments()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'date_cr DESC';

		$count = Payments::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->pageSize=20;
	    $pages->applyLimit($criteria);


		$model = Payments::model()->findAll($criteria);
		$this->render('payments', array('model'=>$model, 'pages'=>$pages,));
	}
	
	public function actionProfile()
	{
		$model = Profile::model()->findByPk(1);
		if(isset($_POST['Profile'])) {
			$model->attributes=$_POST['Profile'] ;
			if ($model->save()) {
				Yii::app()->user->setFlash('success','Success!');
				$this->refresh();
			}
		}
		$this->render('profile', array('model'=>$model));
	}

	public function actionPrices()
	{
		$form = new PricesFilterForm;
		$form->products_label = Products::BASE_FR;
		$form->lang = Yii::app()->language;
		if(isset($_GET['PricesFilterForm'])){
			$form->attributes = $_GET['PricesFilterForm'];
			if(!$form->validate()){
				$form->products_label = Products::BASE_FR;
				$form->drug_id = false;
			}
		} 

		$criteria = new CDbCriteria;
		$criteria->addInCondition('param', array('price.increase', 'price.sort_order', 'price.increase_old'));
		$model = Config::model()->findAll($criteria);

		if(isset($_POST['Config']))
		{


			foreach($model as $item)
			{
				if(isset($_POST['Config'][$item->param])){
					Yii::app()->config->set($item->param, $_POST['Config'][$item->param]);
				}
			}

			Yii::app()->cache->flush();

			Yii::app()->user->setFlash('success','Saved');

			$this->refresh();
			
		}

		$criteria = new CDbCriteria;
		$criteria->condition = 'label = :label';
		$criteria->params = array(':label'=>$form->products_label);
		if($form->drug_id){
			$criteria->addCondition('t.drug_id = :drug_id');
			$criteria->params = array_merge($criteria->params, array(':drug_id' => $form->drug_id));
		}

		$criteria->order = 't.drug_id ASC, CAST(t.dose as unsigned) DESC, t.amount ASC';

		$products = Products::model()->with('drug')->findAll($criteria);

		Yii::app()->language = $form->lang;
		
		$this->render('prices', array('model'=>$model, 'products' => $products, 'filter_form'=>$form));		
	}

	public function actionChangeProductStatus()
	{
		if(!Yii::app()->request->isAjaxRequest){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		$id = Yii::app()->request->getPost('prodid');
		$status = Yii::app()->request->getPost('status');

		$model = Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		
		if(!in_array($status, Products::getStatusesList())){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		$model->status = $status;
		if($model->save(true, 'status')){

			$data['result'] = true;
			$data['change_to'] = $model->changeTo;
			$data['status_name'] = $model->statusName;
			// $data['url'] = Yii::app()->createUrl('/');

		}

		echo json_encode($data);
		Yii::app()->end();
		// $this->redirect(Yii::app()->createUrl('/admin/prices'));


	}

	public function actionCacheFlush()
	{
		Yii::app()->cache->flush();

		if(Yii::app()->request->getParam('return')){

			Yii::app()->user->setFlash('success', 'Cache cleared.');

			if(Yii::app()->request->urlReferrer != false){
				$this->redirect(Yii::app()->request->urlReferrer);
			} else {
				$this->redirect(array('/admin'));
			}
		}

		
		Yii::app()->end();
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}	

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout='//layouts/login';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}





// ---
	public function actionCountries()
	{
		$form = new OrdersFilterForm;


		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			$form->{$item} = true;
		}

		$form->period = 'thismonth';
		if(isset($_GET['OrdersFilterForm'])){
			$form->attributes = $_GET['OrdersFilterForm'];
			if(!$form->validate()){
				$form->period = 'thismonth';
			}
		} 

		
		
		switch ($form->period) {
			case 'thismonth':
				$date_from = date('Y-n-1');
				break;
			case 'last30days':
				$date_from = date('Y-n-j', time() - 30 * 3600*24);
				break;
			case 'last7days':
				$date_from = date('Y-n-j', time() - 7 * 3600*24);
				break;		
			case 'today':
				$date_from = date('Y-n-j', time() - 0 * 3600*24);
				break;
			case 'yesterday':
				$date_from = date('Y-n-j', time() - 1 * 3600*24);
				break;
			case 'thisweek':
				$date_from = date('Y-n-j', time() - (date('w') - 1) * 3600*24);
				break;
			case 'period':
				$date_from = implode('-', array($form->year_from, $form->month_from, $form->day_from));
				break;
		}
		if($form->period != 'period'){
			$form->day_to = date('j');
			$form->month_to = date('n');
			$form->year_to = date('Y');
			if($form->period == 'yesterday'){
				$form->day_to = date('j', time() - 1 * 3600*24);
			}
			list($form->year_from, $form->month_from, $form->day_from) = explode('-', $date_from);
		}
		$date_to = implode('-', array($form->year_to, $form->month_to, $form->day_to));
		
		$date_from .= ' 00:00:00';
		$date_to .= ' 23:59:59';


		$payment_filter = array();
		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			if($form->{$item}){
				$constant_name = 'Orders::PAYMENT_' . strtoupper($item);
				$payment_filter[] = Constant($constant_name);
			}
		}
		$payment_filter_string = "'" . implode("', '", $payment_filter) . "'";

		$data_traff = TraffStat::getCountriesByDate($date_from, $date_to);

		$data_order = OrdersStat::getByCountry($date_from, $date_to, $payment_filter_string);


		$this->render('countries',array('form'=>$form, 'data_traff'=>$data_traff, 'data_order' =>$data_order));
	}
// ---


// ---
	public function actionTrackids()
	{
		$form = new OrdersFilterForm;


		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			$form->{$item} = true;
		}

		$form->period = 'thismonth';
		if(isset($_GET['OrdersFilterForm'])){
			$form->attributes = $_GET['OrdersFilterForm'];
			if(!$form->validate()){
				$form->period = 'thismonth';
			}
		} 

		
		
		switch ($form->period) {
			case 'thismonth':
				$date_from = date('Y-n-1');
				break;
			case 'last30days':
				$date_from = date('Y-n-j', time() - 30 * 3600*24);
				break;
			case 'last7days':
				$date_from = date('Y-n-j', time() - 7 * 3600*24);
				break;		
			case 'today':
				$date_from = date('Y-n-j', time() - 0 * 3600*24);
				break;
			case 'yesterday':
				$date_from = date('Y-n-j', time() - 1 * 3600*24);
				break;
			case 'thisweek':
				$date_from = date('Y-n-j', time() - (date('w') - 1) * 3600*24);
				break;
			case 'period':
				$date_from = implode('-', array($form->year_from, $form->month_from, $form->day_from));
				break;
		}
		if($form->period != 'period'){
			$form->day_to = date('j');
			$form->month_to = date('n');
			$form->year_to = date('Y');
			if($form->period == 'yesterday'){
				$form->day_to = date('j', time() - 1 * 3600*24);
			}
			list($form->year_from, $form->month_from, $form->day_from) = explode('-', $date_from);
		}
		$date_to = implode('-', array($form->year_to, $form->month_to, $form->day_to));
		
		$date_from .= ' 00:00:00';
		$date_to .= ' 23:59:59';


		$payment_filter = array();
		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			if($form->{$item}){
				$constant_name = 'Orders::PAYMENT_' . strtoupper($item);
				$payment_filter[] = Constant($constant_name);
			}
		}
		$payment_filter_string = "'" . implode("', '", $payment_filter) . "'";

		$data_traff = TraffStat::getTrackidsByDate($date_from, $date_to);

		$data_order = OrdersStat::getByTrackid($date_from, $date_to, $payment_filter_string);


		$this->render('trackids',array('form'=>$form, 'data_traff'=>$data_traff, 'data_order' =>$data_order));
	}
// ---

// ---
	public function actionReferers()
	{
		$form = new OrdersFilterForm;
		$form->period = 'thismonth';
		if(isset($_GET['OrdersFilterForm'])){
			$form->attributes = $_GET['OrdersFilterForm'];
			if(!$form->validate()){
				$form->period = 'thismonth';
			}
		} 

		
		
		switch ($form->period) {
			case 'thismonth':
				$date_from = date('Y-n-1');
				break;
			case 'last30days':
				$date_from = date('Y-n-j', time() - 30 * 3600*24);
				break;
			case 'last7days':
				$date_from = date('Y-n-j', time() - 7 * 3600*24);
				break;		
			case 'today':
				$date_from = date('Y-n-j', time() - 0 * 3600*24);
				break;
			case 'yesterday':
				$date_from = date('Y-n-j', time() - 1 * 3600*24);
				break;
			case 'thisweek':
				$date_from = date('Y-n-j', time() - (date('w') - 1) * 3600*24);
				break;
			case 'period':
				$date_from = implode('-', array($form->year_from, $form->month_from, $form->day_from));
				break;
		}
		if($form->period != 'period'){
			$form->day_to = date('j');
			$form->month_to = date('n');
			$form->year_to = date('Y');
			if($form->period == 'yesterday'){
				$form->day_to = date('j', time() - 1 * 3600*24);
			}
			list($form->year_from, $form->month_from, $form->day_from) = explode('-', $date_from);
		}
		$date_to = implode('-', array($form->year_to, $form->month_to, $form->day_to));

		$data_traff = TraffStat::getReferersByDate($date_from, $date_to);
		// print_r($data_traff);

		$this->render('referers',array('form'=>$form, 'data_traff'=>$data_traff,));
	}
// ---

	//  text editing for SEO
	
	//  drugs
	public function actionDrugsList()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'position ASC, drug_id ASC';

		// hide valif,lida,zyban,levitra_super
		$criteria->addNotInCondition('drug_id',  array(31,13,22,21));
		
		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){

			$model = Drugs::model()->findAll($criteria);
			foreach($model as $item)
			{
				if(isset($_POST['order'][$item->drug_id])){
					$item->position = (int) $_POST['order'][$item->drug_id];
				}
				if(isset($_POST['active'][$item->drug_id])){
					$item->active = (int) $_POST['active'][$item->drug_id];
				}
				if(isset($_POST['home'][$item->drug_id])){
					$item->home = (int) $_POST['home'][$item->drug_id];
				}

				if($item->save(true, array('position', 'active', 'home'))){

				} else {
					// print_r($item->getErrors());
				}
			}

			$data['result'] = true;
		
			echo json_encode($data);
			Yii::app()->end();
		}

		$model = Drugs::model()->findAll($criteria);

		$this->render('drugs_list', array('model'=>$model, ));
	}

	public function actionDrugEdit($id)
	{
		$model=Drugs::model()->multilang()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Drugs']))
		{
			$attributes_to_save = array('drugname', 'title', 'header', 'keywords', 'description', 'content', 'instruction', 'slug', 'rating');
			$model->attributes=$_POST['Drugs'];

			// проверка slug
			foreach(DMultilangHelper::suffixList() as $lang_id => $data)
			{
				$attr_name = 'slug'.$data['suffix'];
				if(!Drugs::check_slug($model->{$attr_name}, $id)){
					$model->addError('slug', "slug not unique!");
				}
			}

			// заполняем данными аттрибуты без суффикса, чтобы пройти валидацию required
			foreach($attributes_to_save as $attr)
			{
				$model->{$attr} = $model->{$attr . '_' . Yii::app()->config->get('defaultLanguage')};
			}
			
			if($model->save(true, $attributes_to_save)){

				Yii::app()->cache->flush();
				$this->redirect(array('drugslist',));
			}
		}

		$this->render('drug_edit',array(
			'model'=>$model,
		));
	}

	//  cats
	public function actionCatsList()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'cat_id ASC';

		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
			// print_r($_POST);

			$model = Categories::model()->findAll();
			foreach($model as $item)
			{
				if(isset($_POST['order'][$item->cat_id])){
					$item->position = (int) $_POST['order'][$item->cat_id];
				}
				if(isset($_POST['active'][$item->cat_id])){
					$item->active = (int) $_POST['active'][$item->cat_id];
				}

				if($item->save(true, array('position', 'active'))){

				} else {
					// print_r($item->getErrors());
				}
			}

			$data['result'] = true;
		
			echo json_encode($data);
			Yii::app()->end();
		}

		$model = Categories::model()->findAll($criteria);

		$this->render('cats_list', array('model'=>$model, ));
	}

	public function actionCatEdit($id)
	{
		$model=Categories::model()->multilang()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Categories']))
		{
			$attributes_to_save = array('title', 'header', 'keywords', 'description', 'content', 'slug', 'catname' );
			$model->attributes=$_POST['Categories'];

			// проверка slug
			foreach(DMultilangHelper::suffixList() as $lang_id => $data)
			{
				$attr_name = 'slug'.$data['suffix'];
				if(!Categories::check_slug($model->{$attr_name}, $id)){
					$model->addError('slug', "slug not unique!");
				}
			}

			foreach($attributes_to_save as $attr)
			{
				$model->{$attr} = $model->{$attr . '_' . Yii::app()->config->get('defaultLanguage')};
			}

			if($model->save(true, $attributes_to_save)){

				Yii::app()->cache->flush();
				$this->redirect(array('catslist',));
			}
		}

		$this->render('cat_edit',array(
			'model'=>$model,
		));
	}
	// articles
	public function actionArticlesList()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'art_id DESC';

		$count = Articles::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->pageSize=20;
	    $pages->applyLimit($criteria);

		$model = Articles::model()->findAll($criteria);

		$this->render('articles_list', array('model'=>$model, 'pages' => $pages,));
	}

	public function actionArticleCreate()
	{
		$model= new Articles;

		$model->lang = Yii::app()->language;
		$model->acat_id = 0;


		if(isset($_POST['Articles']))
		{
			
			$model->attributes=$_POST['Articles'];

			// проверка slug
			
			// $attr_name = 'slug';
			// if(!Articles::check_slug($model->{$attr_name}, $id)){
			// 	$model->addError('slug', "slug not unique!");
			// }

			$model->date_cr = date('Y-m-d H:i:s');
			

			
			if($model->save()){

				$this->redirect(array('articleslist',));
			} else {
				// print_r($model->getErrors());
			}
		}

		$this->render('article_create',array(
			'model'=>$model,
		));
	}

	public function actionArticleEdit($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Articles']))
		{

			$model->attributes=$_POST['Articles'];

			

			
			if($model->save()){

				$this->redirect(array('articleslist',));
			} else {
				print_r($model->getErrors());
			}
		}

		$this->render('article_edit',array(
			'model'=>$model,
		));
	}	
	public function actionArticleDelete($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model !== null){
			$model->delete();
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('articleslist'));
	}
	//  pages
	public function actionPagesList()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'page_id ASC';

		$count = Pages::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->pageSize=20;
	    $pages->applyLimit($criteria);

		$model = Pages::model()->findAll($criteria);

		$this->render('pages_list', array('model'=>$model, 'pages' => $pages,));
	}

	public function actionPageEdit($id)
	{
		$model=Pages::model()->multilang()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Pages']))
		{
			$attributes_to_save = array('title', 'header', 'keywords', 'description', 'content', 'slug' );

			$model->attributes=$_POST['Pages'];

			// проверка slug
			foreach(DMultilangHelper::suffixList() as $lang_id => $data)
			{
				$attr_name = 'slug'.$data['suffix'];
				if(!Pages::check_slug($model->{$attr_name}, $id)){
					$model->addError('slug', "slug not unique!");
				}
			}

			foreach($attributes_to_save as $attr)
			{
				// if($model->isAttributeRequired($attr)){
					$model->{$attr} = $model->{$attr . '_' . Yii::app()->config->get('defaultLanguage')};
				// }
					
			}
			if($model->save(true, $attributes_to_save)){

				$this->redirect(array('pageslist',));
			} else {
				// print_r($model->getErrors());
			}
		}

		$this->render('page_edit',array(
			'model'=>$model,
		));
	}	


	// testimonials

	public function actionDrugsTestimonialsList()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'drug_id ASC';

		$count = DrugsTestimonials::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->pageSize=30;
	    $pages->applyLimit($criteria);

		$model = DrugsTestimonials::model()->findAll($criteria);

		$this->render('drugs_testimonials_list', array('model'=>$model, 'pages' => $pages,));
	}

	public function actionDrugsTestimonialsEdit($id)
	{
		$model=DrugsTestimonials::model()->multilang()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['DrugsTestimonials']))
		{
			$attributes_to_save = array('title', 'header', 'keywords', 'description', 'subheader', 'drug_ancor', 'slug');
			$model->attributes=$_POST['DrugsTestimonials'];

			// проверка slug
			foreach(DMultilangHelper::suffixList() as $lang_id => $data)
			{
				$attr_name = 'slug'.$data['suffix'];
				if(!DrugsTestimonials::check_slug($model->{$attr_name}, $id)){
					$model->addError('slug', "slug not unique!");
				}
			}

			// заполняем данными аттрибуты без суффикса, чтобы пройти валидацию required
			foreach($attributes_to_save as $attr)
			{
				$model->{$attr} = $model->{$attr . '_' . Yii::app()->config->get('defaultLanguage')};
			}
			
			if($model->save(true, $attributes_to_save)){

				Yii::app()->cache->flush();
				$this->redirect(array('drugstestimonialslist',));
			}
		}

		$this->render('drugs_testimonials_edit',array(
			'model'=>$model,
		));
	}

	//reviews
	public function actionReviewsList($id)
	{

		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
			// print_r($_POST);
			$lang_id = $_POST['lang_id'];
			
			$criteria = new CDbCriteria;
			$criteria->order = 'date_cr DESC';
			$criteria->condition = 'drug_id = :drug_id and lang = :lang_id';
			$criteria->params = array(':drug_id'=>$id, ':lang_id' => $lang_id);

			$model = Reviews::model()->findAll($criteria);
			foreach($model as $item)
			{
				if(isset($_POST['active'][$item->rev_id])){
					$item->active = (int) $_POST['active'][$item->rev_id];
				}

				if(isset($_POST['home'][$item->rev_id])){
					$item->home = (int) $_POST['home'][$item->rev_id];
				}

				$item->new = 0;

				if($item->save(true, array('active', 'new', 'home'))){

				} else {
					// print_r($item->getErrors());
				}
			}

			$data['result'] = true;
		
			echo json_encode($data);
			Yii::app()->end();
		}

		$criteria = new CDbCriteria;
		$criteria->order = 'date_cr DESC';
		$criteria->condition = 'drug_id = :drug_id';
		$criteria->params = array(':drug_id'=>$id);

		// $count = DrugsTestimonials::model()->count($criteria);

		// $pages=new CPagination($count);
		// $pages->pageSize=25;
	    // $pages->applyLimit($criteria);

		$model = Reviews::model()->findAll($criteria);

		$this->render('reviews_list', array('model'=>$model, ));
	}

	public function actionReviewEdit($id)
	{
		$model=Reviews::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Reviews']))
		{
			$model->attributes=$_POST['Reviews'];
			$model->new = 0;
			if($model->save(true, array('points', 'author', 'content', 'date_cr', 'tit_name', 'vote_yes', 'vote_all', 'new' ))){
				Yii::app()->user->setFlash('success','Changed.');
				$this->redirect(array('reviewslist', 'id'=>$model->drug_id, 'active_tab'=>$model->lang));
			}
		}

		$this->render('review_edit',array(
			'model'=>$model,
		));
	}

	public function actionReviewDelete($id)
	{
		$model=Reviews::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		Voted::model()->deleteAll('rev_id = :rev_id', array(':rev_id'=>$id));

		$model->delete();

		Yii::app()->user->setFlash('success','Deleted.');

		$this->redirect(array('reviewslist', 'id'=>$model->drug_id, 'active_tab'=>$model->lang));
		
	}

	public function actionReviewRead($id)
	{
		if(Yii::app()->request->isAjaxRequest){
			$model = Reviews::model()->findByPk($id);
			if($model === null)
				throw new CHttpException(404,'The requested page does not exist.');

			$model->new = 0;
			if($model->save(true, array('new'))){
				echo json_encode('success');
			} else {
				echo json_encode('error');
			}

		}
		Yii::app()->end();
	}

	public function actionLanguages()
	{


		$criteria = new CDbCriteria;
		$criteria->condition = 'imported = 1';
		$criteria->order = 'position ASC, lang_id ASC';

		$model = Languages::model()->findAll($criteria);

		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
			// print_r($_POST);

			
			foreach($model as $item)
			{
				if(isset($_POST['order'][$item->lang_id])){
					$item->position = (int) $_POST['order'][$item->lang_id];
				}
				if(isset($_POST['active'][$item->lang_id])){
					$item->show = (int) $_POST['active'][$item->lang_id];
				}

				if(isset($_POST['show_generic'][$item->lang_id])){
					$item->show_generic = (int) $_POST['show_generic'][$item->lang_id];
				}
				foreach(array('wire', 'visa', 'master', 'amex', 'bc', 'bitcoin', 'ideal') as $pm)
				{
					if(isset($_POST[$pm][$item->lang_id])){
						$item->{$pm} = (int) $_POST[$pm][$item->lang_id];
					}
				}

				if($item->save(true, array('position', 'show', 'show_generic', 'wire', 'visa', 'master', 'amex', 'bc', 'bitcoin', 'ideal'))){

				} else {
					// print_r($item->getErrors());
				}
			}

			$data['result'] = true;

			Yii::app()->cache->flush();
		
			echo json_encode($data);
			Yii::app()->end();
		}



		$this->render('languages', array('model'=>$model, ));		
	}


	public function actionLanguageCreate()
	{
		$model= new Languages;

		if(isset($_POST['Languages']))
		{
			
			$model->attributes=$_POST['Languages'];
			// var_dump(Languages::check_id($model->lang_id));
			$validation = true;

			if(!Languages::check_id($model->lang_id)){
					$model->addError('lang_id', "id not unique!");
					$validation = false;
			}
			if($validation && $model->save()){

				$this->redirect(array('languages',));
			} else {
				// print_r($model->getErrors());
			}
		}

		$this->render('language_create',array(
			'model'=>$model,
		));
	}

	public function actionLanguageEdit($lang_id)
	{
		$model=Languages::model()->findByPk($lang_id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if(isset($_POST['Languages']))
		{
			$model->attributes=$_POST['Languages'];
			if($model->save(true, array('name', 'country' )))
				$this->redirect(array('languages',));
		}

		$this->render('language_edit',array(
			'model'=>$model,
		));
	}	

	public function actionConfig()
	{
		$model = Config::model()->findAll();


		if(isset($_POST['Config']))
		{


			foreach($model as $item)
			{
				if(isset($_POST['Config'][$item->param])){
					// prefix.testimonials not empty
					if($item->param == 'prefix.testimonials' && $_POST['Config'][$item->param] == ''){
						continue;
					}
					Yii::app()->config->set($item->param, $_POST['Config'][$item->param]);
				}
			}

			Yii::app()->cache->flush();

			Yii::app()->user->setFlash('success','Saved');

			$active_tab = Yii::app()->request->getPost('active_tab', 0);
			
			$this->redirect(array('config', 'active_tab'=>$active_tab));
			
		}

		$this->render('config',array(
			'model'=>$model,
		));
	}	




	// public function actionCategoriesOrder()
	// {

	// 	if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
	// 		// print_r($_POST);

	// 		$model = Categories::model()->findAll();
	// 		foreach($model as $item)
	// 		{
	// 			if(isset($_POST['order'][$item->cat_id])){
	// 				$item->position = (int) $_POST['order'][$item->cat_id];
	// 			}
	// 			if(isset($_POST['active'][$item->cat_id])){
	// 				$item->active = (int) $_POST['active'][$item->cat_id];
	// 			}

	// 			if($item->save(true, array('position', 'active'))){

	// 			} else {
	// 				// print_r($item->getErrors());
	// 			}
	// 		}

	// 		$data['result'] = true;
		
	// 		echo json_encode($data);
	// 		Yii::app()->end();
	// 	}



	// 	$criteria = new CDbCriteria;
	// 	$criteria->order = 'position ASC, cat_id ASC';



	// 	$model = Categories::model()->findAll($criteria);

	// 	$this->render('cats_order', array('model'=>$model, ));		
	// }


	// public function actionDrugsOrder($cat_id)
	// {

	// 	$criteria = new CDbCriteria;
	// 	$criteria->order = 'position ASC, drug_id ASC';
	// 	$criteria->condition = 'cat_id = :cat_id';
	// 	$criteria->params = array(':cat_id' => $cat_id);


	// 	if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
	// 		// print_r($_POST);

	// 		$model = Drugs::model()->findAll($criteria);
	// 		foreach($model as $item)
	// 		{
	// 			if(isset($_POST['order'][$item->drug_id])){
	// 				$item->position = (int) $_POST['order'][$item->drug_id];
	// 			}
	// 			if(isset($_POST['active'][$item->drug_id])){
	// 				$item->active = (int) $_POST['active'][$item->drug_id];
	// 			}

	// 			if($item->save(true, array('position', 'active'))){

	// 			} else {
	// 				// print_r($item->getErrors());
	// 			}
	// 		}

	// 		$data['result'] = true;
		
	// 		echo json_encode($data);
	// 		Yii::app()->end();
	// 	}






	// 	$model = Drugs::model()->findAll($criteria);

	// 	$this->render('drugs_order', array('model'=>$model, ));		
	// }	


	public function actionReviewCreate($drug_id, $lang_id)
	{
		$model = new Reviews;


		if(isset($_POST['Reviews']))
		{
			$model->attributes=$_POST['Reviews'];

			$model->new = 0;

			$drug_id = $model->drug_id;
			$lang_id = $model->lang;

			if($model->save()){

				Yii::app()->user->setFlash('success','Success!');
				$this->redirect(array('reviewslist','id'=>$drug_id, 'active_tab'=>$lang_id));
			} else{
				// print_r((Languages::getArrayFlip()));
				// print_r($model->getErrors());
				// Yii::app()->end();
			}
		} else {
			
			$model->lang = $lang_id;
			$model->drug_id = $drug_id;
			$model->date_cr = date('Y-m-d H:i:s');
			$model->points = '5.0';

		}

		$this->render('review_create',array(
			'model'=>$model,
		));
	}
	

	public function actionCurrencies()
	{

		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){
			// print_r($_POST);

			$model = Currencies::model()->findAll();
			foreach($model as $item)
			{
				if(isset($_POST['order'][$item->cur_id])){
					$item->position = (int) $_POST['order'][$item->cur_id];
				}
				if(isset($_POST['active'][$item->cur_id])){
					$item->active = (int) $_POST['active'][$item->cur_id];
				}

				if($item->save(true, array('position', 'active'))){

				} else {
					// print_r($item->getErrors());
				}
			}

			$data['result'] = true;

			Yii::app()->cache->flush();
		
			echo json_encode($data);
			Yii::app()->end();
		}



		$criteria = new CDbCriteria;
		$criteria->order = 'position ASC, cur_id ASC';



		$model = Currencies::model()->findAll($criteria);

		$this->render('currencies', array('model'=>$model, ));		
	}	

	public function actionUpdateRating()
	{
		$data = array();
		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){

				if(isset($_POST['rating']['value']) && isset($_POST['rating']['lang_id']) && isset($_POST['rating']['drug_id'])){
					$lang_id = $_POST['rating']['lang_id'];
					$drug_id = $_POST['rating']['drug_id'];
					$rating = $_POST['rating']['value'];

					if(Drugs::updateRating($drug_id, $lang_id, $rating)){
						$data['result']['success'] = true;
						$rating_value = Drugs::getRatingByParams($drug_id, $lang_id, $rating);
						$data['result']['rating'] = $rating_value;
					} else {
						$data['result'] = false;
					}
				}
		}

		
		echo json_encode($data);

		Yii::app()->end();
	}

	public function actionUpdateLikes()
	{
		$data = array();
		if(isset($_POST['ajax']) && $_POST['ajax'] == 1){

				if(isset($_POST['likes']['value']) && isset($_POST['likes']['lang_id']) && isset($_POST['likes']['drug_id'])){
					$lang_id = $_POST['likes']['lang_id'];
					$drug_id = $_POST['likes']['drug_id'];
					$likes = $_POST['likes']['value'];

					if(Drugs::updateLikes($drug_id, $lang_id, $likes)){
						$data['result']['success'] = true;
						$data['result']['likes'] = $likes;
					} else {
						$data['result'] = false;
					}
				}
		}

		
		echo json_encode($data);

		Yii::app()->end();
	}
// ---
	public function actionProducts()
	{
		$form = new OrdersFilterForm;



		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			$form->{$item} = true;
		}


		$form->period = 'thismonth';
		if(isset($_GET['OrdersFilterForm'])){
			$form->attributes = $_GET['OrdersFilterForm'];
			if(!$form->validate()){
				$form->period = 'thismonth';
			}
		} 

		
		
		switch ($form->period) {
			case 'thismonth':
				$date_from = date('Y-n-1');
				break;
			case 'last30days':
				$date_from = date('Y-n-j', time() - 30 * 3600*24);
				break;
			case 'last7days':
				$date_from = date('Y-n-j', time() - 7 * 3600*24);
				break;		
			case 'today':
				$date_from = date('Y-n-j', time() - 0 * 3600*24);
				break;
			case 'yesterday':
				$date_from = date('Y-n-j', time() - 1 * 3600*24);
				break;
			case 'thisweek':
				$date_from = date('Y-n-j', time() - (date('w') - 1) * 3600*24);
				break;
			case 'period':
				$date_from = implode('-', array($form->year_from, $form->month_from, $form->day_from));
				break;
		}
		if($form->period != 'period'){
			$form->day_to = date('j');
			$form->month_to = date('n');
			$form->year_to = date('Y');
			if($form->period == 'yesterday'){
				$form->day_to = date('j', time() - 1 * 3600*24);
			}
			list($form->year_from, $form->month_from, $form->day_from) = explode('-', $date_from);
		}
		$date_to = implode('-', array($form->year_to, $form->month_to, $form->day_to));
		$date_to .= ' 23:59:59';


		$payment_filter = array();
		foreach(Orders::getArrayPaymentMethod() as $item)
		{
			if($form->{$item}){
				$constant_name = 'Orders::PAYMENT_' . strtoupper($item);
				$payment_filter[] = Constant($constant_name);
			}
		}
		$payment_filter_string = "'" . implode("', '", $payment_filter) . "'";

		$data_order = OrdersStat::getSalesByDrugs($date_from, $date_to, $payment_filter_string);
		$data_order_profit = OrdersStat::getProfitByDrugs($date_from, $date_to, $payment_filter_string);
		
		foreach($data_order as &$item)
		{
			$drugname = $item['drugname'];
			foreach($data_order_profit as $item_profit)
			{
				if($item_profit['drugname'] == $drugname){
					$item['amount'] = $item_profit['amount'];
					$item['profit'] = $item_profit['profit'];
				}
			}
		}

		$this->render('products',array('form'=>$form, 'data_order' =>$data_order));
	}
// ---

	public function actionBankDetails()
	{
		$this->render('bankdetails', array());
	}



	public function actionImportContent()
	{

		# импорт нужных языков с ipillcash (drugs, drugstestimonials, drugsimages, pages, categories)

		$errors = [];
		$model = [];
		$goodsave = 0;
		$goodmsg = [];

		

		$forma = new ContentImportForm;

		$forma->defaultLang = Yii::app()->config->get('defaultLanguage');
		$forma->dontreplace = true;


		$forma->textcontent = true;
		$forma->products = false;
		$forma->templateimages = false;

		if(isset($_POST['ContentImportForm'])){


			$forma->attributes = $_POST['ContentImportForm'];


			if($this->installmode){
				$forma->textcontent = true;
				$forma->products = true;
				$forma->templateimages = true;			
			}

			if($forma->validate()){


				if($forma->languages[$forma->defaultLang] == 0 ){
					$forma->languages[$forma->defaultLang] =  1;
				}

				$newimport = false;
				$newimport_array = [];
				
				$model = Languages::model()->findAll();
				
				foreach($model as $item)
				{

					// echo "'{$item->lang_id}' => '{$item->country}[{$item->lang_id}]',\n"; continue;


					if(isset($forma->languages[$item->lang_id]) && $forma->languages[$item->lang_id] == 1) {

						if($item->imported == 0){
							$newimport = true;
						} 
						$newimport_array[$item->lang_id] = $item->imported ? 0 : 1;

						$item->imported = 1;
						$item->show = 1;
					}


					if($item->show == 1 && $item->imported == 0){
						$item->show = 0; // иначе ошибка вылазит
					} 

					$item->save(true, array('imported', 'show'));
				}
				// exit;



				Yii::app()->config->set('defaultLanguage', $forma->defaultLang);
				Yii::app()->cache->flush();
				Yii::app()->language = $forma->defaultLang;


				$languages_update = [];
				foreach($forma->languages as $lang=>$value)
				{
					if($value == 1){
						$languages_update[] = $lang;
					}
				}



				$res = Request::getContent($languages_update);

				if(isset($res['error'])){

					$errors[] = $res['error'];

				} else {

					if($forma->textcontent){
					
						$goodmsg[] = 'Importing texts;';

						//drugs, drugstestimonials, drugsimages, pages, categories
						$objects = array(
							'Categories'=> 'cat_id',
							'Drugs'=> 'drug_id',
							'DrugsTestimonials'=> 'drug_id',
							'Pages'=> 'page_id',
							);
						// print_r($res);Yii::app()->end();
						foreach($objects as $objectname=> $idname)
						{

							$data = $res['data']->{strtolower($objectname)};
							// print_r($data);
							foreach($data as $row)
							{
								$attributes_to_save = [];
								$model=$objectname::model()->multilang()->findByPk($row->attr->{$idname});


								$empty_lang_attributes = true;
								
								foreach($row->attr_l as $language => $item_attr_l)
								{
									
									$suffix = '_'.$language;
									if($language == Yii::app()->language) { $suffix = ''; }

									// проверка на пустое поле content
									if($empty_lang_attributes){
										foreach($item_attr_l as $attr=>$value)
										{
											if($attr == 'content' && (string) $value != ''){
												$empty_lang_attributes = false;
												break;
											}
										}
									}
								}



								
								
								if($model === null){

									if(isset($row->attr->active) && $row->attr->active == 0){
										continue;
									}

									$create_model = true;
									$model = new $objectname;


									foreach($row->attr as $attr=>$value)
									{
										$model->{$attr} = $value;
										$attributes_to_save[] = $attr;
									}

									// новые drugs без контента делаем не активными
									if( $empty_lang_attributes == true && $objectname == 'Drugs'){
										$model->active = 0;
									}

								} else{

									$create_model = false;

									if(isset($model->active)){

										$model->active = $row->attr->active;

										// drugs без контента делаем не активными
										if( $empty_lang_attributes == true && $objectname == 'Drugs'){
											if($model->content == ''){
												$model->active = 0;
											}
										}

										$attributes_to_save[] = 'active';


										if($newimport == false){

											if($model->save(true, array('active'))){
											$goodsave++;

											} else {
												$errors[] = $model->getErrors();
											}

											if($model->active == 0){
												continue;
											}

											if($forma->dontreplace && $model->title != ''){
												continue;
											}

											
										} 

									} elseif($newimport == false && $forma->dontreplace  && $model->title != '') {

										continue;

									}
								}
								
								foreach($row->attr_l as $language => $item_attr_l)
								{
									
									if($forma->dontreplace && $newimport_array[$language] == 0 && $create_model == false  && $model->title != ''){ // 

										continue;
									}

									$suffix = '_'.$language;
									if($language == Yii::app()->language) { $suffix = ''; }

									foreach($item_attr_l as $attr=>$value)
									{

										$model->{$attr.$suffix} = (string) $value;
										
										if($attr == 'likes'){
											$model->{$attr.$suffix} = (int) $value;
										}

										$attributes_to_save[] = $attr;
									}

								}

								
								
								$attributes_to_save = array_unique($attributes_to_save);
								

								if($model->save(true, $attributes_to_save)){
									$goodsave++;
								} else {
									$errors[] = $model->getErrors();
								}
								
							}
						}
					

						//images
						$data = $res['data']->images;
						foreach($data as $language=>$item)
						{
							foreach($item as $row)
							{
								$model=DrugsImages::model()->findByAttributes(array('theme'=>$row->theme, 'type'=>$row->type, 'drug_id'=>$row->drug_id, 'lang'=>$language));
								if($model === null) {
									$model = new DrugsImages;
								}

								$model->lang = $language;

								$attributes_to_save = array('theme', 'img', 'drug_id', 'type');
								
								foreach($attributes_to_save as $attr)
								{
									$model->{$attr} = $row->{$attr};
								}
								$attributes_to_save = array_merge($attributes_to_save, array('lang'));

								if($model->save(true, $attributes_to_save)){
									$goodsave++;
								} else {
									$errors[] = $model->getErrors();
								}
							}
						}
					}



					if($forma->products){

						// products
						$res = Request::getProductsList();

						if(is_array($res)){
							$goodmsg[] = 'Loading products;';
							
							
							foreach($res as $item)
							{
								$label = $item->base_label;
								foreach($item->products as $product)
								{
									$model = Products::model()->findByAttributes(array('prod_id'=>$product->prod_id, 'drug_id'=>$product->drug_id, 'dose'=>$product->dose, 'amount'=>$product->amount));
									$new = false;
									if($model === null) {
										
										$drug = Drugs::model()->findByPk($product->drug_id);
										if($drug === null){ continue; }
										
										$model = new Products;
										$new = true;
									}

									$model->label = $label;
									
									$model->a_status = $product->status;

									if($new){
										$model->status = 1;
										if($model->a_status == 0) {continue;}
										$attributes_to_save = array('prod_id', 'drug_id', 'dose', 'amount', 'bonus', 'amount_in', 'price', 'discount', );
									} else {
										$attributes_to_save = array( 'price', );
									}


									foreach($attributes_to_save as $attr)
									{
										$model->{$attr} = $product->{$attr};
									}

									if($new){
										$attributes_additional = array('label', 'status', 'a_status');
									} else {
										$attributes_additional = array('a_status');
									}
									
									$attributes_to_save = array_merge($attributes_to_save, $attributes_additional);

									if($model->save(true, $attributes_to_save)){
										$goodsave++;

										// testpacks
										if(isset($product->items) && is_array($product->items)){
											foreach($product->items as $t_product)
											{
												// print_r($t_product);exit;
												$t_model = TestpackProducts::model()->findByAttributes(array('id'=>$t_product->id, 'prod_id'=>$model->prod_id, 'dose'=>$t_product->dose, 'amount'=>$t_product->amount));
												if($t_model === null){

													$t_model = new TestpackProducts;
													$t_model->prod_id = $model->prod_id;
													$attributes_to_save = array('id', 'drug_id', 'drugname',  'dose', 'amount', 'amount_in',  );
													foreach($attributes_to_save as $attr)
													{
														$t_model->{$attr} = $t_product->{$attr};
													}
													$attributes_to_save = array_merge($attributes_to_save, array('prod_id'));
													if($t_model->save(true, $attributes_to_save)){
														$goodsave++;
													} else {
														$errors[] = $t_model->getErrors();
													}
												}

											}
										}
										// \testpacks

									} else {
										$errors[] = $model->getErrors();
									}

								}
							}

							
						} else{
							$errors[] = "Request::getProductsList error";
						}
						// \products
					}

					if($forma->templateimages){

						// template images
						$res = Request::downloadTemplateImgsZip();

						if($res){
							$goodmsg[] = 'Loading template images;';

							$zipFullPath = $res;
							require_once(YiiBase::getPathOfAlias('webroot') . '/protected/data/pclzip.lib.dat');
					        $archive = new PclZip($zipFullPath);
					        $f_list = $archive->extract(PCLZIP_OPT_PATH, Theme::getImgPathTemplate(), PCLZIP_OPT_REPLACE_NEWER);
					        if ($f_list == 0) {
					            $errors[] = "Error : ".$archive->errorInfo(true);
					        }else{
					            
					            unlink($zipFullPath);

					        }

						} else {
							$errors[] = "Request::downloadTemplateImgsZip error";
						}
						// \template images

					}


					if($this->installmode){
						// bank upd
						Config::updateBankDetails();
						// currencies upd
						Yii::app()->currencies->updateRates();
					}

					
				}


				if(count($errors) == 0){

					$goodmsg_string = implode(" ", $goodmsg);
					Yii::app()->user->setFlash('success',  $goodmsg_string .' Successfully! Updated ' . $goodsave . ' rows');
					
					if($this->installmode)
						$this->redirect(Yii::app()->createUrl('/admin/importreviews', array('install'=>1)));
					else
						$this->refresh();
				}

				// print_r($errors);
				// print_r($goodsave);
			} else {
				// print_r($forma->getErrors());
				$errors = array_merge($errors, $forma->getErrors());
			}


		}


		$criteria = new CDbCriteria;
		$criteria->order = 'position ASC, lang_id ASC';

		// $criteria->addNotInCondition('lang_id', array('us', 'au', 'ca', 'ca-fr', 'br'));



		$languages = Languages::model()->findAll($criteria);


		$this->render('importcontent', array('errors'=>$errors, 'goodmsg'=>$goodmsg, 'model'=>$forma, 'languages'=>$languages ));
	}



	public function actionImportReviews()
	{
		# импорт комментариев


		$errors = [];
		$model = [];
		$goodsave = 0;



		$forma = new ReviewsImportForm;

		$forma->min=35;
		$forma->max=40;
		$forma->votes_min=200;
		$forma->votes_max=500;
		$forma->votes_yes_prc_min=70;
		$forma->votes_yes_prc_max=90;

		$forma->fake_ratings = 4;
		$forma->smart_random = true;


		$forma->use_reviews = Yii::app()->config->get('use_reviews');



		if(isset($_POST['ReviewsImportForm'])){

			$forma->attributes = $_POST['ReviewsImportForm'];

			if($forma->validate()){

				
				if($forma->fake_ratings != 0){
					$forma->min = 999;
					$forma->max = 999;
				}

				$params = array(
					'min'=>$forma->min,
					'max'=>$forma->max,
					'smart_random'=>$forma->smart_random,
					'fake_ratings'=>$forma->fake_ratings,
					'votes_min'=>$forma->votes_min,
					'votes_max'=>$forma->votes_max,
					'votes_yes_prc_min'=>$forma->votes_yes_prc_min,
					'votes_yes_prc_max'=>$forma->votes_yes_prc_max,
				);


				$languages_update = [];
				foreach($forma->languages as $lang=>$value)
				{
					if($value == 1){
						$languages_update[] = $lang;
					}
				}

				
				Voted::model()->deleteAll();
				
				$criteria = new CDbCriteria;
				$criteria->addInCondition('lang', $languages_update);
				
				Reviews::model()->deleteAll($criteria);
				

				

				Yii::app()->config->set('use_reviews', $forma->use_reviews);
				Yii::app()->cache->flush();

				if($forma->use_reviews == true){


					$res = Request::getReviews($languages_update, $params);
					// print_r($res); Yii::app()->end();
					if(isset($res['error'])){

						$errors[] = $res['error'];

					} else {


						$data = $res['data']->ratings;

						foreach($data as $id => $item)
						{

							$model=Drugs::model()->multilang()->findByPk($id);
							foreach($item as $language => $rating)
							{
								$suffix = '_'.$language;
								if($language == Yii::app()->language) { $suffix = ''; }
								$model->{'rating' . $suffix} = $rating;
							}

							if($model->update('rating')){
								$goodsave++;
							} else {
								$errors[] = $model->getErrors();
							}
						}

						$data = $res['data']->reviews;
						foreach($data as $language=>$item)
						{
							foreach($item as $row)
							{
								$model=Reviews::model()->findByAttributes(array('date_cr'=>$row->date_cr, 'author'=>$row->author, 'drug_id'=>$row->drug_id, 'lang'=>$language));
								if($model === null) {
									$model = new Reviews;
								}

								$model->lang = $language;

								// $model->vote_all = rand($forma->votes_min, $forma->votes_max);
								// $model->vote_yes = round($model->vote_all / 100 * rand($forma->votes_yes_prc_min, $forma->votes_yes_prc_max));

								// $model->points = self::rand_points();

								$attributes_to_save = array('date_cr', 'author', 'drug_id', 'content', 'tit_name', 'vote_all' , 'vote_yes', 'points' );
								
								foreach($attributes_to_save as $attr)
								{
									$model->{$attr} = $row->{$attr};
								}
								$attributes_to_save = array_merge($attributes_to_save, array('lang', ));

								if($model->save(true, $attributes_to_save)){
									$goodsave++;
								} else {
									$errors[] = $model->getErrors();
								}
							}
						}

					}
				}

				if(count($errors) == 0){

					Yii::app()->user->setFlash('success','Successfully! Updated ' . $goodsave . ' rows');
					if($this->installmode)
						$this->redirect(Yii::app()->createUrl('/admin/config'));
					else 
						$this->refresh();
				}

				// print_r($errors);
				// print_r($goodsave);

				// Yii::app()->end();
			}

		}


		$this->render('importreviews', array('errors'=>$errors, 'model'=>$forma, ));
	}



	public function actionGenerateLinks()
	{

		$links = [];

		$form = new GenerateLinkForm;
		$current_language = Yii::app()->language;

		$form->format = GenerateLinkForm::FORMAT_TEXT;
		$form->ancors = GenerateLinkForm::ANCOR_TITLE;
		$form->language = $current_language;


		if(isset($_POST['GenerateLinkForm'])){
			$form->attributes = $_POST['GenerateLinkForm'];

			Yii::app()->language = $form->language;
		}


		if($form->ancors == GenerateLinkForm::ANCOR_TITLE){
			$ancor_attr = 'title';
 		}
 		elseif($form->ancors == GenerateLinkForm::ANCOR_H1){
 			$ancor_attr = 'header';
 		}
 		elseif($form->ancors == GenerateLinkForm::ANCOR_NAME){
 			$ancor_attr = 'drugname';
 		}

		if($form->index){
			$model = Pages::model()->findByAttributes(array('slug'=>'_index'));
			if($model !== null){
				$ancor = '';
				if(isset($model->{$ancor_attr})){
					$ancor = $model->{$ancor_attr};
				}
				$links[$model->url] = $ancor;
			}
		}

		foreach(array('Categories', 'Drugs',  ) as $name)
		{
			if($form->{strtolower($name)}){
				$model = $name::model()->findAllByAttributes(array('active'=>1));
				foreach($model as $item)
				{
					$ancor = '';
					if(isset($item->{$ancor_attr})){
						$ancor = $item->{$ancor_attr};
					} elseif($ancor_attr == 'drugname' && $name == 'Categories'){
						$ancor = $item->catname;
					}
					$links[$item->url] = $ancor;
				}
			}

		}

		if($form->pages){
			$model = Pages::model()->findAll();
			foreach($model as $item)
			{
				$ancor = '';
				if(isset($item->{$ancor_attr})){
					$ancor = $item->{$ancor_attr};
				}
				if(!preg_match('@^_@si', $item->slug)){
					$links[$item->url] = $ancor;
				}
			}
		}

		if($form->testimonials){
			$criteria = new CDbCriteria;
			$criteria->with = array('drug');
			$criteria->condition = 'drug.active = 1';
			$model = DrugsTestimonials::model()->findAll($criteria);
			foreach($model as $item)
			{
				$ancor = '';
				if(isset($item->{$ancor_attr})){
					$ancor = $item->{$ancor_attr};
				}
				$links[$item->url] = $ancor;
			}
		}

		if($form->articles){
			$model = Articles::model()->findAllByAttributes(array('lang'=>$form->language));
			foreach($model as $item)
			{
				$ancor = '';
				if(isset($item->{$ancor_attr})){
					$ancor = $item->{$ancor_attr};
				} elseif($ancor_attr == 'drugname'){
					$ancor = $item->postname;
				}
				$links[$item->url] = $ancor;
			}
		}

		Yii::app()->language = $current_language;

		// 'Articles', 'Pages', 'Testimonials'

		// $domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);

		// $links[Yii::app()->getBaseUrl(true)] = 'index';
		$this->render('generatelinks', array('links'=>$links, 'model'=>$form));
	}

}