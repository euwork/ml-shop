<?php

class AffiliatesController extends PageWithFormController
{



	public function actionIndex()
	{
		$model=new Affiliates;

		if(isset($_POST['Affiliates']))
		{
			$model->attributes=$_POST['Affiliates'];
			if($model->validate()) {
				Request::sendAffiliate($model);
				Yii::app()->user->setFlash('success', Yii::t('contacts', 'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!'));
				$this->redirect(Yii::app()->getRequest()->getOriginalUrl());
			}
		}

		$text = Pages::model()->findByAttributes(array('slug' => '_affiliates'));
		
		if($text===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$this->pageTitle = $text->title;
		$text->header = str_replace('{domain}', $this->domain, $text->header); 
		$text->content = str_replace('{domain}', $this->domain, $text->content); 

		$this->render('index',array(
			'model'=>$model, 'text' => $text,
		));
	}

	
}
