<?php

class ArticlesController extends PageViewController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// public $layout='//layouts/column3_articles';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view', 'index'),
				'users'=>array('*'),
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	// public function actionView($slug)
	// {
	// 	if($slug !== strtolower($slug)){
	// 		throw new CHttpException(404,'The requested page does not exist.');
	// 	}
		
	// 	$model = $this->loadModelBySlug($slug);

	// 	if($model->lang != Yii::app()->language){
	// 		$this->redirect(Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.article')));
	// 	}
		
	// 	$this->pageTitle = $model->title;
	// 	$this->keywords = $model->keywords;
	// 	$this->description = $model->description;

	// 	$this->render('view',array(
	// 		'model'=>$model,
	// 	));
	// }

	// public function loadModelBySlug($slug)
	// {
	// 	$model=Articles::model()->findByAttributes(array('slug' => $slug));
	// 	if($model===null)
	// 		throw new CHttpException(404,'The requested page does not exist.');
	// 	return $model;
	// }

	public function actionIndex()
	{
		$model = Articles::getArticles();
		$this->render('index', array('model' => $model));
	}

}
