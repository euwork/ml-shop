<?php
class CartController extends FrontEndController
{

	public $billing_internal_url;

	public function init()
	{
		parent::init();

		$this->billing_internal_url = Yii::app()->createUrl('/cart/checkout');

	}


	public function actionIndex()
	{
		if(Yii::app()->shoppingCart->isEmpty()){
			$this->redirect(Yii::app()->createUrl('/'));
		}

		$this->render('index', array());
	}


    /* добавление товара в корзину */
	public function actionAdd($id)
	{
		# $id = $prod_id
		$model = Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested product does not exist.');

		if($model->status == Products::NOACTIVE){
			$this->redirect($model->drug->url);
		}

		if(!Yii::app()->shoppingCart->contains($model->id)){
			Yii::app()->shoppingCart->put($model);
		} else{
			$position = Yii::app()->shoppingCart->itemAt($model->id);
			if($position->use_discount){
				Yii::app()->shoppingCart->remove($model->id);
				Yii::app()->shoppingCart->put($model);
			}
		}

		$this->redirect(Yii::app()->createUrl('/cart'));
	}
	/* удаление товара из корзины */
	public function actionRemove($id)
	{
		# $id = 'Prod'.$prod_id
		if(Yii::app()->shoppingCart->contains($id)){
			Yii::app()->shoppingCart->remove($id);
		}

		$this->redirect(Yii::app()->createUrl('/cart'));
	}
	/* переход на следующий пак, скидка */
	public function actionUpgrade($id)
	{
		# $id = 'Prod'.$prod_id
		if(Yii::app()->shoppingCart->contains($id)){

			$position = Yii::app()->shoppingCart->itemAt($id);
			
			$model = Products::getNextpack($position);
			if($model===null)
				throw new CHttpException(404,'The requested product does not exist.');

			Yii::app()->shoppingCart->remove($id);

			if(Yii::app()->shoppingCart->contains($model->id)) {
				Yii::app()->shoppingCart->remove($model->id);
			}

			$model->use_discount = true;

			Yii::app()->shoppingCart->put($model);
		}

		$this->redirect(Yii::app()->createUrl('/cart'));
	}	

	public function actionCheckout()
	{

		if(Yii::app()->shoppingCart->isEmpty()){
			$this->redirect(Yii::app()->createUrl('/'));
		}

		$one_step = false;
		if(Config::getOrderFormInOneStep() == true ){
			$one_step = true;
		}

		$step = isset($_POST['CheckoutForm']) && isset($_POST['CheckoutForm']['step']) ?
			$_POST['CheckoutForm']['step'] : 1 ;
		
		if($one_step){ $step = 2;}

		if(!in_array($step, range(1,3)))
			throw new CHttpException(404,'The requested step does not exist.');

		$model = new CheckoutForm('step' . $step) ;

		if(!isset($_POST['CheckoutForm']) && ($step == 1 or $one_step) ){
			Yii::app()->traffCounter->goal('bill');
		}
		
	
		$positions = Yii::app()->shoppingCart->getPositions();
		$is_original = false;
        foreach($positions as $item)
        {
            if($item->isOriginal()){
                $is_original = true;
                break;
            }
        }

		// validate the previous step
		if(isset($_POST['CheckoutForm'])) {
			$model->attributes=$_POST['CheckoutForm'];
			
			$next_step = true;
			// если есть оригинал таблы, а оплата мастер или амекс
			if( $step == 2 && $is_original && 
				in_array($model->payment_method, array(Orders::PAYMENT_MASTER, Orders::PAYMENT_AMEX))  ){

					$model->addError('payment_method', 'Not valid payment method');
					$next_step = false;
					
			}
			// валидация номера банковской карты
			if( $step == 2 && !in_array( $model->payment_method, array( Orders::PAYMENT_WIRE, Orders::PAYMENT_IDEAL)) ){
				switch ($model->payment_method) {
					case Orders::PAYMENT_VISA:
						if(!preg_match('@^[4][0-9]{15}$@', $model->card_number)){
							$model->addError('card_number', 'Not valid card_number');
							$next_step = false;
						}
						break;
					case Orders::PAYMENT_MASTER:
						if(!preg_match('@^[5][0-9]{15}$@', $model->card_number)){
							$model->addError('card_number', 'Not valid card_number');
							$next_step = false;
						}
						break;	
					case Orders::PAYMENT_AMEX:
						if(!preg_match('@^[3][0-9]{14}$@', $model->card_number)){
							$model->addError('card_number', 'Not valid card_number');
							$next_step = false;
						}
						break;		
					case Orders::PAYMENT_BC:
						if(!preg_match('@^[4][0-9]{15}$@', $model->card_number)){
							$model->addError('card_number', 'Not valid card_number');
							$next_step = false;
						}
						break;			
					default:
						if(!preg_match('@^[0-9]{16}$@', $model->card_number)){
							$model->addError('card_number', 'Not valid card_number');
							$next_step = false;
						}
						break;
				}
			}

			// валидация cvv
			if( $step == 2 && !in_array( $model->payment_method, array( Orders::PAYMENT_WIRE, Orders::PAYMENT_IDEAL)) ){
				if($model->payment_method == Orders::PAYMENT_AMEX){
					if(!preg_match('@^[0-9]{4}$@', $model->card_cvv)){
						$model->addError('card_cvv', 'Not valid card_cvv');
						$next_step = false;
					}
				} else {
					if(!preg_match('@^[0-9]{3}$@', $model->card_cvv)){
						$model->addError('card_cvv', 'Not valid card_cvv');
						$next_step = false;
					}
				}

			}

			// валидация card_year, card_month
			if( $step == 2 && !in_array( $model->payment_method, array( Orders::PAYMENT_WIRE, Orders::PAYMENT_IDEAL)) ){
				if($model->card_year < date('Y')){
					$model->addError('card_year', 'Not valid card_year');
					$next_step = false;
				}

				if($model->card_year == date('Y') && $model->card_month < date('m')){
					$model->addError('card_month', 'Not valid card_month');
					$next_step = false;
				}
			}


			$model->validate();

			if ($next_step && $model->validate()) {

				$step++ ;
				if ($step < 3) {
					
					$model = new CheckoutForm('step' . $step) ;
					$model->attributes=$_POST['CheckoutForm'] ;

					
				} else { // $step == 3

					$model = new Orders;
					$model->attributes=$_POST['CheckoutForm'] ;


					$model->language = Yii::app()->language;
					$model->products_label = Config::getCurrentProductsBase();

					// отображаемая комиссия 0 для карт
                    if($model->isIpillBilling()){
                    	$model->comission = 0;
                    } else {
						$model->comission = Profile::getComission();
                    }
                    	
					$model->affid = Yii::app()->config->get('affid');
					
					$model->subtotal = Yii::app()->shoppingCart->getCost(); // стоимость товаров с учетом скидки, без доставки и страховки
					$model->shipping_price = $model->getItemShippingPrice($is_original);
					$model->insurance = $model->insurance ? Orders::getInsurancePrice() : '0.0';
					$model->total = $model->subtotal + $model->insurance;

					// дублируем поля с ценам в usd
					$rate_usd = Currencies::getRate('USD');
					$model->subtotal_usd = round($model->subtotal * $rate_usd, 2);
					$model->total_usd = round($model->total * $rate_usd, 2);
					$model->insurance_usd = round($model->insurance * $rate_usd, 2);
					$model->shipping_price_usd = round($model->shipping_price * $rate_usd, 2);


					if($model->payment_method == Orders::PAYMENT_WIRE){
						$model->card_year = '';
						$model->card_month = '';
						$model->card_number = '';
						$model->card_cvv = '';
					}

					if( in_array($model->payment_method, array(Orders::PAYMENT_BC))) {
						// $model->card_date_birth = date('Y-m-d', $bc_time);
					}

					$model->http_ip = Yii::app()->request->getUserHostAddress();
					$model->http_country = Yii::app()->sypexGeo->getCountry(Yii::app()->request->getUserHostAddress());
					$model->http_referer = Yii::app()->traffCounter->getReferer();
					$model->trackid = Yii::app()->traffCounter->getTrackid();

					$model->http_user_agent = Yii::app()->request->getUserAgent();
					$model->http_x_forwarded_for = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
					$model->http_x_forwarded = isset($_SERVER['HTTP_X_FORWARDED']) ? $_SERVER['HTTP_X_FORWARDED'] : '';
					$model->http_forwarded = isset($_SERVER['HTTP_FORWARDED']) ? $_SERVER['HTTP_FORWARDED'] : '';
					$model->http_proxy_agent = isset($_SERVER['HTTP_PROXY_AGENT']) ? $_SERVER['HTTP_PROXY_AGENT'] : '';
					$model->http_via = isset($_SERVER['HTTP_VIA']) ? $_SERVER['HTTP_VIA'] : '';
					$model->http_proxy_connection = isset($_SERVER['HTTP_PROXY_CONNECTION']) ? $_SERVER['HTTP_PROXY_CONNECTION'] : '';

					$http_data_info = array();
					foreach($_SERVER as $key=>$value)
					{
						if($key === 'HTTP_COOKIE') continue;
						if($key === 'REMOTE_ADDR' or substr($key, 0, 5) === 'HTTP_') {
							$http_data_info[$key] = $value;
						}
					}

					$model->http_data_info = json_encode($http_data_info, 320) ;
					if(strlen($model->http_data_info) > 2048){ $model->http_data_info = substr($model->http_data_info, 0, 2048); }
					
					$model->log_tzinfo = $_POST['CheckoutForm']['tzinfo'];

					$model->status = Orders::STATUS_NEW;

					$model->reorder = Orders::isReorder($model->email) ? 1 : 0;

					$model->gender = Orders::GENDER_MALE; // 


					if($model->save()){
						
						$orderStatuses = new OrderStatuses;
						$orderStatuses->order_id = $model->order_id;
						$orderStatuses->status = Orders::STATUS_NEW;
						if(!$orderStatuses->save()){
								// print_r($orderStatuses);
						}
						foreach($positions as $prod)
						{
							$orderProduct = new OrderProducts;
							$orderProduct->order_id = $model->order_id;
							$orderProduct->prod_id = $prod->prod_id;
							$orderProduct->price = $prod->getPrice();
							$orderProduct->price_usd = round($prod->getPrice() * $rate_usd, 2);
							$orderProduct->discount = number_format( (100 - $prod->getSumPrice() * 100 / $prod->getPrice()), 2);
							if(!$orderProduct->save()){
								// print_r($orderProduct);
							}
							
						}
						Yii::app()->shoppingCart->clear();
						Yii::app()->session->add("order_id", $model->order_id);

						$this->redirect(Yii::app()->createUrl('/cart/thankyou'));
					} else {
						print_r($model);
						print_r($model->getErrors());
						Yii::app()->end();
					}
				}
					//if step == 3 do save order!!!
			}
		}

		if($one_step){
			$view = 'checkout_one_step';
		} else {
			$view = 'checkout_step' . $step;
		}

		$this->render($view, array('model' => $model, 'is_original'=>$is_original,
					      )) ;
	}


	public function actionThankyou()
	{

		$checkwire = Yii::app()->request->getQuery('checkwire');
		if($checkwire == 1){

			$slug = '_thankyou_wire';
			$model = Orders::model()->with(array('order_products'=>array('order'=>'order_products.price ASC')))->find();

		} else {

			$order_id = Yii::app()->session->get("order_id");
			// echo $order_id;
			if(!$order_id) $this->redirect(Yii::app()->createUrl('/'));
			Yii::app()->session->remove("order_id");
			// Yii::app()->end();

			$model = Orders::model()->with(array('order_products'=>array('order'=>'order_products.price ASC')))->findByPk($order_id);
			if($model===null)
					throw new CHttpException(404,'The requested product does not exist.');
				
			if(Yii::app()->config->get('cart.send_in_thankyou')){
				Request::sendOrder($model); // отправляем заказ
			}

			Yii::app()->traffCounter->goal('ord');

			if($model->payment_method == Orders::PAYMENT_WIRE){
				$slug = '_thankyou_wire';
			}elseif($model->payment_method == Orders::PAYMENT_IDEAL){
				$slug = '_thankyou_ideal';
			} else {
				$slug = '_thankyou_card';
			}

		}


		$text = Pages::model()->findByAttributes(array('slug' => $slug));
		
		$this->render('thankyou', array('model' => $model, 'text' =>$text)) ;

	}
	
}