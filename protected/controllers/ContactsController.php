<?php

class ContactsController extends PageWithFormController
{


	public function actionIndex()
	{
		$model=new Contacts;

		if(isset($_POST['Contacts']))
		{
			$model->attributes=$_POST['Contacts'];
			if($model->validate()) {
				Request::sendContact($model);
				Yii::app()->user->setFlash('success', Yii::t('contacts', 'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!'));
				$this->redirect(Yii::app()->getRequest()->getOriginalUrl());
			}
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}


}
