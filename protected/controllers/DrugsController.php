<?php

class DrugsController extends PageViewController
{


	public $related_posts = array();

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'captcha', 'like'),
				'users'=>array('*'),
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'foreColor'=>'0x000000',
                'height' => 28,
                'width' => 104,
                // 'verifyCode' => 'code',
                'maxLength' => 4,
                'minLength' => 3,
                'offset'=> 5,
                'testLimit'=>1,
            ),
        );
    }



	public function actionLike()
	{
		// Yii::app()->end();
		if(!Yii::app()->request->isAjaxRequest){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		if(!isset($_POST['drug']) or !((int)$_POST['drug'] > 0) or !isset($_POST['like'])) {
			throw new CHttpException(404,'The requested page does not exist.');
		}



		$data = array();

		// rev_id, vote (0|1)
		$drug_id = (int) $_POST['drug'];
		$like = (bool) $_POST['like'];

		$ip = Yii::app()->request->getUserHostAddress();
		
		if(!Likes::check($ip, $drug_id)){
			throw new CHttpException(404,'Access denied.');
		}

		$drug = Drugs::model()->multilang()->findByPk($drug_id);

		if($drug===null){
			throw new CHttpException(404,'The requested review does not exist.');
		}

		if($like){
			$drug->{'likes' . '_' . Yii::app()->language} += 1;
		} 

		if($drug->save(true, array('likes'))){
			$data['likes'] = $drug->{'likes' . '_' . Yii::app()->language};

			Likes::add($ip, $drug_id);
		}

		echo json_encode($data);

		
		Yii::app()->end();
	}
}
