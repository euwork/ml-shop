<?php

class DrugsTestimonialsController extends PageViewController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view', 'captcha', 'vote'),
				'users'=>array('*'),
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'foreColor'=>'0x000000',
                // 'backColor'=>'0xEFEFEF',
                'height' => 32,
                'width' => 130,
                // 'verifyCode' => 'code',
                'maxLength' => 4,
                'minLength' => 3,
                'offset'=> 5,
                'testLimit'=>1,
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($slug)
	{

		if(!Yii::app()->config->get('use_reviews')){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		if($slug !== strtolower($slug)){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		$model = $this->loadModelBySlug($slug);

		$model->title = Filter::generic($model->title);
		$model->description = Filter::generic($model->description);
		$model->header = Filter::generic($model->header);
		$model->subheader = Filter::generic($model->subheader);
		$model->drug_ancor = Filter::generic($model->drug_ancor);
		
		$this->pageTitle = $model->title;
		$this->keywords = $model->keywords;
		$this->description = $model->description;

		$form = new Reviews('frontadd');
		if(isset($_POST['Reviews'])){

			$ratefeature = Yii::app()->request->getPost('ratefeature', 0);
			$rateperformance = Yii::app()->request->getPost('rateperformance', 0);
			$ratevalue = Yii::app()->request->getPost('ratevalue', 0);
			$rateoverall = Yii::app()->request->getPost('rateoverall', 0);
			$points = number_format(($ratefeature + $rateperformance + $ratevalue + $rateoverall) / 4, 2);

			$form->attributes=$_POST['Reviews'];
			$form->drug_id = $model->drug_id;
			$form->lang = Yii::app()->language;
			$form->points = $points;
			$form->date_cr = date('Y-m-d H:i:s');
			$form->active = 0;
			$form->new = 1;
			if($form->save()){
				// Request::sendReview($form);
				Yii::app()->user->setFlash('success', Yii::t('testimonials', 'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird'));
				// $this->refresh();
				$this->redirect(Yii::app()->getRequest()->getOriginalUrl() . '#newtestimonial');
			} else {
				// print_r($form->getErrors());
				// Yii::app()->end();
			}
		}

		// отдельно грузим reviews
		$criteria = new CDbCriteria;
		$criteria->order = 'date_cr DESC';
		$criteria->condition = 't.lang = :lang and t.active=1 and t.drug_id = :drug_id';
		$criteria->params = array(':lang'=>Yii::app()->language, ':drug_id' =>$model->drug_id);
		$count = Reviews::model()->count($criteria);

		$pages = new CPagination($count);
		$pages->pageSize = Yii::app()->config->get('reviews.count.testimonials');
	    $pages->applyLimit($criteria);

	    if((Yii::app()->request->getQuery($pages->pageVar, 1)-1) != $pages->currentPage)
	    	throw new CHttpException(404,'The requested page does not exist.');

		$reviews = Reviews::model()->findAll($criteria);

		$this->render('view',array(
			'model'=>$model, 'form' => $form, 'reviews' => $reviews, 'pages'=>$pages,
		));
	}

	public function actionVote()
	{
		// Yii::app()->end();
		if(!Yii::app()->request->isAjaxRequest){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		if(!isset($_POST['rev']) or !((int)$_POST['rev'] > 0) or !isset($_POST['vote'])) {
			throw new CHttpException(404,'The requested page does not exist.');
		}



		$data = array();

		// rev_id, vote (0|1)
		$rev_id = (int) $_POST['rev'];
		$vote = (bool) $_POST['vote'];

		$ip = Yii::app()->request->getUserHostAddress();

		$voted = Voted::check($ip, $rev_id);
		// if($voted ==! false){
		// 	$last_vote = $voted->value;
		// }
			
// var_dump($voted ==! false);
// var_dump($voted);
// var_dump((bool) $kkk == $vote);
		
		if($voted ==! false && (bool) $voted->value == $vote){
			throw new CHttpException(404,'Access denied.');
		}

		$review = Reviews::model()->findByPk($rev_id);

		if($review===null){
			throw new CHttpException(404,'The requested review does not exist.');
		}

		if($voted === false){
			if($vote){
				$review->vote_yes += 1;
			} 
			$review->vote_all += 1;
		}elseif($voted->value == true){
			$review->vote_yes -= 1;
		}elseif($voted->value == false){
			$review->vote_yes += 1;
		}
			

		if($review->save(true, array('vote_yes', 'vote_all'))){
			$data['vote_yes'] = $review->vote_yes;
			$data['vote_all']  = $review->vote_all;

			Voted::add($ip, $rev_id, $vote);
		}

		echo json_encode($data);

		


		Yii::app()->end();
	}



	// public function loadModelBySlug($slug)
	// {
	// 	$model=DrugsTestimonials::model()->findByAttributes(array('slug' => $slug));
	// 	if($model===null)
	// 		throw new CHttpException(404,'The requested page does not exist.');
	// 	return $model;
	// }

	public function loadModelBySlug($slug)
	{
		$lang = Yii::app()->language;
		$id = DrugsTestimonials::findByLSlug($slug, $lang);
		if($id){
			$model=DrugsTestimonials::model()->findByPk($id);

			// if($model->active == 0){
			// 	throw new CHttpException(404,'The requested page does not exist.');
			// }
		} 
			

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
