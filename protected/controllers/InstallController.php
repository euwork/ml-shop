<?php

class InstallController extends BackEndController
{

	public function init()
	{
		parent::init();

		$this->layout='//layouts/install';
	}


	public function actionIndex()
	{


		$webroot = YiiBase::getPathOfAlias('webroot');

		# todo: если шоп установлен, редирект на /admin
		# 
		$db_config = $webroot . '/protected/config/db.php';
		if(file_exists($db_config) && strlen(file_get_contents($db_config)) > 0){
			$this->redirect('/admin');
		}
		

		# проверка сервера

		$errors = [];


		if((float)phpversion() > 7.4){
			$errors[] = 'Php version 7.4 or lower required'; 
		}

		$writable_dirs = array('/assets', '/protected/runtime', '/images/d2/template');
		$writable_files = array('/protected/config/db.php', '/protected/config/params.php');

		foreach($writable_dirs as $item)
		{
			if(!is_writable($webroot . $item)){
				$errors[] = 'Directory "'.$item.'" is not writable. Please, chmod it to 0777.';
			}

		}

		foreach($writable_files as $item)
		{
			if(!is_writable($webroot . $item)){
				$errors[] = 'File "'.$item.'" is not writable. Please, chmod it to 0666.';
			}

		}

		// print_r(get_loaded_extensions());
		$extension_list = array('json', 'curl', 'pdo_mysql', 'pdo_sqlite');
		foreach($extension_list as $item)
		{
			if(!extension_loaded($item)){
				$errors[] = 'You need to enable php extension "'.$item.'".';
			}
		}
		// var_dump($extension_yes);

		# ввод aff_id, api_key
		# ввод login:pass
		# ввод db_login, db_pass, db_name

		$model = new InstallForm;

		$model->db_host = 'localhost';
		$model->db_port = '3306';
		$model->api_domain = 'ipillcash.com';

		if(isset($_POST['InstallForm']))
		{
			$model->attributes=$_POST['InstallForm'];
			if($model->validate() ){

				$request = Request::checkApiKey($model->api_domain, $model->aff_id, $model->api_key);
				
				if(!$request){
					$errors[] = 'Api Key error.';
					$model->addError('api_key', '');
					$model->addError('aff_id', '');
				}

				$connect = @mysqli_connect($model->db_host, $model->db_login, $model->db_password, $model->db_name, $model->db_port);
				if(!$connect){
					$errors[] = 'MySQL connection error.';
					$model->addError('db_host', '');
					$model->addError('db_port', '');
					$model->addError('db_login', '');
					$model->addError('db_password', '');
					$model->addError('db_name', '');
				}

				if($request && $connect){
					# запись доступов к бд в файл
					$result = InstallHelp::generateDbConfigCode($model->db_host, $model->db_port, $model->db_login, $model->db_password, $model->db_name);
					if(is_array($result)){
						$errors = array_merge($errors, $result);
					}
					# импорт дампа
					$result = InstallHelp::makeImportBaseSqlDump($model->db_host, $model->db_port, $model->db_login, $model->db_password, $model->db_name);
					if(is_array($result)){
						$errors = array_merge($errors, $result);
					}

					$result = InstallHelp::updateApiParams($model->db_host, $model->db_port, $model->db_login, $model->db_password, $model->db_name, $model->api_domain , $model->api_key, $model->aff_id);
					if(is_array($result)){
						$errors = array_merge($errors, $result);
					}

					# Запись api_key, aff_id в файл
					$result = InstallHelp::generateParamsConfigCode($model->login, $model->password /*$model->aff_id, $model->api_key*/);
					if(is_array($result)){
						$errors = array_merge($errors, $result);
					}

					if(count($errors) == 0){


						$lform = new LoginForm;
						$lform->username = $model->login;
						$lform->password = $model->password;
						$lform->rememberMe = true;
						if($lform->validate() && $lform->login()){
							$this->redirect(Yii::app()->createUrl('/admin/importcontent', array('install'=>1)));
						} else {
							$model->addError('login', '');
							$model->addError('password', '');
						}

					}
				}
			}

		}

		

		$this->render('index', array('errors'=>$errors, 'model'=>$model, ));
			
	}

	// private function std_to_array($std)
	// {
	// 	$data = [];
	// 	foreach($std as $k=>$v)
	// 	{
	// 		$data[$k] = $v;
	// 	}
	// 	return $data;
	// }







}