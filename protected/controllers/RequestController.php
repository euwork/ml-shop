<?php
class RequestController extends BackEndController
{
	public $layout='//layouts/clear';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
						'sendnew', 
						'api', 
						'clearstat', 
						'test1', 
						'updatecurrenciesrate', 
						'updategoodslist', 
						'updatebankdetails',
						'updateproductslist',
						'productlist'
					),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{

	}

	public function actionClearStat()
	{
		TraffStat::clearStat();

		$out = Yii::app()->request->getParam('out');

		if($out == 1){
			echo "run ok\n";
		}

		Yii::app()->end();
	}

	public function actionSendNew() // отправка заказов в ipill
	{
		Yii::log('run action sendnew', CLogger::LEVEL_INFO, 'application.controllers.request');
		# берем модели со статусом new && processed = 0
		$criteria = new CDbCriteria;
		$criteria->condition = 'status = :status and processed = :processed';
		$criteria->params = array(':status'=>Orders::STATUS_NEW, ':processed'=>0);
		$criteria->order = 'date_cr ASC';
		$criteria->limit = 5;
		$model = Orders::model()->with(
				array('order_products'=>array(  'order'=>'order_products.price ASC', 
												'with'=>'product')))->findAll($criteria);

		if(count($model) > 0 ){

			foreach($model as $order)
			{
				Yii::app()->language = $order->language;
				// echo "{$order->oder_id}\n";
				Yii::log('send new order:' . $order->order_id , CLogger::LEVEL_INFO, 'application.controllers.request');
				if(Request::sendOrder($order)){
					Yii::log('order:' . $order->order_id . ' sent' , CLogger::LEVEL_INFO, 'application.controllers.request');
				} else {
					Yii::log('order:' . $order->order_id . ' not sent', CLogger::LEVEL_ERROR, 'application.controllers.request');
				}
				// Yii::app()->end();
			}
		}

		$out = Yii::app()->request->getParam('out');
		if($out == 1){
			echo "run ok\n";
		}

		Yii::app()->end();
	}




	public function actionApi()
	{

		if(strlen(Yii::app()->config->get('api_key')) != 32) {
			Yii::app()->end();
		}

		$apikey = Yii::app()->request->getPost('apikey');

		if( $apikey != Yii::app()->config->get('api_key')){
			Yii::app()->end();
		}

		$action = Yii::app()->request->getPost('action');

		if(!in_array($action, 
				array(
					'stat', 
					'changestatus', 
					'newpayment', 
					'paymentinfo', 
					'balance', 
					'comission', 
					'changeordercomission', 
					'unsentorders', 
					'bankdetails',
					'currencyrates',
					'change_api_domain',
					'change_shipping_settings',
					)
				)
			) {
			Yii::app()->end();
		}

		$period_from = Yii::app()->request->getPost('period_from');
		$period_to = Yii::app()->request->getPost('period_to');
		
		if($period_from !== null){
			if(preg_match('@[^\d\-]@', $period_from) ) {
				Yii::app()->end();
			}

			$date_format = "%Y %m %d";
			$from = date('Y-m-d 00:00:00', strtotime($period_from));
		}

		if($period_to !== null){
			if(preg_match('@[^\d\-]@', $period_to) ) {
				Yii::app()->end();
			}

			$to = date('Y-m-d 23:59:59', strtotime($period_to));

		} else{
			$to = date('Y-m-d 23:59:59');
		}

		$res = array();
		$res['action'] = $action;

		switch ($action) {
			case 'stat':
				$data_traff = TraffStat::getByDate($from, $to, $date_format);
				$data_order = OrdersStat::getByDateApiTraff($from, $to, $date_format);
				$data_on_days = array();

				$from_time = strtotime($period_from);
				$to_time = strtotime($period_to);
				

				for($i=$to_time; $i > ($from_time-1) ;$i=$i-3600*24)
				{
					// echo date('Y-m-d', $i)."\n";
					$data = array();
					$cur_date = date('Y m d', $i);
					$data['dat'] 		= $cur_date;
					$data['accept'] 	= 0;
					$data['wait'] 		= 0;
					$data['decline'] 	= 0;
					$data['hit'] 		= 0;
					$data['uniq'] 		= 0;
					$data['bill'] 		= 0;
					$data['amount'] 	= 0;
					$data['profit'] 	= 0;
					$data['shipping'] 	= 0;
					$data['insurance'] 	= 0;
					foreach($data_traff as $item)
					{
						if($item['dat'] == $cur_date){
							$data['hit'] 	= $item['hit'];
							$data['uniq'] 	= $item['uniq'];
							$data['bill'] 	= $item['bill'];
						}
					}
					foreach($data_order as $item)
					{
						if($item['dat'] == $cur_date){
							$data['accept'] = $item['accept'];
							$data['wait'] = $item['wait'];
							$data['decline'] = $item['decline'];
							$data['amount'] = $item['amount'];
							$data['profit'] = $item['profit'];
							$data['shipping'] = $item['shipping'];
							$data['insurance'] = $item['insurance'];
						}
					}
					$data_on_days[] = $data;
				}

			

				$res['stat'] 	= $data_on_days;

				break;
			
			case 'balance':

				$balance = Yii::app()->request->getPost('balance');
				if(Profile::updateBalance($balance)){
					$res['result'] = true;
				} else {
					$res['result'] = false;
				}

				break;

			case 'comission':

				$comission = Yii::app()->request->getPost('comission');
				if(Profile::updateComission($comission)){
					$res['result'] = true;
				} else {
					$res['result'] = false;
				}

				break;

			case 'paymentinfo':
			
				$res = array_merge($res, Profile::getPaymentInfo());

				break;

			case 'unsentorders':

				$res = array_merge($res, Orders::getUnsentCount());

				break;

			case 'newpayment':
				$payout = Yii::app()->request->getPost('payout');
				$comment = Yii::app()->request->getPost('comment');

				$model = new Payments;
				$model->amount = $payout;
				$model->comment = $comment;
				if($model->save()){
					$res['result'] = true;
					Profile::updateBalance($payout*(-1));
				} else {
					$res['result'] = false;
				}

				break;

			case 'changestatus':
				// print_r($_POST);
				$new_status = Yii::app()->request->getPost('new_status');
				$order_id = Yii::app()->request->getPost('order_id');
				if($order_id === null or $new_status === null){
					Yii::app()->end();
				}

				if(Orders::updateOrder($new_status, $order_id)){
					$res['result'] = true;
				}
				break;

			case 'changeordercomission':
				// print_r($_POST);
				$new_comission = Yii::app()->request->getPost('comission');
				$order_id = Yii::app()->request->getPost('order_id');
				if($order_id === null or $new_comission === null){
					Yii::app()->end();
				}

				if(Orders::updateComission($new_comission, $order_id)){
					$res['result'] = true;
				}
				break;

			case 'bankdetails':
				
				$bankdata = [];

				$bankdata['empf'] = Yii::app()->request->getPost('empf');
				$bankdata['iban'] = Yii::app()->request->getPost('iban');
				$bankdata['bic'] = Yii::app()->request->getPost('bic');
				$bankdata['country'] = Yii::app()->request->getPost('country');
				$bankdata['name'] = Yii::app()->request->getPost('name');
				$bankdata['adress'] = Yii::app()->request->getPost('adress');

				$bankdata['empf2'] = Yii::app()->request->getPost('empf2');
				$bankdata['iban2'] = Yii::app()->request->getPost('iban2');
				$bankdata['bic2'] = Yii::app()->request->getPost('bic2');
				$bankdata['country2'] = Yii::app()->request->getPost('country2');
				$bankdata['name2'] = Yii::app()->request->getPost('name2');
				$bankdata['adress2'] = Yii::app()->request->getPost('adress2');
				$bankdata['usefor2'] = Yii::app()->request->getPost('usefor2');

				Config::saveBankDetails($bankdata);
				$res['result'] = true;
				
				break;

			case 'currencyrates':
				// print_r($_POST);
				foreach(Currencies::getList() as $item)
				{
					if($item->default == 1) continue;

					$rate = Yii::app()->request->getPost($item->cur_id);
					
					if($rate) {

						$rate = round($rate, 5);
						if(Currencies::updateRate($item->cur_id, $rate)){
							$res['success'][] = $item->cur_id . ' +';
						} else {
							$res['error'][] = $item->cur_id . ' error';
						}

					}
				}
				Yii::app()->config->set('currency_rates.date_up', date('Y-m-d H:i:s'));
				Yii::app()->cache->flush();
				$res['result'] = true;
				
				break;

			case 'change_api_domain':
				$api_domain = Yii::app()->request->getPost('api_domain');
				Yii::app()->config->set('api_domain', $api_domain);
				Yii::app()->cache->flush();
				$res['result'] = true;

				break;


			case 'change_shipping_settings':

		        $attributes = array(
	                'ship_active_regular_de',
	                'ship_active_express_de',
	                'ship_price_regular_de',
	                'ship_price_express_de',
	                'ship_active_regular',
	                'ship_active_express',
	                'ship_active_normal',
	                'ship_price_regular',
	                'ship_price_express',
	                'ship_price_normal',
	                'ship_price_regular_de_less60',
	            );
	            foreach($attributes as $item)
	            {
	            	$item_var = Yii::app()->request->getPost($item);
	            	if($item_var !== null){
	            		Yii::app()->config->set($item, $item_var);
	            	}
	            }
				Yii::app()->cache->flush();
				$res['result'] = true;

				break;
		}	

		if(count($res) > 1){
			$res['aff_id']	= Yii::app()->config->get('affid');
			echo json_encode(array('success' =>$res));
		}

		Yii::app()->end();	
	}

	public function actionUpdateCurrenciesRate()
	{
		

		$data = Yii::app()->currencies->updateRates();

		Yii::app()->cache->flush();



		if(Yii::app()->request->getParam('return')){
			
			if(count($data['success']) > 0){
				Yii::app()->user->setFlash('success', implode(', ', $data['success']));
			}

			if(count($data['error']) > 0){
				Yii::app()->user->setFlash('error', implode(', ', $data['error']));
			}
			
			$this->redirect(array('/admin/currencies'));
		}	

		echo json_encode($data);
		Yii::app()->end();
	}

	public function actionUpdateBankDetails()
	{
		$data = Config::updateBankDetails();

		if(Yii::app()->request->getParam('return')){
			Yii::app()->user->setFlash('success', 'Updated');
			$this->redirect(array('/admin/bankdetails'));
		}	

		echo json_encode($data);
		Yii::app()->end();
	}

	public function actionUpdateProductsList()
	{
		$data = Request::getProductsList();

		$new = 0;
		$new_items = 0;
		$error = 0;
		$change = 0;
		$without_change = 0;

		foreach($data as $base)
		{
			foreach($base->products as $product)
			{
				$model = Products::model()->findByPk($product->prod_id);
				if($model === null){
					$model = new Products;
					foreach(array('prod_id', 'drug_id', 'dose', 'amount', 'bonus', 'price', 'discount', 'drugname', 'amount_in') as $attr)
					{
						$model->{$attr} = $product->{$attr};
					}
					$model->label = $base->base_label;
					$model->status = Products::ACTIVE;
					if(!$model->save()){
						// print_r($model->getErrors());
						$error++;
						continue;
					}
					$new++;

					if(isset($product->items)){
						foreach($product->items as $item_product)
						{
							$item_model = TestpackProducts::model()->findByPk($item_product->id);
							if($item_model === null){
								$item_model = new TestpackProducts;
								$item_model->prod_id = $product->prod_id;
								foreach(array('id', 'drug_id', 'dose', 'amount', 'drugname', 'amount_in') as $attr)
								{
									$item_model->{$attr} = $item_product->{$attr};
								}
								if(!$item_model->save()){
									// print_r($item_model->getErrors());
									$error++;
								} else {
									$new_items++;
								}
							}
						}
					}




				} else{

					if($model->price != $product->price){
						$model->price = $product->price;
						if(!$model->save(true, array('price'))){
							// print_r($model->getErrors());
							$error++;
						} else{
							$change++;
						}
					} else{
						$without_change++;
					}
				}




			}
		}
		
		

		if(Yii::app()->request->getParam('return')){
			Yii::app()->user->setFlash('success', "new: {$new}, new_items: {$new_items}, change: {$change}, without_change: {$without_change}");
			Yii::app()->user->setFlash('error', "error: {$error}");
			$this->redirect(array('/admin/prices'));
		}	

		$result = array(
			'new'=>$new,
			'new_items'=>$new_items,
			'change'=>$change,
			'without_change'=>$without_change,
			'error'=>$error,
			);

		echo json_encode($result);
		Yii::app()->end();
	}


	public function actionProductList($lang = null, $label = false, $format = 'array')
	{
		if($lang !== null && !in_array($lang, Languages::getArrayFlip())) {
			throw new CHttpException(404,'Page 1not found.');
		}
		if($label !== false && !in_array($label, array_flip(Products::getListProductsLabels()))){
			throw new CHttpException(404,'Page 2not found.');
		}
		// if($format != 'array' or $format != 'json'){
		// 	throw new CHttpException(404,'Page not found.');
		// }

		$data = Products::getList($lang, $label);

		switch ($format) 
		{
			case 'array':
				print_r($data);
				break;
			
			case 'json':
				echo json_encode($data);
				break;
			default:
				throw new CHttpException(404,'Page 3not found.');
				break;
		}
		
		Yii::app()->end();
	}

}