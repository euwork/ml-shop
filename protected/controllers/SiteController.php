<?php

class SiteController extends PageViewController
{


	public function actionIndex()
	{
		$slug = '_index' . '_' . Yii::app()->theme->name;
		
		$text = Pages::model()->findByAttributes(array('slug' => $slug));
		
		if($text===null) {
			$text = Pages::model()->findByAttributes(array('slug' => '_index'));
		
			if($text===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		

		$domain = $this->domain;

		$text->title = str_replace('{domain}', $domain, $text->title);
		$text->description = str_replace('{domain}', $domain, $text->description);
		$text->header = str_replace('{domain}', $domain, $text->header);
		$text->content = str_replace('{domain}', $domain, $text->content);
		
		$this->pageTitle = $text->title;
		$this->description = $text->description;
		$this->keywords = $text->keywords;


		$drugs = Drugs::atCategory();
		$reviews = Reviews::atHome();

		$this->render('index', array('drugs'=>$drugs, 'text' => $text, 'reviews'=>$reviews));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = '/clear';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	public function actionCounter()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404,'The requested product does not exist.');

		Yii::app()->traffCounter->countMe();
		echo json_encode(array(1));

		Yii::app()->end();
	}



}