-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 21 2021 г., 21:45
-- Версия сервера: 5.5.56-MariaDB
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_mlnewlang`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_affiliates`
--

CREATE TABLE `tbl_affiliates` (
  `aff_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `website` varchar(256) DEFAULT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_articles`
--

CREATE TABLE `tbl_articles` (
  `art_id` int(11) NOT NULL,
  `acat_id` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `title` varchar(256) NOT NULL,
  `postname` varchar(256) NOT NULL,
  `header` varchar(256) NOT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `content` text NOT NULL,
  `date_cr` datetime NOT NULL,
  `author` varchar(256) NOT NULL,
  `lang` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `cat_id` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `catname` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `header` varchar(256) NOT NULL,
  `headbaner` varchar(256) DEFAULT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `content` text,
  `position` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_categories`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_categories_articles`
--

CREATE TABLE `tbl_categories_articles` (
  `acat_id` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `name` varchar(256) NOT NULL,
  `lang` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_categories_lang`
--

CREATE TABLE `tbl_categories_lang` (
  `l_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_slug` varchar(128) NOT NULL,
  `l_catname` varchar(255) NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_header` varchar(255) NOT NULL,
  `l_keywords` varchar(256) DEFAULT NULL,
  `l_description` varchar(256) DEFAULT NULL,
  `l_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_categories_lang`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_config`
--

CREATE TABLE `tbl_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `param` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_config`
--

INSERT INTO `tbl_config` (`id`, `param`, `value`, `label`, `type`) VALUES
(1, 'theme', 'd2', 'Theme', 'select'),
(2, 'defaultLanguage', 'uk', 'Default Language', 'select'),
(3, 'textEditor', '1', 'Text Editor', 'select'),
(4, 'prefix.cat', 'category', 'Prefix category', 'string'),
(5, 'prefix.page', 'page', 'Prefix page', 'string'),
(6, 'prefix.drug', 'drug', 'Prefix drug', 'string'),
(7, 'prefix.article', 'articles', 'Prefix article', 'string'),
(8, 'prefix.testimonials', 'testimonials', 'Prefix testimonials', 'string'),
(9, 'reviews.count.home', '5', 'Reviews Count (Home)', 'string'),
(10, 'reviews.count.drug', '5', 'Reviews Count (Drug)', 'string'),
(11, 'home.show_banner', '1', 'Home show banner', 'checkbox'),
(12, 'billing.page', 'internal', 'Billing page', 'select'),
(13, 'reviews.count.testimonials', '5', 'Reviews per page (Testimonials)', 'string'),
(14, 'wire.empf', 'Olider', 'Wire Empfänger', 'string'),
(15, 'wire.country', 'PL', 'Wire Country', 'string'),
(16, 'wire.iban', 'PL78 1020 1068 0000 1202 0360 2315', 'Wire IBAN', 'string'),
(17, 'wire.bic', 'BPKOPLPW', 'Wire BIC', 'string'),
(18, 'price.increase', '0', 'Price increase (FR base)', 'select'),
(19, 'price.sort_order', '0', 'Price sort order', 'select'),
(20, 'price.increase_old', '5', 'Price increase_old', 'select'),
(21, 'cart.steps', '1', 'Cart steps', 'select'),
(23, 'external_counters', '', 'External counters', 'area'),
(24, 'cart.send_in_thankyou', '1', 'Send in cart/thankyou', 'checkbox'),
(25, 'wire.date_up', '2021-07-26 13:41:19', 'Last update', ''),
(26, 'currency_rates.date_up', '2021-07-16 13:38:41', 'currency_rates.date_up', ''),
(27, 'use_https', '0', 'Use HTTPS', 'checkbox'),
(28, 'wire.name', 'PKO', 'Bank Name', 'string'),
(29, 'wire.adress', 'Pulawska 15, 02515 Warszawa', 'Bank Adress', 'string'),
(30, 'use_reviews', '1', 'Reviews enabled', 'checkbox'),
(31, 'api_domain', 'ipillcash.com', 'Api domain', 'string'),
(32, 'api_key', '111111111111111', 'Api key', 'string'),
(33, 'affid', '48484', 'Shop Aff_id', 'string'),
(34, 'ipill_id', 'no_id', 'Your id in ipillcash.com (optional)', 'string'),
(35, 'wire.empf2', 'Olider2', 'Wire Empfänger 2', 'string'),
(36, 'wire.country2', 'PL', 'Wire Country 2', 'string'),
(37, 'wire.iban2', 'PL78 1020 1068', 'Wire IBAN 2', 'string'),
(38, 'wire.bic2', '', 'Wire BIC 2', 'string'),
(39, 'wire.name2', '', 'Bank Name 2', 'string'),
(40, 'wire.adress2', '', 'Bank Adress 2', 'string'),
(41, 'wire.usefor2', '', 'UseFor 2', 'string'),
(42, 'trackid_varname', 'trackid', 'Trackid variable name', 'string'),
(43, 'ship_active_regular_de', '1', 'Shipping regular DE active', 'string'),
(44, 'ship_active_express_de', '1', 'Shipping express DE active', 'string'),
(45, 'ship_price_regular_de', '0', 'Shipping regular DE price', 'string'),
(46, 'ship_price_express_de', '21.99', 'Shipping express DE price', 'string'),
(47, 'ship_active_regular', '1', 'Shipping regular active', 'string'),
(48, 'ship_active_express', '1', 'Shipping express active', 'string'),
(49, 'ship_active_normal', '1', 'Shipping normal active', 'string'),
(50, 'ship_price_regular', '11.99', 'Shipping regular price', 'string'),
(51, 'ship_price_express', '21.99', 'Shipping express price', 'string'),
(52, 'ship_price_normal', '9.50', 'Shipping normal price', 'string'),
(53, 'ship_price_regular_de_less60', '5.95', 'Shipping normal price', 'string');
-- --------------------------------------------------------

--
-- Структура таблицы `tbl_contacts`
--

CREATE TABLE `tbl_contacts` (
  `cont_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `thema` varchar(256) DEFAULT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_currencies`
--

CREATE TABLE `tbl_currencies` (
  `cur_id` char(10) NOT NULL,
  `description` varchar(256) NOT NULL,
  `symbol` char(10) NOT NULL,
  `position` tinyint(3) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `default` tinyint(1) DEFAULT '0',
  `rate` decimal(12,5) DEFAULT '1.00000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_currencies`
--

INSERT INTO `tbl_currencies` (`cur_id`, `description`, `symbol`, `position`, `active`, `default`, `rate`) VALUES
('ALL', 'Albania', 'L', 16, 1, 0, '122.49901'),
('AUD', 'Australia', 'A$', 5, 0, 0, '1.56300'),
('BAM', 'Bosnia and Herzegovina', 'КМ', 20, 1, 0, '1.95518'),
('BGN', 'Bulgaria', 'лв', 14, 1, 0, '1.95266'),
('BRL', 'Brazilia', 'R$', 7, 0, 0, '4.39704'),
('CAD', 'Canada', 'C$', 6, 0, 0, '1.53335'),
('CHF', 'Liechtenstein, Switzerland ', '₣', 4, 1, 0, '1.08485'),
('CZK', 'Czech', 'Kč', 10, 1, 0, '25.55261'),
('DKK', 'Denmark', 'kr', 11, 1, 0, '7.43843'),
('EUR', 'Europe', '€', 1, 1, 1, '1.00000'),
('GBP', 'Great Britain', '£', 2, 1, 0, '0.85410'),
('HRK', 'Croatia', 'Kn', 15, 1, 0, '7.49692'),
('HUF', 'Hungary', 'Ft', 17, 1, 0, '359.75132'),
('IDR', 'Indonesia', 'Rp', 25, 1, 0, '17072.68877'),
('ILS', 'Israel', '₪', 22, 1, 0, '3.87994'),
('JPY', 'Japan', '¥', 24, 1, 0, '129.96207'),
('KRW', 'South Korea', '₩', 27, 1, 0, '1347.15211'),
('MKD', 'Macedonia', 'DEN', 18, 1, 0, '61.59439'),
('MYR', 'Malaysia ', 'RM', 26, 1, 0, '4.96605'),
('NOK', 'Norway', 'kr', 12, 1, 0, '10.38939'),
('NZD', 'New Zealand', 'NZ$', 30, 1, 0, '1.68292'),
('PLN', 'Poland', 'zł', 9, 1, 0, '4.58564'),
('RON', 'Romania', 'L', 13, 1, 0, '4.92901'),
('RSD', 'Serbia', 'din', 19, 1, 0, '117.52068'),
('SEK', 'Sweden', 'kr', 8, 1, 0, '10.24091'),
('SGD', 'Singapore', 'S$', 23, 1, 0, '1.59932'),
('TRY', 'Turkey', '₺', 21, 1, 0, '10.06012'),
('TWD', 'Taiwan', 'NT$', 28, 1, 0, '33.00812'),
('USD', 'USA', '$', 3, 1, 0, '1.18203'),
('ZAR', 'South Africa', 'R', 29, 1, 0, '16.97641');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_drugs`
--

CREATE TABLE `tbl_drugs` (
  `drug_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `drugname` varchar(256) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `title` varchar(256) NOT NULL,
  `header` varchar(256) NOT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `content` text,
  `instruction` text,
  `rating` varchar(256) DEFAULT NULL,
  `position` tinyint(3) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `home` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `likes` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_drugs`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_drugs_images`
--

CREATE TABLE `tbl_drugs_images` (
  `img_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `img` varchar(256) NOT NULL,
  `theme` varchar(128) NOT NULL,
  `lang` varchar(32) NOT NULL,
  `type` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_drugs_images`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_drugs_lang`
--

CREATE TABLE `tbl_drugs_lang` (
  `l_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `l_slug` varchar(128) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_drugname` varchar(256) NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_header` varchar(255) NOT NULL,
  `l_keywords` varchar(256) DEFAULT NULL,
  `l_description` varchar(256) DEFAULT NULL,
  `l_content` text NOT NULL,
  `l_instruction` text NOT NULL,
  `l_rating` varchar(256) DEFAULT NULL,
  `l_likes` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_drugs_lang`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_drugs_testimonials`
--

CREATE TABLE `tbl_drugs_testimonials` (
  `drug_id` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `title` varchar(256) NOT NULL,
  `header` varchar(256) NOT NULL,
  `subheader` varchar(256) NOT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `drug_ancor` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_drugs_testimonials`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_drugs_testimonials_lang`
--

CREATE TABLE `tbl_drugs_testimonials_lang` (
  `l_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `l_slug` varchar(128) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_title` varchar(256) NOT NULL,
  `l_header` varchar(256) NOT NULL,
  `l_subheader` varchar(256) NOT NULL,
  `l_keywords` varchar(256) DEFAULT NULL,
  `l_description` varchar(256) DEFAULT NULL,
  `l_drug_ancor` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_drugs_testimonials_lang`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_languages`
--

CREATE TABLE `tbl_languages` (
  `lang_id` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `show` tinyint(2) NOT NULL DEFAULT '1',
  `imported` tinyint(2) NOT NULL DEFAULT '0',
  `show_generic` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `position` tinyint(3) NOT NULL DEFAULT '0',
  `wire` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `visa` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `master` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `amex` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `bc` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `bitcoin` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ideal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_languages`
--

INSERT INTO `tbl_languages` (`lang_id`, `name`, `country`, `show`, `show_generic`, `position`, `wire`, `visa`, `master`, `amex`, `bc`, `bitcoin`, `ideal`) VALUES
-- ('au', 'English', 'Australia', 0, 1, 4, 1, 1, 1, 1, 0, 1, 0),
('be', 'Belgique', 'Belgian', 1, 1, 4, 1, 1, 1, 1, 1, 1, 0),
('bg', 'Български', 'Bulgaria', 1, 1, 19, 1, 1, 1, 1, 0, 1, 0),
-- ('br', 'Português', 'Brazil', 0, 1, 12, 0, 1, 1, 1, 0, 1, 0),
-- ('ca', 'English', 'Canada (en)', 0, 1, 8, 1, 1, 1, 1, 0, 1, 0),
-- ('ca-fr', 'Français', 'Canada (fr)', 0, 1, 10, 1, 1, 1, 1, 0, 1, 0),
('cs', 'Čeština', 'Czech', 1, 1, 12, 1, 1, 1, 1, 0, 1, 0),
('cy', 'Κύπρος', 'Cyprus', 1, 1, 17, 1, 1, 1, 1, 0, 1, 0),
('da', 'Dansk', 'Denmark', 1, 1, 13, 1, 1, 1, 1, 0, 1, 0),
('de', 'Deutsch', 'Germany', 1, 1, 2, 1, 1, 1, 1, 0, 1, 0),
('el', 'Ελληνικά', 'Greece', 1, 1, 16, 1, 1, 1, 1, 0, 1, 0),
('es', 'Español', 'Spain', 1, 1, 5, 1, 1, 1, 1, 0, 1, 0),
('et', 'Eesti', 'Estonia', 1, 1, 25, 1, 1, 1, 1, 0, 1, 0),
('fi', 'Suomi', 'Finland', 1, 1, 14, 1, 1, 1, 1, 0, 1, 0),
('fr', 'Français', 'France', 1, 1, 3, 1, 1, 1, 1, 1, 1, 0),
('hr', 'Hrvatski', 'Croatia', 1, 1, 20, 1, 1, 1, 1, 0, 1, 0),
('hu', 'Magyar', 'Hungary', 1, 1, 26, 1, 1, 1, 1, 0, 1, 0),
('ie', 'Ireland', 'Ireland', 1, 1, 10, 1, 1, 1, 1, 0, 1, 0),
('it', 'Italiano', 'Italy', 1, 1, 6, 1, 1, 1, 1, 0, 1, 0),
('mk', 'Македонски', 'Macedonia', 1, 1, 27, 1, 1, 1, 1, 0, 1, 0),
('nl', 'Nederlands', 'Netherlands', 1, 1, 8, 1, 1, 1, 1, 0, 1, 1),
('no', 'Norsk', 'Norway', 1, 1, 15, 1, 1, 1, 1, 0, 1, 0),
('pl', 'Polski', 'Poland', 1, 1, 11, 1, 1, 1, 1, 0, 1, 0),
('pt', 'Português', 'Portugal', 1, 1, 7, 1, 1, 1, 1, 0, 1, 0),
('ro', 'Română', 'Romania', 1, 1, 18, 1, 1, 1, 1, 0, 1, 0),
('se', 'Svenska', 'Sweden', 1, 1, 9, 1, 1, 1, 1, 0, 1, 0),
('sg', 'Singapore', 'Singapore', 1, 1, 30, 1, 1, 1, 1, 0, 1, 0),
('sk', 'Slovenčina ', 'Slovakia', 1, 1, 22, 1, 1, 1, 1, 0, 1, 0),
('sl', 'Slovenščina', 'Slovenia', 1, 1, 21, 1, 1, 1, 1, 0, 1, 0),
('sq', 'Shqipja', 'Albania', 1, 1, 23, 1, 1, 1, 1, 0, 1, 0),
('sr', 'Srpski', 'Serbia', 1, 1, 24, 1, 1, 1, 1, 0, 1, 0),
('tr', 'Türkçe', 'Turkey', 1, 1, 28, 1, 1, 1, 1, 0, 1, 0),
-- ('us', 'English', 'USA', 0, 1, 2, 0, 1, 1, 1, 0, 1, 0),
('uk', 'English', 'United Kingdom', 1, 1, 1, 1, 1, 1, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_likes`
--

CREATE TABLE `tbl_likes` (
  `like_id` int(11) UNSIGNED NOT NULL,
  `drug_id` int(11) NOT NULL,
  `date_cr` datetime NOT NULL,
  `ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `order_id` int(11) UNSIGNED NOT NULL,
  `affid` int(5) UNSIGNED NOT NULL,
  `trackid` varchar(32) NOT NULL,
  `comission` decimal(5,2) NOT NULL,
  `date_cr` datetime NOT NULL,
  `language` char(5) NOT NULL,
  `email` varchar(256) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `shipp_fname` varchar(256) NOT NULL,
  `shipp_lname` varchar(256) NOT NULL,
  `shipp_address` varchar(256) NOT NULL,
  `shipp_city` varchar(128) NOT NULL,
  `shipp_zip` varchar(32) NOT NULL,
  `shipp_country` varchar(2) NOT NULL,
  `shipp_state` varchar(3) DEFAULT NULL,
  `bill_fname` varchar(256) NOT NULL,
  `bill_lname` varchar(256) NOT NULL,
  `bill_address` varchar(256) NOT NULL,
  `bill_city` varchar(128) NOT NULL,
  `bill_zip` varchar(32) NOT NULL,
  `bill_country` varchar(2) NOT NULL,
  `bill_state` varchar(3) DEFAULT NULL,
  `payment_method` varchar(10) NOT NULL,
  `card_number` varchar(16) DEFAULT NULL,
  `card_month` varchar(2) DEFAULT NULL,
  `card_year` varchar(4) DEFAULT NULL,
  `card_cvv` varchar(4) DEFAULT NULL,
  `card_date_birth` date DEFAULT NULL,
  `subtotal` decimal(10,2) UNSIGNED NOT NULL,
  `subtotal_usd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) UNSIGNED NOT NULL,
  `total_usd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `shipping_price` decimal(10,2) UNSIGNED NOT NULL,
  `shipping_price_usd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `shipping_method` tinyint(2) NOT NULL,
  `insurance` decimal(10,2) UNSIGNED NOT NULL,
  `insurance_usd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `http_ip` varchar(46) DEFAULT NULL,
  `http_country` varchar(2) DEFAULT NULL,
  `http_referer` varchar(256) DEFAULT NULL,
  `http_user_agent` varchar(256) DEFAULT NULL,
  `http_x_forwarded_for` varchar(256) DEFAULT NULL,
  `http_x_forwarded` varchar(256) DEFAULT NULL,
  `http_forwarded` varchar(256) DEFAULT NULL,
  `http_proxy_agent` varchar(256) DEFAULT NULL,
  `http_via` varchar(256) DEFAULT NULL,
  `http_proxy_connection` varchar(256) DEFAULT NULL,
  `http_data_info` varchar(2048) DEFAULT NULL,
  `js_time_string` varchar(256) DEFAULT NULL,
  `log_sockip` varchar(256) DEFAULT NULL,
  `log_colordepth` varchar(256) DEFAULT NULL,
  `log_browser_lang` varchar(256) DEFAULT NULL,
  `log_browser_reso` varchar(256) DEFAULT NULL,
  `log_machine_type` varchar(256) DEFAULT NULL,
  `log_useragent` varchar(256) DEFAULT NULL,
  `log_plugins` varchar(256) DEFAULT NULL,
  `log_fonts` varchar(256) DEFAULT NULL,
  `log_mtime` varchar(256) DEFAULT NULL,
  `log_os` varchar(256) DEFAULT NULL,
  `log_jsversion` varchar(256) DEFAULT NULL,
  `log_token` varchar(256) DEFAULT NULL,
  `log_beh` varchar(256) DEFAULT NULL,
  `log_tzinfo` varchar(256) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  `products_label` tinyint(3) NOT NULL DEFAULT '1',
  `reorder` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_orders`
--
-- -------------------------------------------------

--
-- Структура таблицы `tbl_order_products`
--

CREATE TABLE `tbl_order_products` (
  `order_id` int(11) UNSIGNED NOT NULL,
  `prod_id` int(11) UNSIGNED NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `price_usd` decimal(10,2) NOT NULL,
  `discount` decimal(5,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_order_products`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_order_statuses`
--

CREATE TABLE `tbl_order_statuses` (
  `stat_id` int(11) UNSIGNED NOT NULL,
  `order_id` int(11) UNSIGNED NOT NULL,
  `status` tinyint(2) NOT NULL,
  `date_cr` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_order_statuses`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pages`
--

CREATE TABLE `tbl_pages` (
  `page_id` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `header` varchar(256) DEFAULT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_pages`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pages_lang`
--

CREATE TABLE `tbl_pages_lang` (
  `l_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `l_slug` varchar(128) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_title` varchar(255) DEFAULT NULL,
  `l_header` varchar(255) DEFAULT NULL,
  `l_keywords` varchar(256) DEFAULT NULL,
  `l_description` varchar(256) DEFAULT NULL,
  `l_content` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_pages_lang`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `pay_id` int(11) UNSIGNED NOT NULL,
  `date_cr` datetime NOT NULL,
  `amount` decimal(10,2) UNSIGNED NOT NULL,
  `comment` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_products`
--

CREATE TABLE `tbl_products` (
  `prod_id` int(11) UNSIGNED NOT NULL,
  `drug_id` int(11) NOT NULL,
  `dose` varchar(128) NOT NULL,
  `amount` int(11) NOT NULL,
  `bonus` int(11) DEFAULT NULL,
  `amount_in` varchar(256) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `discount` decimal(5,2) DEFAULT '0.00',
  `a_status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `label` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Структура таблицы `tbl_profile`
--

CREATE TABLE `tbl_profile` (
  `id` int(11) UNSIGNED NOT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `balance_ext` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comission` decimal(5,2) NOT NULL,
  `payment_method` tinyint(1) NOT NULL DEFAULT '0',
  `webmoney` varchar(20) DEFAULT NULL,
  `epese` varchar(128) DEFAULT NULL,
  `ecoin` varchar(128) DEFAULT NULL,
  `skrill` varchar(128) DEFAULT NULL,
  `bitcoin` varchar(128) DEFAULT NULL,
  `capitalist` varchar(128) DEFAULT NULL,
  `paxum` varchar(128) DEFAULT NULL,
  `usdt` varchar(128) DEFAULT NULL,
  `bank_account_name` varchar(256) DEFAULT NULL,
  `bank_address` varchar(256) DEFAULT NULL,
  `swift_code` varchar(256) DEFAULT NULL,
  `bank_account_number` varchar(256) DEFAULT NULL,
  `bank_code` varchar(256) DEFAULT NULL,
  `correspondent_account` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_profile`
--

INSERT INTO `tbl_profile` (`id`, `balance`, `comission`, `payment_method`, `webmoney`, `epese`, `ecoin`, `skrill`, `bitcoin`, `capitalist`, `paxum`, `bank_account_name`, `bank_address`, `swift_code`, `bank_account_number`, `bank_code`, `correspondent_account`) VALUES
(1, '0.00', '40.00', 0, '', '', '', NULL, NULL, NULL, NULL, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews`
--

CREATE TABLE `tbl_reviews` (
  `rev_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `points` decimal(3,2) DEFAULT '0.00',
  `author` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `date_cr` datetime NOT NULL,
  `tit_name` varchar(256) DEFAULT NULL,
  `vote_yes` int(11) DEFAULT '0',
  `vote_all` int(11) DEFAULT '0',
  `lang` varchar(5) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `new` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_reviews`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_statistics`
--

CREATE TABLE `tbl_statistics` (
  `dat` date NOT NULL,
  `uniq` int(11) UNSIGNED DEFAULT '0',
  `hit` int(11) UNSIGNED DEFAULT '0',
  `bill` int(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_statistics`
--



-- --------------------------------------------------------

--
-- Структура таблицы `tbl_stat_countries`
--

CREATE TABLE `tbl_stat_countries` (
  `dat` date NOT NULL,
  `country` char(2) NOT NULL DEFAULT '',
  `uniq` int(11) UNSIGNED DEFAULT '0',
  `hit` int(11) UNSIGNED DEFAULT '0',
  `bill` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_stat_countries`
--




CREATE TABLE `tbl_stat_trackids` (
  `dat` date NOT NULL,
  `trackid` varchar(32) NOT NULL DEFAULT '',
  `uniq` int(11) UNSIGNED DEFAULT '0',
  `hit` int(11) UNSIGNED DEFAULT '0',
  `bill` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_stat_referers`
--

CREATE TABLE `tbl_stat_referers` (
  `dat` date NOT NULL,
  `referer` varchar(256) DEFAULT NULL,
  `referer_hash` char(32) NOT NULL DEFAULT '',
  `uniq` int(11) UNSIGNED DEFAULT '0',
  `hit` int(11) UNSIGNED DEFAULT '0',
  `bill` int(11) UNSIGNED DEFAULT '0',
  `ord` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_stat_referers`
--
-- ----------------------------------------------------

--
-- Структура таблицы `tbl_testpack_products`
--

CREATE TABLE `tbl_testpack_products` (
  `id` int(11) UNSIGNED NOT NULL,
  `prod_id` int(11) NOT NULL,
  `drugname` varchar(256) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `dose` varchar(128) NOT NULL,
  `amount` int(5) NOT NULL,
  `amount_in` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `tbl_traff_access`
--

CREATE TABLE `tbl_traff_access` (
  `access_id` int(11) UNSIGNED NOT NULL,
  `ses_id` int(11) UNSIGNED NOT NULL,
  `page_id` int(11) UNSIGNED NOT NULL,
  `date_cr` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_traff_access`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_traff_pages`
--

CREATE TABLE `tbl_traff_pages` (
  `page_id` int(11) UNSIGNED NOT NULL,
  `page_path` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_traff_pages`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_traff_sessions`
--

CREATE TABLE `tbl_traff_sessions` (
  `ses_id` int(11) UNSIGNED NOT NULL,
  `uuid` char(32) NOT NULL,
  `date_cr` datetime NOT NULL,
  `bill` tinyint(1) NOT NULL DEFAULT '0',
  `ord` tinyint(1) NOT NULL DEFAULT '0',
  `country` char(2) DEFAULT NULL,
  `referer` varchar(256) DEFAULT NULL,
  `trackid` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_traff_sessions`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_voted`
--

CREATE TABLE `tbl_voted` (
  `vot_id` int(11) UNSIGNED NOT NULL,
  `rev_id` int(11) NOT NULL,
  `date_cr` datetime NOT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `value` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_voted`
--


--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tbl_affiliates`
--
ALTER TABLE `tbl_affiliates`
  ADD PRIMARY KEY (`aff_id`);

--
-- Индексы таблицы `tbl_articles`
--
ALTER TABLE `tbl_articles`
  ADD PRIMARY KEY (`art_id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `acat_id` (`acat_id`);

--
-- Индексы таблицы `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `tbl_categories_articles`
--
ALTER TABLE `tbl_categories_articles`
  ADD PRIMARY KEY (`acat_id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `tbl_categories_lang`
--
ALTER TABLE `tbl_categories_lang`
  ADD PRIMARY KEY (`l_id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Индексы таблицы `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `param` (`param`);

--
-- Индексы таблицы `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  ADD PRIMARY KEY (`cont_id`);

--
-- Индексы таблицы `tbl_currencies`
--
ALTER TABLE `tbl_currencies`
  ADD PRIMARY KEY (`cur_id`);

--
-- Индексы таблицы `tbl_drugs`
--
ALTER TABLE `tbl_drugs`
  ADD PRIMARY KEY (`drug_id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Индексы таблицы `tbl_drugs_images`
--
ALTER TABLE `tbl_drugs_images`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `drug_id_lang` (`drug_id`,`lang`);

--
-- Индексы таблицы `tbl_drugs_lang`
--
ALTER TABLE `tbl_drugs_lang`
  ADD PRIMARY KEY (`l_id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Индексы таблицы `tbl_drugs_testimonials`
--
ALTER TABLE `tbl_drugs_testimonials`
  ADD PRIMARY KEY (`drug_id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `tbl_drugs_testimonials_lang`
--
ALTER TABLE `tbl_drugs_testimonials_lang`
  ADD PRIMARY KEY (`l_id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Индексы таблицы `tbl_languages`
--
ALTER TABLE `tbl_languages`
  ADD PRIMARY KEY (`lang_id`);

--
-- Индексы таблицы `tbl_likes`
--
ALTER TABLE `tbl_likes`
  ADD PRIMARY KEY (`like_id`),
  ADD KEY `drug_id` (`drug_id`);

--
-- Индексы таблицы `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  ADD PRIMARY KEY (`order_id`,`prod_id`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Индексы таблицы `tbl_order_statuses`
--
ALTER TABLE `tbl_order_statuses`
  ADD PRIMARY KEY (`stat_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `tbl_pages`
--
ALTER TABLE `tbl_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `tbl_pages_lang`
--
ALTER TABLE `tbl_pages_lang`
  ADD PRIMARY KEY (`l_id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Индексы таблицы `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`pay_id`);

--
-- Индексы таблицы `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`prod_id`),
  ADD KEY `drug_id` (`drug_id`);

--
-- Индексы таблицы `tbl_profile`
--
ALTER TABLE `tbl_profile`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
  ADD PRIMARY KEY (`rev_id`),
  ADD KEY `drug_id` (`drug_id`),
  ADD KEY `drug_id_lang_active` (`drug_id`,`lang`,`active`),
  ADD KEY `drug_id_lang_points_active` (`drug_id`,`lang`,`points`,`active`),
  ADD KEY `home_lang_active_date_cr` (`home`,`lang`,`active`,`date_cr`);

--
-- Индексы таблицы `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  ADD PRIMARY KEY (`dat`);

--
-- Индексы таблицы `tbl_stat_countries`
--
ALTER TABLE `tbl_stat_countries`
  ADD PRIMARY KEY (`dat`,`country`);

--
-- Индексы таблицы `tbl_stat_trackids`
--
ALTER TABLE `tbl_stat_trackids`
  ADD PRIMARY KEY (`dat`,`trackid`);

--
-- Индексы таблицы `tbl_stat_referers`
--
ALTER TABLE `tbl_stat_referers`
  ADD PRIMARY KEY (`dat`,`referer_hash`);

--
-- Индексы таблицы `tbl_testpack_products`
--
ALTER TABLE `tbl_testpack_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Индексы таблицы `tbl_traff_access`
--
ALTER TABLE `tbl_traff_access`
  ADD PRIMARY KEY (`access_id`),
  ADD KEY `ses_id` (`ses_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Индексы таблицы `tbl_traff_pages`
--
ALTER TABLE `tbl_traff_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `page_path` (`page_path`(40));

--
-- Индексы таблицы `tbl_traff_sessions`
--
ALTER TABLE `tbl_traff_sessions`
  ADD PRIMARY KEY (`ses_id`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD KEY `referer` (`referer`(16)),
  ADD KEY `country` (`country`);

--
-- Индексы таблицы `tbl_voted`
--
ALTER TABLE `tbl_voted`
  ADD PRIMARY KEY (`vot_id`),
  ADD KEY `rev_id` (`rev_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tbl_affiliates`
--
ALTER TABLE `tbl_affiliates`
  MODIFY `aff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_articles`
--
ALTER TABLE `tbl_articles`
  MODIFY `art_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tbl_categories_articles`
--
ALTER TABLE `tbl_categories_articles`
  MODIFY `acat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_categories_lang`
--
ALTER TABLE `tbl_categories_lang`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT для таблицы `tbl_config`
--
ALTER TABLE `tbl_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  MODIFY `cont_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_drugs`
--
ALTER TABLE `tbl_drugs`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `tbl_drugs_images`
--
ALTER TABLE `tbl_drugs_images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3312;

--
-- AUTO_INCREMENT для таблицы `tbl_drugs_lang`
--
ALTER TABLE `tbl_drugs_lang`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1217;

--
-- AUTO_INCREMENT для таблицы `tbl_drugs_testimonials`
--
ALTER TABLE `tbl_drugs_testimonials`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `tbl_drugs_testimonials_lang`
--
ALTER TABLE `tbl_drugs_testimonials_lang`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1251;

--
-- AUTO_INCREMENT для таблицы `tbl_likes`
--
ALTER TABLE `tbl_likes`
  MODIFY `like_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `order_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325729;

--
-- AUTO_INCREMENT для таблицы `tbl_order_statuses`
--
ALTER TABLE `tbl_order_statuses`
  MODIFY `stat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=319;

--
-- AUTO_INCREMENT для таблицы `tbl_pages`
--
ALTER TABLE `tbl_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `tbl_pages_lang`
--
ALTER TABLE `tbl_pages_lang`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=620;

--
-- AUTO_INCREMENT для таблицы `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `pay_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `prod_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;

--
-- AUTO_INCREMENT для таблицы `tbl_profile`
--
ALTER TABLE `tbl_profile`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
  MODIFY `rev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14896;

--
-- AUTO_INCREMENT для таблицы `tbl_testpack_products`
--
ALTER TABLE `tbl_testpack_products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT для таблицы `tbl_traff_access`
--
ALTER TABLE `tbl_traff_access`
  MODIFY `access_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15269;

--
-- AUTO_INCREMENT для таблицы `tbl_traff_pages`
--
ALTER TABLE `tbl_traff_pages`
  MODIFY `page_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1658;

--
-- AUTO_INCREMENT для таблицы `tbl_traff_sessions`
--
ALTER TABLE `tbl_traff_sessions`
  MODIFY `ses_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2930;

--
-- AUTO_INCREMENT для таблицы `tbl_voted`
--
ALTER TABLE `tbl_voted`
  MODIFY `vot_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tbl_categories_lang`
--
ALTER TABLE `tbl_categories_lang`
  ADD CONSTRAINT `tbl_categories_lang_owner` FOREIGN KEY (`owner_id`) REFERENCES `tbl_categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_drugs`
--
ALTER TABLE `tbl_drugs`
  ADD CONSTRAINT `tbl_drugs_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `tbl_categories` (`cat_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_drugs_lang`
--
ALTER TABLE `tbl_drugs_lang`
  ADD CONSTRAINT `tbl_drugs_lang_owner` FOREIGN KEY (`owner_id`) REFERENCES `tbl_drugs` (`drug_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_drugs_testimonials`
--
ALTER TABLE `tbl_drugs_testimonials`
  ADD CONSTRAINT `tbl_drugs_testimonials_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `tbl_drugs` (`drug_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_drugs_testimonials_lang`
--
ALTER TABLE `tbl_drugs_testimonials_lang`
  ADD CONSTRAINT `tbl_drugs_testimonials_lang_owner` FOREIGN KEY (`owner_id`) REFERENCES `tbl_drugs_testimonials` (`drug_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  ADD CONSTRAINT `tbl_order_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_orders` (`order_id`),
  ADD CONSTRAINT `tbl_order_products_ibfk_2` FOREIGN KEY (`prod_id`) REFERENCES `tbl_products` (`prod_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_order_statuses`
--
ALTER TABLE `tbl_order_statuses`
  ADD CONSTRAINT `tbl_order_statuses_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_orders` (`order_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_pages_lang`
--
ALTER TABLE `tbl_pages_lang`
  ADD CONSTRAINT `tbl_pages_lang_owner` FOREIGN KEY (`owner_id`) REFERENCES `tbl_pages` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_products`
--
-- ALTER TABLE `tbl_products`
--   ADD CONSTRAINT `tbl_products_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `tbl_drugs` (`drug_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
  ADD CONSTRAINT `tbl_reviews_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `tbl_drugs_testimonials` (`drug_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_traff_access`
--
ALTER TABLE `tbl_traff_access`
  ADD CONSTRAINT `tbl_traff_access_ibfk_1` FOREIGN KEY (`ses_id`) REFERENCES `tbl_traff_sessions` (`ses_id`),
  ADD CONSTRAINT `tbl_traff_access_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `tbl_traff_pages` (`page_id`);

--
-- Ограничения внешнего ключа таблицы `tbl_voted`
--
ALTER TABLE `tbl_voted`
  ADD CONSTRAINT `tbl_voted_ibfk_1` FOREIGN KEY (`rev_id`) REFERENCES `tbl_reviews` (`rev_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
