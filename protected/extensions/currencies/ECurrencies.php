<?php


class ECurrencies
{
	public $get_var_name = 'curr';
	public $ses_var_name = 'curr';
	public $currency;
	public $default;
	public $currency_list;

	public function init()
	{
		$this->currency = Yii::app()->request->getQuery($this->get_var_name);
		$this->default = Currencies::getDefault();
		$this->currency_list = Currencies::getArray();
	}

	public function check()
	{
		// если текущее значение в сессии неактивно в базе - ставим дефолтное значение
		if(!Currencies::isActive($this->getValue())){
			$this->setActive($this->default);
		}

		// если в гет параметрах нет переменной с валютой - ниче не делаем
		if($this->currency === null){
			return true;
		}

		
		// меняем текущее значение
		$this->setActive($this->currency);

		Yii::app()->request->redirect($this->clearUrl());

	}

	// устанавливаем новое значение
	public function setActive($value)
	{
		//  валидация
		if(array_key_exists($value, $this->currency_list)){
			$this->save($value);
		}
	}

	// пишем в сессию
	private function save($value)
	{
		Yii::app()->user->setState($this->ses_var_name, $value);
	}

	// получаем текущее значение
	public function getValue()
	{
		return Yii::app()->user->getState($this->ses_var_name, $this->default);
	}

	// url без переменной с валютой
	private function clearUrl()
	{
		$url = Yii::app()->request->getOriginalUrl();
		
		$path = parse_url($url, PHP_URL_PATH);
		$params = explode('&', parse_url($url, PHP_URL_QUERY));
		$new_params = array();
		foreach($params as $item)
		{
			if($item == $this->get_var_name . '=' . $this->currency){
				continue;
			}
			$new_params[] = $item;
		}

		$redirect_url = $path;
		if(count($new_params) > 0){
			$redirect_url .= '?' . implode('&', $new_params);
		}
		return $redirect_url;		
	}

	// public $rates_url = 'https://api.heretrustpolicy.com/receive/?get=currency_rates&client_id=2131&campaign_id=2311';
	
	// public function updateRates()
	// {
	// 	$page = Yii::app()->curl->get(Yii::app()->extbilling->getRatesUrl());
	// 	$answer = json_decode($page);
	// 	if(!isset($answer->currency_rates)){
	// 		return false;
	// 	}

	// 	$success = array();
	// 	$error = array();

	// 	$rates = $answer->currency_rates;

	// 	if(!isset($rates->{'EUR'})) {return false;}

	// 	$rate_default = $rates->{'EUR'};
	// 	foreach(Currencies::getList() as $item)
	// 	{
	// 		if($item->default == 1) continue;

	// 		if(isset($rates->{$item->cur_id})){

	// 			$rate = round($rates->{$item->cur_id} / $rate_default, 5);
	// 			if(Currencies::updateRate($item->cur_id, $rate)){
	// 				$success[] = $item->cur_id . ' +';
	// 			} else {
	// 				$error[] = $item->cur_id . ' error';
	// 			}

	// 		}
	// 	}


	// 	return array('success'=>$success, 'error'=>$error);

	// }

	public function updateRates()
	{
		$rates = Request::getCurencyRates();
		// echo $page;Yii::app()->end();
		// var_dump($rates);
		if(count((array)$rates) == 0){
			return false;
		}

		$success = array();
		$error = array();



		foreach(Currencies::getList() as $item)
		{
			if($item->default == 1) continue;

			if(isset($rates->{$item->cur_id})){

				$rate = round($rates->{$item->cur_id}, 5);
				if(Currencies::updateRate($item->cur_id, $rate)){
					$success[] = $item->cur_id . ' +';
				} else {
					$error[] = $item->cur_id . ' error';
				}

			}
		}
		Yii::app()->config->set('currency_rates.date_up', date('Y-m-d H:i:s'));
		Yii::app()->cache->flush();


		return array('success'=>$success, 'error'=>$error);

	}	
}