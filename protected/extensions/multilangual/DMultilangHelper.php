<?php


/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 */
class DMultilangHelper
{
    public static function enabled()
    {
        return Languages::countEnabled() > 0;
    }
 
    public static function langList()
    {
        $list = array();
        $enabled = self::enabled();
 
        foreach (Languages::getList() as $item)
        {
            if ($item->lang_id === Yii::app()->config->get('defaultLanguage')) {
                $list[$item->lang_id] = $enabled ? $item->name : '';
            } else {
                $list[$item->lang_id] = $item->name;
            }
        }
 
        return $list;
    }

    public static function langHash()
    {
        $list = array();
        $enabled = self::enabled();
 
        foreach (Languages::getList() as $item)
        {
            if ($item->lang_id === Yii::app()->config->get('defaultLanguage')) {
                $name = $enabled ? $item->name : '';
            } else {
                $name = $item->name;
            }

            $list[$item->lang_id] = array('name'=>$name, 'country'=>$item->country);
        }
 
        return $list;
    }
 
    public static function processLangInUrl($url)
    {
        if (self::enabled())
        {
            $domains = explode('/', ltrim($url, '/'));

            $isLangExists = in_array($domains[0], array_keys(Languages::getArray()));
            $isDefaultLang = $domains[0] == Yii::app()->config->get('defaultLanguage');

            // Yii::app()->language должно быть равно  defaultLanguage
            Yii::app()->setLanguage(Yii::app()->config->get('defaultLanguage'));
 
            if ($isLangExists && !$isDefaultLang)
            {
                $lang = array_shift($domains);
                Yii::app()->setLanguage($lang);
            }
 
            $url = '/' . implode('/', $domains);
        }
 
        return $url;
    }
 
    public static function addLangToUrl($url)
    {
        if (self::enabled())
        {
            $domains = explode('/', ltrim($url, '/'));
            $isHasLang = in_array($domains[0], array_keys(Languages::getArray()));
            $isDefaultLang = Yii::app()->getLanguage() == Yii::app()->config->get('defaultLanguage');
 
            if ($isHasLang && $isDefaultLang)
                array_shift($domains);
 
            if (!$isHasLang && !$isDefaultLang)
                array_unshift($domains, Yii::app()->getLanguage());
 
            $url = '/' . implode('/', $domains);
        }
 
        return $url;
    }



    public static function suffixList()
    {
        $list = array();
        $enabled = self::enabled();
        foreach (Languages::getList() as $item)
        {
            if ($item->lang_id === Yii::app()->config->get('defaultLanguage')) {
                // $suffix = '';
                $suffix = '_' . $item->lang_id;
                $name = $enabled ? $item->name : '';
            } else {
                $suffix = '_' . $item->lang_id;
                $name = $item->name;
            }
            $list[$item->lang_id] = array('suffix'=>$suffix, 'name'=>$name, 'country'=>$item->country);
        }
        return $list;
    }
}