<?php


class ESypexGeo {

	public $sxgeo;

	public function init(){
		$this->sxgeo = $this->_getSxgeo();
	}
	public function getCountry($ip)
	{
		return $this->sxgeo->get($ip);
	}

	private function _getSxgeo()
	{
	   
	    $sxgeo = new SxGeo(__DIR__ .'/data/SxGeo.dat');

	    
	    return $sxgeo;
	}
}