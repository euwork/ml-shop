<?php



class ETraffCounter
{

	public  $cookie_uuid = '_etraff';
	public  $cookie_test = '_iamman';
	public  $cookie_trackid = '_trackid';
	public  $uuid = false;
	public  $trackid = null;
	public  $type; // first|uniq|hit
	private $_ses_id;
	# ставим куку i am man
	# сохраняем реферер в сессион
	#  елси есть ajax запрос - пишем в бд, ставим куку с ууид
	public function init()
	{

		if(Yii::app()->request->cookies[$this->cookie_uuid]){
			$uuid   = Yii::app()->request->cookies[$this->cookie_uuid]->value;
			$ses_id = $this->getSesIdByUuid($uuid);
			if($ses_id){
				$this->type   = 'hit';
				$this->_ses_id = $ses_id;
				$this->uuid   = $uuid;
			} else { // uuid нет в бд
				$this->type = 'first';
			}
		}

		if(!Yii::app()->request->isAjaxRequest){
			if(!Yii::app()->request->cookies[$this->cookie_uuid]){
				$this->type   = 'first'; // первое обращение к сайту
			} 
		}

		if(!Yii::app()->request->isAjaxRequest && $this->type == 'first'){
			$this->putTestCookie();
			Yii::app()->session['referer'] = Yii::app()->request->urlReferrer;
		}

		


		if(Yii::app()->request->isAjaxRequest){
			if(Yii::app()->request->cookies[$this->cookie_test] && 
				(!Yii::app()->request->cookies[$this->cookie_uuid] or !$ses_id) ) { // есть тест кука - браузер принимает куки
				$this->type       = 'uniq';
				$this->uuid       = self::genUuid();
			}

			if(!Yii::app()->request->cookies[$this->cookie_test]){
				$this->type = 'first';
			}
		}

		// trackid
		if(!Yii::app()->request->isAjaxRequest){
			$trackid_varname = Yii::app()->config->get('trackid_varname');
			$this->trackid = Yii::app()->request->getParam($trackid_varname, null);

			if( $this->trackid !== null) {
				$this->trackid = str_replace('@', '', $this->trackid);
				$this->trackid = preg_replace('@[^a-z0-9\.\-]+@i', '', $this->trackid);
				$this->trackid = substr($this->trackid, 0, 32);
				$this->putTrackidCookie();
			}
		}

		if(Yii::app()->request->cookies[$this->cookie_trackid]){
			$this->trackid   = Yii::app()->request->cookies[$this->cookie_trackid]->value;
		}

		if(Yii::app()->request->isAjaxRequest){

			
			if($this->type == 'hit' && Yii::app()->request->cookies[$this->cookie_trackid]){
				$_trackid = $this->getTrackIdByUuid($this->uuid);

				if($this->trackid != $_trackid){
					$this->type = 'uniq';
					$this->uuid = self::genUuid();
				}
			}

		}
		

	}

	public function countMe()
	{
		
		if(!Yii::app()->request->isAjaxRequest){
			return false;
		}

		if($this->type == 'first'){
			return false;
		}

		// if(!Yii::app()->user->isGuest){
		// 	return false;
		// }
		
		if ($this->saveToDb()){

			if($this->type == 'uniq'){	 
				 $this->putCookie();
			} 
		}

		
		return true;
		
	}

	public function goal($goal)
	{
		// if(!in_array($goal, array('bill', 'ord'))) 
		// 	return false;
		if($this->type != 'hit')
			return false;

		if($goal == 'bill'){
			$sql = 'UPDATE {{traff_sessions}} SET bill = 1 WHERE ses_id = :ses_id';
		} elseif($goal == 'ord'){
			$sql = 'UPDATE {{traff_sessions}} SET ord = 1 WHERE ses_id = :ses_id';
		} else {
			return false;
		}
			
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":ses_id", $this->_ses_id, PDO::PARAM_INT);
		$row_count = $command->execute();
		if($row_count > 0 ) 
			return true;
		return false;
	}

	private static function genUuid()
	{
        mt_srand((double)microtime()*10000);
        $charid = md5(uniqid(rand(), true));
        $hyphen = "";//chr(45);// "-"
        $uuid = "" // chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        return $uuid;
	} 


	private function saveToDb()
	{

			 # пишем уника в бд
			 $connection=Yii::app()->db;
			 $transaction=$connection->beginTransaction();
			 try
			 {
			 	$date_cr = date('Y-m-d H:i:s');
	 			if($this->type == 'uniq'){
	
					 // $ip = ip2long(CHttpRequest::getUserHostAddress());
					 // $ua = CHttpRequest::getUserAgent();
					 $country = Yii::app()->sypexGeo->getCountry(Yii::app()->request->getUserHostAddress());
					 $referer = Yii::app()->session['referer'];
					 $trackid = $this->trackid;
				
					 // -- $sql = 'INSERT INTO {{traff_sessions}} (uuid, ip, ua, country, referer, date_cr) VALUES(:uuid, :ip, :ua, :country, :referer, :date_cr)';
					 $sql = 'INSERT INTO {{traff_sessions}} (uuid, country, referer, date_cr, trackid) VALUES(:uuid, :country, :referer, :date_cr, :trackid)';
					 $command=$connection->createCommand($sql);
					 $command->bindParam(":uuid", $this->uuid, PDO::PARAM_STR);
					 // $command->bindParam(":ip", $ip, PDO::PARAM_INT);
					 // $command->bindParam(":ua", $ua, PDO::PARAM_STR);
					 $command->bindParam(":country", $country, PDO::PARAM_STR);
					 $command->bindParam(":referer", $referer, PDO::PARAM_STR);
					 $command->bindParam(":trackid", $trackid, PDO::PARAM_STR);
					 $command->bindParam(":date_cr", $date_cr, PDO::PARAM_STR);
				
					 $row_count = $command->execute();
					 if($row_count > 0){
						 $ses_id = $connection->getLastInsertID();
					 }
				 } else {
				 	 $ses_id = $this->_ses_id;
				 }
				 if(!$ses_id ){ 
				 	throw new Exception('!ses_id');
				 }
				 $page_path = $this->getPath();

				 $sql = 'SELECT page_id FROM {{traff_pages}} WHERE page_path = :page_path';
			 	 $command=$connection->createCommand($sql);
				 $command->bindParam(":page_path", $page_path, PDO::PARAM_STR);
				 $page_id = $command->queryScalar();

				 
				 if(!$page_id){
					 $sql = 'INSERT IGNORE INTO {{traff_pages}} SET page_path = :page_path';
					 $command=$connection->createCommand($sql);
					 $command->bindParam(":page_path", $page_path, PDO::PARAM_STR);
					 $row_count = $command->execute();
					 $page_id = $connection->getLastInsertID();
				 }
				 
				 if(!$page_id ){ 
				 	throw new Exception('!page_id');
				 }

				 $sql = 'INSERT INTO {{traff_access}} SET page_id = :page_id, ses_id = :ses_id, date_cr = :date_cr';
				 $command=$connection->createCommand($sql);
				 $command->bindParam(":page_id", $page_id, PDO::PARAM_INT);
				 $command->bindParam(":ses_id", $ses_id, PDO::PARAM_INT);
				 $command->bindParam(":date_cr", $date_cr, PDO::PARAM_STR);
				 $row_count = $command->execute();
				 if($row_count == 0) {
					throw new Exception('!insert into traff_access');
				 }
				 //if($access_id) return true;
				 $transaction->commit();
				 return true;
			 }
			 catch(Exception $e) // в случае ошибки при выполнении запроса выбрасывается исключение
			 {
			     $transaction->rollBack();
			     echo $e->getMessage();
			     return false;
			 }
	}

	private function putCookie()
	{
		$cookie = new CHttpCookie($this->cookie_uuid, $this->uuid );
		$cookie->expire = time()+3600*24;
		Yii::app()->request->cookies[$this->cookie_uuid] = $cookie;
		return true;
	}

	private function putTestCookie()
	{
		$cookie = new CHttpCookie($this->cookie_test, 1 );
		$cookie->expire = time()+3600*24;
		Yii::app()->request->cookies[$this->cookie_test] = $cookie;
		return true;
	}

	private function putTrackidCookie()
	{
		$cookie = new CHttpCookie($this->cookie_trackid, $this->trackid );
		$cookie->expire = time()+3600*24*30;
		Yii::app()->request->cookies[$this->cookie_trackid] = $cookie;
		return true;
	}

	private function getSesIdByUuid($uuid)
	{
		$sql = 'SELECT ses_id FROM {{traff_sessions}} WHERE uuid = :uuid';
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":uuid", $uuid, PDO::PARAM_STR);
		return $command->queryScalar();
	}
	private function getTrackIdByUuid($uuid)
	{
		$sql = 'SELECT trackid FROM {{traff_sessions}} WHERE uuid = :uuid';
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":uuid", $uuid, PDO::PARAM_STR);
		return $command->queryScalar();
	}
	private function getPath()
	{
		// $referer = CHttpRequest::getUrlReferrer();
		return urldecode(Yii::app()->request->getParam('p', ''));
		// if(strlen($referer) > 0){
		// 	$data = parse_url(CHttpRequest::getUrlReferrer());
		// 	return $data['path'];
		// }
		// return '';
	}

	public function getReferer()
	{
		if($this->type == 'uniq'){
			return CHttpRequest::getUrlReferrer();
		} else {
			# запрос к бд
			$sql = 'SELECT referer FROM {{traff_sessions}} WHERE ses_id = :ses_id';
			$connection=Yii::app()->db;
		 	$command=$connection->createCommand($sql);
		    $command->bindParam(":ses_id", $this->_ses_id, PDO::PARAM_INT);
			$referer = $command->queryScalar();
			return $referer ? $referer : '';
		}	
	}


	public function getTrackid()
	{
		if(Yii::app()->request->cookies[$this->cookie_trackid]){
			return Yii::app()->request->cookies[$this->cookie_trackid]->value;
		}

		return false;
	}



}