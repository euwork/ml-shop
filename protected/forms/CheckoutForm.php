<?php

class CheckoutForm extends CFormModel
{
	public $step ;
	public $email ;
	// public $email2 ;
	public $gender;
	public $shipp_lname;
	public $shipp_fname;
	public $shipp_address;
	public $shipp_zip;
	public $shipp_city;
	public $shipp_country;
	public $shipp_state;
	public $phone;
	public $phone2;

	public $bill_lname;
	public $bill_fname;
	public $bill_address;
	public $bill_zip;
	public $bill_city;
	public $bill_country;
	public $bill_state;

	//step2
	public $shipping_method;
	public $payment_method;
	public $card_number;
	public $card_month;
	public $card_year;
	public $card_cvv;
	public $agreement;
	public $insurance;

	public $js_time_string;

	//blue_card
	public $bc_day;
	public $bc_month;
	public $bc_year;

	// правка индусов 1
	public $log_sockip;
	public $log_browser_lang;
	public $log_browser_reso;
	public $log_colordepth;
	public $log_machine_type;

	// правка индусов 2
	public $log_useragent;
	public $log_plugins;
	public $log_fonts;
	public $log_mtime;
	public $log_os;
	public $log_jsversion;

	public $focusStatus = 0 ;
	public $pasteStatus_billing_address = 0 ;
	public $pasteStatus_billing_zip = 0 ;
	public $pasteStatus_billing_city = 0 ;
	public $pasteStatus_billing_phone = 0 ;
	public $pasteStatus_card_number = 0 ;
	public $pasteStatus_cc_cvv2 = 0 ;
	public $deleteStatus_card_number = 0 ;
	public $deleteStatus_cc_cvv2 = 0 ;
	public $backspaceStatus_card_number = 0 ;
	public $backspaceStatus_cc_cvv2 = 0 ;
	public $tzinfo;
	

	public function attributeLabels()
	{
		return array(
			'email' => Yii::t('checkout', 'E-Mail'),
			// 'email2' => Yii::t('checkout', 'E-Mail bestätigen'),
			'gender' => '',
			'shipp_fname' => Yii::t('checkout', 'Vorname'),
			'shipp_lname' => Yii::t('checkout', 'Nachname'),
			'shipp_address' => Yii::t('checkout', 'Strasse / Hausnr'),
			'shipp_zip' => Yii::t('checkout', 'Postleitzahl'),
			'shipp_city' => Yii::t('checkout', 'Ort'),
			'shipp_country' => Yii::t('checkout', 'Land'),
			'shipp_state' => Yii::t('checkout', 'State'),
			'phone' => Yii::t('checkout', 'Telefonnummer'),
			'phone2' => Yii::t('checkout', 'Mobilrufnummer'),
			'bill_fname' => Yii::t('checkout', 'Vorname'),
			'bill_lname' => Yii::t('checkout', 'Nachname'),
			'bill_address' => Yii::t('checkout', 'Strasse / Hausnr'),
			'bill_zip' => Yii::t('checkout', 'Postleitzahl'),
			'bill_city' => Yii::t('checkout', 'Ort'),
			'bill_country' => Yii::t('checkout', 'Land'),
			'bill_state' => Yii::t('checkout', 'State'),
		);
	}


	public function rules() {
		return array(
			array('step, email, shipp_fname, shipp_lname, shipp_address, shipp_zip, shipp_city, shipp_country, phone, bill_fname, bill_lname, bill_address, bill_zip, bill_city, bill_country', 'required', 'on' => array('step1', 'step2', )),
			// array('email2', 'required', 'on' => 'step1'),
			array('shipping_method, payment_method, insurance', 'required', 'on' => 'step2'),
			array('agreement', 'required', 'requiredValue' => 1, 'on' => 'step2'),
			array('insurance', 'boolean'),
			array('email', 'email'),
			// array('shipp_zip, bill_zip', 'match', 'pattern'=>'/^[\d]{3,7}$/'),
			array('card_number, card_cvv', 'numerical', 'integerOnly'=>true),
			array('bc_day, bc_month, bc_year', 'numerical', 'integerOnly'=>true),
			array('shipp_zip, bill_zip', 'length', 'max'=>32),
			array('phone, phone2', 'length', 'max'=>32, 'min'=>7),
			array('shipp_city, bill_city', 'length', 'max'=>128),
			array('card_month', 'length', 'max'=>2),
			array('card_number', 'length', 'max'=>16),
			array('card_number', 'match', 'pattern'=>'/^[\d]+$/'),
			array('card_year, card_cvv', 'length', 'max'=>4),
			array('email, shipp_fname, shipp_lname, shipp_address, bill_fname, bill_lname, bill_address', 'length', 'max'=>256),
			array('gender, step', 'in', 'range'=> array(1,2), 'strict'=>false),
			array('payment_method', 'in', 'range'=>array_flip(Orders::getCurrentListPaymentMethodInternal())),
			array('shipping_method', 'in', 'range'=>Orders::getListShippingMethodNames()),
			array('card_month', 'in', 'range'=>self::getMonthList()),
			//array('shipp_country, bill_country', 'in', 'range' => array_flip(self::getCountryList())),
			array('shipp_country, bill_country', 'in', 'range' => array_flip(Country::getCountryList())),
			array('shipp_country, bill_country', 'length', 'max'=>2),
			array('shipp_state, bill_state', 'in', 'range' => array_flip(Country::getStateList())),
			array('shipp_state, bill_state', 'length', 'max'=>3),
			array('card_year', 'in', 'range'=>self::getYearList()),
			array('phone, phone2', 'match', 'pattern'=>'/^[\-\+\(\)\s\d]+$/'),
			array('shipp_fname, shipp_lname, shipp_address, shipp_city, bill_fname, bill_lname, bill_address, bill_city','filter', 'filter'=>'strip_tags'),
			array('shipp_fname, shipp_lname, shipp_address, shipp_city, bill_fname, bill_lname, bill_address, bill_city','filter', 'filter'=>'trim'),
			
			array('log_sockip, log_browser_lang, log_browser_reso, log_colordepth, log_machine_type', 'filter', 'filter'=>array('Filter', 'max256')),
			array('log_useragent, log_plugins, log_fonts, log_mtime, log_os, log_jsversion', 'filter', 'filter'=>array('Filter', 'max256')),
			array('focusStatus, pasteStatus_billing_address, pasteStatus_billing_zip, pasteStatus_billing_city, pasteStatus_billing_phone', 'filter', 'filter'=>array('Filter', 'max256')),
			array('pasteStatus_card_number, pasteStatus_cc_cvv2, deleteStatus_card_number, deleteStatus_cc_cvv2, backspaceStatus_card_number, backspaceStatus_cc_cvv2, tzinfo', 'filter', 'filter'=>array('Filter', 'max256')),



			) ;
	}

	public static function getCountryList()
	{
		$country = Yii::app()->language;
		
		if(in_array($country, array('uk', 'us', 'au', 'ca', 'ie', 'sg'))){
			$_c = Country::getCountryList();
		} else {

			$_c = array(
			"AF"=> Yii::t('countries', "Afghanistan"),
			"AL"=> Yii::t('countries', "Albanien"),
			"AD"=> Yii::t('countries', "Andorra"),
			"AR"=> Yii::t('countries', "Argentinien"),
			"AW"=> Yii::t('countries', "Aruba"),
			"AU"=> Yii::t('countries', "Australien"),
			"AT"=> Yii::t('countries', "Österreich"),
			"BH"=> Yii::t('countries', "Bahrain"),
			"BD"=> Yii::t('countries', "Bangladesch"),
			"BB"=> Yii::t('countries', "Barbados"),
			"BY"=> Yii::t('countries', "Weißrussland"),
			"BE"=> Yii::t('countries', "Belgien"),
			"BA"=> Yii::t('countries', "Bosnien und Herzegowina"),
			"BW"=> Yii::t('countries', "Botswana"),
			"BR"=> Yii::t('countries', "Brasilien"),
			"BN"=> Yii::t('countries', "Brunei"),
			"BG"=> Yii::t('countries', "Bulgarien"),
			"BF"=> Yii::t('countries', "Burkina Faso"),
			"CM"=> Yii::t('countries', "Kamerun"),
			"CA"=> Yii::t('countries', "Kanada"),
			"CL"=> Yii::t('countries', "Chile"),
			"CR"=> Yii::t('countries', "Costa Rica"),
			"HR"=> Yii::t('countries', "Kroatien"),
			"CW"=> Yii::t('countries', "Curacao"),
			"CY"=> Yii::t('countries', "Zypern"),
			"CZ"=> Yii::t('countries', "Tschechische Republik"),
			"DK"=> Yii::t('countries', "Dänemark"),
			"DJ"=> Yii::t('countries', "Dschibuti"),
			"DO"=> Yii::t('countries', "Dominikanische Republik"),
			"EG"=> Yii::t('countries', "Ägypten"),
			"SV"=> Yii::t('countries', "El Salvador"),
			"EE"=> Yii::t('countries', "Estland"),
			"ET"=> Yii::t('countries', "Äthiopien"),
			"FI"=> Yii::t('countries', "Finnland"),
			"FR"=> Yii::t('countries', "Frankreich"),
			"GE"=> Yii::t('countries', "Georgien"),
			"DE"=> Yii::t('countries', "Deutschland"),
			"GR"=> Yii::t('countries', "Griechenland"),
			"GU"=> Yii::t('countries', "Guam"),
			"GT"=> Yii::t('countries', "Guatemala"),
			"HK"=> Yii::t('countries', "Hongkong"),
			"HU"=> Yii::t('countries', "Ungarn"),
			"IS"=> Yii::t('countries', "Island"),
			"IN"=> Yii::t('countries', "Indien"),
			"ID"=> Yii::t('countries', "Indonesien"),
			"IQ"=> Yii::t('countries', "Irak"),
			"IE"=> Yii::t('countries', "Irland"),
			"IL"=> Yii::t('countries', "Israel"),
			"IT"=> Yii::t('countries', "Italien"),
			"CI"=> Yii::t('countries', "Elfenbeinküste"),
			"JM"=> Yii::t('countries', "Jamaika"),
			"JP"=> Yii::t('countries', "Japan"),
			"JO"=> Yii::t('countries', "Jordanien"),
			"KZ"=> Yii::t('countries', "Kasachstan"),
			"KE"=> Yii::t('countries', "Kenia"),
			"KW"=> Yii::t('countries', "Kuwait"),
			"LV"=> Yii::t('countries', "Lettland"),
			"LB"=> Yii::t('countries', "Libanon"),
			"LR"=> Yii::t('countries', "Liberia"),
			"LI"=> Yii::t('countries', "Liechtenstein"),
			"LT"=> Yii::t('countries', "Litauen"),
			"LU"=> Yii::t('countries', "Luxemburg"),
			"MK"=> Yii::t('countries', "Nordmazedonien"),
			"MW"=> Yii::t('countries', "Malawi"),
			"MY"=> Yii::t('countries', "Malaysia"),
			"MT"=> Yii::t('countries', "Malta"),
			"MX"=> Yii::t('countries', "Mexiko"),
			"MD"=> Yii::t('countries', "Moldawien"),
			"MC"=> Yii::t('countries', "Monaco"),
			"ME"=> Yii::t('countries', "Montenegro"),
			"NA"=> Yii::t('countries', "Namibia"),
			"NP"=> Yii::t('countries', "Nepal"),
			"NL"=> Yii::t('countries', "Niederlande"),
			"NC"=> Yii::t('countries', "Neukaledonien"),
			"NZ"=> Yii::t('countries', "Neuseeland"),
			"NE"=> Yii::t('countries', "Nigeria"),
			"MP"=> Yii::t('countries', "Nördliche Marianen"),
			"NO"=> Yii::t('countries', "Norwegen"),
			"OM"=> Yii::t('countries', "Oman"),
			"PK"=> Yii::t('countries', "Pakistan"),
			"PA"=> Yii::t('countries', "Panama"),
			"PE"=> Yii::t('countries', "Peru"),
			"PH"=> Yii::t('countries', "Philippinen"),
			"PL"=> Yii::t('countries', "Polen"),
			"PT"=> Yii::t('countries', "Portugal"),
			"PR"=> Yii::t('countries', "Puerto Rico"),
			"QA"=> Yii::t('countries', "Katar"),
			"RE"=> Yii::t('countries', "Reunion"),
			"RO"=> Yii::t('countries', "Rumänien"),
			"RU"=> Yii::t('countries', "Russland"),
			"SM"=> Yii::t('countries', "San Marino"),
			"SA"=> Yii::t('countries', "Saudi-Arabien"),
			"SN"=> Yii::t('countries', "Senegal"),
			"RS"=> Yii::t('countries', "Serbien"),
			"SG"=> Yii::t('countries', "Singapur"),
			"SK"=> Yii::t('countries', "Slowakei"),
			"SI"=> Yii::t('countries', "Slowenien"),
			"ZA"=> Yii::t('countries', "Südafrika"),
			"KR"=> Yii::t('countries', "Südkorea"),
			"ES"=> Yii::t('countries', "Spanien"),
			"LK"=> Yii::t('countries', "Sri Lanka"),
			"SE"=> Yii::t('countries', "Schweden"),
			"CH"=> Yii::t('countries', "Schweiz"),
			"TW"=> Yii::t('countries', "Taiwan"),
			"TZ"=> Yii::t('countries', "Tansania"),
			"TH"=> Yii::t('countries', "Thailand"),
			"TG"=> Yii::t('countries', "Togo"),
			"TT"=> Yii::t('countries', "Trinidad und Tobago"),
			"TR"=> Yii::t('countries', "Türkei"),
			"UG"=> Yii::t('countries', "Uganda"),
			"UA"=> Yii::t('countries', "Ukraine"),
			"AE"=> Yii::t('countries', "Vereinigte Arabische Emirate"),
			"GB"=> Yii::t('countries', "Vereinigtes Königreich"),
			"US"=> Yii::t('countries', "Vereinigte Staaten"),
			"VA"=> Yii::t('countries', "Vatikanstadt"),
			"ZM"=> Yii::t('countries', "Sambia"),
			"ZW"=> Yii::t('countries', "Simbabwe"),
			);
		}
		if($country == 'ca-fr') $country = 'ca';

		switch ($country) {
			case 'de':
				$c = self::c_sort($_c, 'DE,AT,CH,LU,LI');
				break;
			case 'fr':
				$c = self::c_sort($_c, 'FR,CH,LU,MC');
				break;	
			case 'us':
				$c = self::c_sort($_c, 'US,CA,AU,GB');
				break;		
			case 'ca':
				$c = self::c_sort($_c, 'CA,US,AU,GB');
				break;		
			case 'au':
				$c = self::c_sort($_c, 'AU,CA,US,GB');
				break;	
			case 'uk':
				$c = self::c_sort($_c, 'GB,IE,AU,CA,US');
				break;
			case 'da':
				$c = self::c_sort($_c, 'DK');
				break;
			case 'cs':
				$c = self::c_sort($_c, 'CZ');
				break;
			case 'sl':
				$c = self::c_sort($_c, 'SI');
				break;
			case 'sq':
				$c = self::c_sort($_c, 'AL');
				break;
			case 'sr':
				$c = self::c_sort($_c, 'RS');
				break;
			case 'el':
				$c = self::c_sort($_c, 'GR');
				break;
			case 'et':
				$c = self::c_sort($_c, 'EE');
				break;
			default:
				$c = self::c_sort($_c, strtoupper($country));
				break;
		}

		return $c;
		
	}

	private static function c_sort($_array, $c_string)
	{
		$codes = explode(',', $c_string);
		$_res = array();
		foreach($codes as $code)
		{
			if(array_key_exists($code, $_array)){
				if($_array[$code] == "empty") continue; // убрать когда будет перевод
				$_res[$code] = $_array[$code];
			}
		}
		$_res['empty'] = '-----';
		
		$_tmp = array();
		foreach($_array as $code => $value)
		{
			if(in_array($code, $codes)) continue;
			if($value == "empty") continue; // убрать когда будет перевод
			$_tmp[$code] = $value;
		}

		if(!in_array(Yii::app()->language, array('el', 'cy')) ){
			$_tmp = self::array_multisort_with_umlaut($_tmp);
		} else {
			array_multisort($_tmp, SORT_ASC, SORT_STRING);
		}
		$_res = array_merge($_res, $_tmp);

		return $_res;

	}

	private static function array_multisort_with_umlaut($_a)
	{
		$_umlauts = array(
			'ä', 'ü', 'ö', 'Ä', 'Ü', 'Ö', 'ä', 'ü', 'ö', 'à','ѐ','ñ','ù','î','ğ','ę','ń','џ','ľ','ξ','ň','ê','đ','ď','ř','ż','ý','ϊ','θ','ș','ě','ś','è','ø','õ','ô','ă','ț','ã','ş','ύ','ή','â','ό','έ','ς','η','ú','ž','ç','δ','ó','č','ë','ά','σ','í', 'š','é','á', 'ł','i̇',
			'Ä', 'Ü', 'Ö', 'À','Ѐ','Ñ','Ù','Î','Ğ','Ę','Ń','Џ','Ľ','Ξ','Ň','Ê','Đ','Ď','Ř','Ż','Ý','Ϊ','Θ','Ș','Ě','Ś','È','Ø','Õ','Ô','Ă','Ț','Ã','Ş','Ύ','Ή','Â','Ό','Έ','Σ','Η','Ú','Ž','Ç','Δ','Ó','Č','Ë','Ά','Σ',/*'Í',*/ 'Š','É','Á', 'Ł','İ','Ј','Í',
		);
		$_noumlauts = array(
			'a', 'u', 'o', 'A', 'U', 'O', 'a', 'u', 'o', 'a','e','n','u','i','g','e','n','u','i','e','n','e','d','d','r','z','y','i','o','s','e','s','e','o','o','o','a','t','a','s','u','n','a','o','e','c','n','u','z','c','d','o','c','e','a','o','o', 's','e','a', 'l','i',
			'A', 'U', 'O', 'A','E','N','U','I','G','E','N','Чч','I','E','N','E','D','D','R','Z','Y','I','O','S','E','S','E','O','O','O','A','T','A','S','U','N','A','O','E','S','N','U','Z','C','D','O','C','E','A','O',/*'O',*/ 'S','E','A', 'L','I','Й','I',
		);
		$remember = array();
		foreach($_a as $k=>$v)
		{
			if(preg_match('@[äüöàѐñùîğęńџľξňêđďřżýϊθșěśèøõôățãşύήâόέςηúžçδóčëάσíšéáłİЏЈ]@iu', $v)){
				$remember[$k] = $v;
				$_a[$k] = str_replace($_umlauts, $_noumlauts, $v);
			}
		}

		array_multisort($_a, SORT_ASC, SORT_STRING);

		foreach($remember as $k=>$v)
		{
			$_a[$k] = $v;
		}
		return $_a;
	}	

	public static function getNameByCode($code)
	{
		$name = array_search($code, array_flip(self::getCountryList()));
		if(!$name){
			$name = $code;
		}
		return  $name;
	}
	
	public static function getMonthList()
	{
		return array(
			"01"=>"01",
			"02"=>"02",
			"03"=>"03",
			"04"=>"04",
			"05"=>"05",
			"06"=>"06",
			"07"=>"07",
			"08"=>"08",
			"09"=>"09",
			"10"=>"10",
			"11"=>"11",
			"12"=>"12",
		);
	}

	public static function getYearList()
	{
		$data = array();
		$year = date('Y');
		foreach(range($year,$year+15) as $v)
		{
			$data[$v] = $v;
		}
		return $data;
	}

	public static function getStateList()
	{
		return array();
	}
}