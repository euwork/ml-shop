<?php

class ContentImportForm extends CFormModel
{
	public $languages;

	public $defaultLang;

	public $dontreplace;

    public $textcontent;
    public $products;
    public $templateimages;



	// public $reviews;



		public function rules() {
		return array(
			array('languages', 'arrayOfLang', 'allowEmpty' => true), 
			array('defaultLang', 'in', 'range' => Languages::getArrayFlipAll()),
			array('dontreplace', 'boolean'),
            array('textcontent, products, templateimages', 'boolean'),
			
			
			);
	}

    public function arrayOfLang($attributeName, $params)
    {
        $allowEmpty = false;
        if (isset($params['allowEmpty']) and is_bool($params['allowEmpty'])) {
            $allowEmpty = $params['allowEmpty'];
        }
        if (!is_array($this->$attributeName)) {
            $this->addError($attributeName, "$attributeName must be array.");
        }
        if (empty($this->$attributeName) and !$allowEmpty) {
            $this->addError($attributeName, "$attributeName cannot be empty array.");
        }


        foreach ($this->$attributeName as $key => $value) {
            if (!in_array($value, array(0,1))) {
                $this->addError($attributeName, "$attributeName contains invalid value: $value.");
            }
        }
    }


}