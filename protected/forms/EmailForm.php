<?php

class EmailForm extends CFormModel
{
	public $email;
	public $first_name;

		public function rules() {
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('first_name', 'match', 'pattern'=>'/^[\w\s\.\-]+$/'),
			array('email, first_name', 'length', 'max'=>256),
			) ;
	}
}