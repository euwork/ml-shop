<?php

class GenerateLinkForm extends CFormModel
{
	public $index = 1;
	public $drugs = 1;
	public $categories = 1;
	public $pages = 0;
	public $articles = 0;
	public $testimonials = 0;
	public $format;
	public $ancors;
	public $language;

	public function rules() {
		return array(
			array('drugs, categories, pages, index, articles, testimonials', 'boolean'),
			array('format, ancors', 'numerical', 'integerOnly'=>true),
			array('language', 'in', 'range' => Languages::getArrayFlipAll()),
			) ;
	}


	const FORMAT_HTML = 1;
	const FORMAT_TEXT = 2;
	public static function getFormatList()
	{
		return array(
			self::FORMAT_HTML=>'HTML',
			self::FORMAT_TEXT=>'URL|KEY',
		);
	}
	const ANCOR_TITLE = 1;
	const ANCOR_H1 = 2;
	const ANCOR_NAME = 3;
	public static function getAncorsList()
	{
		return array(
			self::ANCOR_TITLE=>'Title',
			self::ANCOR_H1=>'H1',
			self::ANCOR_NAME=>'Name',
		);
	}
}