<?php

class InstallForm extends CFormModel
{
	public $aff_id;
	public $api_key;
	public $login;
	public $password;
	public $db_host;
	public $db_port;
	public $db_login;
	public $db_password;
	public $db_name;
	public $api_domain;
	// public $password;
	// public $password;

		public function rules() {
		return array(
			array('aff_id, api_key, login, password, db_login, db_name, db_password, api_domain, db_port, db_host', 'required'),
			array('aff_id', 'numerical', 'integerOnly'=>true),
			array('api_key', 'length', 'max'=>32),
			array('api_key', 'length', 'min'=>32),
			array('api_domain', 'length', 'max'=>64),
			);
	}
}