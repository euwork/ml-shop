<?php


class OrdersFilterForm extends CFormModel
{
	public $approved;
	public $period;
	public $day_from;
	public $month_from;
	public $year_from;
	public $day_to;
	public $month_to;
	public $year_to;

	public $wire;
	public $visa;
	public $master;
	public $amex;
	public $bc;
	public $ideal;
	
	public $currency;
	public $internal;
	public $external;

	public function rules()
	{
		return array(

			
			array('period', 'in', 'range' => array('period', 'today', 'last7days', 'thisweek', 'yesterday', 'last30days', 'thismonth')),
			array('day_from, day_to', 'in', 'range'=>self::getDayList()),
			array('month_from, month_to', 'in', 'range'=>array_flip(self::getMonthList())),
			array('year_from, year_to', 'in', 'range'=>self::getYearList()),
			array('approved', 'boolean'),
			array('day_from, day_to, month_from, month_to, year_from, year_to', 'required'),
			array('wire, visa, master, amex, bc, ideal', 'boolean'),

			array('internal, external', 'boolean'),
			array('currency', 'in', 'range' => array('USD', 'EUR')),

		);
	}

	public static function getDayList()
	{
		$data = array();
		foreach(range(1,31) as $v)
		{
			$data[$v] = $v;
		}
		return $data;
	}
	public static function getYearList()
	{
		$data = array();
		$last_year = date('Y') + 1;
		foreach(range('2014',$last_year) as $v)
		{
			$data[$v] = $v;
		}
		return $data;
	}

	public static function getMonthList()
	{
		return array(
			"1"=>"January",
			"2"=>"February",
			"3"=>"March",
			"4"=>"April",
			"5"=>"May",
			"6"=>"June",
			"7"=>"July",
			"8"=>"August",
			"9"=>"September",
			"10"=>"October",
			"11"=>"November",
			"12"=>"December",
		);
	}
}