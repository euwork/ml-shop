<?php

class PricesFilterForm extends CFormModel
{
	public $products_label;
	public $drug_id;
	public $lang;

		public function rules() {
		return array(
			array('products_label', 'required'),
			array('products_label', 'in', 'range' => array_flip(Products::getProductsBasesList())),
			array('lang', 'in', 'range' => Languages::getArrayFlip()),
			array('drug_id', 'numerical', 'integerOnly'=>true),
			) ;
	}
}