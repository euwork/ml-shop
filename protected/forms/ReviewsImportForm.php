<?php

class ReviewsImportForm extends CFormModel
{
	public $min;
	public $max;
    public $smart_random;
    public $votes_min;
    public $votes_max;
    public $votes_yes_prc_min;
    public $votes_yes_prc_max;

    // public $delete;

    public $use_reviews;

    public $fake_ratings;

    public $languages;




		public function rules() {
		return array(
			array('min, max, votes_min, votes_max, votes_yes_prc_max, votes_yes_prc_min', 'numerical', 'integerOnly'=>true),
            array('min, max', 'length', 'max'=>3, 'min'=>1),
            array('votes_yes_prc_min, votes_yes_prc_max', 'length', 'max'=>2, 'min'=>1),
            array('votes_min, votes_max', 'length', 'max'=>4, 'min'=>1),
            array('smart_random', 'boolean'),
            array('use_reviews', 'boolean'),
            array('fake_ratings', 'in', 'range' => array_flip(self::fakeRatings())),
            array('languages', 'arrayOfLang', 'allowEmpty' => true), 
			);
	}

    public static function fakeRatings()
    {
        return array(
            0 => 'No',
            1 => 'Small (100-200)',
            2 => 'Medium (300-600)',
            3 => 'Large (700-1200)',
            4 => 'Large XXL (1800-2200)',
        );
    }

    public function arrayOfLang($attributeName, $params)
    {
        $allowEmpty = false;
        if (isset($params['allowEmpty']) and is_bool($params['allowEmpty'])) {
            $allowEmpty = $params['allowEmpty'];
        }
        if (!is_array($this->$attributeName)) {
            $this->addError($attributeName, "$attributeName must be array.");
        }
        if (empty($this->$attributeName) and !$allowEmpty) {
            $this->addError($attributeName, "$attributeName cannot be empty array.");
        }


        foreach ($this->$attributeName as $key => $value) {
            if (!in_array($value, array(0,1))) {
                $this->addError($attributeName, "$attributeName contains invalid value: $value.");
            }
        }
    }

}