<?php


class TraffFilterForm extends CFormModel
{
	public $period;
	public $week;
	public $year1;
	public $year2;
	public $year3;
	public $month;

	public $wire;
	public $visa;
	public $master;
	public $amex;
	public $bc;
	public $ideal;

	public function rules()
	{
		return array(

			
			array('period', 'in', 'range' => array('week', 'monthly', 'month')),
			array('week', 'in', 'range'=>array_flip(self::getWeekList())),
			array('month', 'in', 'range'=>array_flip(self::getMonthList())),
			array('year1, year2, year3', 'in', 'range'=>self::getYearList()),
			array('year1, year2, year3, month, week, period', 'required'),

			array('wire, visa, master, amex, bc, ideal', 'boolean'),
		);
	}


	public static function getYearList()
	{
		$data = array();
		$last_year = date('Y') + 1;
		foreach(range('2014',$last_year) as $v)
		{
			$data[$v] = $v;
		}
		return $data;
	}

	public static function getMonthList()
	{
		return array(
			"1"=>"January",
			"2"=>"February",
			"3"=>"March",
			"4"=>"April",
			"5"=>"May",
			"6"=>"June",
			"7"=>"July",
			"8"=>"August",
			"9"=>"September",
			"10"=>"October",
			"11"=>"November",
			"12"=>"December",
		);
	}
	public static function getWeekList()
	{
		return array(
			"1"=>"1st",
			"2"=>"2nd",
			"3"=>"3rd",
			"4"=>"4th",
			"5"=>"5th",
			"6"=>"6th",
			"7"=>"7th",
			"8"=>"8th",
			"9"=>"9th",
			"10"=>"10th",
			"11"=>"11th",
			"12"=>"12th",
			"13"=>"13th",
			"14"=>"14th",
			"15"=>"15th",
			"16"=>"16th",
			"17"=>"17th",
			"18"=>"18th",
			"19"=>"19th",
			"20"=>"20th",
			"21"=>"21st",
			"22"=>"22nd",
			"23"=>"23rd",
			"24"=>"24th",
			"25"=>"25th",
			"26"=>"26th",
			"27"=>"27th",
			"28"=>"28th",
			"29"=>"29th",
			"30"=>"30th",
			"31"=>"31st",
			"32"=>"32nd",
			"33"=>"33rd",
			"34"=>"34th",
			"35"=>"35th",
			"36"=>"36th",
			"37"=>"37th",
			"38"=>"38th",
			"39"=>"39th",
			"40"=>"40th",
			"41"=>"41st",
			"42"=>"42nd",
			"43"=>"43rd",
			"44"=>"44th",
			"45"=>"45th",
			"46"=>"46th",
			"47"=>"47th",
			"48"=>"48th",
			"49"=>"49th",
			"50"=>"50th",
			"51"=>"51st",
			"52"=>"52nd",
			"53"=>"53rd",
		);
	}

	public static function getMondayOfWeek($week, $year="") 
	{
		//получаем дату первого января заданного года
		$monday  = strtotime("1 january ".($year ? $year : date("Y")));
		$dw = date("w", $monday);
		if ($dw!=1) {//если не понедельник, то переходим на дату понедельника
		    $monday = strtotime("next Monday", $monday);
		    $week--;
		    }
		//начало текущего года может приходиться на последнюю неделю предыдущего
		if (($dw>0)&($dw<5)) $week--;//учитываем это

		$plus_week = "$week week";
		if ($week>0) $plus_week = '+'.$plus_week;
		return strtotime($plus_week, $monday);//переходим к понедельнику заданной недели
	}

	public static function getCountWeek($year)
	{
	    $date = date('w', mktime(0, 0, 0, 12, 31, $year));
	    $day = ($date < 4 ? 31 - $date : 31);
	    return date('W', mktime(0, 0, 0, 12, $day, $year));  
	}
}