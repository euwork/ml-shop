<?php


class Filter 
{
	public static function max256($value)
	{
		if(strlen($value) > 256){
			$value = substr($value, 0, 256);
		}
		return $value;
	}

	public static function generic($str)
	{
		// $replace_generic = !Yii::app()->config->get('show_generic');
		$replace_generic = !Languages::isGenericEnabled();
		$_replacement = array('generika', 'generico', 'generic', 'genérico', 'générique',  'generieke', 'generisk');
		if($replace_generic){
			$str = str_ireplace($_replacement, '', $str);
			$str = str_replace('  ', ' ', $str);
			$str = trim($str);
		}

		return $str;
	}

	// public static function generic_text($str)
	// {
	// 	// $replace_generic = !Yii::app()->config->get('show_generic');
	// 	$replace_generic = !Languages::isGenericEnabled();
	// 	$_replacement1 = array(' generika', ' generic', ' genérico', ' générique', ' generico', ' generiek', ' generisk');
	// 	$_replacement2 = array('generika ', 'generic ', 'genérico ', 'générique ', 'generico ', 'generiek ', 'generisk ');
	// 	$_replacement3 = array(' generika ', ' generic ', ' genérico ', ' générique ', ' generico ', ' generiek ', ' generisk ');
	// 	if($replace_generic){
	// 		$str = str_ireplace($_replacement1, '', $str);
	// 		$str = str_ireplace($_replacement2, '', $str);
	// 		$str = str_ireplace($_replacement3, ' ', $str);
	// 		//$str = str_replace('  ', ' ', $str);
	// 		//$str = trim($str);
	// 	}

	// 	return $str;
	// }	
}