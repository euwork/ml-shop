<?php


class Format
{
	public static function tag($tag, $string, $attributes = array() )
	{
		$attr_list = array();
		foreach($attributes as $name => $value)
		{
			$attr_list[] = $name . '="' . $value .'"';
		}
		$attr_string = '';
		if(count($attr_list) > 0){
			$attr_string = ' ' . implode(' ', $attr_list);
		}
			
		return '<'.$tag. $attr_string .'>' . $string . '</'.$tag.'>';
	}

	public static function count($tag, $string, $attributes = array())
	{
		$string = $string == 0 ? '' : $string;
		return self::tag($tag, $string, $attributes = array() );
	}

	public static function price($price, $tag = '', $symbol = '€')
	{
		$price = number_format($price,2);
		if($tag){
			$price = self::tag($tag, $price);
		}
		return $symbol . ' ' . $price;
	}

	public static function price_doll($price, $tag = '')
	{
		$price = number_format($price,2);
		if($tag){
			$price = self::tag($tag, $price);
		}
		return  $price.'$';
	}

	public static function percent($price, $tag = '')
	{
		$price = number_format($price,1);
		if($tag){
			$price = self::tag($tag, $price);
		}
		return  $price . '%';
	}
	public static function datetime($datetime, $tag = 'small', $razd = '<br />')
	{ 
		$sec = strtotime($datetime);
		$date = date('Y-m-d', $sec);
		$time = date('H:i:s', $sec);
	    return  $date . $razd . self::tag($tag, $time);

	}

	public static function link($string)
	{
		return self::tag('span', $string, array('class'=>'url'));
	}
}