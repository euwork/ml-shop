<?php

class InstallHelp
{
	public static function generateDbConfigCode($db_host, $db_port, $db_user, $db_password, $db_name)
	{
		$template = file_get_contents(YiiBase::getPathOfAlias('webroot') . '/protected/config/db.php.sample');

		$template = str_replace('{db_host}', $db_host, $template);
		$template = str_replace('{db_port}', $db_port, $template);
		$template = str_replace('{db_user}', $db_user, $template);
		$template = str_replace('{db_pass}', $db_password, $template);
		$template = str_replace('{db_name}', $db_name, $template);

		$fg = fopen(YiiBase::getPathOfAlias('webroot') . '/protected/config/db.php', 'w+');
		fputs($fg, $template);
		fclose($fg);

		return true;
	}

    public static function generateParamsConfigCode($login, $password)
    {
        $template = file_get_contents(YiiBase::getPathOfAlias('webroot') . '/protected/config/params.php.sample');

        $template = str_replace('{login}', $login, $template);
        $template = str_replace('{pass}', $password, $template);

        $fg = fopen(YiiBase::getPathOfAlias('webroot') . '/protected/config/params.php', 'w+');
        fputs($fg, $template);
        fclose($fg);

        Yii::app()->params['login'] = $login;
        Yii::app()->params['pass'] = $password;

        return true;
    }

	public static function makeImportBaseSqlDump($db_host, $db_port, $db_user, $db_password, $db_name)
	{

        $errors = [];

		$import = new SQLImport(YiiBase::getPathOfAlias('webroot')  . '/protected/data/base.sql', false, $db_host, $db_port, $db_user, $db_password, $db_name);
        
        $import->connect();

        if ($import->con)
        {
            $import->select_db();
            $import->set_charset();
            $import->import();
            
            if ($import->error)
            {
                $errors[] = 'MySQL import error: '.$import->error;
            }
            else
            {

            }
        }
        else
        {
            $errors[] = 'MySQL connection error.';
        }




        if(count($errors) == 0){
            return true;
        }

        return $errors;
	}

    public static function updateApiParams($db_host, $db_port, $db_user, $db_password, $db_name, $api_domain, $api_key, $affid)
    {
        $errors = [];

        $import = new SQLImport('', false, $db_host, $db_port, $db_user, $db_password, $db_name);





        $import->connect();

        if ($import->con)
        {
            $import->select_db();
            $import->set_charset();
            
            $import->query("update `tbl_config` set `value`='".$api_domain."' where `param`='api_domain';");
            if ($import->error)
            {
                $errors[] = 'MySQL import error: '.$import->error;
            }

            $import->query("update `tbl_config` set `value`='".$api_key."' where `param`='api_key';");
            if ($import->error)
            {
                $errors[] = 'MySQL import error: '.$import->error;
            }

            $import->query("update `tbl_config` set `value`='".$affid."' where `param`='affid';");
            if ($import->error)
            {
                $errors[] = 'MySQL import error: '.$import->error;
            }

            $start_order_id = rand(10000, 99999);
            $import->query("alter table tbl_orders auto_increment = ".$start_order_id.";");
            if ($import->error)
            {
                $errors[] = 'MySQL import error: '.$import->error;
            }
        }
        else
        {
            $errors[] = 'MySQL connection error.';
        }




        if(count($errors) == 0){
            return true;
        }

        return $errors;

    }

    public static function getTablesCount()
    {
        $connection=Yii::app()->db;
        $command=$connection->createCommand("SHOW TABLES;");
        $rowCount=$command->execute(); 
        return $rowCount;
    }

}