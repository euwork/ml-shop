<?php
// ------------------------------------------------------------------------
class SQLImport
{
    // ------------------------------------------------------------------------
    public $host, $port, $database, $user, $pass, $sql_file, $check, $con, $error;
    // ------------------------------------------------------------------------
    function __construct($sql_file, $check = false, $host = false, $port = false, $user = false, $pass = false, $database = false)
    {
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
        $this->user = $user;
        $this->pass = $pass;
        $this->sql_file = $sql_file;
        $this->check = $check;
    }
    // ------------------------------------------------------------------------
    function connect()
    {
        $this->con = @mysqli_connect($this->host, $this->user, $this->pass, $this->database, $this->port);
        
        if (!$this->con)
        {
            $this->error = "<b>Error (connect): " . mysqli_error() . "</b>";
        }
    }
    // ------------------------------------------------------------------------
    function select_db()
    {
        $result = @mysqli_select_db($this->con, $this->database);
        
        if (!$result)
        {
            $this->error = "<b>Error (select_db): " . mysqli_error($this->con) . "</b>";
        }
    }
    // ------------------------------------------------------------------------
    function set_charset()
    {
        $result = mysqli_set_charset($this->con, 'utf8');
        if (!$result)
        {
            $this->error = "<b>Error (set_charset): " . mysqli_error($this->con) . "</b>";
        }
        // $this->query('SET NAMES "utf8" COLLATE "utf8_general_ci" ;');
    }
    // ------------------------------------------------------------------------
    function query($sql = null)
    {
        if ($sql)
        {
            $result =  mysqli_query($this->con, $sql);
            if (!$result)
            {
                $this->error = "<b>Error (mysql_query): " . mysqli_error($this->con) . "</b>";
            }  
        }
    }
    // ------------------------------------------------------------------------
    function import()
    {
        if ($this->host)
        {
            $this->connect();
            
            if ($this->error)
            {
                return;
            }
        }
        if ($this->database)
        {
            $this->select_db();

            $this->set_charset();
            
            if ($this->error)
            {
                return;
            }
        }
        if ($this->con !== false || $this->check)
        {
            $lines = file($this->sql_file);
            $buffer = '';
            
            foreach ($lines as $line)
            {
                if (($line = trim($line)) == '')
                {
                    continue;
                }
                if (substr(ltrim($line), 0, 2) == '--')
                {
                    continue;
                }
                if (substr($line, -1) != ';')
                {
                    $buffer .= $line;
                    continue;
                }
                else
                {
                    if ($buffer)
                    {
                        $line = $buffer . $line;
                        $buffer = '';
                    }
                }
                
                $line = substr($line, 0, -1);
                
                if (!$this->check)
                {
                    $result = mysqli_query($this->con, $line);
                }
                else
                {
                    echo substr($line, 0, 180) . ((strlen($line) > 180) ? "...<br>" : "<br>");
                    
                    $this->error = "<b>No data has been written (check = true)</b>";
                }
                if (@!$result and !$this->check)
                {
                    $this->error = "<b>Error (mysql_query): " . mysqli_error($this->con) . "</b>";
                    return;
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
?>