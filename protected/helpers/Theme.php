<?php


// //images/{themename}/content  # products images
// //images/{themename}/template # header, contacts, sidebar
// //themes/{themename}/images # 

class Theme 
{

	// ImgUrl
	public static function getImgUrl()
	{
		$label = self::getLabel();

		$baseUrl = Yii::app()->baseUrl . '/images/' . $label;

		return $baseUrl;
	}

	public static function getImgUrlTemplate()
	{
		return self::getImgUrl() . '/template';
	}

	public static function getImgUrlContent()
	{
		return self::getImgUrl() . '/content';
	}	

	// get themes list
	public static function getList()
	{
		$dirs = glob(self::getPath(). '/*', GLOB_ONLYDIR);
		$list = [];
		foreach($dirs as $dir)
		{
			if($dir == 'admin') continue;
			if(file_exists($dir .'/' . 'screenshort.png')){
				$name = basename($dir.'/');
				$list[$name] = $name;
			}

		}
		return $list;
	}

	// метка темы. для использования content картинок  в другой теме;
	public static function getLabel()
	{
		$themename = Yii::app()->config->get('theme');
		$path = self::getPath().'/' . $themename . '/' . 'label.txt';
		if(file_exists($path)){
			$label = file_get_contents($path);
			$label = trim($label);
			return $label;
		} else {
			return $themename;
		}
	}

	//ImgPath
	private static function getPath()
	{
		return YiiBase::getPathOfAlias('webroot'). '/themes';
	}

	private static function getImgPath()
	{
		$label = self::getLabel();

		return YiiBase::getPathOfAlias('webroot'). '/images/' . $label;
	}

	public static function getImgPathTemplate()
	{
		return self::getImgPath() . '/template';
	}
	
}