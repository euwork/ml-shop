<?php

return array(
	'Persönliche Details' => 'Données personnelles',
	'Vorname' => 'Prénom',
	'Email' => 'Email',
	'Webseite' => 'Page Web',
	'Nachricht' => 'Message',
	'Geben Sie die Zeichen in dem Bild ein' => 'Tapez les symboles que vous voyez sur l’image',
	'Senden' => 'à envoyer',
	);