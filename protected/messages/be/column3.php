<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Informations utiles',
    'Lieferkonditionen' => 'Conditions de livraison',
    'Rückerstattung' => 'Retour',
    'Affiliates' => 'Affiliâtes',
    'AGB' => 'Conditions générales de l’accord',
    'Datenschutz' => 'Protection des données',
    'Impressum' => 'Mentions Légales',
    'Garantie' => 'Garantie',
    'Warenkorb' => 'Mon Panier',
    'Kein Produkt im Warenkorb' => '0 produits',
    'Gesamtbetrag' => 'Total',
    '1 Produkt im Warenkorb' => '1 produit',
    'Produkte im Warenkorb' => 'produits',
    // 'Angebot' => 'Offre',
    'kostenloser online support' => 'Aide en ligne',
    // 'Vorteile' => 'Avantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Méthode de paiement',
    'VERSANDPARTNER' => 'Livraison',
    'Secure shopping protected by SSL' => 'connexion sécurisée ssl',
);