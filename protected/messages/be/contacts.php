<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Tapez les symboles que vous voyez sur l’image',
	'Name'=> 'Nom',
	'E-Mail'=> 'Email',
	'Betreff'=> 'Sujet',
	'Nachricht'=> 'Message',
	'Kontakt' => 'Nous contacter',
	'Senden' => 'à envoyer',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Merci! Nous avons reçu votre message et nous vous contacterons très tôt!',
	// 'Captcha' => 'Código de imagen',
	);