<?php


return array(
	'Ihr Warenkorb ist leer' => 'Votre panier est vide',
	'Navigation' => 'Navigation',
	'Top Produkte' => 'Top produits',
	'Top Produkt' => 'Top produit',
	'Verfügbarkeit' => 'Disponibilité',
	'Lieferung' => 'Livraison',
	'Zahlungsart' => 'Paiement',
	'Ja' => 'Oui',
	'Werktage' => 'Jours ouvrables',
	'Kreditkarten, Banküberweisung' => 'Cartes de crédit, Virement bancaire',
);