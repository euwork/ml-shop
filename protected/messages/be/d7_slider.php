<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Viagra, Cialis, Kamagra, Levitra - traitement de l\'impuissance',
	'Potenzmittel rezeptfrei' => 'Traitement de l\'impuissance',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Viagra, Cialis, Kamagra et Levitra en France',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => 'Viagra, Cialis, Kamagra et Levitra en ligne en France contre l\'impuissance masculine',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra Pour Femme - Lovegra',
	'Viagra für die Frau rezeptfrei' => 'Viagra Pour Femme - Lovegra',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra Pour Femme - Lovegra en France sans ordonnance.',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Lovegra (Womenra) - Viagra pour femmes en {domain} pas cher en France sans ordonnance',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Lida Daidaihua, Xenical Générique en France',
	'Lida Daidaihua, Xenical tabletten' => 'Produits minceur pour maigrir vite',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Lida Daidaihua, Xenical Générique en France',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Perdre du poids - Lida Daidaihua, Xenical Générique en France - produits minceur pour maigrir vite',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Propecia Générique Finastéride 1mg sans ordonnance en France',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Propecia Générique (Finastéride 1mg)',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Générique (Finastéride 1mg) sans ordonnance en France',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Générique (Finastéride 1mg) a prix pas cher en ligne en France sans ordonnance',
	
	'Potenzmittel rezeptfrei - {domain}' => '{domain} en France',
	'Potenzmittel rezeptfrei.' => '{domain} en France',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => 'Viagra, Cialis, Levitra, Kamagra sans ordonnance en France',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => 'Achat Simple et Sécurisé, Expédition rapide, Expédition Gratuite et Retour Sans Frais',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);