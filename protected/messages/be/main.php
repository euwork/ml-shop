<?php

return array(
    'Startseite' => 'Accueil',//perevod
    'Über uns' => 'À propos de nous',
    'F.A.Q.' => 'Questions et réponses',
    'Kontakt' => 'Contactez-nous',
    'So bestellen Sie' => 'Comment passer la commande',
    'To top' => 'en haut',//perevod
    'Urheberrecht' => 'Droit d\'auteur',
    'Alle Rechte vorbehalten' => 'Tous droits réservés',
);