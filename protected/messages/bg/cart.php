<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Моята количка', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Обновете и запазете', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Вземете {tabl_count} допълнителните хапчета за {symbol}{tabl_price} ({symbol}{tabl_price_one} за хапче)', //13
    'Aktualisieren' => 'Обновете', //13
    'Gesamtbetrag' => 'Обща сума', // 0
    'Mit Einkauf fortfahren' => 'Продължете пазаруването', //13
    'Zur Kasse' => 'Оформяне на поръчка', //13
    'Produkt' => 'Продуктът', //13
);