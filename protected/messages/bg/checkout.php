<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'Електронна поща', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Име', //13
	'Name' => 'Пълно име', //14
	'Nachname' => 'Фамилното име', //13
	'Strasse / Hausnr' => 'Адрес', //13
	'Postleitzahl' => 'Пощенски код', //13
	'Ort' => 'Град', //13
	'Land' => 'Страна', //13
	'Telefonnummer' => 'Телефонен номер', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Вашата поръчка', //13
	'Lieferadresse' => 'Информация за доставка', //13
	'Rechnungsadresse' => 'Информация за фактуриране', //13
	'Lieferadresse & Rechnungsadresse' => 'Адрес за доставка и адрес за фактуриране ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Ако адресът за фактуриране и адресът за доставка съвпадат, кликнете тук', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Моята количка', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Обща сума', //13
	'Versandart' => 'Начин на доставка', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Редовна доставка (8-13 работни дни)', //vit nenado
    'Regulärer Postversand (8-21 Werktage)' => 'Редовна доставка (8-21 работни дни)', //0
	'Express Versand (4-7 Werktage)' => 'Експресна доставка (4-7 работни дни)', //0
    'Express Versand (7-13 Werktage)' => 'Експресна доставка (7-13 работни дни)', //vit nenado
    'Standardlieferung (8-21 Werktage)' => 'Стандартна доставка (8-21 работни дни)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Застраховка за доставка - {symbol}{price} (Гарантирано повторно изпращане при неуспешната доставка)', //13
	'Bitte Zahlungsart auswählen' => 'Моля, изберете начин на плащане', //13
	'Zahlungsart' => 'Вид плащане', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Банков трансфер', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Номер на кредитната карта', //13
	'Gültig bis' => 'Дата на изтичане на кредитната карта', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Завърши поръчка', //13
	'Allg. Geschäftsbedingungen' => 'Общи условия за транзакция', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Общи условия за транзакцията: С настоящото потвърждавам, че всички мои лични данни (или реквизити) са верни, аз прочетох обяснението и разбирам условията относно тях.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);