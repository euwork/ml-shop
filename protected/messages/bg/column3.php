<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Полезна информация', //1
    'Lieferkonditionen' => 'Условия за доставка', //1
    'Rückerstattung' => 'Връщане на плащане', //1
    'Affiliates' => 'Партньори', //1
    'AGB' => 'Правила и условия', //1
    'Datenschutz' => 'Политика за поверителност', //1
    'Impressum' => 'Отпечатък', //1
    'Garantie' => 'Гаранция', //1
    'Warenkorb' => 'Моята количка', //1
    'Kein Produkt im Warenkorb' => 'Няма артикули в моята количка', //1
    'Gesamtbetrag' => 'Обща стойност на поръчката', //1
    '1 Produkt im Warenkorb' => '1 артикул в моята количка', //1
    'Produkte im Warenkorb' => 'артикула в моята количка', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Начини на плащане', //1
    'VERSANDPARTNER' => 'Методи за доставка', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);