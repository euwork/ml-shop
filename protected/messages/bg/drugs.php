<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Отзиви на клиенти', //13
    'Rezensionen' => 'Отзиви', //13
    'Alle {count} Bewertungen anzeigen...' => 'Вижте всички {count} отзиви', //13
    'Stern:' => 'Звезда:', //13
    'Sterne:' => 'Звезди:', //13 
    'Produkt' => 'Препаратът', //13
    'Preis' => 'Цена', //13
    'Jetzt kaufen' => 'Поръчай сега', //13
    'Dosierung' => 'Дозировка', //13
    'Menge + Bonus' => 'Количество + бонус', //13
    'Menge' => 'Количество', //13
    // '' => '',

    'kaufen' => 'Добави в кошницата', //0
    'Eigene Bewertung erstellen' => 'Нова препоръка', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} от {vote_all} хора смятат следния отзив за полезен', //41
    'nein' => 'Не', //41
    'ja' => 'Да', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Полезен ли Ви беше този отзив?', //41
    'von' => 'от', //41

    'Rezension abgeben' => 'Създайте своят собствен отзив', //42
    'Name' => 'Име', //42
    'Email' => 'Електронна поща', //42
    'Thema' => 'Заглавие', //42
    'Bewertung abgeben' => 'Рейтинг на уебсайта', //42
    'Webseitenfreundlichkeit' => 'Уеб сайт', //42
    'Versand Zufriedenheit' => 'Доставка', //42
    'Preis / Leistung' => 'Цена / изпълнение', //42
    'Empfehlung' => 'Препоръка', //42
    'Kundenkommentar' => 'Отзив на клиента', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Въведете символите, които виждате на снимката.', //42
    'Rezension abschicken' => 'Изпратете отзив', //42

);