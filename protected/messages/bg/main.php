<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Главна страница',//1
    'Über uns' => 'За нас', //1
    'F.A.Q.' => 'Често задаваните въпроси', //1
    'Kontakt' => 'Свържете се с нас', //1
    'So bestellen Sie' => 'Как да поръчам', //1
    'To top' => 'Нагоре',//0
    'Urheberrecht' => 'Авторско право',//0
    'Alle Rechte vorbehalten' => 'Всички права са запазени',//0
);