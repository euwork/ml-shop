<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Благодарим Ви за направената поръчка.', //14
	'Bestellnummer' => 'Поръчка No.',//14
    'Bestellte Produkte' => 'Поръчани стоки',//14
    'Versendungsart' => 'Начин на доставка',//14
    'Versicherung' => 'Застраховка',//14
    'Telefonnummer' => 'Телефонен номер',//14
    'Drucken' => 'Разпечати',//14

    'Bankverbindung' => 'Банкова информация',//14
    'Empfänger' => 'Бенефициент',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Цел на платежа',//14
    'Betrag' => 'Сума', //14
    'Bank' => 'Банка', //14
    'Adresse' => 'Адрес', //14
    // '' => '',


);