<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Můj vozík', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Aktualizujte a uložte', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Vezměte {tabl_count} dalších pilulek za {symbol}{tabl_price} ({symbol}{tabl_price_one} za pilulku)', //13
    'Aktualisieren' => 'Aktualizujte', //13
    'Gesamtbetrag' => 'Celkem', // 0
    'Mit Einkauf fortfahren' => 'Pokračovat v nakupování', //13
    'Zur Kasse' => 'Dokončit objednávku', //13
    'Produkt' => 'Produkt', //13
);