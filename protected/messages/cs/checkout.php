<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Křestní jméno', //13
	'Name' => 'Celé jméno', //14
	'Nachname' => 'Příjmení', //13
	'Strasse / Hausnr' => 'Adresa', //13
	'Postleitzahl' => 'PSČ', //13
	'Ort' => 'Město', //13
	'Land' => 'Země', //13
	'Telefonnummer' => 'Telefonní číslo', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Vaše objednávka', //13
	'Lieferadresse' => 'Přepravní informace', //13
	'Rechnungsadresse' => 'Fakturační údaje', //13
	'Lieferadresse & Rechnungsadresse' => 'Doručovací adresa a fakturační adresa ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Pokud se fakturační adresa a dodací adresa shodují, klikněte sem', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Můj vozík', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Celková suma', //13
	'Versandart' => 'Způsob dopravy', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Pravidelné doručení (8-13 pracovních dnů)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Pravidelné doručení (8-21 pracovních dnů)', //0
	'Express Versand (4-7 Werktage)' => 'Expresní doručení (4-7 pracovních dnů)', //0
    'Express Versand (7-13 Werktage)' => 'Expresní doručení (7-13 pracovních dnů)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standardní doručení (8-21 pracovních dnů)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Pojištění dodávky - {symbol}{price} (Zaručeno opětovné odeslání v případě selhání dodávky)', //13
	'Bitte Zahlungsart auswählen' => 'Vyberte způsob platby', //13
	'Zahlungsart' => 'Způsob platby', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bankovní převod', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Číslo kreditní karty', //13
	'Gültig bis' => 'Konec platnosti kreditní karty', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Kompletní objednávka', //13
	'Allg. Geschäftsbedingungen' => 'Všeobecné podmínky transakce', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Všeobecné podmínky transakce: Potvrzuji, že všechny mé osobní údaje (nebo podrobnosti) jsou správné, přečetl jsem si vysvětlení a rozumím jejich podmínkám.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);