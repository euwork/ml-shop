<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Užitečné informace', //1
    'Lieferkonditionen' => 'Dodací podmínky', //1
    'Rückerstattung' => 'Náhrada', //1
    'Affiliates' => 'Pobočky', //1
    'AGB' => 'Pravidla a podmínky', //1
    'Datenschutz' => 'Zásady ochrany osobních údajů', //1
    'Impressum' => 'Otisk', //1
    'Garantie' => 'Záruka', //1
    'Warenkorb' => 'Můj vozík', //1
    'Kein Produkt im Warenkorb' => 'V košíku nejsou žádné položky', //1
    'Gesamtbetrag' => 'Celková objednávka', //1
    '1 Produkt im Warenkorb' => '1 položka v košíku', //1
    'Produkte im Warenkorb' => 'položky v košíku', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Možnosti platby', //1
    'VERSANDPARTNER' => 'Způsoby doručení', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);