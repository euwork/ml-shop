<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Hodnocení zákazníků', //13
    'Rezensionen' => 'Hodnocení', //13
    'Alle {count} Bewertungen anzeigen...' => 'Zobrazit všech {count} hodnocení', //13
    'Stern:' => 'Hvězdička:', //13
    'Sterne:' => 'Hvězdičky:', //13 
    '5Sterne:' => 'Hvězdiček:', //13 
    'Produkt' => 'Položka', //13
    'Preis' => 'Cena', //13
    'Jetzt kaufen' => 'Objednat teď', //13
    'Dosierung' => 'Dávkování', //13
    'Menge + Bonus' => 'Množství + Bonus', //13
    'Menge' => 'Množství', //13
    // '' => '',

    'kaufen' => 'Přidat do košíku', //0
    'Eigene Bewertung erstellen' => 'Nový posudek', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => 'Následující recenze pomohlo {vote_yes} z {vote_all} lidí', //41
    'nein' => 'Ne', //41
    'ja' => 'Ano', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Pomohla vám tato recenze?', //41
    'von' => 'od', //41

    'Rezension abgeben' => 'Vytvořte si vlastní recenzi', //42
    'Name' => 'Jméno', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Název', //42
    'Bewertung abgeben' => 'Hodnocení webu', //42
    'Webseitenfreundlichkeit' => 'Webová stránka', //42
    'Versand Zufriedenheit' => 'Doprava', //42
    'Preis / Leistung' => 'Cena/ Výkon', //42
    'Empfehlung' => 'Doporučení', //42
    'Kundenkommentar' => 'Zákaznická recenze', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Zadejte znaky, které vidíte na obrázku.', //42
    'Rezension abschicken' => 'Odeslat recenzi', //42

);