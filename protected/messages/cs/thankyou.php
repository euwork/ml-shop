<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Děkujeme za Vaši objednávku.', //14
	'Bestellnummer' => 'Objednávka č.',//14
    'Bestellte Produkte' => 'Objednané produkty',//14
    'Versendungsart' => 'Způsob doručení',//14
    'Versicherung' => 'Pojištění',//14
    'Telefonnummer' => 'Telefonní číslo',//14
    'Drucken' => 'Tisk',//14

    'Bankverbindung' => 'Bankovní údaje',//14
    'Empfänger' => 'Příjemce',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Účel platby',//14
    'Betrag' => 'Částka', //14
    'Bank' => 'Banka', //14
    'Adresse' => 'Adresa', //14
    // '' => '',


);