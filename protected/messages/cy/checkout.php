<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Όνομα', //13
	'Name' => 'Πλήρες όνομα', //14
	'Nachname' => 'Επίθετο', //13
	'Strasse / Hausnr' => 'Διεύθυνση', //13
	'Postleitzahl' => 'Ταχυδρομικός κώδικας', //13
	'Ort' => 'Πόλη', //13
	'Land' => 'Χώρα', //13
	'Telefonnummer' => 'Τηλεφωνικός αριθμός', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'H παραγγελία σας', //13
	'Lieferadresse' => 'Πληροφορίες αποστολής', //13
	'Rechnungsadresse' => 'Πληροφορίες Χρέωσης', //13
	'Lieferadresse & Rechnungsadresse' => 'Διεύθυνση παράδοσης & Διεύθυνση χρέωσης ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Εάν η διεύθυνση χρέωσης και η διεύθυνση αποστολής ταιριάζουν, κάντε κλικ εδώ', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Το Καλάθι μου', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Συνολικό ποσό', //13
	'Versandart' => 'Μέθοδος αποστολής', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Τακτική αποστολή (8-13 εργάσιμες ημέρες)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Τακτική αποστολή (8-21 εργάσιμες ημέρες)', //0
	'Express Versand (4-7 Werktage)' => 'Γρήγορη αποστολή (4-7 εργάσιμες ημέρες)', //0
    'Express Versand (7-13 Werktage)' => 'Γρήγορη αποστολή (7-13 εργάσιμες ημέρες)', //00
    'Standardlieferung (8-21 Werktage)' => 'Κανονική παράδοση (8-21 εργάσιμες ημέρες)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Ασφάλιση παράδοσης - {symbol}{price} (Εγγυημένη επαναποστολή εάν η παράδοση αποτύχει)', //13
	'Bitte Zahlungsart auswählen' => 'Επιλέξτε τρόπο πληρωμής', //13
	'Zahlungsart' => 'Τρόπος πληρωμής', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Τραπεζική μεταφορά', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Αριθμός πιστωτικής κάρτας', //13
	'Gültig bis' => 'Λήξη πιστωτικής κάρτας', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Ολοκληρωμένη παραγγελία', //13
	'Allg. Geschäftsbedingungen' => 'Γενικοί Όροι Συναλλαγών', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Γενικοί Όροι Συναλλαγής: Δια του παρόντος, επιβεβαιώνω ότι όλα τα προσωπικά μου δεδομένα (ή τα στοιχεία) είναι σωστά, έχω διαβάσει την εξήγηση και κατανοώ τους όρους τους.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);