<?php

return array(

		"Afghanistan" => "Αφγανιστάν",
		"Albanien" => "Αλβανία",
		"Andorra" => "Ανδόρρα",
		"Argentinien" => "Αργεντινή",
		"Aruba" => "Αρούμπα",
		"Australien" => "Αυστραλία",
		"Österreich" => "Αυστρία",
		"Bahrain" => "Μπαχρέιν",
		"Bangladesch" => "Μπανγκλαντές",
		"Barbados" => "Μπαρμπάντος",
		"Weißrussland" => "Λευκορωσία",
		"Belgien" => "Βέλγιο",
		"Bosnien und Herzegowina" => "Βοσνία και Ερζεγοβίνη",
		"Botswana" => "Μποτσουάνα",
		"Brasilien" => "Βραζιλία",
		"Brunei" => "Μπρουνέι",
		"Bulgarien" => "Βουλγαρία",
		"Burkina Faso" => "Μπουρκίνα Φάσο",
		"Kamerun" => "Καμερούν",
		"Kanada" => "Καναδάς",
		"Chile" => "Χιλή",
		"Costa Rica" => "Κόστα Ρίκα",
		"Kroatien" => "Κροατία",
		"Curacao" => "Κουρασάο",
		"Zypern" => "Κύπρος",
		"Tschechische Republik" => "Τσεχική Δημοκρατία",
		"Dänemark" => "Δανία",
		"Dschibuti" => "Τζιμπουτί",
		"Dominikanische Republik" => "Δομινικανή Δημοκρατία",
		"Ägypten" => "Αίγυπτος",
		"El Salvador" => "Ελ Σαλβαδόρ",
		"Estland" => "Εσθονία",
		"Äthiopien" => "Αιθιοπία",
		"Finnland" => "Φινλανδία",
		"Frankreich" => "Γαλλία",
		"Georgien" => "Γεωργία",
		"Deutschland" => "Γερμανία",
		"Griechenland" => "Ελλάδα",
		"Guam" => "Γκουάμ",
		"Guatemala" => "Γουατεμάλα",
		"Hongkong" => "Χονγκ Κονγκ",
		"Ungarn" => "Ουγγαρία",
		"Island" => "Ισλανδία",
		"Indien" => "Ινδία",
		"Indonesien" => "Ινδονησία",
		"Irak" => "Ιράκ",
		"Irland" => "Ιρλανδία",
		"Israel" => "Ισραήλ",
		"Italien" => "Ιταλία",
		"Elfenbeinküste" => "Ακτή Ελεφαντοστού",
		"Jamaika" => "Τζαμάικα",
		"Japan" => "Ιαπωνία",
		"Jordanien" => "Ιορδανία",
		"Kasachstan" => "Καζακστάν",
		"Kenia" => "Κένυα",
		"Kuwait" => "Κουβέιτ",
		"Lettland" => "Λετονία",
		"Libanon" => "Λίβανος",
		"Liberia" => "Λιβερία",
		"Liechtenstein" => "Λίχτενσταϊν",
		"Litauen" => "Λιθουανία",
		"Luxemburg" => "Λουξεμβούργο",
		"Nordmazedonien" => "Βόρεια Μακεδονία",
		"Malawi" => "Μαλάουι",
		"Malaysia" => "Μαλαισία",
		"Malta" => "Μάλτα",
		"Mexiko" => "Μεξικό",
		"Moldawien" => "Μολδαβία",
		"Monaco" => "Μονακό",
		"Montenegro" => "Μαυροβούνιο",
		"Namibia" => "Ναμίμπια",
		"Nepal" => "Νεπάλ",
		"Niederlande" => "Ολλανδία",
		"Neukaledonien" => "Νέα Καληδονία",
		"Neuseeland" => "Νέα Ζηλανδία",
		"Nigeria" => "Νιγηρία",
		"Nördliche Marianen" => "Βόρειες Μαριάνες Νήσοι",
		"Norwegen" => "Νορβηγία",
		"Oman" => "Ομάν",
		"Pakistan" => "Πακιστάν",
		"Panama" => "Παναμάς",
		"Peru" => "Περού",
		"Philippinen" => "Φιλιππίνες",
		"Polen" => "Πολωνία",
		"Portugal" => "Πορτογαλία",
		"Puerto Rico" => "Πουέρτο Ρίκο",
		"Katar" => "Κατάρ",
		"Reunion" => "Ρεϋνιόν",
		"Rumänien" => "Ρουμανία",
		"Russland" => "Ρωσία",
		"San Marino" => "Άγιος Μαρίνος",
		"Saudi-Arabien" => "Σαουδική Αραβία",
		"Senegal" => "Σενεγάλη",
		"Serbien" => "Σερβία",
		"Singapur" => "Σιγκαπούρη",
		"Slowakei" => "Σλοβακία",
		"Slowenien" => "Σλοβενία",
		"Südafrika" => "Νότια Αφρική",
		"Südkorea" => "Νότια Κορέα",
		"Spanien" => "Ισπανία",
		"Sri Lanka" => "Σρι Λάνκα",
		"Schweden" => "Σουηδία",
		"Schweiz" => "Ελβετία",
		"Taiwan" => "Ταϊβάν",
		"Tansania" => "Τανζανία",
		"Thailand" => "Ταϊλάνδη",
		"Togo" => "Τόγκο",
		"Trinidad und Tobago" => "Τρίνινταντ και Tομπάγκο",
		"Türkei" => "Τουρκία",
		"Uganda" => "Ουγκάντα",
		"Ukraine" => "Ουκρανία",
		"Vereinigte Arabische Emirate" => "Ηνωμένα Αραβικά Εμιράτα",
		"Vereinigtes Königreich" => "Ηνωμένο Βασίλειο",
		"Vereinigte Staaten" => "Ηνωμένες Πολιτείες",
		"Vatikanstadt" => "Βατικανό",
		"Sambia" => "Ζάμπια",
		"Simbabwe" => "Ζιμπάμπουε",

	);