<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Κριτικές πελατών', //13
    'Rezensionen' => 'Κριτικές', //13
    'Alle {count} Bewertungen anzeigen...' => 'Δείτε όλες τις {count} αξιολογήσεις', //13
    'Stern:' => 'αστέρι:', //13
    'Sterne:' => 'αστέρια:', //13 
    'Produkt' => 'Είδος', //13
    'Preis' => 'Τιμή', //13
    'Jetzt kaufen' => ' ', //13
    'Dosierung' => 'Δοσολογία', //13
    'Menge + Bonus' => 'Ποσότητα + Μπόνους', //13
    'Menge' => 'Ποσότητα', //13
    // '' => '',

    'kaufen' => 'Προσθήκη στο καλάθι', //0
    'Eigene Bewertung erstellen' => 'Νέα μαρτυρία', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} από {vote_all} άτομα θεώρησαν χρήσιμη την ακόλουθη κριτική', //41
    'nein' => 'Όχι', //41
    'ja' => 'Ναι', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Ήταν αυτή η κριτική χρήσιμη σε σας;', //41
    'von' => 'από τον', //41

    'Rezension abgeben' => 'Δημιουργήστε τη δική σας κριτική', //42
    'Name' => 'Όνομα', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Τίτλος', //42
    'Bewertung abgeben' => 'Αξιολόγηση ιστοτόπου', //42
    'Webseitenfreundlichkeit' => 'Ιστοσελίδα', //42
    'Versand Zufriedenheit' => 'Αποστολή', //42
    'Preis / Leistung' => 'Τιμή / απόδοση', //42
    'Empfehlung' => 'Σύσταση / Προτάσεις', //42
    'Kundenkommentar' => 'Κριτική πελατών', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Πληκτρολογήστε τους χαρακτήρες που βλέπετε στην εικόνα.', //42
    'Rezension abschicken' => 'Αποστολή κριτικής', //42

);