<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Κύρια σελίδα',//1
    'Über uns' => 'Σχετικά με εμάς', //1
    'F.A.Q.' => 'F.A.Q.', //1
    'Kontakt' => 'Επικοινωνήστε μαζί μας', //1
    'So bestellen Sie' => 'Πως να παραγγείλετε', //1
    'To top' => 'Στην κορυφή',//0
    'Urheberrecht' => 'Πνευματική ιδιοκτησία',//0
    'Alle Rechte vorbehalten' => 'Ολα τα δικαιώματα διατηρούνται',//0
);