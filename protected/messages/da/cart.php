<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Min indkøbskurv', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Opdater og gem', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Tag {tabl_count} ekstra piller til {symbol}{tabl_price} ({symbol}{tabl_price_one} pr. en pille)', //13
    'Aktualisieren' => 'Opdater', //13
    'Gesamtbetrag' => 'Total', // 0
    'Mit Einkauf fortfahren' => 'Fortsætte med at handle', //13
    'Zur Kasse' => 'Gå til kassen', //13
    'Produkt' => 'Produkt', //13
);