<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Fornavn', //13
	'Name' => 'Fulde navn', //14
	'Nachname' => 'Efternavn', //13
	'Strasse / Hausnr' => 'Adresse', //13
	'Postleitzahl' => 'Postnummer', //13
	'Ort' => 'By', //13
	'Land' => 'Land', //13
	'Telefonnummer' => 'Telefonnummer', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Din bestilling', //13
	'Lieferadresse' => 'Forsendelsesinformation', //13
	'Rechnungsadresse' => 'Betalingsinformation', //13
	'Lieferadresse & Rechnungsadresse' => 'Leveringsadresse & faktureringsadresse ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Hvis faktureringsadressen og leveringsadressen stemmer overens, skal du klikke her', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Min indkøbskurv', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Totalbeløb', //13
	'Versandart' => 'Leveringsmåde', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Regelmæssig levering (8-13 hverdage)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Regelmæssig levering (8-21 hverdage)', //0
	'Express Versand (4-7 Werktage)' => 'Hurtig levering (4-7 hverdage)', //0
    'Express Versand (7-13 Werktage)' => 'Hurtig levering (7-13 hverdage)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standard levering (8-21 hverdage)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Leveringsforsikring - {symbol}{price} (Garanteret genforsendelse, hvis leveringen mislykkedes)', //13
	'Bitte Zahlungsart auswählen' => 'Vælg betalingsmetode', //13
	'Zahlungsart' => 'Betalingstype', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bankoverførsel', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Kortnummer', //13
	'Gültig bis' => 'Kortets udløbsdato', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Fuldfør bestillingen', //13
	'Allg. Geschäftsbedingungen' => 'Generelle transaktionsbetingelser', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Jeg bekræfter hermed, at alle mine personlige data (eller personlige oplysninger) er korrekte. Jeg har læst forklaringen og forstå vilkårene for den.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);