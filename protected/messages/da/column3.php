<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Vigtig information', //1
    'Lieferkonditionen' => 'Leveringsbetingelser', //1
    'Rückerstattung' => 'Tilbagebetaling', //1
    'Affiliates' => 'Partnere', //1
    'AGB' => 'Vilkår og betingelser', //1
    'Datenschutz' => 'Fortrolighedspolitik', //1
    'Impressum' => 'Juridiske oplysninger', //1
    'Garantie' => 'Garanti', //1
    'Warenkorb' => 'Min indkøbskurv', //1
    'Kein Produkt im Warenkorb' => 'Ingen varer i min indkøbskurv', //1
    'Gesamtbetrag' => 'Samlet bestilling', //1
    '1 Produkt im Warenkorb' => '1 vare i min indkøbskurv', //1
    'Produkte im Warenkorb' => 'varer i min indkøbskurv', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Betalingsmuligheder', //1
    'VERSANDPARTNER' => 'Leveringsmåde', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);