<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Kundeanmeldelser', //13
    'Rezensionen' => 'Anmeldelser', //13
    'Alle {count} Bewertungen anzeigen...' => 'Se alle {count} anmeldelser', //13
    'Stern:' => ' Stjerne:', //13
    'Sterne:' => 'Stjerner:', //13 
    'Produkt' => 'Artikel', //13
    'Preis' => 'Pris', //13
    'Jetzt kaufen' => 'Bestil nu', //13
    'Dosierung' => 'Dosering', //13
    'Menge + Bonus' => 'Antal + bonus', //13
    'Menge' => 'Antal', //13
    // '' => '',

    'kaufen' => 'Tilføj til kurv', //0
    'Eigene Bewertung erstellen' => 'Ny vidnesbyrd', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} ud af {vote_all} mennesker fandt følgende anmeldelse nyttig', //41
    'nein' => 'Nej', //41
    'ja' => 'Ja', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Var denne anmeldelse nyttig for dig?', //41
    'von' => 'fra', //41

    'Rezension abgeben' => 'Opret din egen anmeldelse', //42
    'Name' => 'Navn', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Titel', //42
    'Bewertung abgeben' => 'Webstedsvurdering', //42
    'Webseitenfreundlichkeit' => 'Hjemmeside', //42
    'Versand Zufriedenheit' => 'Levering', //42
    'Preis / Leistung' => 'Pris / Præstation', //42
    'Empfehlung' => 'Henstilling', //42
    'Kundenkommentar' => 'Kundeanmeldelse', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Indtast de tegn, du ser på billedet.', //42
    'Rezension abschicken' => 'Send meddelelsen', //42

);