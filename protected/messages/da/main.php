<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Startsiden',//1
    'Über uns' => 'Om os', //1
    'F.A.Q.' => 'Ofte stillede spørgsmål', //1
    'Kontakt' => 'Kontakt os', //1
    'So bestellen Sie' => 'Hvordan bestiller jeg', //1
    'To top' => 'Til toppen',//0
    'Urheberrecht' => 'Ophavsret',//0
    'Alle Rechte vorbehalten' => 'Alle rettigheder forbeholdes',//0
);