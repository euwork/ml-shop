<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Tak for din bestilling', //14
	'Bestellnummer' => 'Bestillingnummer',//14
    'Bestellte Produkte' => 'Bestilte produkter',//14
    'Versendungsart' => 'Leveringsmåde',//14
    'Versicherung' => 'Forsikring',//14
    'Telefonnummer' => 'Telefonnummer',//14
    'Drucken' => 'Udskriv',//14

    'Bankverbindung' => 'Bank detaljer',//14
    'Empfänger' => 'Modtager',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Betalingsformål',//14
    'Betrag' => 'Beløb', //14
    'Bank' => 'Bank', //14
    'Adresse' => 'Adresse', //14
    // '' => '',


);