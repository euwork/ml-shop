<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Το Καλάθι μου', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Αναβάθμιση και αποθήκευση', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Πάρτε {tabl_count} επιπλέον χάπια για {symbol}{tabl_price} ({symbol}{tabl_price_one} ανά χάπι)', //13
    'Aktualisieren' => 'Αναβάθμιση', //13
    'Gesamtbetrag' => 'Σύνολο', // 0
    'Mit Einkauf fortfahren' => 'Συνεχίστε τις αγορές', //13
    'Zur Kasse' => 'Τέλος παραγγελίας', //13
    'Produkt' => 'Προϊόν', //13
);