<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Χρήσιμες Πληροφορίες', //1
    'Lieferkonditionen' => 'Όροι Παράδοσης', //1
    'Rückerstattung' => 'Επιστροφή Χρημάτων', //1
    'Affiliates' => 'Συνεργάτες', //1
    'AGB' => 'Όροι και Προϋποθέσεις', //1
    'Datenschutz' => 'Πολιτική Aπορρήτου', //1
    'Impressum' => 'Imprint', //1
    'Garantie' => 'Eγγύηση', //1
    'Warenkorb' => 'Το καλάθι μου', //1
    'Kein Produkt im Warenkorb' => 'Δεν υπάρχουν προϊόντα στο καλάθι μου', //1
    'Gesamtbetrag' => 'Σύνολο παραγγελίας', //1
    '1 Produkt im Warenkorb' => '1 αντικείμενο στο καλάθι μου', //1
    'Produkte im Warenkorb' => 'αντικείμενα στο καλάθι μου', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Επιλογές πληρωμών', //1
    'VERSANDPARTNER' => 'Μέθοδοι αποστολής', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);