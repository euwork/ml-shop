<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Ευχαριστούμε για την παραγγελία σας.', //14
	'Bestellnummer' => 'Αριθμός παραγγελίας.',//14
    'Bestellte Produkte' => 'Παραγγελία προϊόντων',//14
    'Versendungsart' => 'Μέθοδος παράδοσης',//14
    'Versicherung' => 'Ασφάλεια',//14
    'Telefonnummer' => 'Τηλεφωνικός αριθμός',//14
    'Drucken' => 'Εκτύπωση',//14

    'Bankverbindung' => 'Στοιχεία τράπεζας',//14
    'Empfänger' => 'Δικαιούχος',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Σκοπός πληρωμής',//14
    'Betrag' => 'Ποσό', //14
    'Bank' => 'Τράπεζα', //14
    'Adresse' => 'Διεύθυνση', //14
    // '' => '',


);