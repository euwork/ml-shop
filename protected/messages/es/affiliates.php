<?php

return array(
	'Persönliche Details' => 'Datos personales',
	'Vorname' => 'Nombre',
	'Email' => 'E-mail',
	'Webseite' => 'Página web',
	'Nachricht' => 'Mensaje',
	'Geben Sie die Zeichen in dem Bild ein' => 'Introduce símbolos que ve en la imagen',
	'Senden' => 'Enviar',
	);