<?php

return array(
    'Warenkorb' => 'Mi carrito',
    'Zum Warenkorb hinzufügen' => 'Añadir a la cesta',
    'Jetzt Rabatt sichern!' => 'Elija',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => ' {tabl_count} pastillas adicionales por {symbol} {tabl_price} ({symbol}{tabl_price_one} por una pastilla)',
    'Aktualisieren' => 'Actualizar',
    'Gesamtbetrag' => 'Total',
    'Mit Einkauf fortfahren' => 'Continuar comprando',
    'Zur Kasse' => 'CONTINUAR',
    'Produkt' => 'Producto',
);