<?php


return array(
    // 'Artikel' => 'Artículos',
    'Nützliche Informationen' => 'Información Util',
    'Lieferkonditionen' => 'Politica de envió',
    'Rückerstattung' => 'Politica de Reembolso',
    'Affiliates' => 'Programa de afiliación',
    'AGB' => 'Responsabilidad Legal',
    'Datenschutz' => 'Política de Privacidad',
    'Impressum' => 'Información de contacto',
    'Garantie' => 'Nuestra Garantía',
    'Warenkorb' => 'Mi carrito',
    'Kein Produkt im Warenkorb' => 'No hay productos en mi carrito',
    'Gesamtbetrag' => 'Total del pedido',
    '1 Produkt im Warenkorb' => '1 producto en mi carrito',
    'Produkte im Warenkorb' => 'productos en mi carrito',
    // 'Angebot' => 'Ofertas',
    'kostenloser online support' => 'apoyo en línea gratis',
    // 'Vorteile' => 'Ventajas',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Métodos de pago',
    'VERSANDPARTNER' => 'Formas de Envio',
    'Secure shopping protected by SSL' => 'Compras seguras protegidas por SSL',
);