<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Escriba los caracteres que ve en la imagen',
	'Name'=> 'Su nombre',
	'E-Mail'=> 'E-mail',
	'Betreff'=> 'Asunto',
	'Nachricht'=> 'Mensaje',
	'Kontakt' => 'Contáctenos',
	'Senden'=> 'Enviar',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => '¡Gracias! ¡Hemos recibido su mensaje y en breve nos ponemos en contacto con Ud.!',
	'Captcha' => 'Código de imagen',

	);