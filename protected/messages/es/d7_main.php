<?php


return array(
	'Ihr Warenkorb ist leer' => 'No hay productos en mi carrito',
	'Navigation' => 'Navegación',
	'Top Produkte' => 'Mejores productos',
	'Top Produkt' => 'TOP product',
	'Verfügbarkeit' => 'Existencia',
	'Lieferung' => 'Entrega',
	'Zahlungsart' => 'Pago',
	'Ja' => 'Si',
	'Werktage' => 'días',
	'Kreditkarten, Banküberweisung' => 'tarjetas bancarias, transferencia bancaria',
);