<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Disfunción eréctil tratamiento',
	'Potenzmittel rezeptfrei' => 'Disfunción eréctil tratamiento',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Viagra sin receta. Cialis sin receta. Kamagra en España',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => 'Viagra, Cialis, Kamagra, Levitra sin receta online en España',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Lovegra - Viagra Femenina',
	'Viagra für die Frau rezeptfrei' => 'Lovegra - Viagra Femenina',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Lovegra - Viagra Femenina en España sin receta',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Viagra Femenina - Lovegra sin receta en {domain}',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Xenical para adelgazar, Lida Daidaihua en España',
	'Lida Daidaihua, Xenical tabletten' => 'Medicamentos para adelgazar (Lida, Xenical)',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Xenical para adelgazar, Lida Daidaihua en España',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Pastillas para adelgazar Xenical y Lida Daidaihua en España',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Propecia Genérico - Finasteride 1mg en España',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Propecia Genérico (Finasteride 1mg)',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Xenical para adelgazar, Lida Daidaihua en España',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Genérico (Finasteride 1mg) en España sin receta',
	
	'Potenzmittel rezeptfrei - {domain}' => '{domain} en España',
	'Potenzmittel rezeptfrei.' => '{domain} en España',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => 'Viagra, Levitra, Kamagra, Cialis sin receta en España',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => 'Una Navegación Fácil de Uso Intuitivo, Una Compra es Sencilla y Segura, Un envío rápido en {domain}',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);