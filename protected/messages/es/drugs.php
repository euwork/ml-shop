<?php


return array(
    'Kundenrezensionen' => 'opiniones de clientes',
    'Rezensionen' => 'opiniones',
    'Alle {count} Bewertungen anzeigen...' => 'Consultar las {count} opiniones de clientes',
    'Stern:' => 'estrella:',
    'Sterne:' => 'estrellas:',
    'Produkt' => 'Producto',
    'Preis' => 'Precio',
    'Jetzt kaufen' => 'Comprar Ahora',
    'Dosierung' => 'Dosis',
    'Menge + Bonus' => 'Cantidad + Bonificación',
    'Menge' => 'Cantidad',
    // '' => '',

    'kaufen' => 'Comprar',
    'Eigene Bewertung erstellen' => 'Crear mi opinión',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} de {vote_all} personas piensan que la opinión es útil.',
    'nein' => 'no',
    'ja' => 'sí',
    'Hat Ihnen diese Bewertung geholfen?' => '¿Esta opinión te ha parecido útil?',
    'von' => 'Por',

    'Rezension abgeben' => 'Sus detalles',
    'Name' => 'Nombre',
    'Email' => 'Correo Electrónico',
    'Thema' => 'Tema',
    'Bewertung abgeben' => 'Valore su Sitio',
    'Webseitenfreundlichkeit' => 'Sitio web',
    'Versand Zufriedenheit' => 'Envío Satisfacción',
    'Preis / Leistung' => 'Precio / Rendimiento',
    'Empfehlung' => 'Recomendación',
    'Kundenkommentar' => 'Opinión del cliente',
    'Geben Sie die Zeichen in dem Bild ein' => 'Escriba los caracteres que ve en la imagen',
    'Rezension abschicken' => 'Comentario enviar',

);