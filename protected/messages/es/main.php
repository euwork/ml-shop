<?php

return array(
    'Startseite' => 'Inicio',
    'Über uns' => 'Acerca de Nosotros',
    'F.A.Q.' => 'PP. FF.',
    'Kontakt' => 'Contáctenos',
    'So bestellen Sie' => 'Cómo hacer pedidos',
    'To top' => 'top',
    'Urheberrecht' => 'Propiedad intelectual',
    'Alle Rechte vorbehalten' => 'Todos los derechos reservados',
);