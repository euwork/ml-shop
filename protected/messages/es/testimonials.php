<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => '¡Gracias por su opinión! Se publicará en nuestro sitio después de verificación por el moderador',
	'Geben Sie die Zeichen in dem Bild ein!' => '¡Por favor, indique el código de imagen!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => '¿Está seguro de que quiere enviar la opinión vacía?',
	);