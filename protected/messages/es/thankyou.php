<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Muchas gracias por su pedido',
	'Bestellnummer' => 'Número de pedido',
    'Bestellte Produkte' => 'Productos pedidos',
    'Versendungsart' => '',
    'Versicherung' => 'Seguro',
    'Telefonnummer' => 'Numéro de téléphone',
    'Drucken' => 'Impresión',

    'Bankverbindung' => 'Requisitos bancarios',
    'Empfänger' => 'Destinatario',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'País de destino',
    'Verwendungszweck' => 'Razón de pago',
    'Betrag' => 'Total',
    'Bank' => 'Banco',
    'Adresse' => 'Dirección',
    // '' => '',


);