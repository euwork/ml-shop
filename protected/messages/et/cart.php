<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Minu ostukorv', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Uuendage ja salvestage', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Võtke {tabl_count} lisatabletti hinnaga {symbol}{tabl_price} ({symbol}{tabl_price_one} pilli kohta)', //13
    'Aktualisieren' => 'Uuenda', //13
    'Gesamtbetrag' => 'Kokku', // 0
    'Mit Einkauf fortfahren' => 'Jätka ostlemist', //13
    'Zur Kasse' => 'Telli', //13
    'Produkt' => 'Toode', //13
);