<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-post', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Eesnimi', //13
	'Name' => 'Täisnimi', //14
	'Nachname' => 'Perekonnanimi', //13
	'Strasse / Hausnr' => 'Aadress', //13
	'Postleitzahl' => 'Postiindeks', //13
	'Ort' => 'Linn', //13
	'Land' => 'Riik', //13
	'Telefonnummer' => 'Telefoninumber', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Sinu tellimus', //13
	'Lieferadresse' => 'Saadetise teave', //13
	'Rechnungsadresse' => 'Arveldusinfo', //13
	'Lieferadresse & Rechnungsadresse' => 'Tarneaadress ja arveldusaadress ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Kui arveldusaadress ja saatmisaadress kattuvad, klõpsake siin', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Minu ostukorv', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Kogu summa', //13
	'Versandart' => 'Saatmisviis', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Regulaarne saatmine (8-13 tööpäeva)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Regulaarne saatmine (8-21 tööpäeva)', //0
	'Express Versand (4-7 Werktage)' => 'Kiirlaadimine (4-7 tööpäeva)', //0
    'Express Versand (7-13 Werktage)' => 'Kiirlaadimine (7-13 tööpäeva)', //00
    'Standardlieferung (8-21 Werktage)' => 'Tavaline kohaletoimetamine (8-21 tööpäeva)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Tarnekindlustus - {symbol}{price} (garanteeritud edasisaatmine, kui kohaletoimetamine ebaõnnestus)', //13
	'Bitte Zahlungsart auswählen' => 'Valige makseviis', //13
	'Zahlungsart' => 'Makse tüüp', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Pangaülekanne', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Krediit kaardi number', //13
	'Gültig bis' => 'Krediitkaardi aegumine', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Täielik tellimus', //13
	'Allg. Geschäftsbedingungen' => 'Tehingu üldtingimused', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Üldised tehingutingimused: kinnitan, et kõik minu isikuandmed (või üksikasjad) on õiged, olen selgituse läbi lugenud ja nende tingimustest aru saanud.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);