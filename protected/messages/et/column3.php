<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Kasulik informatsioon', //1
    'Lieferkonditionen' => 'Tarnetingimused', //1
    'Rückerstattung' => 'Tagasimakse', //1
    'Affiliates' => 'Sidusettevõtted', //1
    'AGB' => 'Tingimused', //1
    'Datenschutz' => 'Privaatsuspoliitika', //1
    'Impressum' => 'Jäljend', //1
    'Garantie' => 'Garantii', //1
    'Warenkorb' => 'Minu ostukorv', //1
    'Kein Produkt im Warenkorb' => 'Minu ostukorvis pole ühtegi toodet', //1
    'Gesamtbetrag' => 'Kokku', //1
    '1 Produkt im Warenkorb' => '1 toode minu ostukorvis', //1
    'Produkte im Warenkorb' => 'toodet minu ostukorvis', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Maksevõimalused', //1
    'VERSANDPARTNER' => 'Saatmismeetodid', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);