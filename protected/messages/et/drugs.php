<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Klientide arvustused', //13
    'Rezensionen' => 'ülevaadet', //13
    'Alle {count} Bewertungen anzeigen...' => 'Vaadake kõiki {count} arvustust', //13
    'Stern:' => 'tärn:', //13
    'Sterne:' => 'tärni:', //13 
    'Produkt' => 'Üksus', //13
    'Preis' => 'Hind', //13
    'Jetzt kaufen' => 'Telli nüüd', //13
    'Dosierung' => 'Annustamine', //13
    'Menge + Bonus' => 'Kogus + boonus', //13
    'Menge' => 'Kogus', //13
    // '' => '',

    'kaufen' => 'Lisa ostukorvi', //0
    'Eigene Bewertung erstellen' => 'Uus iseloomustus', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} - {vote_all} inimesest leidis, et järgmine arvustus oli kasulik', //41
    'nein' => 'Ei', //41
    'ja' => 'Jah', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Kas see arvustus oli teile kasulik?', //41
    // 'von' => 'lt', //41
    'von' => ' ', //41

    'Rezension abgeben' => 'Looge oma arvustus', //42
    'Name' => 'Nimi', //42
    'Email' => 'E-post', //42
    'Thema' => 'Pealkiri', //42
    'Bewertung abgeben' => 'Veebisaidi hinnang', //42
    'Webseitenfreundlichkeit' => 'Veebisait', //42
    'Versand Zufriedenheit' => 'Saatmine', //42
    'Preis / Leistung' => 'Hind / jõudlus', //42
    'Empfehlung' => 'Soovitus', //42
    'Kundenkommentar' => 'Kliendi ülevaade', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Sisestage tähemärgid, mida näete pildil.', //42
    'Rezension abschicken' => 'Saada arvustus', //42

);