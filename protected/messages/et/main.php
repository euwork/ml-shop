<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Pealeht',//1
    'Über uns' => 'Meist', //1
    'F.A.Q.' => 'F.A.Q.', //1
    'Kontakt' => 'Võta meiega ühendust', //1
    'So bestellen Sie' => 'Kuidas tellida', //1
    'To top' => 'Tagasi algusesse',//0
    'Urheberrecht' => 'Autoriõigus',//0
    'Alle Rechte vorbehalten' => 'Kõik õigused kaitstud',//0
);