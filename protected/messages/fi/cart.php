<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Ostoskorini', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Päivitä ja säästä', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Ota {tabl_count} ylimääräistä pilleriä hintaan {symbol}{tabl_price} ({symbol}{tabl_price_one} per pilleri)', //13
    'Aktualisieren' => 'Päivitä', //13
    'Gesamtbetrag' => 'Yhteensä', // 0
    'Mit Einkauf fortfahren' => 'Jatka ostoksia', //13
    'Zur Kasse' => 'Siirry kassalle', //13
    'Produkt' => 'Tuote', //13
);