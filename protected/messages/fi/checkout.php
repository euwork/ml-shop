<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'Sähköposti', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Etunimi', //13
	'Name' => 'Koko nimi', //14
	'Nachname' => 'Sukunimi', //13
	'Strasse / Hausnr' => 'Osoite', //13
	'Postleitzahl' => 'Postinumero', //13
	'Ort' => 'Kaupunki', //13
	'Land' => 'Maa', //13
	'Telefonnummer' => 'Puhelinnumero', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Tilauksesi', //13
	'Lieferadresse' => 'Toimitustiedot', //13
	'Rechnungsadresse' => 'Laskutustiedot', //13
	'Lieferadresse & Rechnungsadresse' => 'Toimitusosoite & laskutusosoite ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Jos laskutusosoite ja toimitusosoite täsmäävät, napsauta tästä', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Ostoskorini', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Kokonaismäärä', //13
	'Versandart' => 'Toimitustapa', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Tavallinen toimitus (8-13 arkipäivää)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Tavallinen toimitus (8-21 arkipäivää)', //0
	'Express Versand (4-7 Werktage)' => 'Pikakuljetus (4-7 arkipäivää)', //0
    'Express Versand (7-13 Werktage)' => 'Pikakuljetus (7-13 arkipäivää)', //00
    'Standardlieferung (8-21 Werktage)' => 'Tavallinen toimitus (8-21 arkip&auml;iv&auml;&auml;)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Toimitusvakuutus - {symbol}{price} (taattu uudelleenlähetys, jos toimitus epäonnistuu)', //13
	'Bitte Zahlungsart auswählen' => 'Valitse maksutapa', //13
	'Zahlungsart' => 'Maksutapa', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Pankkisiirto', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Luottokortin numero', //13
	'Gültig bis' => 'Luottokortin voimassaoloaika', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Täydellinen tilaus', //13
	'Allg. Geschäftsbedingungen' => 'Yleiset transaktioehdot', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Yleiset transaktioehdot: Olen lukenut selityksen ja ymmärrän sen ehdot.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);