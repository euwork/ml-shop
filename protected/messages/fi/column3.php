<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Hyödyllistä tietoa', //1
    'Lieferkonditionen' => 'Toimitusehdot', //1
    'Rückerstattung' => 'Palautus', //1
    'Affiliates' => 'Kumppanuusohjelma', //1
    'AGB' => 'Käyttöehdot ja edellytykset', //1
    'Datenschutz' => 'Tietosuojakäytäntö', //1
    'Impressum' => 'Jätä jälki', //1
    'Garantie' => 'Takuu', //1
    'Warenkorb' => 'Ostoskori', //1
    'Kein Produkt im Warenkorb' => 'Ostoskorissani ei ole tuotteita', //1
    'Gesamtbetrag' => 'Tilaus yhteensä', //1
    '1 Produkt im Warenkorb' => '1 tuote ostoskorissani', //1
    'Produkte im Warenkorb' => 'tuotetta ostoskorissani', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Maksuvaihtoehdot', //1
    'VERSANDPARTNER' => 'Toimitustavat', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);