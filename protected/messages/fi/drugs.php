<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Asiakkaiden arvostelut', //13
    'Rezensionen' => 'Arvostelua', //13
    'Alle {count} Bewertungen anzeigen...' => 'Katso kaikki {count} arvostelua', //13
    'Stern:' => 'tähti:', //13
    'Sterne:' => 'tähteä:', //13 
    'Produkt' => 'Tuote', //13
    'Preis' => 'Hinta', //13
    'Jetzt kaufen' => 'Tilaa nyt', //13
    'Dosierung' => 'Annostus', //13
    'Menge + Bonus' => 'Määrä + Bonus', //13
    'Menge' => 'Määrä', //13
    // '' => '',

    'kaufen' => 'Lisää ostoskoriin', //0
    'Eigene Bewertung erstellen' => 'Uusi suosittelu', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} / {vote_all}:sta ihmisestä seuraava arvostelu oli hyödyllinen', //41
    'nein' => 'Ei', //41
    'ja' => 'Kyllä', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Oliko tästä arvostelusta sinulle hyötyä?', //41
    'von' => 'from', //41

    'Rezension abgeben' => 'Luo oma arvostelu', //42
    'Name' => 'Nimi', //42
    'Email' => 'Sähköposti', //42
    'Thema' => 'Otsikko', //42
    'Bewertung abgeben' => 'Verkkosivuston luokitus', //42
    'Webseitenfreundlichkeit' => 'Verkkosivusto', //42
    'Versand Zufriedenheit' => 'Toimitus', //42
    'Preis / Leistung' => 'Hinta / Suorituskyky', //42
    'Empfehlung' => 'Suositus', //42
    'Kundenkommentar' => 'Asiakasarviointi', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Kirjoita kuvassa näkyvät merkit.', //42
    'Rezension abschicken' => 'Lähetä arvostelu', //42

);