<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Etusivu',//1
    'Über uns' => 'Tietoa meistä', //1
    'F.A.Q.' => 'F.A.Q.', //1
    'Kontakt' => 'Ota yhteyttä', //1
    'So bestellen Sie' => 'Miten tehdä tilauksen', //1
    'To top' => 'Alkuun',//0
    'Urheberrecht' => 'Tekijänoikeus',//0
    'Alle Rechte vorbehalten' => 'Kaikki oikeudet pidätetään',//0
);