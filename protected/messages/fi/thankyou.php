<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Kiitos tilauksestanne.', //14
	'Bestellnummer' => 'Tilausnumero',//14
    'Bestellte Produkte' => 'Tilatut tuotteet',//14
    'Versendungsart' => 'Toimitustapa ',//14
    'Versicherung' => 'Vakuutus',//14
    'Telefonnummer' => 'Puhelinnumero',//14
    'Drucken' => 'Tulosta',//14

    'Bankverbindung' => 'Pankkiyhteydet',//14
    'Empfänger' => 'Edunsaaja',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Maksun tarkoitus',//14
    'Betrag' => 'Summa', //14
    'Bank' => 'Pankki', //14
    'Adresse' => 'Osoite', //14
    // '' => '',


);