<?php

return array(
    'Warenkorb' => 'Panier',
    'Zum Warenkorb hinzufügen' => 'Enlever cet élément',
    'Jetzt Rabatt sichern!' => 'Sélectionnez',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => '{tabl_count} comprimés supplémentaires pour {symbol}{tabl_price} ({symbol}{tabl_price_one} par comprimé)',
    'Aktualisieren' => 'Rénover',
    'Gesamtbetrag' => 'Montant Total',
    'Mit Einkauf fortfahren' => 'Retour à la boutique',
    'Zur Kasse' => 'Procéder à la commande',
    'Produkt' => 'Produit',
);