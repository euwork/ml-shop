<?php


return array(
    'Kundenrezensionen' => 'commentaires client',
    'Rezensionen' => 'évaluations',
    'Alle {count} Bewertungen anzeigen...' => 'Afficher les {count} commentaires client...',
    'Stern:' => 'étoile:',
    'Sterne:' => 'étoiles:',
    'Produkt' => 'Produit',
    'Preis' => 'Prix',
    'Jetzt kaufen' => 'Commander',
    'Dosierung' => 'Dosage',
    'Menge + Bonus' => 'Quantité + Bonus',
    'Menge' => 'Quantité',
    // '' => '',

    'kaufen' => 'AJOUTER AU PANIER',
    'Eigene Bewertung erstellen' => 'Créer votre propre commentaire',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} internautes sur {vote_all} ont trouvé ce commentaire utile',
    'nein' => 'Non',
    'ja' => 'Oui',
    'Hat Ihnen diese Bewertung geholfen?' => 'Avez-vous trouvé ce commentaire utile ?',
    'von' => 'Par',

    'Rezension abgeben' => 'Créer votre propre commentaire',
    'Name' => 'Nom',
    'Email' => 'Email',
    'Thema' => 'Sujet',
    'Bewertung abgeben' => 'évaluation',
    'Webseitenfreundlichkeit' => 'Notre site',
    'Versand Zufriedenheit' => 'Satisfaction livraison',
    'Preis / Leistung' => 'Prix ​​/ Performance',
    'Empfehlung' => 'Recommandation',
    'Kundenkommentar' => 'Votre commentaire',
    'Geben Sie die Zeichen in dem Bild ein' => 'Saisissez les caractères apparaissant dans l\'image.',
    'Rezension abschicken' => 'Commentaire envoyer',

);