<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Merci de votre avis! Il sera sur notre site après la vérification du modérateur',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Veuillez écrire le code de l\'image!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Vous êtes sûr que vous voulez envoyer l\'avis vide?',
	);