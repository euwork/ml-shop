<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Merci beaucoup de votre commande',
	'Bestellnummer' => 'Numéro de commande',
    'Bestellte Produkte' => 'Produits commandés',
    'Versendungsart' => 'Mode de livraison',
    'Versicherung' => 'Assurance',
    'Telefonnummer' => 'Numéro de téléphone',
    'Drucken' => 'Imprimer',

    'Bankverbindung' => 'Coordonnées bancaires',
    'Empfänger' => 'Destinataire',
    'IBAN' => 'Identification banque IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'Pays de destination',
    'Verwendungszweck' => 'Destination de paiement',
    'Betrag' => 'Montant',
    'Bank' => 'Banque',
    'Adresse' => 'Adresse',
    // '' => '',


);