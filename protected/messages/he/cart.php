<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'העגלה שלי', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'שדרג ושמור', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Take {tabl_count} extra pills for {symbol}{tabl_price} ({symbol}{tabl_price_one} per pill)', //13 
    'Aktualisieren' => 'שדרוג', //13
    'Gesamtbetrag' => 'סך הכל', // 0
    'Mit Einkauf fortfahren' => 'המשך בקניות', //13
    'Zur Kasse' => 'לבדוק', //13
    'Produkt' => 'מוצר', //13
);