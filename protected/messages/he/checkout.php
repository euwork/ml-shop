<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'אימייל', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'שם פרטי', //13
	'Name' => 'שם מלא', //14
	'Nachname' => 'שם משפחה', //13
	'Strasse / Hausnr' => 'כתובת', //13
	'Postleitzahl' => 'מיקוד', //13
	'Ort' => 'עיר', //13
	'Land' => 'מדינה', //13
	'Telefonnummer' => 'מספר טלפון', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'ההזמנה שלך', //13
	'Lieferadresse' => 'פרטי משלוח', //13
	'Rechnungsadresse' => 'מידע על חיוב', //13
	'Lieferadresse & Rechnungsadresse' => 'כתובת למשלוח וכתובת חיוב ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'If the billing address and shipping address match, click here', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'העגלה שלי', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'סכום כללי', //13
	'Versandart' => 'שיטת משלוח', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Regular Shipping (13-8 business days)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Regular Shipping (21-8 business days)', //0
	'Express Versand (4-7 Werktage)' => 'Express Shipping (7-4 business days)', //0
    'Express Versand (7-13 Werktage)' => 'Express Shipping (13-7 business days)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standard delivery (21-8 business days)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Delivery Insurance - {symbol}{price} (Guaranteed reshipment if delivery failed)', //13 
	'Bitte Zahlungsart auswählen' => 'אנא בחר אמצעי תשלום', //13
	'Zahlungsart' => 'סוג תשלום', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'העברה בנקאית', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'מספר כרטיס אשראי ', //13
	'Gültig bis' => 'תפוגת כרטיס אשראי', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'הזמנה מלאה', //13
	'Allg. Geschäftsbedingungen' => 'תנאי עסקה כלליים', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'תנאי עסקה כלליים: בזאת, אני מאשר כי כל הנתונים האישיים (או הפרטים שלי) נכונים, קראתי את ההסבר והבנתי את תנאיו.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);