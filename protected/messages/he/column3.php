<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'מידע שימושי', //1
    'Lieferkonditionen' => 'תנאי משלוח', //1
    'Rückerstattung' => 'הֶחזֵר', //1
    'Affiliates' => 'שותפים', //1
    'AGB' => 'תנאים', //1
    'Datenschutz' => 'מדיניות פרטיות', //1
    'Impressum' => 'חוֹתָם', //1
    'Garantie' => 'להבטיח', //1
    'Warenkorb' => 'העגלה שלי', //1
    'Kein Produkt im Warenkorb' => 'אין פריטים בעגלה שלי', //1
    'Gesamtbetrag' => 'סך כל ההזמנה', //1
    '1 Produkt im Warenkorb' => 'פריט אחד בעגלה שלי', //1
    'Produkte im Warenkorb' => 'פריטים בעגלה שלי', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'אפשרויות תשלום', //1
    'VERSANDPARTNER' => 'שיטות משלוח', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);