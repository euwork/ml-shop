<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'חוות דעת של לקוחות', //13
    'Rezensionen' => '92 ביקורות', //13
    'Alle {count} Bewertungen anzeigen...' => 'ראו את כל {count} הביקורות', //13
    'Stern:' => 'כוכב:', //13
    'Sterne:' => 'כוכבים:', //13 
    'Produkt' => 'פריט', //13
    'Preis' => 'מחיר', //13
    'Jetzt kaufen' => 'הזמן עכשיו', //13
    'Dosierung' => 'מִנוּן', //13
    'Menge + Bonus' => 'כמות + בונוס', //13
    'Menge' => 'ַּכַּמוּת', //13
    // '' => '',

    'kaufen' => 'הוסף לעגלה', //0
    'Eigene Bewertung erstellen' => 'המלצה חדשה', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} of {vote_all} people found the following review helpful', //41
    'nein' => 'לא', //41
    'ja' => 'כן', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'הראיון הזה עזר לך?', //41
    'von' => 'from', //41

    'Rezension abgeben' => 'צור ביקורת משלך', //42
    'Name' => 'שֵׁם', //42
    'Email' => 'אימייל', //42
    'Thema' => 'כותרת', //42
    'Bewertung abgeben' => 'דירוג אתר', //42
    'Webseitenfreundlichkeit' => 'אתר אינטרנט', //42
    'Versand Zufriedenheit' => 'משלוח', //42
    'Preis / Leistung' => 'מחיר / ביצועים', //42
    'Empfehlung' => 'המלצה', //42
    'Kundenkommentar' => 'סקירת לקוחות', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'הקלד את התווים שאתה רואה בתמונה.', //42
    'Rezension abschicken' => 'שלח ביקורת', //42

);