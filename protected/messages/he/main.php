<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'עמוד ראשי',//1
    'Über uns' => 'עלינו', //1
    'F.A.Q.' => 'שאלות נפוצות', //1
    'Kontakt' => 'צור קשר', //1
    'So bestellen Sie' => 'איך להזמין', //1
    'To top' => 'למעלה',//0
    'Urheberrecht' => 'זכויות יוצרים',//0
    'Alle Rechte vorbehalten' => 'כל הזכויות שמורות',//0
);