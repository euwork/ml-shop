<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Hvala na Vašoj Narudžbi.', //14
	'Bestellnummer' => 'Narudžba br.',//14
    'Bestellte Produkte' => 'Proizvodi naručeni',//14
    'Versendungsart' => 'Način isporuke',//14
    'Versicherung' => 'Osiguranje',//14
    'Telefonnummer' => 'Broj telefona',//14
    'Drucken' => 'Ispis',//14

    'Bankverbindung' => 'Bankovni detalji',//14
    'Empfänger' => 'Korisnik',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Svrha naloga za plaćanje',//14
    'Betrag' => 'Iznos', //14
    'Bank' => 'Banka', //14
    'Adresse' => 'Adresa', //14
    // '' => '',


);