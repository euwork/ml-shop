<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'A kosaram', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Frissítsen és mentse', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Vegyen {tabl_count} extra tablettát {symbol}{tabl_price} ({symbol}{tabl_price_one} tablettánként)', //13
    'Aktualisieren' => 'Frissítés', //13
    'Gesamtbetrag' => 'Összes', // 0
    'Mit Einkauf fortfahren' => 'Folytatom a vásárlást', //13
    'Zur Kasse' => 'Tovább a rendeléshez', //13
    'Produkt' => 'Termék', //13
);