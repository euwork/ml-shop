<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Keresztnév', //13
	'Name' => 'Teljes név', //14
	'Nachname' => 'Vezetéknév', //13
	'Strasse / Hausnr' => 'Cím', //13
	'Postleitzahl' => 'Irányítószám', //13
	'Ort' => 'Város', //13
	'Land' => 'Ország', //13
	'Telefonnummer' => 'Telefonszám ', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'A rendelése', //13
	'Lieferadresse' => 'Szállítási információ', //13
	'Rechnungsadresse' => 'Számlázási info', //13
	'Lieferadresse & Rechnungsadresse' => 'Szállítási cím és a számlázási cím ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Ha a számlázási cím és a szállítási cím megegyezik, kattintson ide', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'A kosaram', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Teljes összeg', //13
	'Versandart' => 'Szállítási Mód', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Rendszeres szállítás (8-13 munkanap)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Rendszeres szállítás (8-21 munkanap)', //0
	'Express Versand (4-7 Werktage)' => 'Expressz szállítás (4-7 munkanap)', //0
    'Express Versand (7-13 Werktage)' => 'Expressz szállítás (7-13 munkanap)', //00
    'Standardlieferung (8-21 Werktage)' => 'Normál szállítás (8-21 munkanap)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Szállítási biztosítás - {symbol}{price} (Garantált újraküldés, ha a szállítás sikertelen)', //13
	'Bitte Zahlungsart auswählen' => 'Kérjük, válassza ki a fizetési módot', //13
	'Zahlungsart' => 'Fizetési mód', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Banki átutalás', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Bankkártya száma', //13
	'Gültig bis' => 'Hitelkártya lejárata', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Teljes megrendelés', //13
	'Allg. Geschäftsbedingungen' => 'Általános tranzakciós feltételek', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Általános tranzakciós feltételek: Ezennel megerősítem, hogy minden személyes adatom (vagy részletek) helytálló, elolvastam a magyarázatot és megértettem annak feltételeit.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);