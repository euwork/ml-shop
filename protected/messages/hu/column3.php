<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Hasznos információ', //1
    'Lieferkonditionen' => 'Kiszállítási feltételek', //1
    'Rückerstattung' => 'Visszatérítés', //1
    'Affiliates' => 'Partnerprogram', //1
    'AGB' => 'Felhasználási feltételek', //1
    'Datenschutz' => 'Adatvédelmi irányelvek', //1
    'Impressum' => 'Kimeneti adatok', //1
    'Garantie' => 'Garancia', //1
    'Warenkorb' => 'A kosaram', //1
    'Kein Produkt im Warenkorb' => 'Nincs kosárba helyezett termék', //1
    'Gesamtbetrag' => 'Rendelés összege', //1
    '1 Produkt im Warenkorb' => '1 tétel a kosaramban', //1
    'Produkte im Warenkorb' => 'tétel a kosaramban', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Fizetési lehetőségek', //1
    'VERSANDPARTNER' => 'Szállítási módszerek', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);