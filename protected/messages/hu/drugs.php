<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Vásárlói vélemények', //13
    'Rezensionen' => 'vélemény', //13
    'Alle {count} Bewertungen anzeigen...' => 'Az összes {count} vélemény megtekintése', //13
    'Stern:' => 'csillag:', //13
    'Sterne:' => 'csillag:', //13 
    'Produkt' => 'Tétel', //13
    'Preis' => 'Ár', //13
    'Jetzt kaufen' => 'Rendeljen most', //13
    'Dosierung' => 'Adagolás', //13
    'Menge + Bonus' => 'Mennyiség + bónusz', //13
    'Menge' => 'Mennyiség', //13
    // '' => '',

    'kaufen' => 'Kosárba', //0
    'Eigene Bewertung erstellen' => 'Új ajánlás', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} ember közül {vote_all} találta hasznosnak az alábbi értékelést', //41
    'nein' => 'Nem', //41
    'ja' => 'Igen', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Hasznos volt az ön számára ez az értékelés?', //41
    // 'von' => '-től', //41
    'von' => ' ', //41

    'Rezension abgeben' => 'Készítse el saját véleményét', //42
    'Name' => 'Név', //42
    'Email' => 'Email', //42
    'Thema' => 'Cím', //42
    'Bewertung abgeben' => 'Weboldal értékelése', //42
    'Webseitenfreundlichkeit' => 'Weboldal', //42
    'Versand Zufriedenheit' => 'Szállítás', //42
    'Preis / Leistung' => 'Ár / Teljesítmény', //42
    'Empfehlung' => 'Javaslat', //42
    'Kundenkommentar' => 'Felhasználói értékelés', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Írja be a képen látható karaktereket.', //42
    'Rezension abschicken' => 'Küldje el véleményét', //42

);