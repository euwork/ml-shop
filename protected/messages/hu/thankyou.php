<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Köszönjük a rendelését.', //14
	'Bestellnummer' => 'Rendelési szám.',//14
    'Bestellte Produkte' => 'Rendelt termékek',//14
    'Versendungsart' => 'Szállítási mód',//14
    'Versicherung' => 'Biztosítás',//14
    'Telefonnummer' => 'Telefonszám',//14
    'Drucken' => 'Nyomtatás',//14

    'Bankverbindung' => 'Banki adatok',//14
    'Empfänger' => 'Kedvezményezett',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'A fizetés célja',//14
    'Betrag' => 'Összeg', //14
    'Bank' => 'Bank', //14
    'Adresse' => 'Cím', //14
    // '' => '',


);