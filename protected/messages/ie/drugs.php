<?php


return array(
    'Kundenrezensionen' => 'Customer reviews',
    'Rezensionen' => 'Reviews',
    'Alle {count} Bewertungen anzeigen...' => 'See all {count} reviews',
    'Stern:' => 'Star:',
    'Sterne:' => 'Stars:',
    'Produkt' => 'Item',
    'Preis' => 'Price',
    'Jetzt kaufen' => 'Order Now',
    'Dosierung' => 'Dosage',
    'Menge + Bonus' => 'Quantity + Bonus',
    'Menge' => 'Quantity',
    // '' => '',

    'kaufen' => 'Add to cart',
    'Eigene Bewertung erstellen' => 'New Testimonial',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} of {vote_all} people found the following review helpful',
    'nein' => 'No',
    'ja' => 'Yes',
    'Hat Ihnen diese Bewertung geholfen?' => 'Was this review helpful to you?',
    'von' => 'from',

    'Rezension abgeben' => 'Create your own review',
    'Name' => 'Name',
    'Email' => 'E-mail',
    'Thema' => 'Title',
    'Bewertung abgeben' => 'Website Rating',
    'Webseitenfreundlichkeit' => 'Web site',
    'Versand Zufriedenheit' => 'Shipping',
    'Preis / Leistung' => 'Price / Performance',
    'Empfehlung' => 'Recommendation',
    'Kundenkommentar' => 'Customer review',
    'Geben Sie die Zeichen in dem Bild ein' => 'Type the characters you see in the picture.',
    'Rezension abschicken' => 'Send review',

);