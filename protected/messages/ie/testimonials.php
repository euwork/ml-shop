<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Thanks for your response! It will be published on our site after the moderator\'s verification.',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Please, type the code from the pic!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Are you sure you want to send an empty response?',
	);