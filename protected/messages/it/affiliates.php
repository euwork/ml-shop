<?php

return array(
	'Persönliche Details' => 'Dati personali',
	'Vorname' => 'Nome',
	'Email' => 'Email',
	'Webseite' => 'Pagina web',
	'Nachricht' => 'Messaggio',
	'Geben Sie die Zeichen in dem Bild ein' => 'Digita i caratteri che vedi nell\'immagine',
	'Senden' => 'Inviare',
	);