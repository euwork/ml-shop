<?php

return array(
    'Warenkorb' => 'Il mio carrello',
    'Zum Warenkorb hinzufügen' => 'Aggiungi al carrello',
    'Jetzt Rabatt sichern!' => 'Selezionare',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => ' {tabl_count} compresse supplementari per {symbol} {tabl_price} ({symbol} {tabl_price_one} per compressa)',
    'Aktualisieren' => 'Aggiornare',
    'Gesamtbetrag' => 'L’importo totale',
    'Mit Einkauf fortfahren' => 'Continuare a fare acquisti',
    'Zur Kasse' => 'Effettuare l\'ordine',
    'Produkt' => 'Prodotto',
);