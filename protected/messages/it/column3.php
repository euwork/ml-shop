<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Informazioni utili',
    'Lieferkonditionen' => 'Condizioni di spedizione',
    'Rückerstattung' => 'Rimborso',
    'Affiliates' => 'Affiliati',
    'AGB' => 'Termini e condizioni',
    'Datenschutz' => 'Tutela della privacy',
    'Impressum' => 'Stampa',
    'Garantie' => 'Garanzia',
    'Warenkorb' => 'Il mio carrello',
    'Kein Produkt im Warenkorb' => 'Non ci sono prodotti nel carrello',
    'Gesamtbetrag' => 'L’importo totale',
    '1 Produkt im Warenkorb' => 'Un oggetto nel mio carrello',
    'Produkte im Warenkorb' => 'Prodotti nel carrello',
    // 'Angebot' => 'Offerta',
    'kostenloser online support' => 'supporto online gratuito',
    // 'Vorteile' => 'Vantaggi',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Opzioni di pagamento',
    'VERSANDPARTNER' => 'Metodi di pagamento',
    'Secure shopping protected by SSL' => 'Secure shopping protetto da SSL',
);