<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Introduce símbolos que ve en la imagen',
	'Name'=> 'Nome',
	'E-Mail'=> 'Email',
	'Betreff'=> 'Soggetto',
	'Nachricht'=> 'Messaggio',
	'Kontakt' => 'Contatto',
	'Senden' => 'Inviare',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Grazie! Abbiamo ricevuto il vostro messaggio e vi contatteremo più presto possibile!',
	'Captcha' => 'Inserire',
	);