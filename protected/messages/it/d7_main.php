<?php


return array(
	'Ihr Warenkorb ist leer' => 'Non ci sono prodotti nel carrello',
	'Navigation' => 'Navigazione',
	'Top Produkte' => 'I prodotti migliori',
	'Top Produkt' => 'Prodotto top',
	'Verfügbarkeit' => 'Disponibilità',
	'Lieferung' => 'Consegna',
	'Zahlungsart' => 'Pagamento',
	'Ja' => 'Sì',
	'Werktage' => 'giorni',
	'Kreditkarten, Banküberweisung' => 'carte di credito, bonifico bancario',
);