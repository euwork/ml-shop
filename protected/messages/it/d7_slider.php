<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Viagra, Cialis o Kamagra in Italia',
	'Potenzmittel rezeptfrei' => 'Viagra, Cialis o Kamagra in Italia senza ricetta',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Viagra, Cialis o Kamagra - prodotti di potenziamento maschile in Italia',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => 'Viagra, Cialis o Kamagra senza ricetta in Italia',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Lovegra-Viagra femminile',
	'Viagra für die Frau rezeptfrei' => 'Lovegra-Viagra femminile',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Lovegra - Viagra per le donne in Italia',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Lovegra - Viagra per le donne senza ricetta in Italia in {domain}',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Lida Daidaihua, Xenical Generico 120mg',
	'Lida Daidaihua, Xenical tabletten' => 'Lida Daidaihua, Xenical Generico 120mg',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Xenical Generico 120mg, Lida Daidaihua senza ricetta in Italia',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Xenical Generico senza ricetta in Italia. Lida Daidaihua senza ricetta in {domain}.',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Propecia Generico - Finasteride 1mg',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Propecia Generico (Finasteride 1mg)',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Generico (Finasteride 1mg) senza ricetta in Italia',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Generico (Finasteride 1mg) senza ricetta in {domain} in Italia',
	
	'Potenzmittel rezeptfrei - {domain}' => '{domain} - Viagra, Cialis o Levitra senza ricetta',
	'Potenzmittel rezeptfrei.' => 'Viagra, Cialis o Levitra senza ricetta',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => '{domain} - Viagra, Cialis o Levitra senza ricetta',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => 'Viagra, Cialis o Kamagra in Italia online senza ricetta in Italia',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);