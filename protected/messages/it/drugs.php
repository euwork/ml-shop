<?php


return array(
    'Kundenrezensionen' => 'recensioni clienti',
    'Rezensionen' => 'recensioni',
    'Alle {count} Bewertungen anzeigen...' => 'Visualizza tutte e {count} le recensioni clienti...',
    'Stern:' => 'stelle:',
    'Sterne:' => 'stelle:',
    'Produkt' => 'Prodotto',
    'Preis' => 'Prezzo',
    'Jetzt kaufen' => 'Acquistare ora',
    'Dosierung' => 'Dosaggio',
    'Menge + Bonus' => 'Quantità + Bonus',
    'Menge' => 'Quantità',
    // '' => '',

    'kaufen' => 'Aggiungi al carrello',
    'Eigene Bewertung erstellen' => 'Scrivi una recensione',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} di {vote_all} persone hanno trovato utile la seguente recensione.',
    'nein' => 'No',
    'ja' => 'Si',
    'Hat Ihnen diese Bewertung geholfen?' => 'Questa recensione ti è stata utile?',
    'von' => 'di',

    'Rezension abgeben' => 'le referenze',
    'Name' => 'Nome',
    'Email' => 'Email',
    'Thema' => 'Tema',
    'Bewertung abgeben' => 'Feature',
    'Webseitenfreundlichkeit' => 'Voto sito',
    'Versand Zufriedenheit' => 'Envío Satisfacción',
    'Preis / Leistung' => 'Prezzo / Prestazioni',
    'Empfehlung' => 'Raccomandazione',
    'Kundenkommentar' => 'Recensione',
    'Geben Sie die Zeichen in dem Bild ein' => 'Tipi di carattere che vedi nell\'immagine',
    'Rezension abschicken' => 'Inviare commenti',

);