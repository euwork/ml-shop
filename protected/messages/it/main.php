<?php

return array(
    'Startseite' => 'Pagina iniziale',
    'Über uns' => 'Su di noi',
    'F.A.Q.' => 'Domande frequenti',
    'Kontakt' => 'Contattaci',
    'So bestellen Sie' => 'Come ordinare',
    'To top' => 'in alto',
    'Urheberrecht' => 'Diritti d\'autore',
    'Alle Rechte vorbehalten' => 'Tutti i diritti riservati',
);