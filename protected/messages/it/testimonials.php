<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Grazie per la vostra recensione! Sarà pubblicata sul nostro sito web dopo la verifica da parte del moderatore',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Si prega di inserire il codice dall\'immagine!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Siete sicuri di aver volere inviare una recensione vuota?',
	);