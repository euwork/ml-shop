<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Il tuo ordine',
	'Bestellnummer' => 'Numero dell\'ordine',
    'Bestellte Produkte' => 'Prodotti ordinati',
    'Versendungsart' => 'Metodo di consegna',
    'Versicherung' => 'Assicurazione',
    'Telefonnummer' => 'Numero di telefono',
    'Drucken' => 'Stampa',

    'Bankverbindung' => 'Coordinate bancarie',
    'Empfänger' => 'Destinatario',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'Paese destinataria',
    'Verwendungszweck' => 'Causale del pagamento',
    'Betrag' => 'L’importo è di',
    'Bank' => 'Banca',
    'Adresse' => 'Indirizzo',
    // '' => '',


);