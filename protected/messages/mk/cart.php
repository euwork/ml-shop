<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Мојата кошничка', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Надградете и зачувајте', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Земете {tabl_count} дополнителни апчиња за {symbol}{tabl_price} ({symbol}{tabl_price_one} по пилула)', //13
    'Aktualisieren' => 'Надградба', //13
    'Gesamtbetrag' => 'Вкупно', // 0
    'Mit Einkauf fortfahren' => 'Продолжите', //13
    'Zur Kasse' => 'Поставете нарачка', //13
    'Produkt' => 'Производ', //13
);