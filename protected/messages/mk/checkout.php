<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'Е-пошта', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Име', //13
	'Name' => 'Целосно име', //14
	'Nachname' => 'Презиме', //13
	'Strasse / Hausnr' => 'Адреса', //13
	'Postleitzahl' => 'Поштенски / поштенски код', //13
	'Ort' => 'Град', //13
	'Land' => 'Земја', //13
	'Telefonnummer' => 'Телефонски број', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Вашата нарачка', //13
	'Lieferadresse' => 'Информации за испорака', //13
	'Rechnungsadresse' => 'Информации за наплата', //13
	'Lieferadresse & Rechnungsadresse' => 'Адреса за испорака и адреса за наплата ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Ако адресата за наплата и адресата за испорака се совпаѓаат, кликнете тука', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Мојата кошничка', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Вкупна количина', //13
	'Versandart' => 'Метод на испорака', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Редовна испорака (8-13 работен ден)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Редовна испорака (8-21 работен ден)', //0
	'Express Versand (4-7 Werktage)' => 'Експресна испорака (4-7 работни дена)', //0
    'Express Versand (7-13 Werktage)' => 'Експресна испорака (7-13 работни дена)', //00
    'Standardlieferung (8-21 Werktage)' => 'Стандардна испорака (8-21 работен ден)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Осигурување за испорака - {symbol}{price} (Гарантирано испраќање на пратката доколку испораката не успее)', //13
	'Bitte Zahlungsart auswählen' => 'Изберете начин на плаќање', //13
	'Zahlungsart' => 'Начин на плаќање', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Банкарски трансфер', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Број на кредитна картичка', //13
	'Gültig bis' => 'Рок на истекување на кредитната картичка', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Комплетна нарачка', //13
	'Allg. Geschäftsbedingungen' => 'Општи услови за трансакција', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Општи услови за трансакција: Со ова, потврдувам дека сите мои лични податоци (или детали) се точни, го прочитав објаснувањето и ги разбрав неговите термини.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);