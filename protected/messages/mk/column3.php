<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Корисни информации', //1
    'Lieferkonditionen' => 'Услови за испорака', //1
    'Rückerstattung' => 'Наплата', //1
    'Affiliates' => 'Филијали', //1
    'AGB' => 'Правила и услови', //1
    'Datenschutz' => 'Политика за приватност', //1
    'Impressum' => 'Информации', //1
    'Garantie' => 'Гаранција', //1
    'Warenkorb' => 'Мојата кошничка', //1
    'Kein Produkt im Warenkorb' => 'Нема производи во мојата кошничка', //1
    'Gesamtbetrag' => 'Вкупно порачајте', //1
    '1 Produkt im Warenkorb' => '1 производ во мојата кошничка', //1
    'Produkte im Warenkorb' => 'производи во мојата кошничка', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Опции за плаќање', //1
    'VERSANDPARTNER' => 'Методи на испорака', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);