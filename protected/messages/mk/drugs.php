<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Прегледи на клиенти', //13
    'Rezensionen' => 'Прегледи', //13
    'Alle {count} Bewertungen anzeigen...' => 'Погледнете ги сите {count} прегледи', //13
    'Stern:' => 'Sвезда:', //13
    'Sterne:' => 'Sвезди:', //13 
    'Produkt' => 'Предмет', //13
    'Preis' => 'Цена', //13
    'Jetzt kaufen' => 'Нарачајте сега', //13
    'Dosierung' => 'Дозирање', //13
    'Menge + Bonus' => 'Количина + бонус', //13
    'Menge' => 'Квантитет', //13
    // '' => '',

    'kaufen' => 'Додади го во кошничка', //0
    'Eigene Bewertung erstellen' => 'Нов преглед', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => 'За {vote_yes} од {vote_all} лица, следниов преглед е корисен.', //41
    'nein' => 'Не', //41
    'ja' => 'Да', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Дали овој преглед ви беше корисен?', //41
    'von' => 'од', //41

    'Rezension abgeben' => 'Направете ваш сопствен преглед', //42
    'Name' => 'Име', //42
    'Email' => 'Е-пошта', //42
    'Thema' => 'Наслов', //42
    'Bewertung abgeben' => 'Рејтинг на веб-страница', //42
    'Webseitenfreundlichkeit' => 'Веб-страница', //42
    'Versand Zufriedenheit' => 'Испорака', //42
    'Preis / Leistung' => 'Цена / изведба', //42
    'Empfehlung' => 'Препорака', //42
    'Kundenkommentar' => 'Преглед на клиенти', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Внесете ги ликовите што ги гледате на сликата.', //42
    'Rezension abschicken' => 'Испрати преглед', //42

);