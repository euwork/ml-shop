<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Ви благодариме за вашата нарачка.', //14
	'Bestellnummer' => 'Нарачка бр.',//14
    'Bestellte Produkte' => 'Нарачани производи',//14
    'Versendungsart' => 'Начин на достава',//14
    'Versicherung' => 'Осигурување',//14
    'Telefonnummer' => 'Телефонски број',//14
    'Drucken' => 'Печати',//14

    'Bankverbindung' => 'Детали за банката',//14
    'Empfänger' => 'Корисник',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Цел на плаќање',//14
    'Betrag' => 'Износ', //14
    'Bank' => 'Банка', //14
    'Adresse' => 'Адреса', //14
    // '' => '',


);