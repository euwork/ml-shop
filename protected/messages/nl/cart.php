<?php

return array(
    'Warenkorb' => 'Winkelwagen',
    'Zum Warenkorb hinzufügen' => 'Aan mandje toevoegen',
    'Jetzt Rabatt sichern!' => 'Selecteer',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => ' {tabl_count} extra pillen voor {symbol} {tabl_price} ({symbol} {tabl_price_one} per pil)',
    'Aktualisieren' => 'Update',
    'Gesamtbetrag' => 'Totaal',
    'Mit Einkauf fortfahren' => 'Verder winkelen',
    'Zur Kasse' => 'VERDER GAAN',
    'Produkt' => 'Product',
);