<?php


return array(
    // 'Artikel' => 'artikelen',
    'Nützliche Informationen' => 'Bruikbare informatie',
    'Lieferkonditionen' => 'Leveringsvoorwaarden',
    'Rückerstattung' => 'Restitutie',
    'Affiliates' => 'Webmasters',
    'AGB' => 'Voorwaarden en condities',
    'Datenschutz' => 'Privacy Policy',
    'Impressum' => 'Print',
    'Garantie' => 'Garantie',
    'Warenkorb' => 'Winkelwagen',
    'Kein Produkt im Warenkorb' => 'Uw winkelwagen is leeg',
    'Gesamtbetrag' => 'Bestelling totaal',
    '1 Produkt im Warenkorb' => 'Een item in mijn mandje',
    'Produkte im Warenkorb' => 'Items in mijn mandje',
    // 'Angebot' => 'Aanbieding',
    'kostenloser online support' => 'gratis online support',
    // 'Vorteile' => 'Voordelen',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Betalingsmogelijkhedens',
    'VERSANDPARTNER' => 'Verzendmethode',
    'Secure shopping protected by SSL' => 'Veilig winkelen beschermd door SSL',
);