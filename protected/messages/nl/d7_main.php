<?php


return array(
	'Ihr Warenkorb ist leer' => 'Uw winkelwagen is leeg',
	'Navigation' => 'Navigation',
	'Top Produkte' => 'Top produits',
	'Top Produkt' => 'Top produit',
	'Verfügbarkeit' => 'Disponibilité',
	'Lieferung' => 'Livraison',
	'Zahlungsart' => 'Paiement',
	'Ja' => 'Ja',
	'Werktage' => 'Jours ouvrables',
	'Kreditkarten, Banküberweisung' => 'Cartes de crédit, Virement bancaire',
);