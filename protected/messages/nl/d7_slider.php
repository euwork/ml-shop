<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Erectie middelen - Viagra, Cialis, Kamagra',
	'Potenzmittel rezeptfrei' => 'Viagra, Cialis, Kamagra in Nederland',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Erectie middelen (Viagra, Cialis, Kamagra) zonder recept in Nederland',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => '{domain} - erectie middelen (Viagra, Cialis, Kamagra) zonder recept in Nederland',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra voor vrouwen - Lovegra',
	'Viagra für die Frau rezeptfrei' => 'Viagra voor vrouwen - Lovegra',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra voor vrouwen (Lovegra of Womenra)',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Viagra voor vrouwen - Lovegra zonder recept in Nederland',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Lida Daidaihua, Xenical 120mg in Nederland',
	'Lida Daidaihua, Xenical tabletten' => 'Lida Daidaihua, Xenical 120mg',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Lida Daidaihua, Xenical 120mg in Nederland',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Xenical Orlistat 120mg zonder recept in Nederland. Lida Daidaihua.',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Propecia Generiek - Finasteride 1mg',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Propecia Generiek (Finasteride 1mg)',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Finasteride 1mg (Propecia Generiek) in Nederland',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Generiek (Finasteride 1mg) zonder recept in Nederland',
	
	'Potenzmittel rezeptfrei - {domain}' => '{domain} in Nederland',
	'Potenzmittel rezeptfrei.' => '{domain} in Nederland',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => 'Viagra, Cialis, Kamagra zonder recept in Nederland',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => '{domain} in Nederland - Viagra, Cialis, Kamagra zonder recept',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);