<?php


return array(
    'Kundenrezensionen' => 'Klantenbeoordeling',
    'Rezensionen' => 'Recensies',
    'Alle {count} Bewertungen anzeigen...' => 'Bekijk alle {count} Beoordeling',
    'Stern:' => 'Ster:',
    'Sterne:' => 'Sterren:',
    'Produkt' => 'Product',
    'Preis' => 'Prijs',
    'Jetzt kaufen' => 'Bestellen',
    'Dosierung' => 'Dosering',
    'Menge + Bonus' => 'Hoeveelheid + Bonus',
    'Menge' => 'Hoeveelheid',
    // '' => '',

    'kaufen' => 'Voeg toe aan winkelwagen',
    'Eigene Bewertung erstellen' => 'Maak je eigen beoordeling',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} van {vote_all} vond deze recensie bruikbaar',
    'nein' => 'Nee',
    'ja' => 'Ja',
    'Hat Ihnen diese Bewertung geholfen?' => 'Vond u deze recensie nuttig?',
    'von' => 'van',

    'Rezension abgeben' => 'Título',
    'Name' => 'Naam',
    'Email' => 'E-mail',
    'Thema' => 'Onderwerp',
    'Bewertung abgeben' => 'kenmerken',
    'Webseitenfreundlichkeit' => 'Website cijfer',
    'Versand Zufriedenheit' => 'Verzenden Tevredenheid',
    'Preis / Leistung' => 'Prijs / Prestatie',
    'Empfehlung' => 'Aanbeveling',
    'Kundenkommentar' => 'Uw mening',
    'Geben Sie die Zeichen in dem Bild ein' => 'Typt u hier de karakters die u in de afbeelding ziet',
    'Rezension abschicken' => 'Beoordeling opsturen',

);