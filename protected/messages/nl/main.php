<?php

return array(
    'Startseite' => 'Hoofdpagina',//perevod
    'Über uns' => 'Over ons',
    'F.A.Q.' => 'Vragen en antwoorden',
    'Kontakt' => 'Neem contact met ons op',
    'So bestellen Sie' => 'Hoe te bestellen',
    'To top' => 'omhoog',//perevod
    'Urheberrecht' => 'Copyright',
    'Alle Rechte vorbehalten' => 'Alle rechten voorbehouden',
);