<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Bedankt voor je feedback!Het wordt geplaatst op onze website na verificatie door de moderator',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Geef alsjeblieft de code van de foto!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Weet je zeker dat je een leeg review wilt sturen?',
	);