<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Bedankt voor Uw bestelling',
	'Bestellnummer' => 'Bestellingsnummer',
    'Bestellte Produkte' => 'Bestelde artikelen',
    'Versendungsart' => 'Verzendmethode',
    'Versicherung' => 'Verzekering',
    'Telefonnummer' => 'Het telefoon nummer',
    'Drucken' => 'afdruk',

    'Bankverbindung' => 'Bank gegevens',
    'Empfänger' => 'De ontvanger',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'Het land van bestemming',
    'Verwendungszweck' => 'Het doel van betaling',
    'Betrag' => 'Het bedrag',
    'Bank' => 'Bank',
    'Adresse' => 'Adres',
    // '' => '',


);