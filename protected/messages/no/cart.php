<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Min handlekurv', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Oppdater og lagre', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Ta {tabl_count} ekstra piller for {symbol}{tabl_price} ({symbol}{tabl_price_one} per pille)', //13
    'Aktualisieren' => 'Oppdater', //13
    'Gesamtbetrag' => 'Total', // 0
    'Mit Einkauf fortfahren' => 'Fortsett å handle', //13
    'Zur Kasse' => 'Gå til kassen', //13
    'Produkt' => 'Produkt', //13
);