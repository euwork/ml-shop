<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-post', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Fornavn', //13
	'Name' => 'Fullt navn', //14
	'Nachname' => 'Etternavn', //13
	'Strasse / Hausnr' => 'Adresse', //13
	'Postleitzahl' => 'Post kode', //13
	'Ort' => 'By', //13
	'Land' => 'Land', //13
	'Telefonnummer' => 'Telefonnummer', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Din bestilling', //13
	'Lieferadresse' => 'Fraktinformasjon', //13
	'Rechnungsadresse' => 'Betalingsinformasjon', //13
	'Lieferadresse & Rechnungsadresse' => 'Leveringsadresse & faktureringsadresse ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Hvis faktureringsadressen og leveringsadressen samsvarer, klikk her', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Min handlekurv', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Totalbeløp', //13
	'Versandart' => 'Leverings metode', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Vanlig levering (8-13 virkedager)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Vanlig levering (8-21 virkedager)', //0
	'Express Versand (4-7 Werktage)' => 'Rask levering (4-7 virkedager)', //0
    'Express Versand (7-13 Werktage)' => 'Rask levering (7-13 virkedager)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standard levering (8-21 virkedager)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Leveringsforsikring - {symbol}{price} (Returnerer garantert hvis levering mislyktes)', //13
	'Bitte Zahlungsart auswählen' => 'Velge betalingsmetode', //13
	'Zahlungsart' => 'Betalingstype', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bankoverføring', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Kortnummer', //13
	'Gültig bis' => 'Utløpsdato på kortet', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Fullfør bestillingen', //13
	'Allg. Geschäftsbedingungen' => 'Generelle transaksjonsbetingelser', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Jeg bekrefter herved at alle mine personlige data (eller personlige opplysninger) er korrekte. Jeg har lest forklaringen og forstå vilkårene for den.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);