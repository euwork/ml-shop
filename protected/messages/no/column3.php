<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Viktig informasjon', //1
    'Lieferkonditionen' => 'Leveringsvilkår', //1
    'Rückerstattung' => 'Tilbakebetaling', //1
    'Affiliates' => 'Partnere', //1
    'AGB' => 'Vilkår og betingelser', //1
    'Datenschutz' => 'Personvernregler', //1
    'Impressum' => 'Juridisk informasjon', //1
    'Garantie' => 'Garanti', //1
    'Warenkorb' => 'Min handlekurv', //1
    'Kein Produkt im Warenkorb' => 'Ingen varer i handlekurven min', //1
    'Gesamtbetrag' => 'Total bestilling', //1
    '1 Produkt im Warenkorb' => '1 vare i handlekurven min', //1
    'Produkte im Warenkorb' => 'varer i handlekurven min', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Betalingsalternativer', //1
    'VERSANDPARTNER' => 'Leverings metode', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);