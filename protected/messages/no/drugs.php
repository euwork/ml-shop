<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Kundeanmeldelser', //13
    'Rezensionen' => 'anmeldelser', //13
    'Alle {count} Bewertungen anzeigen...' => 'Se alle {count} anmeldelsene', //13
    'Stern:' => 'Stjerner:', //13
    'Sterne:' => 'Stjerner:', //13 
    'Produkt' => 'Artikkel', //13
    'Preis' => 'Pris', //13
    'Jetzt kaufen' => 'Bestill nå', //13
    'Dosierung' => 'Dosering', //13
    'Menge + Bonus' => 'Antall + bonus', //13
    'Menge' => 'Mengde', //13
    // '' => '',

    'kaufen' => 'Legg i handlekurv', //0
    'Eigene Bewertung erstellen' => 'Ny anmeldelse', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} av {vote_all} personer syntes at følgende anmeldelse var nyttig', //41
    'nein' => 'Nei', //41
    'ja' => 'Ja', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Var denne anmeldelsen nyttig for deg?', //41
    'von' => 'fra', //41

    'Rezension abgeben' => 'Lag din egen anmeldelse', //42
    'Name' => 'Navn', //42
    'Email' => 'E -post', //42
    'Thema' => 'Tittel', //42
    'Bewertung abgeben' => 'Nettstedsvurdering', //42
    'Webseitenfreundlichkeit' => 'Nettsted', //42
    'Versand Zufriedenheit' => 'Leveranse', //42
    'Preis / Leistung' => 'Pris / Opptreden', //42
    'Empfehlung' => 'Anbefaling', //42
    'Kundenkommentar' => 'Kundeanmeldelse', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Skriv inn tegnene du ser på bildet.', //42
    'Rezension abschicken' => 'Send meldingen', //42

);