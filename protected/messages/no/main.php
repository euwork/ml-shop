<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Startsiden',//1
    'Über uns' => 'Om oss', //1
    'F.A.Q.' => 'Ofte stilte spørsmål', //1
    'Kontakt' => 'Kontakt oss', //1
    'So bestellen Sie' => 'Hvordan bestiller jeg', //1
    'To top' => 'Til toppen',//0
    'Urheberrecht' => 'Opphavsrett',//0
    'Alle Rechte vorbehalten' => 'Alle rettigheter forbeholdt',//0
);