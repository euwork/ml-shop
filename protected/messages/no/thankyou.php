<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Takk for din bestilling', //14
	'Bestellnummer' => 'Ordrenummer',//14
    'Bestellte Produkte' => 'Bestilte produkter',//14
    'Versendungsart' => 'Leverings metode',//14
    'Versicherung' => 'Forsikring',//14
    'Telefonnummer' => 'Telefonnummer',//14
    'Drucken' => 'Skrive ut',//14

    'Bankverbindung' => 'Bankinformasjon',//14
    'Empfänger' => 'Mottaker',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Betalingsformål',//14
    'Betrag' => 'Beløp', //14
    'Bank' => 'Bank', //14
    'Adresse' => 'Adresse', //14
    // '' => '',


);