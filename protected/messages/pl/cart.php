<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Mój koszyk', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Zaktualizuj i zapisz', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Weź {tabl_count} dodatkowe tabletki za {symbol}{tabl_price} ({symbol}{tabl_price_one} za tabletkę)', //13
    'Aktualisieren' => 'Aktualizacja', //13
    'Gesamtbetrag' => 'Razem', // 0
    'Mit Einkauf fortfahren' => 'Kontynuować zakupy', //13
    'Zur Kasse' => 'Złóż zamówienie', //13
    'Produkt' => 'Produkt', //13
);