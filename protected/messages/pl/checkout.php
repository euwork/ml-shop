<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Imię', //13
	'Name' => 'Pełne imię i nazwisko', //14
	'Nachname' => 'Nazwisko', //13
	'Strasse / Hausnr' => 'Adres', //13
	'Postleitzahl' => 'Kod pocztowy', //13
	'Ort' => 'Miasto', //13
	'Land' => 'Kraj', //13
	'Telefonnummer' => 'Numer telefonu', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Twoje zamówienie', //13
	'Lieferadresse' => 'Informacje o dostawie', //13
	'Rechnungsadresse' => 'Informacja rozliczeniowa', //13
	'Lieferadresse & Rechnungsadresse' => 'Adres dostawy i Adres rozliczeniowy ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Jeśli adres rozliczeniowy i adres wysyłkowy są zgodne, kliknij tutaj', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Mój koszyk', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Całkowita kwota', //13
	'Versandart' => 'Sposób wysyłki', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Regularna wysyłka (8-13 dni roboczych)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Regularna wysyłka (8-21 dni roboczych)', //0
	'Express Versand (4-7 Werktage)' => 'Ekspresowa wysyłka (4-7 dni roboczych)', //0
    'Express Versand (7-13 Werktage)' => 'Ekspresowa wysyłka (7-13 dni roboczych)', //00
    'Standardlieferung (8-21 Werktage)' => 'Dostawa standardowa (8-21 dni roboczych)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Ubezpieczenie dostawy - {symbol}{price} (Gwarantowana ponowna wysyłka w przypadku niepowodzenia dostawy)', //13
	'Bitte Zahlungsart auswählen' => 'Proszę wybrać metodę płatności', //13
	'Zahlungsart' => 'Rodzaj płatności', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Przelew bankowy', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Numer karty kredytowej', //13
	'Gültig bis' => 'Termin ważności karty kredytowej', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Zakończ zamówienie', //13
	'Allg. Geschäftsbedingungen' => 'Ogólne Warunki Transakcji', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Ogólne Warunki Transakcji: Niniejszym potwierdzam, że wszystkie moje dane osobowe (lub szczegóły) są poprawne, zapoznałem/-am się z regulaminem i rozumiem jego warunki.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);