<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Przydatne informacje', //1
    'Lieferkonditionen' => 'Warunki dostawy', //1
    'Rückerstattung' => 'Zwrot', //1
    'Affiliates' => 'Partnerzy', //1
    'AGB' => 'Zasady i warunki', //1
    'Datenschutz' => 'Polityka prywatności', //1
    'Impressum' => 'Odcisk', //1
    'Garantie' => 'Gwarancja', //1
    'Warenkorb' => 'Mój koszyk', //1
    'Kein Produkt im Warenkorb' => 'Brak produktów w moim koszyku', //1
    'Gesamtbetrag' => 'Suma zamówienia', //1
    '1 Produkt im Warenkorb' => '1 pozycja w moim koszyku', //1
    'Produkte im Warenkorb' => 'pozycje w moim koszyku', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Opcje płatności', //1
    'VERSANDPARTNER' => 'Metody wysyłki', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);