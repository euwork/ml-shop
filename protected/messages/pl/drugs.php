<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Opinie klientów', //13
    'Rezensionen' => 'opinii', //13
    'Alle {count} Bewertungen anzeigen...' => 'Zobacz wszystkie {count} opinii', //13
    'Stern:' => 'gwiazdka:', //13
    'Sterne:' => 'gwiazdki:', //13 
    '5Sterne:' => 'gwiazdek:', //13 
    'Produkt' => 'Pozycja', //13
    'Preis' => 'Cena', //13
    'Jetzt kaufen' => 'Zamów teraz', //13
    'Dosierung' => 'Dawkowanie', //13
    'Menge + Bonus' => 'Ilość + Bonus', //13
    'Menge' => 'Ilość', //13
    // '' => '',

    'kaufen' => 'Dodaj do koszyka', //0
    'Eigene Bewertung erstellen' => 'Nowa referencja', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} z {vote_all} osób uznało tę recenzję za pomocną', //41
    'nein' => 'Nie', //41
    'ja' => 'Tak', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Czy ta opinia była dla ciebie pomocna?', //41
    'von' => 'od', //41

    'Rezension abgeben' => 'Stwórz własną recenzję', //42
    'Name' => 'Imię', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Tytuł', //42
    'Bewertung abgeben' => 'Ocena witryny', //42
    'Webseitenfreundlichkeit' => 'Strona internetowa', //42
    'Versand Zufriedenheit' => 'Wysyłka', //42
    'Preis / Leistung' => 'Cena / Wykonanie', //42
    'Empfehlung' => 'Rekomendacje', //42
    'Kundenkommentar' => 'Recenzja klienta', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Wpisz znaki, które widzisz na obrazku.', //42
    'Rezension abschicken' => 'Wyślij recenzję', //42

);