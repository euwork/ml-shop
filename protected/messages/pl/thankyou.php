<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Dziękuję za Twoje zamówienie.', //14
	'Bestellnummer' => 'Zamówienie nr ',//14
    'Bestellte Produkte' => 'Zamówione produkty',//14
    'Versendungsart' => 'Sposób dostawy',//14
    'Versicherung' => 'Ubezpieczenie',//14
    'Telefonnummer' => 'Numer telefonu',//14
    'Drucken' => 'Wydrukuj',//14

    'Bankverbindung' => 'Dane bankowe',//14
    'Empfänger' => 'Beneficjent',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Cel płatności',//14
    'Betrag' => 'Kwota', //14
    'Bank' => 'Bank', //14
    'Adresse' => 'Adres', //14
    // '' => '',


);