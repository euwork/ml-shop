<?php

return array(
	'Persönliche Details' => 'Dados pessoais',
	'Vorname' => 'Nome',
	'Email' => 'E-mail',
	'Webseite' => 'Página web',
	'Nachricht' => 'Mensagem',
	'Geben Sie die Zeichen in dem Bild ein' => 'Digite os caráteres de imagem',
	'Senden' => 'Enviar',
	);