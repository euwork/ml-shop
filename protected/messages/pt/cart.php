<?php

return array(
    'Warenkorb' => 'Meu carrinho',
    'Zum Warenkorb hinzufügen' => 'Adicionar ao carrinho',
    'Jetzt Rabatt sichern!' => 'Selecione',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => ' {tabl_count} pílulas adicionais por {symbol} {tabl_price} ({symbol} {tabl_price_one} por uma pílula)',
    'Aktualisieren' => 'Atualizar',
    'Gesamtbetrag' => 'Valor total',
    'Mit Einkauf fortfahren' => 'Continuar compras',
    'Zur Kasse' => 'Formalizar encomenda',
    'Produkt' => 'Produto',
);