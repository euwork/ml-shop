<?php


return array(
    // 'Artikel' => 'Artigo',
    'Nützliche Informationen' => 'Informação útil',
    'Lieferkonditionen' => 'Condições de envio',
    'Rückerstattung' => 'Reembolso',
    'Affiliates' => 'Para desenvolvedores',
    'AGB' => 'Termos e condições',
    'Datenschutz' => 'Política de privacidade',
    'Impressum' => 'Carimbo',
    'Garantie' => 'Garantia',
    'Warenkorb' => 'Meu carrinho',
    'Kein Produkt im Warenkorb' => 'Sem produtos selecionados',
    'Gesamtbetrag' => 'Valor total',
    '1 Produkt im Warenkorb' => 'Um item no meu carrinho',
    'Produkte im Warenkorb' => 'Itens no meu carrinho',
    // 'Angebot' => 'Oferta',
    'kostenloser online support' => 'Suporte on-line gratuito',
    // 'Vorteile' => 'Vantagens',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Opções de pagamento',
    'VERSANDPARTNER' => 'Métodos de envio',
    'Secure shopping protected by SSL' => 'Compras seguras protegidas por SSL',
);