<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Digite os caráteres de imagem',
	'Name'=> 'Nome',
	'E-Mail'=> 'E-mail',
	'Betreff'=> 'Assunto',
	'Nachricht'=> 'Mensagem',
	'Kontakt' => 'Contacte-nos',
	'Senden' => 'Enviar',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Obrigado! Nós recebemos a sua mensagem e daqui a pouco entraremos em contato com você!',

	);