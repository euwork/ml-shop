<?php

return array(

		"Afghanistan" => "Afeganistão",
		"Albanien" => "Albânia",
		"Andorra" => "Andorra",
		"Argentinien" => "Argentina",
		"Aruba" => "Aruba",
		"Australien" => "Austrália",
		"Österreich" => "Áustria",
		"Bahrain" => "Barém",
		"Bangladesch" => "Bangladesh",
		"Barbados" => "Barbados",
		"Weißrussland" => "Bielorrússia",
		"Belgien" => "Bélgica",
		"Bosnien und Herzegowina" => "Bósnia e Herzegovina",
		"Botswana" => "Botsuana",
		"Brasilien" => "Brasil",
		"Brunei" => "Brunei",
		"Bulgarien" => "Bulgária",
		"Burkina Faso" => "Burquina Faso",
		"Kamerun" => "Camarões",
		"Kanada" => "Canadá",
		"Chile" => "Chile",
		"Costa Rica" => "Costa Rica",
		"Kroatien" => "Croácia",
		"Curacao" => "Curaçao",
		"Zypern" => "Chipre",
		"Tschechische Republik" => "República Checa",
		"Dänemark" => "Dinamarca",
		"Dschibuti" => "Djibuti",
		"Dominikanische Republik" => "República Dominicana",
		"Ägypten" => "Egito",
		"El Salvador" => "El Salvador",
		"Estland" => "Estônia",
		"Äthiopien" => "Etiópia",
		"Finnland" => "Finlândia",
		"Frankreich" => "França",
		"Georgien" => "Geórgia",
		"Deutschland" => "Alemanha",
		"Griechenland" => "Grécia",
		"Guam" => "Guam",
		"Guatemala" => "Guatemala",
		"Hongkong" => "Hong Kong",
		"Ungarn" => "Hungría",
		"Island" => "Islândia",
		"Indien" => "Índia",
		"Indonesien" => "Indonésia",
		"Irak" => "Iraque",
		"Irland" => "Irlanda",
		"Israel" => "Israel",
		"Italien" => "Itália",
		"Elfenbeinküste" => "Costa do Marfim",
		"Jamaika" => "Jamaica",
		"Japan" => "Japão",
		"Jordanien" => "Jordania",
		"Kasachstan" => "Cazaquistão",
		"Kenia" => "Quênia",
		"Kuwait" => "Kuwait",
		"Lettland" => "Letônia",
		"Libanon" => "Líbano",
		"Liberia" => "Libéria",
		"Liechtenstein" => "Liechtenstein",
		"Litauen" => "Lituânia",
		"Luxemburg" => "Luxemburgo",
		"Nordmazedonien" => "Macedónia do Norte",
		"Malawi" => "Malawi",
		"Malaysia" => "Malásia",
		"Malta" => "Malta",
		"Mexiko" => "México",
		"Moldawien" => "Moldávia",
		"Monaco" => "Mônaco",
		"Montenegro" => "Montenegro",
		"Namibia" => "Namíbia",
		"Nepal" => "Nepal",
		"Niederlande" => "Países Baixos",
		"Neukaledonien" => "Nova Caledônia",
		"Neuseeland" => "Nova Zelândia",
		"Nigeria" => "Nigéria",
		"Nördliche Marianen" => "Ilhas Marianas do Norte",
		"Norwegen" => "Noruega",
		"Oman" => "Omã",
		"Pakistan" => "Paquistão",
		"Panama" => "Panamá",
		"Peru" => "Peru",
		"Philippinen" => "Filipinas",
		"Polen" => "Polónia",
		"Portugal" => "Portugal",
		"Puerto Rico" => "Porto rico",
		"Katar" => "Qatar",
		"Reunion" => "Reunião",
		"Rumänien" => "Roménia",
		"Russland" => "Rússia",
		"San Marino" => "San Marino",
		"Saudi-Arabien" => "Arábia Saudita",
		"Senegal" => "Senegal",
		"Serbien" => "Sérvia",
		"Singapur" => "Singapura",
		"Slowakei" => "Eslováquia",
		"Slowenien" => "Eslovênia",
		"Südafrika" => "África do Sul",
		"Südkorea" => "Coreia do Sul",
		"Spanien" => "Espanha",
		"Sri Lanka" => "Sri Lanka",
		"Schweden" => "Suécia",
		"Schweiz" => "Suíça",
		"Taiwan" => "Taiwan",
		"Tansania" => "Tanzânia",
		"Thailand" => "Tailândia",
		"Togo" => "Togo",
		"Trinidad und Tobago" => "Trinidad e Tobago",
		"Türkei" => "Turquia",
		"Uganda" => "Uganda",
		"Ukraine" => "Ucrânia",
		"Vereinigte Arabische Emirate" => "Emirados Árabes Unidos",
		"Vereinigtes Königreich" => "Reino Unido",
		"Vereinigte Staaten" => "Estados Unidos",
		"Vatikanstadt" => "Cidade do Vaticano",
		"Sambia" => "Zâmbia",
		"Simbabwe" => "Zimbábue",

	);