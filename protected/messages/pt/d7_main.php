<?php


return array(
	'Ihr Warenkorb ist leer' => 'Nenhum item no meu carrinho',
	'Navigation' => 'Navegação',
	'Top Produkte' => 'Principais produtos',
	'Top Produkt' => 'Top product',
	'Verfügbarkeit' => 'Disponibilidade',
	'Lieferung' => 'Entrega',
	'Zahlungsart' => 'Forma de pagamento',
	'Ja' => 'Sim',
	'Werktage' => 'Dias úteis',
	'Kreditkarten, Banküberweisung' => 'Cartões de crédito, transferência bancária',
);