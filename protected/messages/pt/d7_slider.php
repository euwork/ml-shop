<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Comprimidos de potência Kamagra, Viagra, Levitra e Cialis sem receita médica',
	'Potenzmittel rezeptfrei' => 'Medicamentos potentes sem receita médica',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Comprimidos de potência Kamagra, Viagra, Levitra e Cialis na Alemanha',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => 'Medicamentos potentes sem receita médica na Alemanha. Comprimidos de potência on-line sem receita médica.',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra para mulheres',
	'Viagra für die Frau rezeptfrei' => 'Viagra para mulheres',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Lovegra - Viagra para mulheres (Viagra feminino)',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Lovegra, viagra feminino, Viagra para mulheres sem receita',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Xenical orlistate 120mg, Lida Daidaihua',
	'Lida Daidaihua, Xenical tabletten' => 'Produtos para perda de peso: Xenical, Lida',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Xenical orlistate 120mg, Lida Daidaihua - Remedios para emagrecer sem receita em Portugal',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Xenical orlistate 120mg sem receita, Lida Daidaihua - Remedios para emagrecer sem receita em Portugal',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Finasterida 1mg - Propecia Genérico',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Propecia Genérico (Finasterida 1mg)',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Propecia Genérico (Finasterida 1mg)',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Finasterida 1mg - Propecia Genérico sem receita em Portugal',
	
	'Potenzmittel rezeptfrei - {domain}' => 'Medicamentos potentes sem receita médica - {domain}',
	'Potenzmittel rezeptfrei.' => 'Medicamentos potentes sem receita médica.',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => 'Ofertas especiais e programas de bônus. Medicamentos potentes sem receita médica.',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => 'Medicamentos potentes sem receita médica na Alemanha. Entrega rápida e gratuita. Ofertas especiais e programas de bônus.',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);