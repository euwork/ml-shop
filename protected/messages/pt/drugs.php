<?php


return array(
    'Kundenrezensionen' => 'Opiniões de clientes',
    'Rezensionen' => 'Opiniãos',
    'Alle {count} Bewertungen anzeigen...' => 'Ver todos os {count} Comentários de clientes',
    'Stern:' => 'estrela:',
    'Sterne:' => 'estrelas:',
    'Produkt' => 'Produto',
    'Preis' => 'Preço',
    'Jetzt kaufen' => 'Comprar já',
    'Dosierung' => 'Dosagem',
    'Menge + Bonus' => 'Quantidade + Bónus',
    'Menge' => 'Quantidade',
    // '' => '',

    'kaufen' => 'Adicionar ao carrinho',
    'Eigene Bewertung erstellen' => 'Crie sua própria opinião',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} de {vote_all} pessoas pensam que o comentário útil',
    'nein' => 'Não',
    'ja' => 'Sim',
    'Hat Ihnen diese Bewertung geholfen?' => 'Este comentário foi útil para você?',
    'von' => 'por',

    'Rezension abgeben' => 'Os seus dados',
    'Name' => 'Nome',
    'Email' => 'E-mail',
    'Thema' => 'Tópico',
    'Bewertung abgeben' => 'Por favor votar',
    'Webseitenfreundlichkeit' => 'Voto nosso site',
    'Versand Zufriedenheit' => 'Satisfação Envio',
    'Preis / Leistung' => 'Preço / Desempenho',
    'Empfehlung' => 'Recomendaçãon',
    'Kundenkommentar' => 'Comentario',
    'Geben Sie die Zeichen in dem Bild ein' => 'Digite os caráteres de imagem',
    'Rezension abschicken' => 'Enviar Comentário',

);