<?php

return array(
    'Startseite' => 'Página principal',//perevod
    'Über uns' => 'Sobre nós',
    'F.A.Q.' => 'Perguntas e respostas',
    'Kontakt' => 'Contacte-nos',
    'So bestellen Sie' => 'Como encomendar',
    'To top' => 'para cima',//perevod
    'Urheberrecht' => 'Direitos de autor',
    'Alle Rechte vorbehalten' => 'Todos direitos reservados',
);