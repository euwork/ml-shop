<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Obrigado para o seu comentário! O comentário vai aparecer no nosso site depois de verificação de moderador',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Por favor digite o código da imagem!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Você tem certeza que quer deixar o comentário vazio?',
	);