<?php


return array(

    'Vielen Dank für Ihre Bestellung' => 'Agradecemos pelo seu pedido!',
    'Bestellnummer' => 'Número de pedido',
    'Bestellte Produkte' => 'Produtos encomendados',
    'Versendungsart' => 'Modo de entrega',
    'Versicherung' => 'Seguros',
    'Telefonnummer' => 'Número de telefone',
    'Drucken' => 'Print',

    'Bankverbindung' => 'Dados bancários',
    'Empfänger' => 'Beneficiário',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'País de destino',
    'Verwendungszweck' => 'Referência de pagamento',
    'Betrag' => 'Valor total',
    'Bank' => 'Banco',
    'Adresse' => 'Morada',
    // '' => '',


);