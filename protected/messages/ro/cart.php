<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Coșul meu', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Faceți upgrade și salvați', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Luați {tabl_count} de pastile suplimentare pentru {symbol}{tabl_price} ({symbol}{tabl_price_one} pe pastilă)', //13
    'Aktualisieren' => 'Upgrade', //13
    'Gesamtbetrag' => 'Total', // 0
    'Mit Einkauf fortfahren' => 'A continua cumparaturile', //13
    'Zur Kasse' => 'A aranja comanda', //13
    'Produkt' => 'Produs', //13
);