<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Prenume', //13
	'Name' => 'Numele complet', //14
	'Nachname' => 'Nume', //13
	'Strasse / Hausnr' => 'Adresa', //13
	'Postleitzahl' => 'Zip / Cod Poștal', //13
	'Ort' => 'Oraș', //13
	'Land' => 'Țară', //13
	'Telefonnummer' => 'Numar de telefon', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Comanda dumneavoastră', //13
	'Lieferadresse' => 'Informații de expediere', //13
	'Rechnungsadresse' => 'Informații de facturare', //13
	'Lieferadresse & Rechnungsadresse' => 'Adresa de livrare & adresa de facturare ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Dacă adresa de facturare și adresa de expediere se potrivesc, faceți clic aici', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Coșul meu', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Valoare totală', //13
	'Versandart' => 'Metodă de livrare', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Expediere regulată (8-13 zile lucrătoare)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Expediere regulată (8-21 zile lucrătoare)', //0
	'Express Versand (4-7 Werktage)' => 'Expediere expres (4-7 zile lucrătoare)', //0
    'Express Versand (7-13 Werktage)' => 'Expediere expres (7-13 zile lucrătoare)', //00
    'Standardlieferung (8-21 Werktage)' => 'Livrare standard (8-21 zile lucrătoare)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Asigurare pentru livrare - {symbol}{price} (Retrimiterea garantată dacă livrarea a eșuat)', //13
	'Bitte Zahlungsart auswählen' => 'Vă rugăm să selectați metoda de plată', //13
	'Zahlungsart' => 'Tipul de plată', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Transfer bancar', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Numărul cărții de credit', //13
	'Gültig bis' => 'Expirarea cardului de credit', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Comandă completă', //13
	'Allg. Geschäftsbedingungen' => 'Condiții generale de tranzacție', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Condiții generale de tranzacție: Prin prezenta, confirm că toate datele mele personale (sau detaliile) sunt corecte, am citit explicația și am înțeles termenii acestora.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);