<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Informații utile', //1
    'Lieferkonditionen' => 'Conditii de livrare', //1
    'Rückerstattung' => 'Restituire', //1
    'Affiliates' => 'Afiliați', //1
    'AGB' => 'Termeni și condiții', //1
    'Datenschutz' => 'Politica de Confidențialitate', //1
    'Impressum' => 'Imprima', //1
    'Garantie' => 'Garanție', //1
    'Warenkorb' => 'Coșul meu', //1
    'Kein Produkt im Warenkorb' => 'Nu există articole în coș', //1
    'Gesamtbetrag' => 'Comanda totală', //1
    '1 Produkt im Warenkorb' => '1 articol în coș', //1
    'Produkte im Warenkorb' => 'articole în coșul meu', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Opțiuni de plată', //1
    'VERSANDPARTNER' => 'Metode de expediere', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);