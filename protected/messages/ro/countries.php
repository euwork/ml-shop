<?php

return array(

		"Afghanistan" => "Afganistan",
		"Albanien" => "Albania",
		"Andorra" => "Andorra",
		"Argentinien" => "Argentina",
		"Aruba" => "Aruba",
		"Australien" => "Australia",
		"Österreich" => "Austria",
		"Bahrain" => "Bahrain",
		"Bangladesch" => "Bangladesh",
		"Barbados" => "Barbados",
		"Weißrussland" => "Belarus",
		"Belgien" => "Belgia",
		"Bosnien und Herzegowina" => "Bosnia și Herțegovina",
		"Botswana" => "Botswana",
		"Brasilien" => "Brazilia",
		"Brunei" => "Brunei",
		"Bulgarien" => "Bulgaria",
		"Burkina Faso" => "Burkina Faso",
		"Kamerun" => "Camerun",
		"Kanada" => "Canada",
		"Chile" => "Chile",
		"Costa Rica" => "Costa Rica",
		"Kroatien" => "Croația",
		"Curacao" => "Curaçao",
		"Zypern" => "Cipru",
		"Tschechische Republik" => "Republica Cehă / Cehia",
		"Dänemark" => "Danemarca",
		"Dschibuti" => "Djibouti",
		"Dominikanische Republik" => "Republica Dominicană",
		"Ägypten" => "Egipt",
		"El Salvador" => "El Salvador",
		"Estland" => "Estonia",
		"Äthiopien" => "Etiopia",
		"Finnland" => "Finlanda",
		"Frankreich" => "Franța",
		"Georgien" => "Georgia",
		"Deutschland" => "Germania",
		"Griechenland" => "Grecia",
		"Guam" => "Guam",
		"Guatemala" => "Guatemala",
		"Hongkong" => "Hong Kong",
		"Ungarn" => "Ungaria",
		"Island" => "Islanda",
		"Indien" => "India",
		"Indonesien" => "Indonezia",
		"Irak" => "Irak",
		"Irland" => "Irlanda",
		"Israel" => "Israel",
		"Italien" => "Italia",
		"Elfenbeinküste" => "Coasta de Fildeș",
		"Jamaika" => "Jamaica",
		"Japan" => "Japonia",
		"Jordanien" => "Iordania",
		"Kasachstan" => "Kazahstan",
		"Kenia" => "Kenya",
		"Kuwait" => "Kuweit",
		"Lettland" => "Letonia",
		"Libanon" => "Liban",
		"Liberia" => "Liberia",
		"Liechtenstein" => "Liechtenstein",
		"Litauen" => "Lituania",
		"Luxemburg" => "Luxemburg",
		"Nordmazedonien" => "Macedonia de Nord",
		"Malawi" => "Malawi",
		"Malaysia" => "Malaysia",
		"Malta" => "Malta",
		"Mexiko" => "Mexic",
		"Moldawien" => "Moldova",
		"Monaco" => "Monaco",
		"Montenegro" => "Muntenegru",
		"Namibia" => "Namibia",
		"Nepal" => "Nepal",
		"Niederlande" => "Țările de Jos",
		"Neukaledonien" => "Noua Caledonie",
		"Neuseeland" => "Noua Zeelandă",
		"Nigeria" => "Nigeria",
		"Nördliche Marianen" => "Comunitatea Insulelor Mariane de Nord",
		"Norwegen" => "Norvegia",
		"Oman" => "Oman",
		"Pakistan" => "Pakistan",
		"Panama" => "Panama",
		"Peru" => "Peru",
		"Philippinen" => "Filipine",
		"Polen" => "Polonia",
		"Portugal" => "Portugalia",
		"Puerto Rico" => "Puerto Rico",
		"Katar" => "Qatar",
		"Reunion" => "Reunion",
		"Rumänien" => "România",
		"Russland" => "Federația Rusă",
		"San Marino" => "San Marino",
		"Saudi-Arabien" => "Arabia Saudită",
		"Senegal" => "Senegal",
		"Serbien" => "Serbia",
		"Singapur" => "Republica Singapore",
		"Slowakei" => "Slovacia",
		"Slowenien" => "Slovenia",
		"Südafrika" => "Africa de Sud",
		"Südkorea" => "Coreea de Sud",
		"Spanien" => "Spania",
		"Sri Lanka" => "Sri Lanka",
		"Schweden" => "Suedia",
		"Schweiz" => "Elveția",
		"Taiwan" => "Taiwan",
		"Tansania" => "Tanzania",
		"Thailand" => "Thailanda",
		"Togo" => "Togo",
		"Trinidad und Tobago" => "Trinidad și Tobago",
		"Türkei" => "Turcia",
		"Uganda" => "Uganda",
		"Ukraine" => "Ucraina",
		"Vereinigte Arabische Emirate" => "Emiratele Arabe Unite",
		"Vereinigtes Königreich" => "Regatul Unit",
		"Vereinigte Staaten" => "Statele Unite ale Americii",
		"Vatikanstadt" => "Sfântul Scaun",
		"Sambia" => "Zambia",
		"Simbabwe" => "Zimbabwe",

	);