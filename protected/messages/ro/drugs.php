<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Recenzii ale clienților', //13
    'Rezensionen' => 'Recenzii', //13
    'Alle {count} Bewertungen anzeigen...' => 'A vede toate cele {count} de recenzii', //13
    'Stern:' => 'Stea:', //13
    'Sterne:' => 'Stele:', //13 
    'Produkt' => 'Articol', //13
    'Preis' => 'Preț', //13
    'Jetzt kaufen' => 'Comandați acum', //13
    'Dosierung' => 'Dozare', //13
    'Menge + Bonus' => 'Cantitate + Bonus', //13
    'Menge' => 'Cantitate', //13
    // '' => '',

    'kaufen' => 'Adaugă în coș', //0
    'Eigene Bewertung erstellen' => 'Mărturie nou', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} din {vote_all} de persoane au considerat utilă următoarea recenzie', //41
    'nein' => 'Nu', //41
    'ja' => 'Da', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Considerați utilă această recenzie?', //41
    'von' => 'de la', //41

    'Rezension abgeben' => 'Creați-vă recenzie proprie', //42
    'Name' => 'Nume', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Titlu', //42
    'Bewertung abgeben' => 'Evaluarea site-ului', //42
    'Webseitenfreundlichkeit' => 'Site-ul web', //42
    'Versand Zufriedenheit' => 'Livrare', //42
    'Preis / Leistung' => 'Preț / Performanță', //42
    'Empfehlung' => 'Recomandare', //42
    'Kundenkommentar' => 'Recenzia clientului', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Tastați caracterele pe care le vedeți în imagine.', //42
    'Rezension abschicken' => 'A trimite recenzie', //42

);