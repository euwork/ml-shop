<?php  // https://clip2net.com/s/4cGRbo1

return array(
    'Startseite' => 'Pagina principală',//1
    'Über uns' => 'Despre noi', //1
    'F.A.Q.' => 'FAQ.(Întrebări frecvente)', //1
    'Kontakt' => 'Contactați-ne', //1
    'So bestellen Sie' => 'Cum se comandă', //1
    'To top' => 'Sus',//0
    'Urheberrecht' => 'Copyright',//0
    'Alle Rechte vorbehalten' => 'Toate drepturile rezervate',//0
);