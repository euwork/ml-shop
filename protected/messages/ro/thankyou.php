<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Vă mulțumim pentru comanda dumneavoastră.', //14
	'Bestellnummer' => 'Comanda nr.',//14
    'Bestellte Produkte' => 'Produse comandate',//14
    'Versendungsart' => 'Metoda de livrare',//14
    'Versicherung' => 'Asigurare',//14
    'Telefonnummer' => 'Numar de telefon',//14
    'Drucken' => 'Imprimare',//14

    'Bankverbindung' => 'Detalii bancare',//14
    'Empfänger' => 'Beneficiar',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC (SWIFT)',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Scopul plății',//14
    'Betrag' => 'Suma', //14
    'Bank' => 'Bancă', //14
    'Adresse' => 'Adresa', //14
    // '' => '',


);