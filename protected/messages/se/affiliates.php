<?php

return array(
	'Persönliche Details' => 'Personuppgifter',
	'Vorname' => 'Förnamn',
	'Email' => 'E-post',
	'Webseite' => 'Webbsida',
	'Nachricht' => 'Meddelande',
	'Geben Sie die Zeichen in dem Bild ein' => 'Skriva in de symboler som du ser på bilden',
	'Senden' => 'Skicka',
	);