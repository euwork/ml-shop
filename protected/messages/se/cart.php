<?php

return array(
    'Warenkorb' => 'Min kundvagn',
    'Zum Warenkorb hinzufügen' => 'Add to cart',
    'Jetzt Rabatt sichern!' => 'Välj',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => ' {tabl_count} extra tabletter för {symbol} {tabl_price} ({symbol}{tabl_price_one} per tablett)',
    'Aktualisieren' => 'Uppdatera',
    'Gesamtbetrag' => 'Totalsumma',
    'Mit Einkauf fortfahren' => 'Fortsätta inköp',
    'Zur Kasse' => 'Göra en beställning',
    'Produkt' => 'Produkt',
);