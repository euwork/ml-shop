<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Användbar information',
    'Lieferkonditionen' => 'Leveransvillkor',
    'Rückerstattung' => 'Återbetalning',
    'Affiliates' => 'För webbansvariga',
    'AGB' => 'Villkor',
    'Datenschutz' => 'Integritetspolicy',
    'Impressum' => 'Avtryck',
    'Garantie' => 'Garanti',
    'Warenkorb' => 'Min kundvagn',
    'Kein Produkt im Warenkorb' => 'Inga artiklar i min kundvagn',
    'Gesamtbetrag' => 'Totalsumma',
    '1 Produkt im Warenkorb' => '1 artikel i min kundvagn',
    'Produkte im Warenkorb' => 'artiklar i min kundvagn',
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Köpsupport',
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Betalnings Alternativ',
    'VERSANDPARTNER' => 'Fraktmetoder',
    'Secure shopping protected by SSL' => 'Säker shopping skyddad av SSL',
);