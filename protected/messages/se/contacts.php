<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Skriva in de symboler som du ser på bilden',
	'Name'=> 'Namn',
	'E-Mail'=> 'E-post',
	'Betreff'=> 'Ämne',
	'Nachricht'=> 'Meddelande',
	'Kontakt' => 'Kontakta oss',
	'Senden' => 'Skicka',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Tack! Vi har fått ditt meddelande och kontaktar dig inom den närmaste tiden!',

	);