<?php


return array(
	'Ihr Warenkorb ist leer' => 'Inga produkter i kundvagnen',
	'Navigation' => 'Navigation',
	'Top Produkte' => 'Topp produkter',
	'Top Produkt' => 'Toppprodukt',
	'Verfügbarkeit' => 'Tillgänglighet',
	'Lieferung' => 'Leverans',
	'Zahlungsart' => 'Betalningsmetod',
	'Ja' => 'Ja',
	'Werktage' => 'Arbetsdagar',
	'Kreditkarten, Banküberweisung' => 'Kreditkort, banköverföring',
);