<?php


return array(
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis rezeptfrei' => 'Potentpillen Kamagra, Viagra, Levitra och Cialis utan recept',
	'Potenzmittel rezeptfrei' => 'Kraftläkemedel utan recept',
	'Potenzpillen Kamagra, Viagra, Levitra und Cialis in Deutschland' => 'Potentpillen Kamagra, Viagra, Levitra och Cialis i Tyskland',
	'Potenzmittel rezeptfrei in Deutschland. Potenzpillen online ohne Rezept.' => 'Kraftläkemedel utan recept i Tyskland. Styrkpiller online utan recept.',

	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra för kvinnor utan recept',
	'Viagra für die Frau rezeptfrei' => 'Viagra för kvinnor utan recept',
	'Viagra für die Frau (Lovegra 100mg) ohne Rezept' => 'Viagra för kvinnor utan recept i Sverige utan recept',
	'Viagra für die Frau (Lovegra 100mg) in Deutschland. Lovegra rezeptfrei.' => 'Lovegra är Viagra för kvinnor utan recept i Sverige',
	
	'Lida Daidaihua, Xenical (orlistat) tabletten' => 'Xenical Generisk orlistat , Lida Daidaihua',
	'Lida Daidaihua, Xenical tabletten' => 'Xenical Generisk orlistat , Lida Daidaihua',
	'Lida Daidaihua, Xenical (orlistat) tabletten 120 mg' => 'Xenical Generisk orlistat, Lida Daidaihua kapslar i Sverige',
	'Schlankheitsmittel Lida Daidaihua , Xenical rezeptfrei tabletten in Deutschland.' => 'Xenical Generisk orlistat 120mg i Sverige utan recept. Lida Daidaihua  kapslar i Sverige',
	
	'Propecia Finasterid 1mg gegen Haarausfall' => 'Generisk Propecia finasterid 1mg',
	'Propecia (Finasterid 1mg) gegen Haarausfall' => 'Generisk Propecia finasterid 1mg',
	'Propecia - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Generisk Propecia finasterid 1mg i Sverige',
	'Propecia Generika - Finasterid 1mg - gegen Haarausfall in Deutschland' => 'Generisk Propecia finasterid 1mg  kapslar i Sverige utan recept',
	
	'Potenzmittel rezeptfrei - {domain}' => 'Online apotek, potensmedel apoteket {domain}',
	'Potenzmittel rezeptfrei.' => 'Online apotek, potensmedel apoteket {domain}',
	'Sonderangebote und Bonusprogramme. Potenzmittel rezeptfrei.' => 'Potensmedel apoteket {domain}. Viagra, Cialis, Kamagra, Levitra kapslar i Sverige',
	'Potenzmittel rezeptfrei in Deutschland. Schnelle und kostenlose Lieferung. Sonderangebote und Bonusprogramme.' => 'Potensmedel apoteket {domain}. Viagra, Cialis, Kamagra, Levitra i Sverige utan recept',
	
	// '' => '',
	// '' => '',
	// '' => '',
	// '' => '',
	);