<?php


return array(
    'Kundenrezensionen' => 'Kundrecensioner',
    'Rezensionen' => 'Recensioner',
    'Alle {count} Bewertungen anzeigen...' => 'Se allt {count} recensioner',
    'Stern:' => 'Stjärna:',
    'Sterne:' => 'Stjärna:',
    'Produkt' => 'Produkt',
    'Preis' => 'Pris',
    'Jetzt kaufen' => 'Beställ nu',
    'Dosierung' => 'Dosering',
    'Menge + Bonus' => 'Antal + Bonus',
    'Menge' => 'Antal',
    // '' => '',

    'kaufen' => 'Köp just nu',
    'Eigene Bewertung erstellen' => 'Berätta Här',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} av {vote_all} personer tyckte att följande recension var till hjälp',
    'nein' => 'Nej',
    'ja' => 'Ja',
    'Hat Ihnen diese Bewertung geholfen?' => 'Var denna recension användbar för dig?',
    'von' => 'från',

    'Rezension abgeben' => 'Skapa din egen recension',
    'Name' => 'Namn',
    'Email' => 'E-post',
    'Thema' => 'Titel',
    'Bewertung abgeben' => 'Hemsidans betyg',
    'Webseitenfreundlichkeit' => 'Hemsida',
    'Versand Zufriedenheit' => 'Frakt',
    'Preis / Leistung' => 'Kursutveckling',
    'Empfehlung' => 'Rekomendation',
    'Kundenkommentar' => 'Kund Recension',
    'Geben Sie die Zeichen in dem Bild ein' => 'Skriva in de symboler som du ser på bilden',
    'Rezension abschicken' => 'Skicka en recension',

);