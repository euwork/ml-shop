<?php

return array(
    'Startseite' => 'Hemsida',//perevod
    'Über uns' => 'Om oss',
    'F.A.Q.' => 'Frågor och Svar',
    'Kontakt' => 'Kontakta oss',
    'So bestellen Sie' => 'Så här beställer du',
    'To top' => 'Till toppen',//perevod
    'Urheberrecht' => 'Upphovsrätt',
    'Alle Rechte vorbehalten' => 'Alla rättigheter förbehållna',
);