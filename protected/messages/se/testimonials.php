<?php

return array(
	'Vielen Dank für Ihre Bewertung! Sie wird auf unserer Website veröffentlicht, nachdem Sie von unserem Moderator geprüft wird' => 'Tack för ditt omdöme! Det kommer att publiceras på vår webbplats efter att ha kontrollerats av moderatorn',
	'Geben Sie die Zeichen in dem Bild ein!' => 'Var så snäll och ange koden från bilden!',
	'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?' => 'Är du säker på att du vill lämna ett tomt omdöme?',
	);