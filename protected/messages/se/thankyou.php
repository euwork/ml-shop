<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Tack fördin beställning',
	'Bestellnummer' => 'Beställning nr',
    'Bestellte Produkte' => 'Beställda produkter',
    'Versendungsart' => 'Leveranssätt',
    'Versicherung' => 'Försäkring',
    'Telefonnummer' => 'Telefon',
    'Drucken' => 'Skriva ut',

    'Bankverbindung' => 'Bankdetaljerna',
    'Empfänger' => 'Betalningsmottagare',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'Bestämmelseland',
    'Verwendungszweck' => 'Betalningsmål',
    'Betrag' => 'Summa',
    'Bank' => 'Bank',
    'Adresse' => 'Adress',
    // '' => '',


);