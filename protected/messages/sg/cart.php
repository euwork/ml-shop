<?php

return array(
    'Warenkorb' => 'My cart',
    'Zum Warenkorb hinzufügen' => 'Add to cart',
    'Jetzt Rabatt sichern!' => 'Upgrade and save',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Take {tabl_count} extra pills for {symbol}{tabl_price} ({symbol}{tabl_price_one} per pill)',
    'Aktualisieren' => 'Upgrade',
    'Gesamtbetrag' => 'Total',
    'Mit Einkauf fortfahren' => 'Continue shopping',
    'Zur Kasse' => 'Check out',
    'Produkt' => 'Product',
);