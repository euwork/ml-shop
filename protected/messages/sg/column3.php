<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Useful Information',
    'Lieferkonditionen' => 'Delivery Conditions',
    'Rückerstattung' => 'Refund',
    'Affiliates' => 'Affiliates',
    'AGB' => 'Terms and Conditions',
    'Datenschutz' => 'Privacy Policy',
    'Impressum' => 'Imprint',
    'Garantie' => 'Guarantee',
    'Warenkorb' => 'My cart',
    'Kein Produkt im Warenkorb' => 'No items in my cart',
    'Gesamtbetrag' => 'Order total',
    '1 Produkt im Warenkorb' => 'One item in my cart',
    'Produkte im Warenkorb' => 'Items in my cart',
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support',
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Payment options',
    'VERSANDPARTNER' => 'Shipping Methods',
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL',
);