<?php


return array(
	'Ihr Warenkorb ist leer' => 'No items in my cart',
	'Navigation' => 'Navigation',
	'Top Produkte' => 'Top products',
	'Top Produkt' => 'Top product',
	'Verfügbarkeit' => 'Availability',
	'Lieferung' => 'Delivery',
	'Zahlungsart' => 'Payment',
	'Ja' => 'Yes',
	'Werktage' => 'days',
	'Kreditkarten, Banküberweisung' => 'Credit cards, Wire transfer',
);