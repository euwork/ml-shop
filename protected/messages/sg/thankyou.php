<?php


return array(

	'Vielen Dank für Ihre Bestellung' => 'Thank you for your order.',
	'Bestellnummer' => 'Order No.',
    'Bestellte Produkte' => 'Products ordered',
    'Versendungsart' => 'Delivery method',
    'Versicherung' => 'Insurance',
    'Telefonnummer' => 'Phone number',
    'Drucken' => 'Print',

    'Bankverbindung' => 'Bank details',
    'Empfänger' => 'Beneficiary',
    'IBAN' => 'IBAN',
    'BIC' => 'BIC',
    'Zielland' => 'Destination country',
    'Verwendungszweck' => 'Purpose of payment',
    'Betrag' => 'Amount',
    'Bank' => 'Bank',
    'Adresse' => 'Address',
    // '' => '',


);