<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Môj košík', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Upgradujte a uložte', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Vezmite si {tabl_count} ďalších tabliet za {symbol}{tabl_price} ({symbol}{tabl_price_one} za pilulku)', //13
    'Aktualisieren' => 'Upgradovať', //13
    'Gesamtbetrag' => 'Celkovo', // 0
    'Mit Einkauf fortfahren' => 'Pokračovať v nákupe', //13
    'Zur Kasse' => 'Vytvoriť objednávku', //13
    'Produkt' => 'Výrobok', //13
);