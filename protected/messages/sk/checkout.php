<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Krstné meno', //13
	'Name' => 'Celé meno', //14
	'Nachname' => 'Priezvisko', //13
	'Strasse / Hausnr' => 'Adresa', //13
	'Postleitzahl' => 'PSČ', //13
	'Ort' => 'Mesto', //13
	'Land' => 'Krajina', //13
	'Telefonnummer' => 'Telefónne číslo', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Vaša objednávka', //13
	'Lieferadresse' => 'Informácie o preprave', //13
	'Rechnungsadresse' => 'Fakturačné údaje', //13
	'Lieferadresse & Rechnungsadresse' => 'Doručovacia adresa a fakturačná adresa ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Ak sa fakturačná adresa a dodacia adresa zhodujú, kliknite sem', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Môj košík', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Celková suma', //13
	'Versandart' => 'Spôsob dodania', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Pravidelná doprava (8-13 pracovných dní)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Pravidelná doprava (8-21 pracovných dní)', //0
	'Express Versand (4-7 Werktage)' => 'Expresná doprava (4-7 pracovných dní)', //0
    'Express Versand (7-13 Werktage)' => 'Expresná doprava (7-13 pracovných dní)', //00
    'Standardlieferung (8-21 Werktage)' => 'Štandardné doručenie (8-21 pracovných dní)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Poistenie dodávky - {symbol}{price} (Zaručené opätovné odoslanie v prípade zlyhania dodávky)', //13
	'Bitte Zahlungsart auswählen' => 'Vyberte spôsob platby', //13
	'Zahlungsart' => 'Typ platby', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bankový prevod', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Číslo kreditnej karty', //13
	'Gültig bis' => 'Platnosť kreditnej karty', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Dokončiť objednávku', //13
	'Allg. Geschäftsbedingungen' => 'Všeobecné podmienky transakcie', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Všeobecné podmienky transakcie: Týmto potvrdzujem, že všetky moje osobné údaje (alebo podrobnosti) sú správne, prečítal som si vysvetlenie a rozumiem ich podmienkam.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);