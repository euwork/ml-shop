<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Užitočná informácia', //1
    'Lieferkonditionen' => 'Dodacie podmienky', //1
    'Rückerstattung' => 'Vrátenie peňazí', //1
    'Affiliates' => 'Pridružené spoločnosti', //1
    'AGB' => 'Podmienky', //1
    'Datenschutz' => 'Zásady ochrany osobných údajov', //1
    'Impressum' => 'Odtlačok', //1
    'Garantie' => 'Záruka', //1
    'Warenkorb' => 'Môj košík', //1
    'Kein Produkt im Warenkorb' => 'V košíku nie sú žiadne položky', //1
    'Gesamtbetrag' => 'Celková suma objednávky', //1
    '1 Produkt im Warenkorb' => '1 položka v mojom košíku', //1
    'Produkte im Warenkorb' => 'položky v mojom košíku', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Možnosti platby', //1
    'VERSANDPARTNER' => 'Spôsoby doručenia', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);