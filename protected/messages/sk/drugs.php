<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Hodnotenia zákazníkov', //13
    'Rezensionen' => 'Recenzií', //13
    'Alle {count} Bewertungen anzeigen...' => 'Pozrieť všetký {count} recenzií', //13
    'Stern:' => 'hviezdička:', //13
    'Sterne:' => 'hviezdičky:', //13 
    '5Sterne:' => 'hviezdičiek:', //13 
    'Produkt' => 'Položka', //13
    'Preis' => 'Cena', //13
    'Jetzt kaufen' => 'Objednať', //13
    'Dosierung' => 'Dávkovanie', //13
    'Menge + Bonus' => 'Množstvo + bonus', //13
    'Menge' => 'Množstvo', //13
    // '' => '',

    'kaufen' => 'Pridať do košíka', //0
    'Eigene Bewertung erstellen' => 'Nový posudok', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} z {vote_all} ľudí považovalo nasledujúcu recenziu za užitočnú', //41
    'nein' => 'Nie', //41
    'ja' => 'Áno', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Pomohla vám táto recenzia?', //41
    'von' => 'od', //41

    'Rezension abgeben' => 'Vytvorte  vlastnú recenziu', //42
    'Name' => 'Meno', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Názov', //42
    'Bewertung abgeben' => 'Hodnotenie webových stránok', //42
    'Webseitenfreundlichkeit' => 'Webová stránka', //42
    'Versand Zufriedenheit' => 'Doprava', //42
    'Preis / Leistung' => 'Cena / výkon', //42
    'Empfehlung' => 'Odporúčanie', //42
    'Kundenkommentar' => 'Hodnotenie zákazníkov', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Napíšte znaky, ktoré vidíte na obrázku.', //42
    'Rezension abschicken' => 'Poslať recenziu', //42

);