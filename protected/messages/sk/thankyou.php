<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Ďakujeme za objednávku.', //14
	'Bestellnummer' => 'Číslo objednávky',//14
    'Bestellte Produkte' => 'Výrobky objednané',//14
    'Versendungsart' => 'Spôsob doručenia',//14
    'Versicherung' => 'Poistenie',//14
    'Telefonnummer' => 'Telefónne číslo',//14
    'Drucken' => 'Vytlačiť',//14

    'Bankverbindung' => 'Bankové údaje',//14
    'Empfänger' => 'Príjemca',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Účel platby',//14
    'Betrag' => 'Suma', //14
    'Bank' => 'Banka', //14
    'Adresse' => 'Adresa', //14
    // '' => '',


);