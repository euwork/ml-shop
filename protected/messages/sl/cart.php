<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Moja košarica', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Nadgradite in prihranite', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Vzemite {tabl_count} dodatnih tablet za {symbol}{tabl_price} ({symbol}{tabl_price_one} na tableto)', //13
    'Aktualisieren' => 'Nadgradnja', //13
    'Gesamtbetrag' => 'Skupaj', // 0
    'Mit Einkauf fortfahren' => 'Nadaljuj z nakupovanjem', //13
    'Zur Kasse' => 'Naročite ', //13
    'Produkt' => 'Izdelek', //13
);