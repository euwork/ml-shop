<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-pošta', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Ime', //13
	'Name' => 'Polno ime', //14
	'Nachname' => 'Priimek', //13
	'Strasse / Hausnr' => 'Naslov', //13
	'Postleitzahl' => 'pošta/poštna številka', //13
	'Ort' => 'Mesto', //13
	'Land' => 'Država', //13
	'Telefonnummer' => 'Telefonska številka', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Vaše naročilo', //13
	'Lieferadresse' => 'Informacije o dostavi', //13
	'Rechnungsadresse' => 'Podatki za obračun', //13
	'Lieferadresse & Rechnungsadresse' => 'Naslov za dostavo in naslov za zaračunavanje ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Če se naslov za izdajo računa in naslov za pošiljanje ujemata, kliknite tukaj', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Moja košarica', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Skupni znesek', //13
	'Versandart' => 'Način pošiljanja', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Običajna dostava (8-13 delovnih dni)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Običajna dostava (8-21 delovnih dni)', //0
	'Express Versand (4-7 Werktage)' => 'Hitra dostava (4-7 delovnih dni)', //0
    'Express Versand (7-13 Werktage)' => 'Hitra dostava (7-13 delovnih dni)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standardna dostava (8-21 delovnih dni)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Zavarovanje dostave - {symbol}{price} (Zagotovljeno ponovno pošiljanje, če dostava ne uspe)', //13
	'Bitte Zahlungsart auswählen' => 'Izberite način plačila', //13
	'Zahlungsart' => 'Vrsta plačila', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bančno nakazilo', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Številka kreditne kartice', //13
	'Gültig bis' => 'Rok veljavnosti kreditne kartice', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Dokončajte naročilo', //13
	'Allg. Geschäftsbedingungen' => 'Splošni transakcijski pogoji', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Splošni transakcijski pogoji: s tem potrjujem, da so vsi moji osebni podatki (ali podatki) pravilni, prebral sem obrazložitev in razumem njihove pogoje.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);