<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Koristne informacije', //1
    'Lieferkonditionen' => 'Pogoji dostave', //1
    'Rückerstattung' => 'Vračilo denarja', //1
    'Affiliates' => 'Partnerji', //1
    'AGB' => 'Pravila in pogoji', //1
    'Datenschutz' => 'Politika zasebnosti', //1
    'Impressum' => 'Odtis', //1
    'Garantie' => 'Garancija', //1
    'Warenkorb' => 'Moja košarica', //1
    'Kein Produkt im Warenkorb' => 'V moji košarici ni predmetov', //1
    'Gesamtbetrag' => 'Skupno naročilo', //1
    '1 Produkt im Warenkorb' => '1 artikel v moji košarici', //1
    '2 Produkt im Warenkorb' => '2 artikla v moji košarici', //1
    '{count} Produkt im Warenkorb' => '{count} artikli v moji košarici', //1
    'Produkte im Warenkorb' => 'artiklov v moji košarici', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Možnosti plačila', //1
    'VERSANDPARTNER' => 'Načini pošiljanja', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);