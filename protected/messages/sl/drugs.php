<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Ocene strank', //13
    'Rezensionen' => 'Ocene', //13
    'Alle {count} Bewertungen anzeigen...' => 'Oglejte si vse {count} ocen', //13
    'Stern:' => 'Zvezdica:', //13
    'Sterne:' => 'Zvezdice:', //13 
    '2Sterne:' => 'Zvezdici:', //13 
    '5Sterne:' => 'Zvezdic:', //13 
    'Produkt' => 'Postavka', //13
    'Preis' => 'Cena', //13
    'Jetzt kaufen' => 'Naročite zdaj', //13
    'Dosierung' => 'Doziranje', //13
    'Menge + Bonus' => 'Količina + bonus', //13
    'Menge' => 'Količina', //13
    // '' => '',

    'kaufen' => 'Dodati v košarico', //0
    'Eigene Bewertung erstellen' => 'Novo Pričevanje', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} od {vote_all} ljudi je našlo naslednji pregled kot koristen', //41
    'nein' => 'Ne', //41
    'ja' => 'Da', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Ali vam je bil ta pregled koristen?', //41
    'von' => 'od', //41

    'Rezension abgeben' => 'Ustvarite svoj pregled', //42
    'Name' => 'Ime', //42
    'Email' => 'E-pošta', //42
    'Thema' => 'Naslov', //42
    'Bewertung abgeben' => 'Ocena spletne strani', //42
    'Webseitenfreundlichkeit' => 'Spletna stran', //42
    'Versand Zufriedenheit' => 'Dostava', //42
    'Preis / Leistung' => 'Cena / izvedba', //42
    'Empfehlung' => 'Priporočilo', //42
    'Kundenkommentar' => 'Mnenje strank', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Vpišite znake, ki jih vidite na sliki.', //42
    'Rezension abschicken' => 'Pošlji pregled', //42

);