<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Zahvaljujemo se vam za vaše naročilo.', //14
	'Bestellnummer' => 'Naročilo št.',//14
    'Bestellte Produkte' => 'Naročeni izdelki',//14
    'Versendungsart' => 'Način dostave ',//14
    'Versicherung' => 'Zavarovanje',//14
    'Telefonnummer' => 'Telefonska številka',//14
    'Drucken' => 'Natisni',//14

    'Bankverbindung' => 'Bančni podatki',//14
    'Empfänger' => 'Upravičenec',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Namen plačila',//14
    'Betrag' => 'Znesek', //14
    'Bank' => 'Banka', //14
    'Adresse' => 'Naslov', //14
    // '' => '',


);