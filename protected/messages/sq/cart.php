<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Karroca ime',
    'Zum Warenkorb hinzufügen' => 'Add to cart',
    'Jetzt Rabatt sichern!' => 'Azhurnoni dhe kurseni',
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Merrni {tabl_count} pilula shtesë për {symbol}{tabl_price} ({symbol}{tabl_price_one} për pilulë)',
    'Aktualisieren' => 'Azhurnoni',
    'Gesamtbetrag' => 'Total',
    'Mit Einkauf fortfahren' => 'Vazhdoni blerjet',
    'Zur Kasse' => 'Bëni një porosi',
    'Produkt' => 'Produkt',
);