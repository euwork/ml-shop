<?php

return array(
	'E-Mail' => 'E-mail', 
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Emri', 
	'Name' => 'Emri i plotë', 
	'Nachname' => 'Mbiemri', 
	'Strasse / Hausnr' => 'Adresë', 
	'Postleitzahl' => 'Kodi Postar / Postar', 
	'Ort' => 'Qyteti', 
	'Land' => 'Vend', 
	'Telefonnummer' => 'Numri i telefonit', 
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Porosia juaj', 
	'Lieferadresse' => 'Informacioni i Transportit', 
	'Rechnungsadresse' => 'Informacioni i faturimit', 
	'Lieferadresse & Rechnungsadresse' => 'Adresa e dorëzimit dhe adresa e faturimit ', 
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Nëse adresa e faturimit dhe adresa e transportit përputhen, klikoni këtu', 
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Karroca ime', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Shuma totale', //13
	'Versandart' => 'Metoda e transportit', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Transporti i rregullt (8-13 ditë pune)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Transporti i rregullt (8-21 ditë pune)', //0
	'Express Versand (4-7 Werktage)' => 'Transporti Express (4-7 ditë pune)', //0
    'Express Versand (7-13 Werktage)' => 'Transporti Express (7-13 ditë pune)', //00
    'Standardlieferung (8-21 Werktage)' => 'Dorëzimi standard (8-21 ditë pune)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Sigurimi i dorëzimit - {symbol}{price} (dërgimi i garantuar nëse dështimi i dorëzimit)', //13
	'Bitte Zahlungsart auswählen' => 'Ju lutemi zgjidhni mënyrën e pagesës', //13
	'Zahlungsart' => 'Lloji i pageses', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Transferte bankare', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Numri i kartës së kreditit', //13
	'Gültig bis' => 'Skadimi i Kartës së Kreditit', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Porosi e plotë', //13
	'Allg. Geschäftsbedingungen' => 'Kushtet e përgjithshme të transaksionit', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Kushtet e përgjithshme të transaksionit: Me anë të kësaj, unë konfirmoj që të gjitha të dhënat e mia personale (ose detajet) janë të sakta, kam lexuar shpjegimin dhe kam kuptuar kushtet e tyre.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);