<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Informacion i dobishëm', //1
    'Lieferkonditionen' => 'Kushtet e dorëzimit', //1
    'Rückerstattung' => 'Rimbursimi', //1
    'Affiliates' => 'Bashkëpunëtorët', //1
    'AGB' => 'Termat dhe Kushtet', //1
    'Datenschutz' => 'Politika e privatësisë', //1
    'Impressum' => 'Shtyp', //1
    'Garantie' => 'Garanci', //1
    'Warenkorb' => 'Karroca ime', //1
    'Kein Produkt im Warenkorb' => 'Asnjë artikull në karrocën time', //1
    'Gesamtbetrag' => 'Totali i porosisë', //1
    '1 Produkt im Warenkorb' => '1 artikull në shportën time', //1
    'Produkte im Warenkorb' => 'artikuj në shportën time', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Mundësitë e pagesës', //1
    'VERSANDPARTNER' => 'Metodat e Transportit', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);