<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Shtypni personazhet që shihni në foto',
	'Name'=> 'Emrin',
	'E-Mail'=> 'E-mail',
	'Betreff'=> 'Lënda',
	'Nachricht'=> 'Mesazh',
	'Kontakt' => 'Na kontaktoni',
	'Senden' => 'Dërgoni',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Faleminderit! Ne kemi marrë mesazhin tuaj dhe do të jemi në kontakt me ju së shpejti!',

	);