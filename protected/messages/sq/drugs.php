<?php 


return array(
    'Kundenrezensionen' => 'Rishikimet e blerësve',
    'Rezensionen' => 'Shqyrtime',
    'Alle {count} Bewertungen anzeigen...' => 'Shihni të gjitha {count} shqyrtimet',
    'Stern:' => 'yll:',
    'Sterne:' => 'Yje:',
    'Produkt' => 'Artikulli',
    'Preis' => 'Çmimi',
    'Jetzt kaufen' => 'Porosit Tani',
    'Dosierung' => 'Dozimi',
    'Menge + Bonus' => 'Sasia + Bonusi',
    'Menge' => 'Sasi',
    // '' => '',

    'kaufen' => 'Shtoni në shportë',
    'Eigene Bewertung erstellen' => 'Dëshmi e Re',
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} nga {vote_all} njerëz e konsideruan të dobishme rishikimin e mëposhtëm',
    'nein' => 'Jo',
    'ja' => 'Po',
    'Hat Ihnen diese Bewertung geholfen?' => 'A ishte i dobishëm ky rishikim për ju?',
    'von' => 'nga',

    'Rezension abgeben' => 'Krijoni rishikimin tuaj',
    'Name' => 'Emri',
    'Email' => 'E-mail',
    'Thema' => 'Titulli',
    'Bewertung abgeben' => 'Vlerësimi i faqes në internet',
    'Webseitenfreundlichkeit' => 'Uebfaqe',
    'Versand Zufriedenheit' => 'Transporti detar',
    'Preis / Leistung' => 'Çmimi / Performanca',
    'Empfehlung' => 'Rekomandimi',
    'Kundenkommentar' => 'Rishikimi i klientit',
    'Geben Sie die Zeichen in dem Bild ein' => 'Shtypni personazhet që shihni në foto.',
    'Rezension abschicken' => 'Dërgo shqyrtim',

);