<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Moja korpa', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Nadogradite i spremite', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => 'Uzmite {tabl_count} dodatne tablete za {symbol}{tabl_price} ({symbol}{tabl_price_one} po tableti)', //13
    'Aktualisieren' => 'Nadogradite', //13
    'Gesamtbetrag' => 'Ukupno', // 0
    'Mit Einkauf fortfahren' => 'Nastaviti s kupovinom', //13
    'Zur Kasse' => 'Napravite narudžba', //13
    'Produkt' => 'Proizvod', //13
);