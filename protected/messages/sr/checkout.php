<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-mail', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Ime', //13
	'Name' => 'Puno ime', //14
	'Nachname' => 'Prezime', //13
	'Strasse / Hausnr' => 'Adresa', //13
	'Postleitzahl' => 'Poštanski broj ', //13
	'Ort' => 'Grad', //13
	'Land' => 'Zemlja', //13
	'Telefonnummer' => 'Broj telefona', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Tvoja narudžba', //13
	'Lieferadresse' => 'Informacije o dostavi', //13
	'Rechnungsadresse' => 'Podaci o naplati', //13
	'Lieferadresse & Rechnungsadresse' => 'Adresa za dostavu & Adresa za naplatu ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Ako se adresa za naplatu i adresa za dostavu podudaraju, kliknite ovde', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Moja korpa', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Ukupni iznos', //13
	'Versandart' => 'Metoda slanja', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Redovna poštarina (8-13 radni dan)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Redovna poštarina (8-21 radni dan)', //0
	'Express Versand (4-7 Werktage)' => 'Ekspresna poštarina (4-7 radnih dana)', //0
    'Express Versand (7-13 Werktage)' => 'Ekspresna poštarina (7-13 radni dan)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standardna dostava (8-21 radni dan)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Osiguranje dostave - {symbol}{price} (Zagarantovano ponovno slanje u slučaju neuspeha dostave)', //13
	'Bitte Zahlungsart auswählen' => 'Odaberite način plaćanja', //13
	'Zahlungsart' => 'Način plaćanja', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bankovni prenos', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Broj kreditne kartice', //13
	'Gültig bis' => 'Istek kreditne kartice', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Kompletna narudžba', //13
	'Allg. Geschäftsbedingungen' => 'Opšti uslovi transakcije', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Opšti uslovi transakcije: Ovime potvrđujem da su svi moji lični podaci (ili detalji) tačni, pročitao/la sam objašnjenje i razumem njihove uslove.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);