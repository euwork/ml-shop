<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Recenzije kupaca', //13
    'Rezensionen' => 'Recenzije', //13
    'Alle {count} Bewertungen anzeigen...' => 'Pogledajte sve {count} recenzije', //13
    'Stern:' => 'zvezdica:', //13
    'Sterne:' => 'zvezdice:', //13 
    '5Sterne:' => 'zvezdica:', //13 
    'Produkt' => 'Artikal', //13
    'Preis' => 'Cena', //13
    'Jetzt kaufen' => 'Naručite sada', //13
    'Dosierung' => 'Doziranje', //13
    'Menge + Bonus' => 'Količina + Bonus', //13
    'Menge' => 'Količina', //13
    // '' => '',

    'kaufen' => 'Dodaj u korpu', //0
    'Eigene Bewertung erstellen' => 'Novi Komentar', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} od {vote_all} posoba smatraju da je sledeća recenzija korisna', //41
    'nein' => 'Ne', //41
    'ja' => 'Da', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Da li vam je recenzija bila korisna?', //41
    'von' => 'od', //41

    'Rezension abgeben' => 'Napravite vlastitu recenziju', //42
    'Name' => 'Ime', //42
    'Email' => 'E-mail', //42
    'Thema' => 'Naslov', //42
    'Bewertung abgeben' => 'Ocena web stranice', //42
    'Webseitenfreundlichkeit' => 'Web stranica', //42
    'Versand Zufriedenheit' => 'Isporuka', //42
    'Preis / Leistung' => 'Cena / Performanse', //42
    'Empfehlung' => 'Preporuka', //42
    'Kundenkommentar' => 'Recenzije kupaca', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Upišite znakove koje vidite na slici.', //42
    'Rezension abschicken' => 'Pošaljite recenziju', //42

);