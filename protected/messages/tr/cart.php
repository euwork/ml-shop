<?php // тут для всех полей эта страница https://clip2net.com/s/4cGNhwa

return array(
    'Warenkorb' => 'Sepetim', //13
    'Zum Warenkorb hinzufügen' => 'Add to cart', // не используется, можно не переводить
    'Jetzt Rabatt sichern!' => 'Yükseltme ve kaydetme', //13
    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)' => '{symbol}{tabl_price} için {tabl_count} ekstra hap alın (hap başına {symbol}{tabl_price_one})', //13 Take {tabl_count} extra pills for {symbol}{tabl_price} ({symbol}{tabl_price_one} per pill)
    'Aktualisieren' => 'Yükseltme', //13
    'Gesamtbetrag' => 'Toplam', // 0
    'Mit Einkauf fortfahren' => 'Alışverişe devam et', //13
    'Zur Kasse' => 'Ödeme', //13
    'Produkt' => 'Ürün', //13
);