<?php  //    это всё тут https://clip2net.com/s/4cGOsZD

return array(
	'E-Mail' => 'E-posta', //14 и 13
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'Isim', //13
	'Name' => 'Tam adı', //14
	'Nachname' => 'Soyadı', //13
	'Strasse / Hausnr' => 'Adres', //13
	'Postleitzahl' => 'Posta Kodu', //13
	'Ort' => 'Şehir', //13
	'Land' => 'Ülke', //13
	'Telefonnummer' => 'Telefon numarası', //13
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Siparişiniz', //13
	'Lieferadresse' => 'Teslimat Bilgileri', //13
	'Rechnungsadresse' => 'Fatura Bilgileri', //13
	'Lieferadresse & Rechnungsadresse' => 'Teslimat adresi ve fatura adresi ', //13
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.', //для шаблона д7 (нажмите, если адреса отличаются )
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'Fatura adresi ve gönderim adresi eşleşirse, buraya tıklayın', //13
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details', // не используются, можно удалять в новых переводах
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING', // не используются, можно удалять в новых переводах
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT', // не используются, можно удалять в новых переводах
	'Zusammenfassung' => 'SUMMARY', // не используются, можно удалять в новых переводах
	'Warenkorb' => 'Sepetim', //13
	'Anrede' => 'Salutation', // не используются, можно удалять в новых переводах
	'Herr' => 'Man', // не используются, можно удалять в новых переводах
	'Frau' => 'Woman', // не используются, можно удалять в новых переводах

	'Gesamtbetrag' => 'Toplam tutar', //13
	'Versandart' => 'Teslimat Yöntemi', //13
	'Regulärer Postversand (8-13 Werktage)' => 'Düzenli Teslimat (8-13 iş günü)', //00
    'Regulärer Postversand (8-21 Werktage)' => 'Düzenli Teslimat (8-21 iş günü)', //0
	'Express Versand (4-7 Werktage)' => 'Ekspres Kargo (4-7 iş günü)', //0
    'Express Versand (7-13 Werktage)' => 'Ekspres Kargo (7-13 iş günü)', //00
    'Standardlieferung (8-21 Werktage)' => 'Standart teslimat (8-21 iş günü)', //0
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Teslimat Sigortası - {symbol}{price} (Teslimat başarısız olursa garantili yeniden sevkiyat)', //13
	'Bitte Zahlungsart auswählen' => 'Lütfen ödeme yöntemini seçin', //13
	'Zahlungsart' => 'Ödeme Şekli', //13

	'Visa' => 'Visa',
	'Banküberweisung' => 'Banka Havalesi', //13
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Kredi Kartı Numarası', //13
	'Gültig bis' => 'Kredi Kartı Son Kullanma', //13
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Komple Sipariş', //13
	'Allg. Geschäftsbedingungen' => 'Genel İşlem Koşulları', //13
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'Genel işlem koşulları: burada, tüm kişisel verilerimin (veya ayrıntılarımın) doğru olduğunu onaylıyorum, açıklamayı okudum ve şartlarını anlıyorum.', //13

	'Was ist es?' => 'What is that?', // не используются
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.', // не используются
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.', // не используются
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now', // не используются
	);