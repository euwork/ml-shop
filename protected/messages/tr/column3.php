<?php


return array(
    // 'Artikel' => 'Articles',
    'Nützliche Informationen' => 'Faydalı Bilgiler', //1
    'Lieferkonditionen' => 'Teslimat Koşulları', //1
    'Rückerstattung' => 'Geri ödeme', //1
    'Affiliates' => 'Kuruluşlar', //1
    'AGB' => 'Şartlar ve Koşullar', //1
    'Datenschutz' => 'Gizlilik politikası', //1
    'Impressum' => 'Baskılı', //1
    'Garantie' => 'Garanti', //1
    'Warenkorb' => 'Sepetim', //1
    'Kein Produkt im Warenkorb' => 'Sepetimde ürün yok', //1
    'Gesamtbetrag' => 'Toplam', //1
    '1 Produkt im Warenkorb' => 'Sepetimde 1 ürün', //1
    // 'Produkte im Warenkorb' => 'Items in my cart', //1
    '{count} Produkt im Warenkorb' => 'Sepetimde {count} ürün', //1
    // 'Angebot' => 'Offer',
    'kostenloser online support' => 'Free online support', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
    // 'Vorteile' => 'Advantages',
    'ZAHLUNGSMÖGLICHKEITEN' => 'Ödeme seçenekleri', //1
    'VERSANDPARTNER' => 'Teslimat Yöntemleri', //1
    'Secure shopping protected by SSL' => 'Secure shopping protected by SSL', //????  тут альты картинок в колонке справа. я их уже убрал. можно удалить данные строки
);