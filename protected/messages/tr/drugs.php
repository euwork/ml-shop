<?php https://clip2net.com/s/4cGQGDk     https://clip2net.com/s/4cGQHkD страница с озывами ещё в переводе


return array(
    'Kundenrezensionen' => 'Müşteri yorumları', //13
    'Rezensionen' => 'yorumlar', //13
    'Alle {count} Bewertungen anzeigen...' => 'Tüm {count} yorumlara bakın', //13
    'Stern:' => 'yıldız:', //13
    'Sterne:' => 'yıldız:', //13 
    'Produkt' => 'Öğe', //13
    'Preis' => 'Fiyat', //13
    'Jetzt kaufen' => 'Sipariş ver', //13
    'Dosierung' => 'Doz', //13
    'Menge + Bonus' => 'Miktar bonusu', //13
    'Menge' => 'Miktar', //13
    // '' => '',

    'kaufen' => 'Sepete ekle', //0
    'Eigene Bewertung erstellen' => 'Yeni yorum', //42
    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich' => '{vote_yes} kişiden {vote_all} ü aşağıdaki incelemeyi faydalı buldu', //41
    'nein' => 'Hayır', //41
    'ja' => 'Evet', //41
    'Hat Ihnen diese Bewertung geholfen?' => 'Bu inceleme size yardımcı oldu mu?', //41
    'von' => 'den', //41

    'Rezension abgeben' => 'Kendi özel incelemenizi oluşturun', //42
    'Name' => 'İsim', //42
    'Email' => 'E-posta', //42
    'Thema' => 'Başlık', //42
    'Bewertung abgeben' => 'İnternet Sitenin Derecelendirmesi', //42
    'Webseitenfreundlichkeit' => 'İnternet sitesi', //42
    'Versand Zufriedenheit' => 'Teslimat', //42
    'Preis / Leistung' => 'Fiyat/Performans', //42
    'Empfehlung' => 'Öneri', //42
    'Kundenkommentar' => 'Müşteri yorumu', //42
    'Geben Sie die Zeichen in dem Bild ein' => 'Resimde gördüğünüz sembolleri giriniz.', //42
    'Rezension abschicken' => 'Inceleme göndermek', //42

);