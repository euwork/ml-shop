<?php          //после заказа


return array(

	'Vielen Dank für Ihre Bestellung' => 'Siparişiniz için teşekkürler.', //14
	'Bestellnummer' => 'Sipariş Numarası',//14
    'Bestellte Produkte' => 'Sipariş edilen ürünler',//14
    'Versendungsart' => 'Teslimat yöntemi',//14
    'Versicherung' => 'Sigorta',//14
    'Telefonnummer' => 'Telefon numarası',//14
    'Drucken' => 'Mühür',//14

    'Bankverbindung' => 'Banka bilgileri',//14
    'Empfänger' => 'Lehtar',//14
    'IBAN' => 'IBAN',//14
    'BIC' => 'BIC',//14
    'Zielland' => 'Destination country',//сейчас не используется
    'Verwendungszweck' => 'Ödeme amacı',//14
    'Betrag' => 'Tutar', //14
    'Bank' => 'Banka', //14
    'Adresse' => 'Adres', //14
    // '' => '',


);