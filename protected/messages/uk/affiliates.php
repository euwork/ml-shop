<?php

return array(
	'Persönliche Details' => 'Personal data',
	'Vorname' => 'First Name',
	'Email' => 'E-mail',
	'Webseite' => 'Web-Page',
	'Nachricht' => 'Message',
	'Geben Sie die Zeichen in dem Bild ein' => 'Type the characters you see in the picture',
	'Senden' => 'Send',
	);