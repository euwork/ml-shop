<?php

return array(
	'E-Mail' => 'E-mail',
	// 'E-Mail bestätigen' => 'Confirmez votre E-mail',
	'Vorname' => 'First Name',
	'Name' => 'Full name',
	'Nachname' => 'Last Name',
	'Strasse / Hausnr' => 'Address',
	'Postleitzahl' => 'Zip/Postal Code',
	'Ort' => 'City',
	'Land' => 'Country',
	'Telefonnummer' => 'Phone number',
	// 'Mobilrufnummer' => 'Cell phone number',
	'Ihre Bestellung' => 'Your order',
	'Lieferadresse' => 'Shipping Info',
	'Rechnungsadresse' => 'Billing Info',
	'Lieferadresse & Rechnungsadresse' => 'Delivery address & Billing address ',
	'Klicken Sie hier, wenn die Rechnungs- und Lieferadresse verschieden sind' => 'Please click if the billing and shipping addresses are different.',
	'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier' => 'If the billing address and shipping address match, click here',
	'BESTELLUNG ABSCHLIESSEN' => 'Confirm Details',
	'Versand & Rechnungsanschrift' => 'SHIPPING & BILLING',
	'Versand & Zahlungsart wählen' => 'SHIPPING METHOD & PAYMENT',
	'Zusammenfassung' => 'SUMMARY',
	'Warenkorb' => 'My cart',
	'Anrede' => 'Salutation',
	'Herr' => 'Man',
	'Frau' => 'Woman',

	'Gesamtbetrag' => 'Total amount',
	'Versandart' => 'Shipping Method',
	'Regulärer Postversand (8-13 Werktage)' => 'Regular Shipping (8-13 business days)',
    'Regulärer Postversand (8-21 Werktage)' => 'Regular Shipping (8-21 business days)',
	'Express Versand (4-7 Werktage)' => 'Express Shipping (4-7 business days)',
    'Express Versand (7-13 Werktage)' => 'Express Shipping (7-13 business days)',
    'Standardlieferung (8-21 Werktage)' => 'Standard delivery (8-21 business days)',
	'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)' => 'Delivery Insurance - {symbol}{price} (Guaranteed reshipment if delivery failed)',
	'Bitte Zahlungsart auswählen' => 'Please select payment method',
	'Zahlungsart' => 'Payment Type',

	'Visa' => 'Visa',
	'Banküberweisung' => 'Bank transfer',
	'MasterCard' => 'MasterCard',
	'American Express' => 'American Express',
	'Blue Card' => 'Carte Bleue',
    'Date of birth' => 'Date of birth',

	'Kreditkartennummer' => 'Credit Card Number',
	'Gültig bis' => 'Credit Card Expiration',
	'CVV2' => 'CVV2',
	'Bestellung abschließen' => 'Complete Order',
	'Allg. Geschäftsbedingungen' => 'General Transaction Terms',
	'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.' => 'General Transaction Terms: Hereby, I confirm that all my personal data (or details) are correct, I have read the explanation and understand the terms thereof.',

	'Was ist es?' => 'What is that?',
    'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.' => 'For American Express this is the number on the front side of the card.',
    'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.' => 'For Mastercard, Visa or Discover these are the last three numbers on the back side of the card.',
	// 'Bitte Zahlungsart auswahlen' => 'Sélectionnez s’il vous plaît votre procédé de paiement',
	'Jetzt bezahlen' => 'Pay now',
	);