<?php


return array(
	'Geben Sie die Zeichen in dem Bild ein' => 'Type the characters you see in the picture',
	'Name'=> 'Name',
	'E-Mail'=> 'E-mail',
	'Betreff'=> 'Subject',
	'Nachricht'=> 'Message',
	'Kontakt' => 'Contact us',
	'Senden' => 'Send',
	'Vielen Dank! Wir haben Ihre Nachricht erhalten und werden uns bald mit Ihnen in Verbindung setzen!' => 'Thank you! We\'ve received your  message and will be in touch with you soon!',

	);