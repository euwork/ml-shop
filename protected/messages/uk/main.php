<?php

return array(
    'Startseite' => 'Main Page',//perevod
    'Über uns' => 'About Us',
    'F.A.Q.' => 'F.A.Q.',
    'Kontakt' => 'Contact Us',
    'So bestellen Sie' => 'How to order',
    'To top' => 'To top',//perevod
    'Urheberrecht' => 'Copyright',
    'Alle Rechte vorbehalten' => 'All rights reserved',
);