<?php

/**
 * This is the model class for table "{{affiliates}}".
 *
 * The followings are the available columns in table '{{affiliates}}':
 * @property integer $aff_id
 * @property string $name
 * @property string $email
 * @property string $website
 * @property string $content
 */
class Affiliates extends CActiveRecord
{
	public $code;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{affiliates}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, content', 'required'),
			array('email', 'email'),
			array('website', 'url', 'defaultScheme' => 'http'),
			array('name, email, website', 'length', 'max'=>256),
			array('content', 'length', 'max'=>3000),
			array('name', 'match', 'pattern'=>'/^[\w\s\.\-]+$/'),
			array('code','captcha','allowEmpty'=>!CCaptcha::checkRequirements() ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('aff_id, name, email, website, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'aff_id' => 'Aff',
			'name' => Yii::t('affiliates', 'Vorname'),
			'email' => Yii::t('affiliates', 'Email'),
			'website' => Yii::t('affiliates', 'Webseite'),
			'content' => Yii::t('affiliates', 'Nachricht'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('aff_id',$this->aff_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Affiliates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
