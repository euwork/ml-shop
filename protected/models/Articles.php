<?php

/**
 * This is the model class for table "{{articles}}".
 *
 * The followings are the available columns in table '{{articles}}':
 * @property integer $art_id
 * @property integer $acat_id
 * @property string $slug
 * @property string $title
 * @property string $postname
 * @property string $header
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $date_cr
 * @property string $author
 *
 * The followings are the available model relations:
 * @property CategoriesArticles $acat
 */
class Articles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acat_id, slug, title, header, content, date_cr', 'required'),
			array('acat_id', 'numerical', 'integerOnly'=>true),
			array('slug', 'length', 'max'=>128),
			array('lang', 'in', 'range' => Languages::getArrayFlip()),
			array('title, postname, header, keywords, description, author', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('art_id, acat_id, slug, title, postname, header, keywords, description, content, date_cr, author', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'cat' => array(self::BELONGS_TO, 'CategoriesArticles', 'acat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'art_id' => 'Art',
			'acat_id' => 'Acat',
			'slug' => 'Slug',
			'title' => 'Title',
			'postname' => 'Postname',
			'header' => 'Header',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'content' => 'Content',
			'date_cr' => 'Date Cr',
			'author' => 'Author',
		);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Articles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public static function getArticles()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'art.lang = :lang';
		$criteria->alias = 'art';
		$criteria->params = array(':lang' => Yii::app()->language);
		$criteria->select = array('art.slug', 'art.postname', 'art.date_cr');
		// $criteria->with = array('cat');

		$posts = Articles::model()->findAll($criteria);
		
		return $posts;
	}

	public function getUrl()
	{
		$url = Yii::app()->createUrl( Yii::app()->config->get('prefix.article') . '/' . $this->slug);
		#var_dump($url);Yii::app()->end();

		if(Yii::app()->controller->id == 'admin'){

			if($this->lang == Yii::app()->language){
				
				return $url;
				
			} else {
				$data = parse_url($url);
				$url = $data['scheme'].'://'.$data['host'].'/'.$this->lang  . $data['path'] . (isset($data['query']) ? '?'.$data['query']: '');
				return $url;
			}
		} else {
			return $url;
		}

	}

    // public function behaviors()
    // {
    //     return array(
    //         'ml' => array(
    //             'class' => 'ext.multilangual.MultilingualBehavior',
    //             'localizedAttributes' => array(
    //             	'postname',
    //                 'title',
    //                 'header',
    //                 'keywords',
    //                 'description',
    //                 'content',
    //                 'date_cr',
    //                 'author',
    //             ),
    //             'langClassName' => 'ArticlesLang',
    //             'langTableName' => 'articles_lang',
                // 'languages' => Languages::getArray(),
                // 'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
    //             'langForeignKey' => 'owner_id',
    //             'dynamicLangClass' => true,
    //         ),
    //     );
    // }
}
