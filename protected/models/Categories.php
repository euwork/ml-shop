<?php

/**
 * This is the model class for table "{{categories}}".
 *
 * The followings are the available columns in table '{{categories}}':
 * @property integer $cat_id
 * @property string $slug
 * @property string $catname
 * @property string $title
 * @property string $header
 * @property string $keywords
 * @property string $description
 */
class Categories extends PageModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{categories}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug, catname', 'required'),
			array('slug', 'length', 'max'=>128),
			array('catname, title, header, keywords, description', 'length', 'max'=>256),
			array('title, header, keywords, description', 'filter', 'filter'=>'strip_tags'),
			array('title, header, keywords, description', 'filter', 'filter'=>'trim'),
			array('content', 'safe'),
			array('slug','match', 'pattern'=>'/^[a-z0-9\.\-]+$/'),
			// array('slug', 'check_slug'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cat_id, slug, catname, title, header, keywords, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() 
    { 
        // NOTE: you may need to adjust the relation name and the related 
        // class name for the relations automatically generated below. 
        return array( 
            'drugs' => array(self::HAS_MANY, 'Drugs', 'cat_id', 'order'=>'drugs.position ASC, drug_id ASC', 'condition'=>'drugs.active=1'),
        ); 
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cat_id' => 'Cat',
			'slug' => 'Page url',
			'catname' => 'Catname',
			'title' => 'Title',
			'header' => 'Header &lt;h1&gt;',
			'keywords' => 'Keywords',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.cat_id',$this->cat_id);
		$criteria->compare('t.slug',$this->slug,true);
		$criteria->compare('t.catname',$this->catname,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.header',$this->header,true);
		$criteria->compare('t.keywords',$this->keywords,true);
		$criteria->compare('t.description',$this->description,true);

		return new CActiveDataProvider($this, array(
            'criteria' => $this->ml->modifySearchCriteria($criteria),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public static function getMenu()
	{

		$sql = "SELECT 
				c.l_slug as cat_slug, 
				c.l_catname as cat_name, 
				d.l_slug as drug_slug, 
				d.l_drugname as drug_name,
				cat.position as cat_order,  
				drug.position as drug_order  
				from {{categories}} cat  
				left join {{drugs}} drug on drug.cat_id = cat.cat_id 
				left join {{categories_lang}} c  on cat.cat_id = c.owner_id
				left join {{drugs_lang}} d on d.owner_id = drug.drug_id
				WHERE c.lang_id = :lang_id and d.lang_id = :lang_id and cat.active = 1 and drug.active = 1
				ORDER BY cat.position ASC, cat.cat_id ASC, drug.position ASC, drug.drug_id ASC ";


		$lang_id = Yii::app()->language;

		$connection=Yii::app()->db;
		$command=$connection->cache(Yii::app()->params['cachetime'])->createCommand($sql);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$dataReader=$command->query();

		$data =$dataReader->readAll();

		$result = [];

		foreach($data as $i=> $item)
		{
			if($item['cat_name'] == '') continue;
			if($item['drug_name'] == '') continue;

			if(!isset($result[$item['cat_order']])) {
				$result[$item['cat_order']]['name'] = $item['cat_name'];
				$result[$item['cat_order']]['url'] = Categories::getUrlBySlug($item['cat_slug']);
			}
			$result[$item['cat_order']]['drugs'][] = array('url'=> Drugs::getUrlBySlug($item['drug_slug']), 'name'=>$item['drug_name']);


		}
		
		return $result;
	}

	public static function getUrlBySlug($slug)
	{
		return Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.cat') . '/' . $slug);
	}

	public function getUrl()
	{
		return Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.cat') . '/' . $this->slug);
	}

	public static function getUrlById($id)
	{
		$model = self::model()->findByPk($id);

		return $model->getUrl();
	}

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array(
                	'catname',
                    'title',
                    'header',
                    'keywords',
                    'description',
                    'content',
                    'slug',
                ),
                'langClassName' => 'CategoriesLang',
                'langTableName' => 'categories_lang',
                'languages' => Languages::getArray(),
                'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
                'langForeignKey' => 'owner_id',
                'dynamicLangClass' => true,
                'forceOverwrite'=>true,
            ),
        );
    }
 
    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }

	public static function check_slug($slug, $cat_id)
	{
		// $model=self::model()->findByAttributes(array('slug' => $this->slug));
		// if($model===null or $model->primaryKey == $this->primaryKey){
		// 	return true;
		// }

		$sql = "SELECT owner_id, lang_id from {{categories_lang}}
				WHERE l_slug = :slug and owner_id <> :owner_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":owner_id", $cat_id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		if($l_id == false){
			return true;
		} 
		return false;
			
		// $this->addError ( $attribute, "slug not unique!" );
	}

	public static function findByLSlug($slug, $lang_id)
	{
		$sql = "SELECT owner_id from {{categories_lang}}
				WHERE l_slug = :slug and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		return $l_id;
	}

}
