<?php

/**
 * This is the model class for table "{{categories_articles}}".
 *
 * The followings are the available columns in table '{{categories_articles}}':
 * @property integer $acat_id
 * @property string $slug
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Articles[] $articles
 */
class CategoriesArticles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{categories_articles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug, name', 'required'),
			array('slug', 'length', 'max'=>128),
			array('name', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('acat_id, slug, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articles' => array(self::HAS_MANY, 'Articles', 'acat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'acat_id' => 'Acat',
			'slug' => 'Slug',
			'name' => 'Name',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoriesArticles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public static function getMenu()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = 'cat.lang = :lang';
		$criteria->alias = 'cat';
		$criteria->params = array(':lang' => Yii::app()->language);
		$criteria->select = array('cat.slug', 'cat.name');
		$criteria->with = array('articles' => array('select' => 'articles.slug, articles.postname'));

		$cats = CategoriesArticles::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);
		
		return $cats;
	}



	public function getUrl()
	{
		// return Yii::app()->getBaseUrl(true) . '/categories/' . $this->slug;
		return Yii::app()->createUrl('/catart/' . $this->slug);
	}


    // public function behaviors()
    // {
    //     return array(
    //         'ml' => array(
    //             'class' => 'ext.multilangual.MultilingualBehavior',
    //             'localizedAttributes' => array(
    //             	'name',
    //             ),
    //             'langClassName' => 'CategoriesArticlesLang',
    //             'langTableName' => 'categories_articles_lang',
                // 'languages' => Languages::getArray(),
                // 'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
    //             'langForeignKey' => 'owner_id',
    //             'dynamicLangClass' => true,
    //         ),
    //     );
    // }
}
