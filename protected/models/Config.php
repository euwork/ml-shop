<?php
/**
 * This is the model class for table "{{config}}".
 *
 * The followings are the available columns in table '{{config}}':
 * @property string $id
 * @property string $param
 * @property string $value
 * @property string $label
 * @property string $type
 */
class Config extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return '{{config}}';
    }
 
    public function rules()
    {
        return array(
            array('value', 'safe'),
            array('id, param, value, label, type', 'safe', 'on'=>'search'),
        );
    }


    public static function getCurrentProductsBase()
    {
        // if(Yii::app()->language == 'de'){
        //     $products_label = Products::BASE_GER;
        // } else {
        //     $products_label = Products::BASE_FR;
        // }

        $products_label = Products::BASE_FR;

        return $products_label;
    }  

    public static function getPriceIncrease()
    {
        return Yii::app()->config->get('price.increase');
    }   

    public static function getWireData($name)
    {
        $usefor2 = Yii::app()->config->get('wire.usefor2');
        $currentLang = Yii::app()->language;
        if($usefor2 == $currentLang){
            $name = $name . '2';
        }
        return Yii::app()->config->get($name);
    }   

    public static function getPriceIncreaseOld()
    {
        return Yii::app()->config->get('price.increase_old');
    }  

    public static function priceOldEnabled()
    {
        if(self::getPriceIncreaseOld() > 0){
            return true;
        }
        return false;
    }  

    public static function getPriceSteps()
    {
        $steps = array();
        foreach(range(0, 100, 5) as $number)
        {
            $steps[$number] = $number . ' %';
        }
        return $steps;
    }

    public static function findByParam($model, $param)
    {
        foreach($model as $item)
        {
            if($item->param == $param){
                return $item;
            }
        }

        return false;
    }

    const PRODUCTS_ORDER_ASC = 0;
    const PRODUCTS_ORDER_DESC = 1;
    public static function getProductsOrder()
    {
        $order = Yii::app()->config->get('price.sort_order');
        // if($model === null)
        //     return false;
        if($order == self::PRODUCTS_ORDER_ASC){
            return 'price ASC';
        }
        return 'price DESC';
    }

    public static function getProductsOrderList()
    {
        return array(
            self::PRODUCTS_ORDER_ASC => '↑ price',
            self::PRODUCTS_ORDER_DESC => '↓ price',
            );
    }

    const ORDER_FORM_ONE_STEP = 1;
    const ORDER_FORM_TWO_STEP = 0;

    public static function getOrderFormInOneStep()
    {
        $cart_steps = Yii::app()->config->get('cart.steps');
        // if($model === null)
        //     return false;
        if($cart_steps == self::ORDER_FORM_ONE_STEP){
            return true;
        }
        return false;
    }

    public static function getOrderFormModeList()
    {
        return array(
            self::ORDER_FORM_ONE_STEP => '1 step',
            self::ORDER_FORM_TWO_STEP => '2 steps',
            );
    }


    public static function updateBankDetails()
    {
        $data = Request::getBankDetails();

        // $bankdata = [];

        $bankdata = json_decode(json_encode($data), true);


        Config::saveBankDetails($bankdata);

        return $data;
    }

    public static function saveBankDetails($data)
    {     

        $attributes = explode(',', 'empf,iban,bic,country,name,adress,empf2,iban2,bic2,country2,name2,adress2,usefor2');
        foreach($attributes as $attr)
        {
            Yii::app()->config->set('wire.'.$attr,    $data[$attr] ); 
        }
        Yii::app()->config->set('wire.date_up', date('Y-m-d H:i:s') );


        Yii::app()->cache->flush();

        return true;
    }


    public static function getApiUrl($type = false)
    {
        $domain = Yii::app()->config->get('api_domain');
        switch ($type) {
            case 'content':
                $subd = 'ml-content.';
                if($_SERVER['SERVER_NAME'] == 'ml.local'){
                    $domain = 'local';
                }
                break;
            
            default:
                $subd = 'ml.';
                if($_SERVER['SERVER_NAME'] == 'ml.local'){
                    $subd = '';
                    $domain = 'admin_ml.local';
                }
                break;
        }
        $schema = 'http';

        $url = $schema . '://' . $subd . '' . $domain;

        return $url;
    }

}