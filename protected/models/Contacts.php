<?php

/**
 * This is the model class for table "{{contacts}}".
 *
 * The followings are the available columns in table '{{contacts}}':
 * @property integer $cont_id
 * @property string $name
 * @property string $email
 * @property string $thema
 * @property string $content
 */
class Contacts extends CActiveRecord
{
	public $code;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contacts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, content', 'required'),
			array('email', 'email'),
			array('name, email, thema', 'length', 'max'=>256),
			array('content', 'length', 'max'=>3000),
			array('name, thema', 'match', 'pattern'=>'/^[\w\s\.\-]+$/'),
			array('code','captcha','allowEmpty'=>!CCaptcha::checkRequirements() ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cont_id, name, email, thema, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cont_id' => 'Cont',
			'name' => Yii::t('contacts', 'Name'), 
			'email' => Yii::t('contacts', 'E-Mail'), 
			'thema' => Yii::t('contacts', 'Betreff'), 
			'content' => Yii::t('contacts', 'Nachricht'), 
			'code' => '',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cont_id',$this->cont_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('thema',$this->thema,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
