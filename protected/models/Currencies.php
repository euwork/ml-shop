<?php

/**
 * This is the model class for table "{{contacts}}".
 *
 * The followings are the available columns in table '{{contacts}}':
 * @property integer $cont_id
 * @property string $name
 * @property string $email
 * @property string $thema
 * @property string $content
 */
class Currencies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{currencies}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cur_id, symbol', 'required'),
			array('cur_id, symbol', 'length', 'max'=>10),
			array('rate', 'numerical', 'numberPattern'=>'/^(\d{1,8}\.\d{1,5}|\d{1,9})$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('cont_id, name, email, thema, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cur_id' => 'Id',
			'symbol' => Yii::t('currencies', 'Symbol'), 
		);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function countEnabled()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.active = 1';
		$count = self::model()->cache(Yii::app()->params['cachetime'])->count($criteria);

		return $count;
	}

	public static function getList()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.active = 1';
		$criteria->order = 't.position ASC';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

		return $model;
	}

	public static function getDefault()
	{
		// по массиву язык=>валюта
		$lang = Yii::app()->language;
		$list = self::getCountryAndCurrenciesList();
		if(isset($list[$lang])){
			$curr_id = $list[$lang];
		} else {
			$curr_id = 'EUR';
		}

		if(self::isActive($curr_id)){
			return $curr_id;
		}

		// если активная валюта по умолчанию
		$criteria = new CDbCriteria;
		$criteria->condition = 't.default = 1 and t.active = 1';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->find($criteria);
		if($model ==! null){
			return $model->cur_id;
		}

		// выбираем первую активную валюту
		if(self::countEnabled() > 0)
		{
			foreach(self::getList() as $item)
			{
				return $item->cur_id;
			}
		}


		//  если 0 активных - ставим валюту по умолчанию
		$criteria = new CDbCriteria;
		$criteria->condition = 't.default = 1';

		$model = self::model()->cache(Yii::app()->params['cachetime'])->find($criteria);
		if($model === null){
			return false;
		}
		return $model->cur_id;
	
	}

	private static function getCountryAndCurrenciesList()
	{
		return array(
			'de'=>'EUR',
			'us'=>'USD',
			'fr'=>'EUR',
			'au'=>'AUD',
			'es'=>'EUR',
			'uk'=>'GBP',
			'it'=>'EUR',
			'ca'=>'CAD',
			'nl'=>'EUR',
			'ca-fr'=>'CAD',
			'pt'=>'EUR',
			'br'=>'BRL',
			'se'=>'SEK',
			'pl'=>'PLN',
			'cs'=>'CZK',
			'da'=>'DKK',
			'fi'=>'EUR',
			'no'=>'NOK',
			'el'=>'EUR',
			'ro'=>'RON',
			'bg'=>'BGN',
			'hr'=>'HRK',
			'sl'=>'EUR',
			'sk'=>'EUR',
			'sq'=>'ALL',
			'sr'=>'RSD',
			'et'=>'EUR',
			'hu'=>'HUF',
			'mk'=>'MKD',
			'tr'=>'TRY',
			'he'=>'ILS',
			'sg'=>'SGD',
			);
	}

	public static function getArray()
	{
		$model = self::getList();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->cur_id] = $item->symbol;
		}

		return $array;
	}

	public static function isActive($value)
	{
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findByPk($value);

		if($model === null or $model->active == 0){
			return false;
		}

		return true;
	}

	public static function getRate($value)
	{
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findByPk($value);
		if($model === null){
			return false;
		}

		return $model->rate;
	}

	public static function updateRate($cur_id, $value)
	{
		$model = self::model()->findByPk($cur_id);
		if($model === null){
			return false;
		}		
		$model->rate = $value;
		if($model->save(true, array('rate'))) {
			return true;
		}
		return false;
	}

	public static function getSymbol($value)
	{
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findByPk($value);
		if($model === null){
			return false;
		}

		return $model->symbol;
	}

	// цена в текущей валюте
    public static function price($price)
    {
    	
    	$code = Yii::app()->currencies->getValue();

    	if($code == 'EUR'){
    		return number_format($price,2, '.', '');
    	} else {
    		$rate = Currencies::getRate($code);

    		$price = $price * $rate;

    		if( $rate > 3){
				$price = round($price);
				// return $price;
				return number_format($price,0, '.','');
    		}

    		return number_format($price,2, '.', '');
    	}
    }

    public static function symbol()
    {
    	$code = Yii::app()->currencies->getValue();
    	return Currencies::getSymbol($code);
    }

    public static function code()
    {
    	$code = Yii::app()->currencies->getValue();
    	return $code;
    }
}
