<?php

/**
 * This is the model class for table "{{drugs}}".
 *
 * The followings are the available columns in table '{{drugs}}':
 * @property integer $drug_id
 * @property integer $cat_id
 * @property string $drugname
 * @property string $slug
 * @property string $title
 * @property string $header
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $instruction
 *
 * The followings are the available model relations:
 * @property Categories $cat
 */
class Drugs extends PageModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{drugs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, slug', 'required'),
			// array('cat_id, drugname, slug, title, header', 'required'),
			array('cat_id', 'numerical', 'integerOnly'=>true),
			array('drugname, title, header, keywords, description', 'length', 'max'=>256),
			array('slug', 'length', 'max'=>128),
			array('rating', 'length', 'max'=>256),
			array('content, instruction', 'safe'),
			array('likes', 'numerical', 'integerOnly'=>true),
			array('title, header, keywords, description, slug', 'filter', 'filter'=>'strip_tags'),
			array('title, header, keywords, description, slug', 'filter', 'filter'=>'trim'),
			array('slug','match', 'pattern'=>'/^[a-z0-9\.\-]+$/'),
			// array('slug', 'check_slug'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('drug_id, cat_id, drugname, slug, title, header, keywords, description, content, instruction', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            // 'posts' => array(self::HAS_MANY, 'Blog', 'drug_id'),
            'cat' => array(self::BELONGS_TO, 'Categories', 'cat_id'),
            'products' => array(self::HAS_MANY, 'Products', 'drug_id',
				'condition'=>'label = :label and status = :status and a_status = :a_status', 
				'order'=>'CAST(dose as unsigned) DESC, '.Config::getProductsOrder(),
            	'params'=>array(
            		':label'=>Config::getCurrentProductsBase(), 
            		':status' => Products::ACTIVE,
            		':a_status' => Products::ACTIVE),
            	),
            'reviews' => array(self::HAS_MANY, 'Reviews', 'drug_id', 'condition' => 'reviews.lang = :lang and active=1', 'params' => array(':lang'=>Yii::app()->language), 'order'=>'date_cr DESC'),
            'reviews_all' => array(self::HAS_MANY, 'Reviews', 'drug_id',),
            'dt' => array(self::HAS_ONE, 'DrugsTestimonials', 'drug_id'),
            // 'images' => array(self::HAS_MANY, 'DrugsImages', 'drug_id', 'condition' => 'images.lang = :lang and theme = :theme', 'params' => array(':lang'=>Yii::app()->language, ':theme'=>Yii::app()->theme->name)),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'drug_id' => 'Drug',
			'cat_id' => 'Cat',
			'drugname' => 'Drugname',
			'slug' => 'Page url',
			'title' => 'Title',
			'header' => 'Header &lt;h1&gt;',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'content' => 'Content',
			'instruction' => 'Instruction',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.drug_id',$this->drug_id);
		$criteria->compare('t.cat_id',$this->cat_id);
		$criteria->compare('t.drugname',$this->drugname,true);
		$criteria->compare('t.slug',$this->slug,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.header',$this->header,true);
		$criteria->compare('t.keywords',$this->keywords,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.content',$this->content,true);
		$criteria->compare('t.instruction',$this->instruction,true);

		return new CActiveDataProvider($this, array(
            'criteria' => $this->ml->modifySearchCriteria($criteria),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Drugs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getUrl()
	{
		return Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.drug') . '/' . $this->slug);
	}

	public static function getUrlById($id)
	{
		// $model = self::model()->findByPk($id);

		$lang_id = Yii::app()->language;
		
		$sql = "SELECT l_slug from {{drugs_lang}}
				WHERE owner_id = :id and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->cache(Yii::app()->params['cachetime'])->createCommand($sql);
		$command->bindParam(":id", $id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$slug = $command->queryScalar();

		if($slug == false){
			return false;
		}

		$model = new self;
		$model->slug = $slug;


		return $model->getUrl();
	}

	public static function getUrlBySlug($slug)
	{
		return Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.drug') . '/' . $slug);
	}


	public function price_per_pill($id)
	{
		$query = "SELECT amount, bonus, amount_in, price from {{products}} WHERE drug_id = :drug_id and price = (select  MAX(price) from tbl_products where drug_id=:drug_id)";
		$command =Yii::app()->db->cache(Yii::app()->params['cachetime'])->createCommand($query);
		$command->bindValue(":drug_id", $id, PDO::PARAM_INT);
		$result_array = $command->queryRow();
		//print_r($result_array);
		if($result_array['amount'] > 0){
			$price_per_pill =  $result_array['price'] /($result_array['amount'] + $result_array['bonus']);
		} else{
			preg_match_all('/([\d]+) Pillen/', $result_array['amount_in'], $m);
			$amount = 0;
			foreach($m[1] as $pill)
			{
				$amount += $pill;
			}
			if($amount == 0) $amount = 1;
			$price_per_pill =  $result_array['price'] / $amount;
		}
		$price_per_pill = round($price_per_pill, 2);

		return $price_per_pill;

	}

	public function price_per_pack($id)
	{
		$query = "SELECT price from {{products}} WHERE drug_id = :drug_id and price = (select  MIN(price) from tbl_products where drug_id=:drug_id)";
		$command =Yii::app()->db->cache(Yii::app()->params['cachetime'])->createCommand($query);
		$command->bindValue(":drug_id", $id, PDO::PARAM_INT);
		$result_array = $command->queryRow();
		$price_per_pack = $result_array['price'];
		return $price_per_pack;
	}

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array(
                	'drugname',
                    'title',
                    'header',
                    'keywords',
                    'description',
                    'content',
                    'instruction',
                    'rating',
                    'slug',
                    'likes',
                ),
                'langClassName' => 'DrugsLang',
                'langTableName' => 'drugs_lang',
                'languages' => Languages::getArray(),
                'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
                'langForeignKey' => 'owner_id',
                'dynamicLangClass' => true,
                'forceOverwrite'=>true,
            ),
        );
    }
 
    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }


    public function getImages($type = DrugsImages::TYPE_DRUG)
    {
		$language = Yii::app()->language;
		
    	$criteria = new CDbCriteria;
    	$criteria->condition = 'lang = :lang and drug_id = :drug_id and theme = :theme and type = :type';
    	$criteria->params = array(':lang'=>$language, ':drug_id' => $this->drug_id, ':theme'=>Theme::getLabel(), ':type'=>$type);
		$criteria->order = 'img_id ASC';

    	$model = DrugsImages::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

    	if(count($model) === 0){
    		if($language != 'uk'){
    			$language = 'uk';
		    	$criteria->params = array(':lang'=>$language, ':drug_id' => $this->drug_id, ':theme'=>Theme::getLabel(), ':type'=>$type);
    			$model = DrugsImages::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);
    			if(count($model) === 0){
    				return false;
    			}
    		} else {
	    		return false;
    		}
    	}

    	foreach($model as &$item)
    	{
    		$item->img = Theme::getImgUrlContent() . $item->img;
    	}

    	return $model;
    }

    public function getImage($type = DrugsImages::TYPE_DRUG)
    {
    	$images = $this->getImages($type);

    	if(isset($images[0])){
	    	return $images[0]->img;
    	}
		return false;
    }

	public static function check_slug($slug, $id)
	{

		$sql = "SELECT owner_id, lang_id from {{drugs_lang}}
				WHERE l_slug = :slug and owner_id <> :owner_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":owner_id", $id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		if($l_id == false){
			return true;
		} 
		return false;
	}

	public static function findByLSlug($slug, $lang_id)
	{
		$sql = "SELECT owner_id from {{drugs_lang}}
				WHERE l_slug = :slug and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		return $l_id;
	}


	// public function getRating($type = 'common', $out = 'number', $n = 0 )
	// {
	// 	$result = 0;
	// 	$result_prc = 0;

	// 	$rating = explode('|', $this->rating);
	// 	if($type == 'common'){
	// 		$result = $rating[0];
	// 		$result_prc = $rating[0] / 5 * 100;
	// 	} elseif($type == 'star' && $n > 0 && $n < 6){
	// 		$review_count = 0;
	// 		for($i=1;$i<=5;$i++)
	// 		{
	// 			$review_count += $rating[$i];
	// 		}

	// 		$result = $rating[$n];
	// 		$result_prc = $rating[$n] / $review_count * 100;

	// 	}
	// 	if($out == 'number'){ // number or %
	// 		return $result;
	// 	}

	// 	return number_format($result_prc, 2);
	// }

	public function getRatingReal()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'drug_id = :drug_id and active = 1 and lang = :lang';
		$criteria->params = array(':drug_id'=>$this->drug_id, ':lang'=>Yii::app()->language);
		$model = Reviews::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

		$count_review = count($model);
		if($count_review == 0){
			return 0;
		}
		$points_sum = 0;
		foreach($model as $item)
		{
			$points_sum += $item->points;
		}

		$points_drug = number_format($points_sum / $count_review,2);

		return $points_drug;
		
	}

	public function getRatingFake()
	{
		$data = explode('|', $this->rating);
		$sum = 0; $count = 0;
		$j = 5;
		foreach($data as $i=>$value)
		{
			$sum += $j * $value;
			$count += $value;
			$j--;
		}
		
		$rating = 0;
		if($count > 0){
			$rating = $sum / $count;
		}

		return $rating;

	}

	public function getRating()
	{
		$rating_real = $this->getRatingReal();
		$rating_fake = $this->getRatingFake();

		$rating_real_count = $this->getReviewsCountReal();
		$rating_fake_count = $this->getReviewsCountFake();

		if($rating_real_count == 0 && $rating_fake_count == 0){
			return 0;
		}
		
		$result = number_format( ($rating_real*$rating_real_count + $rating_fake*$rating_fake_count) / ($rating_real_count +$rating_fake_count), 2);
		return $result;
	}

	public function getRatingPrc()
	{
		return number_format($this->getRating() / 5 * 100, 2);
	}

	public function getRatingStarReal( $star)
	{
		if(!in_array($star, array(1,2,3,4,5))) return false;

		// switch ($star) {
		// 	case 5:
		// 		$min = 4.4; $max = 5;
		// 		break;
		// 	case 4:
		// 		$min = 3.4; $max = 4.4;
		// 		break;
		// 	case 3:
		// 		$min = 2.4; $max = 3.4;
		// 		break;
		// 	case 2:
		// 		$min = 1.4; $max = 2.4;
		// 		break;
		// 	case 1:
		// 		$min = 0; $max = 1.4;
		// 		break;
		// }

		$criteria = new CDbCriteria;
		$criteria->condition = 'drug_id = :drug_id and active = 1 and lang = :lang and points >= :min and points < :max';
		$criteria->params = array(
			':drug_id'=>$this->drug_id, 
			':lang'=>Yii::app()->language,
			':min'=>$star-0.5,
			':max'=>$star+0.5,
			);
		$count = Reviews::model()->count($criteria);		

		return $count;
	}

	public function getRatingStarFake( $star)
	{
		$data = explode('|', $this->rating);
		$data = array_reverse($data);

		if(isset($data[$star -1])){
			return $data[$star-1];
		}
		return false;
	}



	public function getReviewsCountReal()
	{
		return count($this->reviews);	
	}

	public function getReviewsCountFake()
	{
		$data = explode('|', $this->rating);

		$count = 0;
		foreach($data as $i=>$value)
		{
			$count += $value;
		}
		return $count;
	}

	public static function getReviewsCountRealByParams($drug_id, $lang_id)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'drug_id = :drug_id and active = 1 and lang = :lang';
		$criteria->params = array(':drug_id'=>$drug_id, ':lang'=>$lang_id);
		$count = Reviews::model()->count($criteria);

		return $count;
	}

	public static function getReviewsCountFakeByParams($rating_string)
	{
		$data = explode('|', $rating_string);

		$count = 0;
		foreach($data as $i=>$value)
		{
			$count += $value;
		}
		return $count;
	}	

	public function getReviewsCount()
	{
		return $this->getReviewsCountReal() + $this->getReviewsCountFake();
	}

	public function getRatingStar( $star)
	{
		return $this->getRatingStarFake($star) + $this->getRatingStarReal($star);
	}

	public function getRatingStarPrc($star)
	{
		$count = $this->getReviewsCount();
		
		if($count == 0){
			return 0;
		} 

		return number_format($this->getRatingStar($star) / $count * 100, 2);
	}

	public static function getRatingFakeString($drug_id, $lang_id)
	{
		$sql = 'select l_rating from {{drugs_lang}} where owner_id = :owner_id and lang_id = :lang_id';

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":owner_id", $drug_id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		return $command->queryScalar();
	}

	// by params
	public static function getRatingRealByParams($drug_id, $lang_id)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'drug_id = :drug_id and active = 1 and lang = :lang';
		$criteria->params = array(':drug_id'=>$drug_id, ':lang'=>$lang_id);
		$model = Reviews::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

		$count_review = count($model);
		if($count_review == 0){
			return 0;
		}
		$points_sum = 0;
		foreach($model as $item)
		{
			$points_sum += $item->points;
		}

		$points_drug = number_format($points_sum / $count_review,2);

		return $points_drug;
		
	}

	public static function getRatingFakeByParams($rating_string)
	{
		$data = explode('|', $rating_string);
		$sum = 0; $count = 0;
		$j = 5;
		foreach($data as $i=>$value)
		{
			$sum += $j * $value;
			$count += $value;
			$j--;
		}
		$rating = 0;
		if($count > 0){
			$rating = $sum / $count;
		}
			

		return $rating;

	}

	public static function getRatingByParams($drug_id, $lang_id, $rating_string)
	{
		$rating_real = self::getRatingRealByParams($drug_id, $lang_id);
		$rating_fake = self::getRatingFakeByParams($rating_string);

		$rating_real_count = self::getReviewsCountRealByParams($drug_id, $lang_id);
		$rating_fake_count = self::getReviewsCountFakeByParams($rating_string);

		if($rating_real_count == 0 && $rating_fake_count == 0){
			return 0;
		}


		$result = number_format( ($rating_real*$rating_real_count + $rating_fake*$rating_fake_count) / ($rating_real_count +$rating_fake_count), 2);

		return $result;
	}
	// end rating

	public static function getCountLikesByParams($drug_id, $lang_id)
	{

		$sql = 'SELECT l_likes FROM {{drugs_lang}} WHERE owner_id = :owner_id and lang_id = :lang_id';
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":owner_id", $drug_id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		return $command->queryScalar();

	}

	public static function updateLikes($drug_id, $lang_id, $likes)
	{
		$sql = 'UPDATE {{drugs_lang}} SET l_likes = :likes WHERE owner_id = :owner_id and lang_id = :lang_id';

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":owner_id", $drug_id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$command->bindParam(":likes", $likes, PDO::PARAM_INT);

		$row_count = $command->execute();
		if($row_count > 0 ) 
			return true;
		return false;		
	}


	public function getReviewsForDrugPage()
	{
		return array_slice($this->reviews, 0, Yii::app()->config->get('reviews.count.drug'));
	}

	public static function getArray()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'active = 1';
		$criteria->order = 'position ASC';
		
		return CHtml::listData(self::model()->findAll($criteria), 'drug_id', 'drugname');
	}

	public static function updateRating($drug_id, $lang_id, $rating)
	{
		$sql = 'UPDATE {{drugs_lang}} SET l_rating = :rating WHERE owner_id = :owner_id and lang_id = :lang_id';

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":owner_id", $drug_id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$command->bindParam(":rating", $rating, PDO::PARAM_STR);

		$row_count = $command->execute();
		if($row_count > 0 ) 
			return true;
		return false;
	}


	public static function getDrugNameById($id)
	{
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findByPk($id);
		if($model === null){
			return false;
		}

		return $model->drugname;
	}

	public function getDrugname()
	{
		if($this->cat_id == 2){
			return $this->drugname;
		}
		return Filter::generic($this->drugname);
	}


	public static function atCategory($cat_id = false)
	{

		$sql = "SELECT 
				d.l_slug as drug_slug, 
				d.l_drugname as drug_name,
				drug.position as drug_order,  
				im.img as src,  
				im.type as img_type,
				im.lang as lang 
				from {{drugs}} drug  
				left join {{drugs_lang}} d on d.owner_id = drug.drug_id
				left join {{drugs_images}} im on drug.drug_id = im.drug_id
				WHERE 
				[param_str]
				d.lang_id = :lang_id and (im.lang = :lang_id or im.lang = 'uk') and drug.active = 1 and 
				im.theme = :theme and im.type != 0
				ORDER BY drug.position ASC, drug.drug_id ASC";

		if($cat_id != false){
			$param_str = 'drug.cat_id = :cat_id and' ;
		}
		else {
			$param_str = 'drug.home = 1 and' ;
		}

		$sql = str_replace('[param_str]', $param_str, $sql);


		$lang_id = Yii::app()->language;
		$theme = Theme::getLabel();

		$connection=Yii::app()->db;
		$command=$connection->cache(Yii::app()->params['cachetime'])->createCommand($sql);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$command->bindParam(":theme", $theme, PDO::PARAM_STR);
		if($cat_id != false){
			$command->bindParam(":cat_id", $cat_id, PDO::PARAM_INT);
		}
		$dataReader=$command->query();

		$data =$dataReader->readAll();


		$result = [];

		foreach($data as $i=> $item)
		{
			if($item['drug_name'] == '') continue;

			if(!isset($result[$item['drug_order']])) {
				$result[$item['drug_order']]['name'] = $item['drug_name'];
				$result[$item['drug_order']]['url'] = Drugs::getUrlBySlug($item['drug_slug']);
			}
			$result[$item['drug_order']]['images'][$item['img_type']][$item['lang']] = Theme::getImgUrlContent() . $item['src'] ;


		}
		//  оставляем только один язык
		foreach($result as &$item)
		{
			foreach($item['images'] as $type => $src_list)
			{
				if(isset($item['images'][$type][$lang_id])){
					$item['images'][$type] = $item['images'][$type][$lang_id];
				}
				elseif(isset($item['images'][$type]['uk'])) {
					$item['images'][$type] = $item['images'][$type]['uk'];
				}
			}
		}
		
		return $result;
	
	}
}
