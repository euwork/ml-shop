<?php


class DrugsImages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{drugs_images}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

		return array(
			array('drug_id, img, theme, lang, type', 'required'),
			array('drug_id, type', 'numerical', 'integerOnly'=>true),
			array('theme', 'length', 'max'=>128),
			array('lang', 'length', 'max'=>32),
			array('img', 'length', 'max'=>256),
			// // The following rule is used by search().
			// // @todo Please remove those attributes that should not be searched.
			// array('drug_id, slug, title, header, subheader, keywords, description, drug_ancor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			// 'drug_id' => 'Drug',
			// 'slug' => 'Slug',
			// 'title' => 'Title',
			// 'header' => 'Header',
			// 'subheader' => 'Subheader',
			// 'keywords' => 'Keywords',
			// 'description' => 'Description',
			// 'drug_ancor' => 'Drug Ancor',
		);
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	const TYPE_DRUG = 0;
	const TYPE_CAT = 1;
	const TYPE_TEXT = 2;

}
