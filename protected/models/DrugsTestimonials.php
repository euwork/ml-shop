<?php

/**
 * This is the model class for table "{{drugs_testimonials}}".
 *
 * The followings are the available columns in table '{{drugs_testimonials}}':
 * @property integer $drug_id
 * @property string $slug
 * @property string $title
 * @property string $header
 * @property string $subheader
 * @property string $keywords
 * @property string $description
 * @property string $drug_ancor
 *
 * The followings are the available model relations:
 * @property Drugs $drug
 * @property Reviews[] $reviews
 */
class DrugsTestimonials extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{drugs_testimonials}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('drug_id, slug', 'required'),
			array('drug_id', 'numerical', 'integerOnly'=>true),
			array('slug', 'length', 'max'=>128),
			array('title, header, subheader, keywords, description, drug_ancor', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('drug_id, slug, title, header, subheader, keywords, description, drug_ancor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'drug_id' => 'Drug',
			'slug' => 'Slug',
			'title' => 'Title',
			'header' => 'Header',
			'subheader' => 'Subheader',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'drug_ancor' => 'Drug Ancor',
		);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DrugsTestimonials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function getUrl()
	{
		return Yii::app()->createUrl('/' . Yii::app()->config->get('prefix.testimonials') . '/' . $this->slug);
	}

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array(
                    'title',
                    'header',
                    'subheader',
                    'keywords',
                    'description',
                    'drug_ancor',
                    'slug',
                ),
                'langClassName' => 'DrugsTestimonialsLang',
                'langTableName' => 'drugs_testimonials_lang',
                'languages' => Languages::getArray(),
                'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
                'langForeignKey' => 'owner_id',
                'dynamicLangClass' => true,
                'forceOverwrite'=>true,
            ),
        );
    }
    
    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }

	public static function check_slug($slug, $id)
	{

		$sql = "SELECT owner_id, lang_id from {{drugs_testimonials_lang}}
				WHERE l_slug = :slug and owner_id <> :owner_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":owner_id", $id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		if($l_id == false){
			return true;
		} 
		return false;
	}

	public static function findByLSlug($slug, $lang_id)
	{
		$sql = "SELECT owner_id from {{drugs_testimonials_lang}}
				WHERE l_slug = :slug and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		return $l_id;
	}
}
