<?php

/**
 * This is the model class for table "{{contacts}}".
 *
 * The followings are the available columns in table '{{contacts}}':
 * @property integer $cont_id
 * @property string $name
 * @property string $email
 * @property string $thema
 * @property string $content
 */
class Languages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{languages}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lang_id, name, country', 'required'),
			array('name, country', 'length', 'max'=>256),
			array('lang_id', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('cont_id, name, email, thema, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lang_id' => 'Id',
			'name' => Yii::t('languages', 'Name'), 
			'country' => Yii::t('languages', 'Country'), 
		);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function countEnabled()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.show = 1';
		$count = self::model()->cache(Yii::app()->params['cachetime'])->count($criteria);

		return $count;
	}

	public static function getList()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.show = 1';
		$criteria->order = 't.position ASC';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

		return $model;
	}

	public static function getListAll()
	{
		$criteria = new CDbCriteria;
		// $criteria->condition = 't.show = 1';
		$criteria->order = 't.position ASC';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);

		return $model;
	}

	public static function getArray()
	{
		$model = self::getList();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->lang_id] = $item->name;
		}

		return $array;
	}

	public static function getArrayAll()
	{
		$model = self::getListAll();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->lang_id] = $item->name;
		}

		return $array;
	}

	public static function getArrayFlipAll()
	{
		$model = self::getListAll();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->name] =  $item->lang_id;
		}

		return $array;
	}

	public static function getArrayExt()
	{
		$model = self::getList();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->lang_id] = $item->name . ' [' . $item->country . ']';
		}

		return $array;
	}	

	public static function getArrayFlip()
	{
		$model = self::getList();
		$array = array();
		foreach ($model as $item) 
		{
			$array[$item->name . ' [' . $item->country . ']'] =  $item->lang_id;
		}

		return $array;
	}


	public static function getEnabledPaymentMethods()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.show = 1 and lang_id = :lang_id';
		$criteria->params = array(':lang_id' => Yii::app()->language);
		$criteria->order = 't.position ASC';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->find($criteria);

		if($model === null){
			return false;
		}
		$result = array();
		foreach(array('wire', 'master', 'visa', 'amex', 'bc', 'bitcoin', 'ideal') as $pm)
		{
			$result[$pm] = $model->{$pm};
		}

		return $result;
	}

	public static function isGenericEnabled()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'lang_id = :lang_id';
		$criteria->params = array(':lang_id' => Yii::app()->language);
		// $criteria->order = 't.position ASC';
		$model = self::model()->cache(Yii::app()->params['cachetime'])->find($criteria);	

		if($model === null){
			return true;// ниче не делаем
		}	

		return (bool) $model->show_generic;
	}


	public static function isBankEnabled()
	{
		$data = self::getEnabledPaymentMethods();
		if(isset($data['wire'])){
			return $data['wire'];
		}

		return false;
	}

	public static function isBitcoinEnabled()
	{
		$data = self::getEnabledPaymentMethods();
		if(isset($data['bitcoin'])){
			return $data['bitcoin'];
		}

		return false;
	}	

	public static function isIdealEnabled()
	{
		$data = self::getEnabledPaymentMethods();
		if(isset($data['ideal'])){
			return $data['ideal'];
		}

		return false;
	}

	public static function getDefaultCountry()
	{
		$country = Yii::app()->sypexGeo->getCountry(Yii::app()->request->getUserHostAddress());
		if(in_array($country, array_flip(CheckoutForm::getCountryList()))){
			return $country;
		} else {
			$country = Yii::app()->language;
			if($country == 'ca-fr'){
				$country = 'ca';
			}
			$country = strtolower($country);
			return $country;
		}
	}
	public static function check_id($lang_id)
	{
		$model = self::model()->findAll();
		foreach($model as $item)
		{
			if($item->lang_id == $lang_id){
				return false;
			}
		}
		return true;
	}
}
