<?php

/**
 * This is the model class for table "{{voted}}".
 *
 * The followings are the available columns in table '{{voted}}':
 * @property string $like_id
 * @property integer $drug_id
 * @property string $date_cr
 * @property string $ip
 *
 * The followings are the available model relations:
 * @property Reviews $rev
 */
class Likes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{likes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('drug_id', 'required'),
			array('drug_id', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('like_id, drug_id, date_cr, ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'like_id' => 'Vot',
			'drug_id' => 'Rev',
			'date_cr' => 'Date Cr',
			'ip' => 'Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	// public function search()
	// {
	// 	// @todo Please modify the following code to remove attributes that should not be searched.

	// 	$criteria=new CDbCriteria;

	// 	$criteria->compare('like_id',$this->like_id,true);
	// 	$criteria->compare('drug_id',$this->drug_id);
	// 	$criteria->compare('date_cr',$this->date_cr,true);
	// 	$criteria->compare('ip',$this->ip,true);

	// 	return new CActiveDataProvider($this, array(
	// 		'criteria'=>$criteria,
	// 	));
	// }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Voted the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_cr',
                'updateAttribute' => null,
            ),
        );
    }


    public static function add($ip, $drug_id)
    {
		$model = new self;
		$model->ip = $ip;
		$model->drug_id = $drug_id;
		if($model->save()){
			return true;
		}
		return false;
    }

    public static function check($ip, $drug_id)
    {
    	$criteria = new CDbCriteria;
    	$criteria->condition = 'ip = :ip AND drug_id = :drug_id AND date_cr > :date';
    	$criteria->params = array(':ip'=>$ip, ':drug_id'=>$drug_id, ':date'=>date('Y-m-d'));
		$model = self::model()->find($criteria);
		if($model === null) {
			return true;
		}
		return false;
    }
}
