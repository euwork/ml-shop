<?php

/**
 * This is the model class for table "{{order_products}}".
 *
 * The followings are the available columns in table '{{order_products}}':
 * @property string $order_id
 * @property string $prod_id
 * @property string $price
 * @property string $discount
 */
class OrderProducts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_products}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, prod_id, price', 'required'),
			array('order_id, prod_id', 'length', 'max'=>11),
			array('price, price_usd', 'length', 'max'=>10),
			array('discount', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, prod_id, price, discount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'order' => array(self::BELONGS_TO, 'Orders', 'order_id'),
            'product' => array(self::BELONGS_TO, 'Products', 'prod_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'prod_id' => 'Prod',
			'price' => 'price',
			'discount' => 'discount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('prod_id',$this->prod_id,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('discount',$this->discount,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getPriceInfo($currency_prefix = '')
	{
		$price = $this->getPriceWithDiscount($currency_prefix);
		$discount = '';
		if($this->discount>0){
			$discount = ' (disc)';
		}
		return  number_format($price,2).$discount;
	}

	public function getPriceWithDiscount($currency_prefix = '')
	{
		return $this->{'price'.$currency_prefix} * (1 - $this->discount / 100);
	}
}
