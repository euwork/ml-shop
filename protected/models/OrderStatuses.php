<?php

/**
 * This is the model class for table "{{order_statuses}}".
 *
 * The followings are the available columns in table '{{order_statuses}}':
 * @property string $stat_id
 * @property string $order_id
 * @property integer $status
 * @property string $date_cr
 *
 * The followings are the available model relations:
 * @property Orders $order
 */
class OrderStatuses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_statuses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, status', 'required'),
			array('status, order_id', 'numerical', 'integerOnly'=>true),
			array('order_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('stat_id, order_id, status, date_cr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'order' => array(self::BELONGS_TO, 'Orders', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'stat_id' => 'Stat',
			'order_id' => 'Order',
			'status' => 'Status',
			'date_cr' => 'Date Cr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('stat_id',$this->stat_id,true);
		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_cr',$this->date_cr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatuses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_cr',
                'updateAttribute' => null,
            ),
        );
    }

	public static function getByDateApi($from, $to)
	{
		$sql = "SELECT
				statuses.date_cr as dat,
				statuses.status as status,
				orders.affid as aff_id,
				orders.order_id as order_id
				from {{order_statuses}} statuses 
				left join {{orders}} orders on statuses.order_id = orders.order_id 
				WHERE statuses.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				ORDER BY dat ASC";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}
}
