<?php

/**
 * This is the model class for table "{{orders}}".
 *
 * The followings are the available columns in table '{{orders}}':
 * @property string $order_id
 * @property string $uuid
 * @property string $comission
 * @property string $date_cr
 * @property string $email
 * @property integer $gender
 * @property string $phone
 * @property string $phone2
 * @property string $shipp_fname
 * @property string $shipp_lname
 * @property string $shipp_address
 * @property string $shipp_city
 * @property string $shipp_zip
 * @property string $shipp_country
 * @property string $bill_fname
 * @property string $bill_lname
 * @property string $bill_address
 * @property string $bill_city
 * @property string $bill_zip
 * @property string $bill_country
 * @property string $payment_method
 * @property string $card_number
 * @property string $card_month
 * @property string $card_year
 * @property string $card_cvv
 * @property string $subtotal
 * @property string $total
 * @property string $shipping_price
 * @property integer $shipping_method
 * @property string $insurance
 * @property string $http_user
 * @property string $http_ip
 * @property string $http_proxy
 * @property string $http_country
 * @property string $http_city
 * @property string $http_referer
 * @property string $http_os
 *
 * The followings are the available model relations:
 * @property Products[] $tblProducts
 * @property OrderStatuses[] $orderStatuses
 */
class Orders extends CActiveRecord
{


	const STATUS_NEW = 0;
	const STATUS_PENDING = 1;
	const STATUS_ACCEPT = 2;
	const STATUS_DECLINE = 3;

	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;

	const PAYMENT_WIRE = 'wire';
	const PAYMENT_IDEAL = 'ideal';
	const PAYMENT_VISA = 'visa';
	const PAYMENT_MASTER = 'master';
	const PAYMENT_AMEX = 'amex';
	const PAYMENT_BC = 'bc'; // blue card для французов
	const PAYMENT_BITCOIN = 'bitcoin';

	// array payment methods для фильтра в стате
	public static function getArrayPaymentMethod()
	{
		return array(
				self::PAYMENT_WIRE,
				self::PAYMENT_IDEAL,
				self::PAYMENT_VISA,
				self::PAYMENT_MASTER,
				self::PAYMENT_BC,
				self::PAYMENT_AMEX,
			);
	}

	const SHIPPING_REGULAR = 8;
	const SHIPPING_EXPRESS = 2;
	const SHIPPING_NORMAL = 4;
	// const SHIPPING_REGULAR_ORIG = 7;
	// const SHIPPING_EXPRESS_ORIG = 1;

	// const SHIPPING_AIR = 3;
	// const SHIPPING_EMS = 5;

	// const EXTB_INTERNAL = 0;
	// const EXTB_EXTERNAL = 1;

	public function isIpillBilling()
	{
		if(in_array($this->payment_method, array(self::PAYMENT_MASTER, self::PAYMENT_AMEX) ))
			return true;
		return false;
	}
	
	public static function getListStatuses()
	{
		return array(
			'preorder' => self::STATUS_NEW,
			'pending' => self::STATUS_PENDING,
			'accept' => self::STATUS_ACCEPT,
			'decline' => self::STATUS_DECLINE,
			);
	}

	public static function getListGender()
	{
		return array(
			'male' => self::GENDER_MALE,
			'female' => self::GENDER_FEMALE,
			);
	}

	// public static function getListPaymentMethodOriginal()
	// {
	// 	$_array = array(
	// 		 self::PAYMENT_VISA => Yii::t('checkout', 'Visa'),
	// 		 self::PAYMENT_WIRE => Yii::t('checkout', 'Banküberweisung'),
	// 		);

	// 	if(Yii::app()->language == 'fr'){
	// 		$_array = array_merge(array(self::PAYMENT_BC => Yii::t('checkout', 'Blue Card')), $_array);
	// 	}
	// 	return $_array;
	// }

	private static function getListPaymentMethod()
	{
		$_array = array(
			 self::PAYMENT_VISA => Yii::t('checkout', 'Visa'),
			 self::PAYMENT_BC => Yii::t('checkout', 'Blue Card'),
			 self::PAYMENT_MASTER => Yii::t('checkout', 'MasterCard'),
			 self::PAYMENT_AMEX => Yii::t('checkout', 'American Express'),
			 self::PAYMENT_BITCOIN => Yii::t('checkout', 'Bitcoin'),
			 self::PAYMENT_WIRE => Yii::t('checkout', 'Banküberweisung'),
			 self::PAYMENT_IDEAL => Yii::t('checkout', 'iDEAL'),
			);

		return $_array;
	}
	//  фунукция для cart/pm
	public static function getCurrentListPaymentMethod()
	{
		$current = array();	
		$enabled = Languages::getEnabledPaymentMethods();
		foreach(self::getListPaymentMethod() as $key => $value)
		{
			if($enabled[$key]){
				$current[$key] = $value;
			}
		}
		return $current;
	}
	// функция для cart/checkout - учитывает включен ли внешний биллинг
	public static function getCurrentListPaymentMethodInternal()
	{
		$current = array();
		// if(Yii::app()->config->get('billing.page') == 'internal' or
		// 	(Yii::app()->config->get('billing.page') == 'external' && !Yii::app()->extbilling->convertProducts(Yii::app()->shoppingCart->getPositions()) )
		//  ) {
			foreach(self::getCurrentListPaymentMethod() as $key => $value)
			{
				if($key != self::PAYMENT_BITCOIN){
					$current[$key] = $value;
				}			
			}

		// } else {
		// 	foreach(self::getCurrentListPaymentMethod() as $key => $value)
		// 	{
		// 		if($key == self::PAYMENT_WIRE){
		// 			$current[$key] = $value;
		// 		}
		// 	}

		// }

		return $current;
	}

	public static function getListShippingMethodNames()
	{
		return array(
			Yii::t('checkout', 'Regulärer Postversand (8-13 Werktage)') 	=> self::SHIPPING_REGULAR,
			Yii::t('checkout', 'Express Versand (4-7 Werktage)')			=>self::SHIPPING_EXPRESS,
			Yii::t('checkout', 'Standardlieferung (8-21 Werktage)')	=>		self::SHIPPING_NORMAL,

			// Yii::t('checkout', 'Regulärer Postversand (8-21 Werktage)') 	=> 		self::SHIPPING_REGULAR_ORIG,
			// Yii::t('checkout', 'Express Versand (7-13 Werktage)')			=>		self::SHIPPING_EXPRESS_ORIG,

			// Yii::t('checkout', 'Registered Air Mail (9-21 business days)') 	=> 		self::SHIPPING_AIR,
			// Yii::t('checkout', 'EMS shipping (5-14 business days)')			=>		self::SHIPPING_EMS,
			);
	}

	public static function getListShippingMethodPrices($is_original = false)
	{
		// switch (Config::getCurrentProductsBase()) {
		// 	case Products::BASE_GER:
		// 		$data = array(
		// 			self::SHIPPING_REGULAR => '0.00', 
		// 			self::SHIPPING_EXPRESS => '21.99', 
		// 			);
		// 		break;
		// 	case Products::BASE_FR:
		// 		// это Франция

		// 		if(Yii::app()->shoppingCart->getCost() > 200){
		// 			$data = array(
		// 				self::SHIPPING_REGULAR => '0.00',
		// 				self::SHIPPING_EXPRESS => '21.99',
		// 				);
		// 		} else {
		// 			$data = array(
		// 				self::SHIPPING_REGULAR => '11.99',
		// 				self::SHIPPING_EXPRESS => '21.99',
		// 				self::SHIPPING_NORMAL  => '9.50',
		// 				);
		// 		}

		// 		break;		

		// }

		$data = array();

		if(Config::getCurrentProductsBase() == Products::BASE_GER){
			if(Yii::app()->config->get('ship_active_regular_de') == true){
				if(Yii::app()->shoppingCart->getCost() > 60){
					$data[self::SHIPPING_REGULAR] =  Yii::app()->config->get('ship_price_regular_de'); 
				} else {
					// $data[self::SHIPPING_REGULAR] = '5.95';
					$data[self::SHIPPING_REGULAR] =  Yii::app()->config->get('ship_price_regular_de_less60'); 
				}
			}
			if(Yii::app()->config->get('ship_active_express_de') == true){
				$data[self::SHIPPING_EXPRESS] =  Yii::app()->config->get('ship_price_express_de'); 
			}
		}

		elseif(Config::getCurrentProductsBase() == Products::BASE_FR){

			$ship_price_regular = Yii::app()->config->get('ship_price_regular');
			if(Yii::app()->shoppingCart->getCost() > 200){
				$ship_price_regular = 0;
			}

			if(Yii::app()->config->get('ship_active_regular') == true){
				$data[self::SHIPPING_REGULAR] =  $ship_price_regular;
			}
			if(Yii::app()->config->get('ship_active_express') == true){
				$data[self::SHIPPING_EXPRESS] = Yii::app()->config->get('ship_price_express');
			}
			if(Yii::app()->shoppingCart->getCost() <= 200){
				if(Yii::app()->config->get('ship_active_normal') == true){
					$data[self::SHIPPING_NORMAL] = Yii::app()->config->get('ship_price_normal'); 
				}
			}
		}

		// Yii::app()->config->get('ship_active_regular_de')
		// Yii::app()->config->get('ship_active_express_de')
		// Yii::app()->config->get('ship_price_regular_de')
		// Yii::app()->config->get('ship_price_express_de')
		
		// Yii::app()->config->get('ship_active_regular')
		// Yii::app()->config->get('ship_active_express')
		// Yii::app()->config->get('ship_active_normal')
		// Yii::app()->config->get('ship_price_regular')
		// Yii::app()->config->get('ship_price_express')
		// Yii::app()->config->get('ship_price_normal')

		if(count($data) == 0){
			$data[] = array(self::SHIPPING_REGULAR => 0);
		}
		
		return $data;
	}

	private static function get_value_by_key($key, $array)
	{
		if(array_key_exists($key, $array)){
			return $array[$key] ;
		}
		return false;
	}

	public function getItemShippingPrice($is_original)
	{
		return self::get_value_by_key($this->shipping_method, self::getListShippingMethodPrices($is_original));
	}

	public static function getShippingPriceById($shipping_method, $is_original)
	{
		return self::get_value_by_key($shipping_method, self::getListShippingMethodPrices($is_original));
	}

	public function getItemShippingName()
	{
		return array_search($this->shipping_method, self::getListShippingMethodNames());
	}

	public static function getShippingNameById($shipping_method)
	{
		return array_search($shipping_method, self::getListShippingMethodNames());
	}

	public function getPaymentName()
	{
		return array_search($this->payment_method, array_flip(self::getListPaymentMethod()));
	}

	public function getPaymentIcon()
	{
		return '<img src="'.Yii::app()->theme->baseUrl.'/img/cc-icons/' . $this->payment_method . '.png" alt="' . $this->paymentname . '" title="' . $this->paymentname . '" />';
	}

	public static function getInsurancePrice()
	{
		if(in_array(Yii::app()->language, array('us',/*'uk',*/'au','ca', 'ca-fr', 'br'))){
			$price_doll = 3.95;
			$rate = Currencies::getRate('USD'); 

			return round($price_doll / $rate,2);

		} else{
			
			return '3.95';
		}

	}

	public function getStatusName()
	{
		return array_search($this->status, self::getListStatuses());
	}

	public static function getStatusNameById($status_id)
	{
		return array_search($status_id, self::getListStatuses());
	}
	public static function getStatusIdByName($status_name)
	{
		return array_search($status_name, array_flip(self::getListStatuses()));
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{orders}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('affid, comission, email, gender, phone, shipp_fname, shipp_lname, shipp_address, shipp_city, shipp_zip, shipp_country, bill_fname, bill_lname, bill_address, bill_city, bill_zip, bill_country, payment_method, subtotal, total, shipping_price, shipping_method, insurance', 'required'),
			array('gender, shipping_method', 'numerical', 'integerOnly'=>true),
			array('affid', 'match', 'pattern'=>'/^[\d]{4,5}$/'),
			// array('shipp_zip, bill_zip', 'match', 'pattern'=>'/^[\d]{3,7}$/'),
			array('email, shipp_fname, shipp_lname, shipp_address, bill_fname, bill_lname, bill_address, http_referer', 'length', 'max'=>256),
			array('http_user_agent, http_x_forwarded_for, http_x_forwarded, http_forwarded, http_proxy_agent, http_via, http_proxy_connection', 'length', 'max'=>256),
			array('js_time_string', 'length', 'max'=>256),
			array('http_data_info', 'length', 'max'=>2048),
			// array('log_browser_lang, log_browser_reso, log_colordepth, log_machine_type, log_sockip', 'length', 'max'=>256),

			array('comission', 'length', 'max'=>5),
			array('email', 'email'),
			array('shipp_zip, bill_zip', 'length', 'max'=>32),
			array('phone, phone2', 'length', 'max'=>32, 'min'=>7),
			array('phone, phone2', 'match', 'pattern'=>'/^[\-\+\(\)\s\d]+$/'),
			array('shipp_city, bill_city', 'length', 'max'=>128),
			array('shipp_country, bill_country, card_month, http_country', 'length', 'max'=>2),
			array('shipp_state, bill_state', 'length', 'max'=>3),
			array('shipp_state, bill_state', 'in', 'range' => array_flip(Country::getStateList())),
			array('payment_method, subtotal, total, shipping_price, insurance', 'length', 'max'=>10),
			array('card_number', 'length', 'max'=>16),
			array('card_number', 'match', 'pattern'=>'/^[\d]+$/'),
			array('card_year, card_cvv', 'length', 'max'=>4),
			array('http_ip', 'length', 'max'=>46),
			array('card_number, card_cvv', 'numerical', 'integerOnly'=>true),
			array('payment_method', 'in', 'range'=>array_flip(Orders::getListPaymentMethod())),
			array('shipping_method', 'in', 'range'=>Orders::getListShippingMethodNames()),
			array('card_month', 'in', 'range'=>CheckoutForm::getMonthList()),
			array('shipp_country, bill_country', 'in', 'range' => array_flip(Country::getCountryList())),
			array('language', 'in', 'range' => Languages::getArrayFlip()),
			array('card_year', 'in', 'range'=>CheckoutForm::getYearList()),
			// array('currency', 'in', 'range'=>array_flip(Currencies::getArray())),
			// array('currency_rate', 'length', 'max'=>7),
			// array('shipp_fname, shipp_lname, shipp_address, shipp_city, bill_fname, bill_lname, bill_address, bill_city', 'match', 'pattern'=>'/^[\d\w\s\.\-,]+$/'),
			array('shipp_fname, shipp_lname, shipp_address, shipp_city, bill_fname, bill_lname, bill_address, bill_city, http_referer', 'filter', 'filter'=>'strip_tags'),
			array('shipp_fname, shipp_lname, shipp_address, shipp_city, bill_fname, bill_lname, bill_address, bill_city, http_referer', 'filter', 'filter'=>'trim'),
			
			array('log_sockip, log_browser_lang, log_browser_reso, log_colordepth, log_machine_type', 'filter', 'filter'=>array('Filter', 'max256')),
			array('log_useragent, log_plugins, log_fonts, log_mtime, log_os, log_jsversion, log_token, log_beh, log_tzinfo', 'filter', 'filter'=>array('Filter', 'max256')),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'order_products'=> array(self::HAS_MANY, 'OrderProducts', 'order_id'),
			'products'=> array(self::HAS_MANY, 'Products', 'prod_id', 'through' =>'order_products'),
			// 'products' => array(self::MANY_MANY, 'Products', '{{order_products}}(order_id, prod_id)'),
			'statuses' => array(self::HAS_MANY, 'OrderStatuses', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'comission' => 'Comission',
			'date_cr' => 'Date Cr',
			'email' => 'Email',
			'gender' => 'Gender',
			'phone' => 'Phone',
			'phone2' => 'Phone2',
			'shipp_fname' => 'Shipp Fname',
			'shipp_lname' => 'Shipp Lname',
			'shipp_address' => 'Shipp Address',
			'shipp_city' => 'Shipp City',
			'shipp_zip' => 'Shipp Zip',
			'shipp_country' => 'Shipp Country',
			'bill_fname' => 'Bill Fname',
			'bill_lname' => 'Bill Lname',
			'bill_address' => 'Bill Address',
			'bill_city' => 'Bill City',
			'bill_zip' => 'Bill Zip',
			'bill_country' => 'Bill Country',
			'payment_method' => 'Payment Method',
			'card_number' => 'Card Number',
			'card_month' => 'Card Month',
			'card_year' => 'Card Year',
			'card_cvv' => 'Card Cvv',
			'subtotal' => 'Subtotal',
			'total' => 'Total',
			'shipping_price' => 'Shipping Price',
			'shipping_method' => 'Shipping Type',
			'insurance' => 'Insurance',
			'http_ip' => 'Http Ip',
			'http_country' => 'Http Country',
			'http_referer' => 'Http Referer',
		);
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_cr',
                'updateAttribute' => null,
            ),
        );
    }

    public static function generateUuid() 
    {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = "" // chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        return $uuid;
	}

	public function getPartId()
	{
		return $this->affid . $this->order_id;
	}

	public function getFullName()
	{
		return $this->shipp_lname . ' ' . $this->shipp_fname;
	}

	public function getProfit($currency_prefix = '')
	{
		return number_format($this->{'subtotal'.$currency_prefix} * ( $this->comission / 100),2, '.','');
	}

	private static function updateStatus($status, $order_id)
	{

		$sql = 'UPDATE {{orders}} SET status = :status, processed = 1';
        $sql .=' WHERE order_id = :order_id';

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":order_id", $order_id, PDO::PARAM_INT);
		$command->bindParam(":status", $status, PDO::PARAM_INT);
		$row_count = $command->execute();
		if($row_count > 0 ) 
			return true;
		return false;
	}

    public static function updateOrder($status, $order_id)
    {

        $model = Orders::model()->findByPk($order_id);

    		
        if($model === null){
            return false;
        }
        $current_status = $model->status;
        // $profit = number_format($model->subtotal * $model->comission / 100,2);
        if(!Orders::updateStatus($status, $order_id)){
            return false;
        }
        // if($current_status != Orders::STATUS_ACCEPT && $status == Orders::STATUS_ACCEPT){
        //     // плюсуем баланс
        //     Profile::updateBalance($profit);
        // } elseif($current_status == Orders::STATUS_ACCEPT && $status != Orders::STATUS_ACCEPT){
        //     // минусуем баланс
        //     Profile::updateBalance($profit*(-1));
        // }

		$order_status = new OrderStatuses;
		$order_status->status = $status;
		$order_status->order_id = $order_id;
		$order_status->save();

		return true;
    }	


    public static function updateComission($comission, $order_id)
    {

        $model = Orders::model()->findByPk($order_id);

    		
        if($model === null){
            return false;
        }
        
        $model->comission = $comission;
        if($model->save(true, array('comission'))){
			return true;
        }
    	return false;

    }	
    
    public static function getUnsentCount()
    {
    	$criteria = new CDbCriteria;
		$criteria->condition = 'processed = 0 and date_cr < :date';
		$criteria->params = array(':date' => date('Y-m-d H:i:s', time() - 60*61));  // час - запас на отправку по крону

    	$count = self::model()->count($criteria);

    	return array('count' => $count);
    }	

	public static function isReorder($email)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'email = :email';
		$criteria->params = array(':email'=>$email);
		$count = self::model()->count($criteria);
		if($count == 0){
			return false;
		}
		return true;
	}
}
