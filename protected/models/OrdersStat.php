<?php


class OrdersStat extends CModel
{
	public function attributeNames()
	{
		return array();
	}

	public static function getByDate($from, $to, $date_format, $payment_methods)
	{
		$sql = "SELECT 
				DATE_FORMAT(a.date_cr, :date_format) as dat, 
				count(orders_accept.order_id) as accept, 
				count(orders_re.order_id) as reorder, 
				count(orders_decline.order_id) as decline, 
				count(orders_wait.order_id) as wait, 
				sum(orders_accept.subtotal) as amount, 
				sum(orders_accept.subtotal * orders_accept.comission / 100)  as profit
				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				left join {{orders}} orders_re on a.order_id = orders_re.order_id and orders_re.status = 2 and orders_re.reorder = 1
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				AND a.payment_method IN(". $payment_methods . ") ";

		$sql .= "GROUP BY dat ORDER BY dat ASC";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getByCountry($from, $to, $payment_methods)
	{
		$sql = "SELECT 
				a.http_country as country, 
				count(orders_accept.order_id) as accept, 
				count(orders_re.order_id) as reorder, 
				count(orders_decline.order_id) as decline, 
				count(orders_wait.order_id) as wait, 
				sum(orders_accept.subtotal) as amount, 
				sum(orders_accept.subtotal * orders_accept.comission / 100)  as profit
				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				left join {{orders}} orders_re on a.order_id = orders_re.order_id and orders_re.status = 2 and orders_re.reorder = 1
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				AND a.payment_method IN(". $payment_methods . ") ";

		$sql .= "GROUP BY country ORDER BY country ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		// $command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getByTrackid($from, $to, $payment_methods)
	{
		$sql = "SELECT 
				a.trackid as trackid, 
				count(orders_accept.order_id) as accept, 
				count(orders_re.order_id) as reorder, 
				count(orders_decline.order_id) as decline, 
				count(orders_wait.order_id) as wait, 
				sum(orders_accept.subtotal) as amount, 
				sum(orders_accept.subtotal * orders_accept.comission / 100)  as profit
				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				left join {{orders}} orders_re on a.order_id = orders_re.order_id and orders_re.status = 2 and orders_re.reorder = 1
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				AND a.payment_method IN(". $payment_methods . ") ";

		$sql .= "GROUP BY trackid ORDER BY trackid ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		// $command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getByDateApiTraff($from, $to, $date_format)
	{
		$sql = "SELECT 
				DATE_FORMAT(a.date_cr, :date_format) as dat,
				sum(orders_accept.shipping_price) as shipping,
				sum(orders_accept.insurance) as insurance,
				count(orders_accept.order_id) as accept, 
				count(orders_decline.order_id) as decline, 
				count(orders_wait.order_id) as wait, 
				sum(orders_accept.subtotal) as amount, 
				sum(orders_accept.subtotal * orders_accept.comission / 100)  as profit
				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				GROUP BY dat ORDER BY dat ASC";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getToday()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00');
		$to = date('Y-m-d 23:59:59');
		$sql = "SELECT 
				DATE_FORMAT(a.date_cr, :date_format) as dat, 
				count(orders_accept.order_id) as accept, 
				count(orders_wait.order_id) as wait, 
				count(orders_decline.order_id) as decline, 
				sum(orders_accept.subtotal) as amount, 
				sum(orders_accept.subtotal * orders_accept.comission / 100)  as profit,
				sum(orders_wait.subtotal) as amount_wait, 
				sum(orders_wait.subtotal * orders_wait.comission / 100)  as profit_wait
				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				GROUP BY dat ORDER BY dat ASC";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$result = $command->queryRow();
		if(!$result){
			return array();
		}
		return $result;
	}
	
	public static function getSalesByDrugs($from, $to, $payment_methods)
	{
		// -- sum(orders_accept.subtotal) as amount,
						// --sum(orderprod_accepted.price) as amount, 
				// --sum(orderprod_accepted.price * orders_accept.comission / 100)  as profit
		$sql = "SELECT 
				products.drug_id as drug_id, 
				drugs.drugname as drugname,
				count(orders_accept.order_id) as accept, 
				count(orders_re.order_id) as reorder, 
				count(orders_decline.order_id) as decline, 
				count(orders_wait.order_id) as wait

				from {{orders}} a 
				left join {{orders}} orders_accept on a.order_id = orders_accept.order_id and orders_accept.status = 2 
				left join {{orders}} orders_decline on a.order_id = orders_decline.order_id and orders_decline.status = 3 
				left join {{orders}} orders_wait on  a.order_id = orders_wait.order_id and (orders_wait.status = 0 or orders_wait.status = 1)
				left join {{orders}} orders_re on a.order_id = orders_re.order_id and orders_re.status = 2 and orders_re.reorder = 1
				left join {{order_products}} orderprod on a.order_id = orderprod.order_id
				left join {{products}} products on orderprod.prod_id = products.prod_id
				left join {{drugs}} drugs on products.drug_id = drugs.drug_id
				WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				AND a.payment_method IN(". $payment_methods . ") ";

		$sql .= "GROUP BY drug_id ORDER BY accept DESC, reorder DESC, wait DESC, decline DESC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		// $command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getProfitByDrugs($from, $to, $payment_methods)
	{
		// -- sum(orders_accept.subtotal) as amount,
						// --sum(orderprod_accepted.price) as amount, 
				// --sum(orderprod_accepted.price * orders_accept.comission / 100)  as profit
		$sql = "SELECT 
				products.drug_id as drug_id, 
				drugs.drugname as drugname, 
				sum(orderprod.price * (1 - orderprod.discount / 100) ) as amount, 
				sum(orderprod.price * (1 - orderprod.discount / 100 ) * orders.comission / 100) as profit

				from {{orders}} orders
				left join {{order_products}} orderprod on orderprod.order_id = orders.order_id
				left join {{products}} products on orderprod.prod_id = products.prod_id
				left join {{drugs}} drugs on products.drug_id = drugs.drug_id
				WHERE orders.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') 
				AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')
				AND orders.status = 2 
				AND orders.payment_method IN(". $payment_methods . ") 
				";

		$sql .= "GROUP BY drug_id ORDER BY amount DESC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		// $command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}
}