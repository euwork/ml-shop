<?php

/**
 * This is the model class for table "{{pages}}".
 *
 * The followings are the available columns in table '{{pages}}':
 * @property integer $page_id
 * @property string $slug
 * @property string $title
 * @property string $header
 * @property string $keywords
 * @property string $description
 */
class Pages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pages}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug', 'required'),
			array('slug', 'length', 'max'=>128),
			array('title, header, keywords, description', 'length', 'max'=>256),
			array('title, header, keywords, description', 'filter', 'filter'=>'strip_tags'),
			array('title, header, keywords, description', 'filter', 'filter'=>'trim'),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('page_id, slug, title, header, keywords, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'page_id' => 'Page',
			'slug' => 'Slug',
			'title' => 'Title',
			'header' => 'Header &lt;h1&gt;',
			'keywords' => 'Keywords',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.page_id',$this->page_id);
		$criteria->compare('t.slug',$this->slug,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.header',$this->header,true);
		$criteria->compare('t.keywords',$this->keywords,true);
		$criteria->compare('t.description',$this->description,true);


		return new CActiveDataProvider($this, array(
            'criteria' => $this->ml->modifySearchCriteria($criteria),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array(
                    'title',
                    'header',
                    'keywords',
                    'description',
                    'content',
                    'slug',
                ),
                'langClassName' => 'PagesLang',
                'langTableName' => 'pages_lang',
                'languages' => Languages::getArray(),
                'defaultLanguage' => Yii::app()->config->get('defaultLanguage'),
                'langForeignKey' => 'owner_id',
                'dynamicLangClass' => true,
                'forceOverwrite'=>true,
            ),
        );
    }
 
    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }

	public function getUrl()
	{
		if($this->slug == '_index'){
			return Yii::app()->createUrl('/');
		}
		if($this->slug == '_affiliates'){
			return Yii::app()->createUrl('/affiliates');
		}
		return Yii::app()->createUrl('/'.Yii::app()->config->get('prefix.page').'/' . $this->slug);
	}

	public static function getUrlById($id)
	{

		$lang_id = Yii::app()->language;

		$sql = "SELECT l_slug from {{pages_lang}}
				WHERE owner_id = :id and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->cache(Yii::app()->params['cachetime'])->createCommand($sql);
		$command->bindParam(":id", $id, PDO::PARAM_INT);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$slug = $command->queryScalar();

		if($slug == false){
			return false;
		}

		$model = new self;
		$model->slug = $slug;

		return $model->getUrl();

	}

	public static function check_slug($slug, $id)
	{

		$sql = "SELECT owner_id, lang_id from {{pages_lang}}
				WHERE l_slug = :slug and owner_id <> :owner_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":owner_id", $id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		if($l_id == false){
			return true;
		} 
		return false;
	}

	public static function findByLSlug($slug, $lang_id)
	{
		$sql = "SELECT owner_id from {{pages_lang}}
				WHERE l_slug = :slug and lang_id = :lang_id";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":slug", $slug, PDO::PARAM_STR);
		$command->bindParam(":lang_id", $lang_id, PDO::PARAM_STR);
		$l_id = $command->queryScalar();

		return $l_id;
	}


}
