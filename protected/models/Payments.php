<?php

/**
 * This is the model class for table "{{payments}}".
 *
 * The followings are the available columns in table '{{payments}}':
 * @property string $pay_id
 * @property string $date_cr
 * @property string $amount
 * @property string $comment
 */
class Payments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('amount', 'required'),
			array('amount', 'length', 'max'=>10),
			array('comment', 'length', 'max'=>256),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pay_id' => 'Pay',
			'date_cr' => 'Date Cr',
			'amount' => 'Amount',
			'comment' => 'Comment',
		);
	}

	public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_cr',
                'updateAttribute' => null,
            ),
        );
    }

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTotalPaid()
	{
		$criteria=new CDbCriteria;
		$criteria->select='sum(amount) as amount';  // подходит только то имя поля, которое уже есть в модели
		return Payments::model()->find($criteria)->getAttribute('amount'); 
	}
}
