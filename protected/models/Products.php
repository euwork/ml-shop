<?php

/**
 * This is the model class for table "{{products}}".
 *
 * The followings are the available columns in table '{{products}}':
 * @property integer $prod_id
 * @property integer $drug_id
 * @property string $drugname
 * @property string $dose
 * @property string $amount
 * @property string $bonus
 * @property string $price
 * @property string $prodtype
 *
 * The followings are the available model relations:
 * @property Drugs $drug
 */
class Products extends CActiveRecord implements IECartPosition
{
	const BASE_GER = 0;
	const BASE_FR  = 1;

	public static function getProductsBasesList()
	{
		return array(
			// Products::BASE_GER 	=> 'Germany (high prices)',
			Products::BASE_FR 	=> 'France (low prices)',
			);
	}

    // public static function getProductsBaseById($id)
    // {
    //     $_array = self::getProductsBasesList();
    //     if(!array_key_exists($id, $_array))
    //         return false;
    //     return $_array[$id];
    // }

    // public static function getCurrentProductsBaseName()
    // {
    //     return self::getProductsBaseById(self::getCurrentProductsBase());
    // }  

	const NOACTIVE 	= 0;
	const ACTIVE 	= 1;

	public static function getStatusesList()
	{
		return array(
			'Off' 	=> self::NOACTIVE,
			'On'	=> self::ACTIVE,
			);
	}

	public function getStatusName()
	{
		return array_search($this->status, self::getStatusesList());
	}

	public function getChangeTo()
	{
		return $this->status == self::ACTIVE ? self::NOACTIVE : self::ACTIVE ;
	}
	
	public $use_discount = false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{products}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('drug_id, dose, amount, price', 'required'),
			array('drug_id, amount, bonus', 'numerical', 'integerOnly'=>true),
			array('amount_in', 'length', 'max'=>256),
			array('dose', 'length', 'max'=>128),
			array('price', 'length', 'max'=>7),
			array('discount', 'length', 'max'=>5),
			array('status, a_status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prod_id, drug_id, dose, amount, bonus, amount_in, price, discount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
			'items' => array(self::HAS_MANY, 'TestpackProducts', 'prod_id',),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prod_id' => 'Prod',
			'drug_id' => 'Drug',
			// 'drugname' => 'drugname',
			'dose' => 'Dose',
			'amount' => 'Amount',
			'bonus' => 'Bonus',
			'amount_in' => 'Amount_in',
			'price' => 'Price',
			'discount' => 'Discount',
		);
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getId()
	{
        return 'Prod'.$this->prod_id;
    }

    public function getPrice()
    {
    	switch (Config::getCurrentProductsBase()) {
    		case self::BASE_GER:
    			$price = $this->price * 0.9;
    			break;
    		
    		case self::BASE_FR:
    			$price = $this->price * ( Config::getPriceIncrease() / 100 +1 );
    			
    			break;
    	}

		$price = number_format($price, 2, '.', '');

		
        return $price;
    }

    public function getPriceOld()
    {
    	$price = $this->getPrice() * ( Config::getPriceIncreaseOld() / 100 +1 );
    	$price = number_format($price, 2, '.', '');

    	return $price;
    }


	public function getImage()
	{
		return $this->drug->getImage(DrugsImages::TYPE_CAT);
	}

	public function isTestpacken()
	{
		return $this->amount == 0;
	}

	public function isOriginal()
	{
		return  false; 
	}

	public function getDrugname()
	{
		$drugname = $this->drug->drugname;
		if(!$this->isTestpacken()){
			$drugname = Filter::generic($drugname);
		}

		$drugname = Yii::t('products', $drugname);

		return $drugname;
	}

	public function getAmountIn()
	{
		return Yii::t('products', $this->amount_in);
	}

	// private static function filter_generic($str)
	// {
	// 	$replace_generic = true;

	// 	if($replace_generic){
	// 		$str = str_ireplace('generika', '', $str);
	// 		$str = str_replace('  ', ' ', $str);
	// 		$str = trim($str);
	// 	}

	// 	return $str;
	// }

	public function isGerBase()
	{
		if($this->label == self::BASE_GER){
			return true;
		}

		return false;
	}

	public function isDiscountable()
	{
		if ($this->amount_in == 'Pillen')
			return true;

		return false;
	}

	// public function pills_data_format_cart() 
	// {
	// 	if($this->isTestpacken()){
	// 		$name = $this->getDrugname() . ' ' . $this->dose;
	// 		$dose = '<p style="font-size: 12px;">' . $this->getItemsDescription() . '</p>';
	// 		$quantity = '1 ' . Yii::t('products', 'Packung');
	// 	} else {
	// 		$name = $this->getDrugname();
	// 		$dose = $this->dose;
	// 		if($this->bonus) {
	// 			$quantity = $this->amount . ' + '. $this->bonus . ' ' . $this->getAmountIn();					
	// 		} else {
	// 			$quantity = $this->amount . ' ' . $this->getAmountIn();
	// 		}
	// 	}
	// 	return array($name, $dose, $quantity);
	// }

	// public function pills_data_format_cart_d7() 
	// {
	// 	if($this->isTestpacken()){
	// 		$name = $this->getDrugname() . ' ' . $this->dose;
	// 		// $dose = '<p style="font-size: 12px;">' . str_replace('mg', 'mg<br>', Yii::t('products', $this->amount_in)) . '</p>';
	// 		$dose = '';
	// 		$quantity = '1 ' . Yii::t('products', 'Packung');
	// 		$bonus = '';
	// 	} else {
	// 		$name = $this->getDrugname();
	// 		$dose = $this->dose;
	// 		if($this->bonus) {
	// 			$quantity = $this->amount ;	
	// 			$bonus = ' + '. $this->bonus . ' ' . $this->getAmountIn();				
	// 		} else {
	// 			$quantity = $this->amount . ' ' . $this->getAmountIn();
	// 			$bonus = '';
	// 		}
	// 	}
	// 	return array($name, $dose, $quantity, $bonus);
	// }

	public function pills_data_format_checkout() 
	{
		$name = $this->getDrugname() . ' ' . $this->dose;
		if($this->isTestpacken()){
			$quantity = $this->getItemsDescription();
		} else {
			if($this->bonus) {
				$quantity = $this->amount . ' + '. $this->bonus . ' ' . $this->getAmountIn();					
			} else {
				$quantity = $this->amount . ' ' . $this->getAmountIn();
			}
		}
		return array($name, $quantity);
	}	

	public function pills_data_format_prddesc() 
	{
		$name = $this->getDrugname() . ' ' . $this->dose;
		if($this->isTestpacken()){
			$name .= ': ' . $this->getItemsDescription();
		} else {
			$name .= ' ' . $this->amount . ' ' . $this->getAmountIn();
			if($this->bonus) {
				$name .= Yii::t('products', ' + {bonus} Bonus {amount_in}', array('{bonus}'=> $this->bonus, '{amount_in}'=>$this->getAmountIn()));
			}				
		}
		return $name;
	}

	public static function getNextpack($position)
	{
		if(!$position->isDiscountable()) return null;

		$criteria = new CDbCriteria;
		$criteria->condition = 'drug_id = :drug_id and dose = :dose and label = :label';
		$criteria->params = array(':drug_id'=>$position->drug_id, ':dose'=>$position->dose, ':label'=>$position->label);
		$criteria->order = 'price ASC';
		$model = self::model()->findAll($criteria);
		if(count($model) < 2) return null;
		$get_item = false;
		foreach($model as $pack)
		{
			if($get_item){
				return $pack;
			}
			if($pack->id == $position->id){
				$get_item = true;
			}
		}
		return null;
	}


	public function getItemsDescription()
	{
		if(!$this->isTestpacken()) return false;

		$desc_sub_template = "{amount} {amount_in} {drugname} {dose}";
		$qty_array = array();
		
		foreach($this->items as $item)
		{
			$amount_in = Yii::t('products', $item->amount_in);
			$drugname = Yii::t('products', $item->getDrugname());

			$desc = $desc_sub_template;
			$desc = str_replace('{drugname}', $drugname, $desc);
			$desc = str_replace('{dose}', $item->dose, $desc);
			$desc = str_replace('{amount_in}', $amount_in, $desc);
			$desc = str_replace('{amount}', $item->amount, $desc);

			$qty_array[] = $desc;
		}

		return implode("<br>\n", $qty_array);
	}

	// public static function getItemsByDrugId($id)
	// {
	// 	$criteria = new CDbCriteria;
	// 	$criteria->condition = 'label = :label and status = :status and a_status = :a_status';
	// 	$criteria->params = array(
 //    		':label'=>Config::getCurrentProductsBase(), 
 //    		':status' => Products::ACTIVE,
 //    		':a_status' => Products::ACTIVE
 //    	);
 //    	$criteria->order = 'CAST(dose as unsigned) DESC, '. Config::getProductsOrder();
 //    	return self::model()->findAll($criteria);
	// }

	public static function getListProductsLabels()
	{
		return array(
			Products::BASE_GER => 'Germany (high prices)',
			Products::BASE_FR => 'France (low prices)',
			);
	}

	public static function getList($lang = null, $label = false)
	{
		if($lang == 'de'){
			$lang = null;
		}
		$data = array(); // out array

		$criteria = new CDbCriteria;


		$label_list = self::getListProductsLabels();
		foreach($label_list as $item_key => $item_desc)
		{
			if($label !== false && $label !== (string)$item_key) continue;
			$data[$item_key]['base_label'] = $item_key;
			$data[$item_key]['description'] = $item_desc;
			$data[$item_key]['currency'] = 'EUR';
			$data[$item_key]['products'] = array();
		}

		$model = self::model()->with('items', 'drug')->findAll($criteria);
		foreach($model as $product)
		{
			if($label !== false && $label !== $product->label) continue;

			$product_array = array();
			foreach(array('prod_id', 'drug_id', 'dose', 'amount', 'bonus', 'price', 'discount', 'status') as $attr)
			{
				$product_array[$attr] = $product->{$attr};
			}
			foreach(array('drugname', 'amount_in') as $attr)
			{
				$product_array[$attr] = Yii::t('products', $product->{$attr}, array(), null, $lang);
			}

			if($product->amount == 0){ // testpack
				$items_array = array();
				foreach($product->items as $k=>$item)
				{
					foreach(array('id', 'drug_id', 'dose', 'amount') as $attr)
					{
						$items_array[$k][$attr] = $item->{$attr};
					}

					foreach(array('drugname', 'amount_in') as $attr)
					{
						$items_array[$k][$attr] = Yii::t('products', $item->{$attr}, array(), null, $lang);
					}
				}
				
				$product_array['items'] = $items_array;
			}

			$data[$product->label]['products'][] = $product_array;				
		}

		return $data;

	}
}
