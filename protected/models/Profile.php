<?php

/**
 * This is the model class for table "{{profile}}".
 *
 * The followings are the available columns in table '{{profile}}':
 * @property string $id
 * @property string $balance
 * @property string $paymentinfo
 */
class Profile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{profile}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment_method', 'required'),
			array('payment_method', 'in', 'range'=>array_flip(self::getPaymentNameList())),
			array('webmoney', 'match', 'pattern'=>'/^Z[\d]{12}$/'),
			array('balance', 'length', 'max'=>10),
			// array('epese, ecoin, skrill, paxum', 'length', 'max'=>128),
			array('bitcoin, capitalist, usdt', 'length', 'max'=>128),
			array('bank_account_name, bank_address, swift_code, bank_account_number, bank_code', 'length', 'max'=>256),
			array('correspondent_account', 'length', 'max'=>512),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'balance' => 'Balance',
			'paymentinfo' => 'Paymentinfo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('balance',$this->balance,true);
		$criteria->compare('paymentinfo',$this->paymentinfo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getBalance()
	{
		$model = self::model()->findByPk(1);
		if($model === null)
			return false;
		return $model->balance;
	}

	public static function getComission()
	{
		$model = self::model()->findByPk(1);
		if($model === null)
			return false;
		return $model->comission;
	}


	public static function getPaymentInfo()
	{
		$model = self::model()->findByPk(1);
		if($model === null)
			return false;
		return $model->getAttributes(array(
				'webmoney', 
				// 'epese', 'ecoin', 'skrill', 
				'bank_account_name', 'bank_address', 
				'swift_code', 'bank_account_number', 
				'bank_code', 'correspondent_account',
				'payment_method', 
				'bitcoin', 'capitalist', 
				// 'paxum', 
				'usdt',
				));
	}
	

	public static function updateBalance($balance)
	{
		$model = self::model()->findByPk(1);
 		if($model === null)
			return false;
		$model->balance = $balance;
 		return $model->save(true, array('balance'));
	}


	public static function updateComission($comission)
	{
		$model = self::model()->findByPk(1);
 		if($model === null)
			return false;
		$model->comission = $comission;
 		return $model->save(true, array('comission'));
	}

	


	const WEBMONEY 	= 0;
	// const EPESE 	= 1;
	// const ECOIN 	= 2;
	const WIRE 		= 3;
	// const SKRILL 	= 4;
	const BITCOIN 	= 5;
	const CAPITALIST = 6;
	// const PAXUM = 7;
	const USDT = 8;

	public static function getPaymentPercList()
	{
		return array(
			self::WEBMONEY => Format::percent(1),
			// self::EPESE => Format::percent(1),
			// self::ECOIN => Format::percent(0),
			self::WIRE => Format::price(14) . ' - ' . Format::price_doll(60),
			// self::SKRILL => Format::percent(3),
			self::BITCOIN => Format::percent(1),
			self::CAPITALIST => Format::percent(1),
			// self::PAXUM => Format::percent(1),
			self::USDT => Format::percent(1),
			);
	}

	public static function getMinPayoutList()
	{
		return array(
			self::WEBMONEY => 100,
			// self::EPESE => 200,
			// self::ECOIN => 200,
			self::WIRE => 1000,
			// self::SKRILL => 350,
			self::BITCOIN => 1000,
			self::CAPITALIST => 100,
			// self::PAXUM => 200,
			self::USDT => 1000,
			);		
	}
	public static function getPaymentNameList()
	{
		return array(
			self::WEBMONEY => 'Webmoney WMZ',
			// self::EPESE => 'Epese ID',
			// self::ECOIN => 'Epayments ID',
			self::WIRE => 'Wire transfer',
			// self::SKRILL => 'Skrill (MB) E-mail',
			self::BITCOIN => 'Bitcoin',
			self::CAPITALIST => 'Capitalist EUR',
			// self::PAXUM => 'Paxum ID',
			self::USDT => 'USDT',
			);		
	}

	public static function getPaymentPercentById($id)
	{
		$_array = self::getPaymentPercList();
		if(!array_key_exists($id, $_array))
			return false;
		return $_array[$id];
	}

	public static function getMinPayoutById($id)
	{
		$_array = self::getMinPayoutList();
		if(!array_key_exists($id, $_array))
			return false;
		return $_array[$id];
	}

	public static function getPaymentNameById($id)
	{
		$_array = self::getPaymentNameList();
		if(!array_key_exists($id, $_array))
			return false;
		return $_array[$id];
	}






}
