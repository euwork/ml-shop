<?php


class Request extends CModel
{
	public function attributeNames()
	{
		return array();
	}



    public static function sendOrder($order) // отправка заказа в админку
    {
        $item_order = $order->attributes;
        foreach($order->order_products as $product)
        {
            $item_product = $product->getAttributes(array('price', 'price_usd', 'discount', 'prodname', 'prod_id') );
            // $item_product['prodname'] = implode(' ', $product->product->pills_data_format_checkout());
            $item_product['prodname'] = $product->product->pills_data_format_prddesc();
            $item_order['products'][] = $item_product;
        }
        foreach($order->statuses as $status)
        {
            $item_order['statuses'][] = $status->getAttributes(array('status', 'date_cr'));
        }

        $params = array(
            'action' => 'neworder',
            'order' => $item_order,
            );

        #отмечаем в бд, что заказ отправлен
        if(self::makeRequest($params)){
            Orders::model()->updateByPk($order->order_id, array('processed'=>1));
            return true;
        } else {
            // Yii::log($data, CLogger::LEVEL_ERROR, 'application.models.request');
            return false;
        }
    }


    public static function sendTestimonial($model)
    {
        $params = array(
            'action' => 'testimonial',
            'author' => $model->author,
            'content' => $model->content,
            );
        return self::makeRequest($params);
    }

    public static function sendEmail($model)
    {
        $params = array(
            'action' => 'email',
            'first_name' => $model->first_name,
            'email' => $model->email,
            );
        return self::makeRequest($params);
    }

    public static function sendReview($model)
    {
        $params = array(
            'action' => 'review',
            'author' => $model->author,
            'content' => $model->content,
            'drug_id' => $model->drug_id,
            );
        return self::makeRequest($params);
    }
    public static function sendContact($model)
    {
        $params = array(
            'action' => 'contact',
            'name' => $model->name,
            'email' => $model->email,
            'thema' => $model->thema,
            'content' => $model->content,
            );
        return self::makeRequest($params);
    }

    public static function sendAffiliate($model)
    {
        $params = array(
            'action' => 'affiliate',
            'name' => $model->name,
            'email' => $model->email,
            'website' => $model->website,
            'content' => $model->content,
            );
        return self::makeRequest($params);
    }
    // ----------------|
    // GET data--------|
    // ----------------|
    public static function getCurencyRates()
    {
         $params = array(
            'action' => 'get_currency_rates',
            );
        return self::makeRequest($params, true);       
    }

    public static function getBankDetails()
    {
         $params = array(
            'action' => 'get_bank_details',
            );
        return self::makeRequest($params, true);       
    }



    public static function checkApiKey($domain, $aff_id, $api_key)
    {
        $params = array(
            'action' => 'check_apikey',
            'apikey'=> $api_key,
            'shop_id'=> $aff_id,
            );

        $page = Yii::app()->curl->post('http://' . 'ml.' . $domain . '/request/api', http_build_query($params));
        $answer = json_decode($page);        

        if(isset($answer->success) && $answer->success == 1)
            return true;
        return false;
        
    }

    public static function getProductsList()
    {
         $params = array(
            'action' => 'get_products_list',
            );
        return self::makeRequest($params, true);       
    }

    private static function makeRequest($params, $return = false)
    {
        $params = array_merge(
            array(
                'apikey'=>Yii::app()->config->get('api_key'),
                'shop_id'=>Yii::app()->config->get('affid'),
            ),
            $params);

        $page = Yii::app()->curl->post(Config::getApiUrl() . '/request/api', http_build_query($params));

        $answer = json_decode($page);
        
        if($return){
            return $answer;
        }

        if(isset($answer->success) && $answer->success == 1)
            return true;
        return false;
    }




    public static function getContent($languages)
    {
        $params = array(
            'apikey'=>Yii::app()->config->get('api_key'),
            'affid'=>Yii::app()->config->get('affid'),
            'languages'=>implode(',',$languages),
            );     

        Yii::app()->curl->setOption(CURLOPT_CONNECTTIMEOUT, 30);
        Yii::app()->curl->setOption(CURLOPT_TIMEOUT, 30);
        $page = Yii::app()->curl->post(Config::getApiUrl('content') . '/request/getcontent', http_build_query($params));   

        if($page != ''){
            return array('data'=>json_decode($page));
        } 

        return array('error'=>Yii::app()->curl->getError());  
    }

    public static function getReviews($languages, $addparams)
    {
        $params = array(
            'apikey'=>Yii::app()->config->get('api_key'),
            'affid'=>Yii::app()->config->get('affid'),
            'languages'=>implode(',',$languages),
            );    

        $params = array_merge($params, $addparams);

        Yii::app()->curl->setOption(CURLOPT_CONNECTTIMEOUT, 45);
        Yii::app()->curl->setOption(CURLOPT_TIMEOUT, 45);
        
        $page = Yii::app()->curl->post(Config::getApiUrl('content') . '/request/getreviews', http_build_query($params)); 
        if($page != ''){
            return array('data'=>json_decode($page));
        } 

        return array('error'=>Yii::app()->curl->getError());  
    }


    public static function downloadTemplateImgsZip()
    {
        $dir = Theme::getImgPathTemplate();
        if(!is_writable($dir)){
            return false;
        }

        $params = array(
            'apikey'=>Yii::app()->config->get('api_key'),
            'affid'=>Yii::app()->config->get('affid'),
            'theme'=>Theme::getLabel(),
            );  

        $page = Yii::app()->curl->post(Config::getApiUrl('content') . '/request/gettemplateimages', http_build_query($params)); 

        if(!$page) {
            return false;
        } 

        $data = json_decode($page);
        if(isset($data->success) && $data->success == true){

            $zipName = 'imgs01.zip';
            $zipFullPath = $dir . '/' . $zipName;

            $zipcontent = Yii::app()->curl->get($data->zipurl);

            if($zipcontent){

                $fg = fopen($zipFullPath, 'w+'); fputs($fg, $zipcontent); fclose($fg);

                return $zipFullPath;
            }

        }


        return false;







    }

    


}