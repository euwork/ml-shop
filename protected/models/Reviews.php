<?php

/**
 * This is the model class for table "{{reviews}}".
 *
 * The followings are the available columns in table '{{reviews}}':
 * @property integer $rev_id
 * @property integer $drug_id
 * @property string $rating
 * @property string $author
 * @property string $content
 * @property string $date_cr
 *
 * The followings are the available model relations:
 * @property Drugs $drug
 */
class Reviews extends CActiveRecord
{
	public $code;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reviews}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('drug_id, date_cr', 'required'),
			array('drug_id', 'numerical', 'integerOnly'=>true),
			array('vote_yes, vote_all', 'numerical', 'integerOnly'=>true),
			array('vote_all', 'compare', 'compareAttribute'=>'vote_yes', 'operator' => '>='),
			array('author, tit_name, points', 'length', 'max'=>256),
			array('content', 'length', 'max'=>3000),
			// array('author', 'match', 'pattern'=>'/^[\w\s\.\-,]+$/'),
			array('code','captcha','allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'frontadd' ),
			array('date_cr', 'date', 'format'=>'yyyy-mm-dd H:m:s', 'allowEmpty'=>false),
			// array('points', 'numerical', 'numberPattern'=>'/^\d{1}\.\d{2}$/'),
			array('lang', 'in', 'range' => Languages::getArrayFlip()),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rev_id' => 'Rev',
			'drug_id' => 'Drug',
			'rating' => 'Rating',
			'author' => 'Author',
			'content' => 'Content',
			'date_cr' => 'Date',
			'tit_name' => 'Title',
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getPointsList()
	{
		$data = array();
		foreach(range(500, 0, 25) as $number)
		{
			$number = $number / 100;
			$data[strval(number_format($number,2))] = $number ;
		}
		return $data;
	}

	const STAT_NEW = 1;
	const STAT_NONEW = 0;

	public static function getCountNew()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.new = :new';
		$criteria->params = array(':new' => self::STAT_NEW);
		$count = self::model()->count($criteria);
		return $count;
	
	}

	public static function getCountByLang($lang)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.lang = :lang';
		$criteria->params = array(':lang' => $lang);
		$count = self::model()->count($criteria);
		return $count;
	
	}

	public static function getCountNewByDrug($id)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.new = :new and drug_id = :drug_id';
		$criteria->params = array(':new' => self::STAT_NEW, ':drug_id'=>$id);
		$count = self::model()->count($criteria);
		return $count;
	
	}

	public static function atHome()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.home = 1 and t.active = 1 and lang = :lang';
		$criteria->params = array(':lang'=>Yii::app()->language);
		$criteria->order = 't.date_cr DESC';
		$criteria->limit = Yii::app()->config->get('reviews.count.home');

		$model = self::model()->cache(Yii::app()->params['cachetime'])->findAll($criteria);		

		return $model;
	}

}
