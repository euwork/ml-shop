<?php


class TestpackProducts extends CActiveRecord
{

	public function tableName()
	{
		return '{{testpack_products}}';
	}

	public function rules()
	{

		return array(
			array('id, prod_id, drug_id, dose, amount, amount_in', 'required'),
			// array('drugname', 'required'),
			// array('drugname', 'length', 'max'=>256),
			array('amount_in', 'length', 'max'=>256),
			array('id, prod_id, drug_id, amount', 'numerical', 'integerOnly'=>true),
			array('dose', 'length', 'max'=>128),
		);
	}

	public function relations()
	{
		return array(
			'drug' => array(self::BELONGS_TO, 'Drugs', 'drug_id'),
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDrugname()
	{
		return $this->drug->drugname;
	}
}