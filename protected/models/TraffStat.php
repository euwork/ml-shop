<?php


class TraffStat extends CModel
{
	public function attributeNames()
	{
		return array();
	}


	public static function getByDate($from, $to, $date_format)
	{
		self::updateStat();
		
		$sql = "SELECT
				DATE_FORMAT(t2.dat, :date_format) as dat,
				sum(t1.uniq)  as uniq,
				sum(t1.bill)  as bill,
				sum(t1.hit)  as hit
				from {{statistics}} t1
				LEFT JOIN {{statistics}} t2 ON t1.dat = t2.dat
				WHERE t1.dat BETWEEN STR_TO_DATE(:from, '%Y-%m-%d') 
				AND STR_TO_DATE(:to, '%Y-%m-%d')
				GROUP BY dat 
				ORDER BY dat ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	}

	public static function getCountriesByDate($from, $to)
	{
		self::updateCountry();


		$sql = "SELECT
				stat.country as country,
				sum(stat.uniq)  as uniq,
				sum(stat.hit)  as hit,
				sum(stat.bill)  as bill
				from {{stat_countries}} stat
				WHERE stat.dat BETWEEN STR_TO_DATE(:from, '%Y-%m-%d') 
				AND STR_TO_DATE(:to, '%Y-%m-%d')
				GROUP BY country ORDER BY uniq DESC, hit DESC, country ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	
	}

	public static function getTrackidsByDate($from, $to)
	{
		self::updateTrackid();


		$sql = "SELECT
				stat.trackid as trackid,
				sum(stat.uniq)  as uniq,
				sum(stat.hit)  as hit,
				sum(stat.bill)  as bill
				from {{stat_trackids}} stat
				WHERE stat.dat BETWEEN STR_TO_DATE(:from, '%Y-%m-%d') 
				AND STR_TO_DATE(:to, '%Y-%m-%d')
				GROUP BY trackid ORDER BY uniq DESC, hit DESC, trackid ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	
	}

	public static function getReferersByDate($from, $to)
	{
		self::updateReferer();


		$sql = "SELECT
				stat.referer as referer,
				sum(stat.uniq)  as uniq,
				sum(stat.hit)  as hit,
				sum(stat.bill)  as bill,
				sum(stat.ord)  as ord
				from {{stat_referers}} stat
				WHERE stat.dat BETWEEN STR_TO_DATE(:from, '%Y-%m-%d') 
				AND STR_TO_DATE(:to, '%Y-%m-%d')
				GROUP BY `referer` ORDER BY uniq DESC, hit DESC, referer ASC";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();
		return $dataReader->readAll();
	
	}

	public static function getToday()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00');
		$to = date('Y-m-d 23:59:59');

		$result = self::getByDate($from, $to, $date_format);
		if(!isset($result[0])){
			return array('uniq'=>0, 'hit'=>0);
		}
		return $result[0];

	}


	private static function updateStat()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00', time()-3600*24*1);
		$to = date('Y-m-d 23:59:59');
		$sql = "SELECT
			COUNT(a.access_id) as hit, 
			COUNT(b.ses_id) as uniq, 
			COUNT(c.bill) as bill, 
			DATE_FORMAT(a.date_cr, :date_format) as dat 
			FROM 
			{{traff_access}} a 
			LEFT JOIN {{traff_sessions}} b on a.date_cr = b.date_cr and a.ses_id = b.ses_id
			LEFT JOIN {{traff_sessions}} c on a.date_cr = c.date_cr and a.ses_id = c.ses_id and c.bill = 1
			WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s') 
			GROUP BY dat 
			ORDER BY dat ASC";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();


		$data = $dataReader->readAll();

		$sql = 'INSERT INTO {{statistics}} 
            (dat, uniq, hit, bill)
            VALUES (:dat, :uniq, :hit, :bill)
            ON DUPLICATE KEY UPDATE 
            uniq = :uniq, hit = :hit, bill = :bill';

		$row_count = 0;
		foreach($data as $item):

			$dat = str_replace(' ', '-', $item['dat']);
	        $command=$connection->createCommand($sql);
	        $command->bindParam(":uniq", $item['uniq'], PDO::PARAM_INT);
	        $command->bindParam(":hit", $item['hit'], PDO::PARAM_INT);
	        $command->bindParam(":bill", $item['bill'], PDO::PARAM_INT);
	        $command->bindParam(":dat", $dat, PDO::PARAM_STR);
	        $row_count += $command->execute();

        endforeach;

		return  $row_count;		
	}


	private static function updateCountry()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00', time()-3600*24*1);
		$to = date('Y-m-d 23:59:59');
		$sql = "SELECT
			COUNT(a.access_id) as hit, 
			COUNT(b.ses_id) as uniq, 
			COALESCE(SUM(b.bill), 0) as bill,
			COALESCE(c.country, '') as country,
			DATE_FORMAT(a.date_cr, :date_format) as dat 
			FROM 
			{{traff_access}} a 
			LEFT JOIN {{traff_sessions}} b on a.date_cr = b.date_cr and a.ses_id = b.ses_id
			LEFT JOIN {{traff_sessions}} c on a.ses_id = c.ses_id
			WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s') 
			GROUP BY dat, country 
			ORDER BY dat ASC, uniq DESC";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();

		$data = $dataReader->readAll();

		$sql = 'INSERT INTO {{stat_countries}} 
            (dat, country, uniq, hit, bill)
            VALUES (:dat, :country, :uniq, :hit, :bill)
            ON DUPLICATE KEY UPDATE 
            uniq = :uniq, hit = :hit, bill = :bill';

		$row_count = 0;
		foreach($data as $item):

			$dat = str_replace(' ', '-', $item['dat']);
	        $command=$connection->createCommand($sql);
	        $command->bindParam(":uniq", $item['uniq'], PDO::PARAM_INT);
	        $command->bindParam(":hit", $item['hit'], PDO::PARAM_INT);
	        $command->bindParam(":bill", $item['bill'], PDO::PARAM_INT);
	        $command->bindParam(":country", $item['country'], PDO::PARAM_STR);
	        $command->bindParam(":dat", $dat, PDO::PARAM_STR);
	        $row_count += $command->execute();

        endforeach;

		return  $row_count;		
	}

	private static function updateReferer()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00', time()-3600*24*1);
		$to = date('Y-m-d 23:59:59');
		$sql = "SELECT
			COUNT(a.access_id) as hit, 
			COUNT(b.ses_id) as uniq, 
			COALESCE(SUM(b.bill), 0) as bill, 
			COALESCE(SUM(b.ord), 0) as ord, 
			COALESCE(c.referer,'') as referer,
			DATE_FORMAT(a.date_cr, :date_format) as dat 
			FROM 
			{{traff_access}} a 
			LEFT JOIN {{traff_sessions}} b on a.date_cr = b.date_cr and a.ses_id = b.ses_id
			LEFT JOIN {{traff_sessions}} c on a.ses_id = c.ses_id
			WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s') 
			GROUP BY dat, referer 
			ORDER BY dat ASC, uniq DESC";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();

		$data = $dataReader->readAll();


		$sql = 'INSERT INTO {{stat_referers}} 
            (dat, referer, referer_hash, uniq, hit, bill, ord)
            VALUES (:dat, :referer, :referer_hash, :uniq, :hit, :bill, :ord)
            ON DUPLICATE KEY UPDATE 
            uniq = :uniq, hit = :hit, bill = :bill, ord = :ord';

		$row_count = 0;
		foreach($data as $item):

			$dat = str_replace(' ', '-', $item['dat']);
			$md5_referer = md5($item['referer']);
	        $command=$connection->createCommand($sql);
	        $command->bindParam(":uniq", $item['uniq'], PDO::PARAM_INT);
	        $command->bindParam(":hit", $item['hit'], PDO::PARAM_INT);
	        $command->bindParam(":bill", $item['bill'], PDO::PARAM_INT);
	        $command->bindParam(":ord", $item['ord'], PDO::PARAM_INT);
	        $command->bindParam(":referer", $item['referer'], PDO::PARAM_STR);
	        $command->bindParam(":referer_hash", $md5_referer, PDO::PARAM_STR);
	        $command->bindParam(":dat", $dat, PDO::PARAM_STR);
	        $row_count += $command->execute();

        endforeach;

		return  $row_count;		
	}


	public static function clearStat()
	{
		self::updateStat();
		self::updateCountry();
		self::updateReferer();
		self::updateTrackid();

		$to = date('Y-m-d 00:00:00', time()-3600*24*1); // с 00:00:00 вчера
		$connection=Yii::app()->db;


		$sql = "DELETE FROM {{traff_access}}
				WHERE date_cr < STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')";

		$command=$connection->createCommand($sql);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$row_count = $command->execute();

		$to = date('Y-m-d 00:00:00', time()-3600*24*2); // с 00:00:00 позавчера (кука хранится 24 часа.)


		$sql = "DELETE FROM {{traff_sessions}}
				WHERE date_cr < STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s')";

		$command=$connection->createCommand($sql);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$row_count = $command->execute();

		return true;
	}

	private static function updateTrackid()
	{
		$date_format = "%Y %m %d";
		$from = date('Y-m-d 00:00:00', time()-3600*24*1);
		$to = date('Y-m-d 23:59:59');
		$sql = "SELECT
			COUNT(a.access_id) as hit, 
			COUNT(b.ses_id) as uniq, 
			COALESCE(SUM(b.bill), 0) as bill,
			COALESCE(c.trackid, '') as trackid,
			DATE_FORMAT(a.date_cr, :date_format) as dat 
			FROM 
			{{traff_access}} a 
			LEFT JOIN {{traff_sessions}} b on a.date_cr = b.date_cr and a.ses_id = b.ses_id
			LEFT JOIN {{traff_sessions}} c on a.ses_id = c.ses_id
			WHERE a.date_cr BETWEEN STR_TO_DATE(:from, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:to, '%Y-%m-%d %H:%i:%s') 
			GROUP BY dat, trackid 
			ORDER BY dat ASC, uniq DESC";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":date_format", $date_format, PDO::PARAM_STR);
		$command->bindParam(":from", $from, PDO::PARAM_STR);
		$command->bindParam(":to", $to, PDO::PARAM_STR);
		$dataReader=$command->query();

		$data = $dataReader->readAll();

		$sql = 'INSERT INTO {{stat_trackids}} 
            (dat, trackid, uniq, hit, bill)
            VALUES (:dat, :trackid, :uniq, :hit, :bill)
            ON DUPLICATE KEY UPDATE 
            uniq = :uniq, hit = :hit, bill = :bill';

		$row_count = 0;
		foreach($data as $item):

			$dat = str_replace(' ', '-', $item['dat']);
	        $command=$connection->createCommand($sql);
	        $command->bindParam(":uniq", $item['uniq'], PDO::PARAM_INT);
	        $command->bindParam(":hit", $item['hit'], PDO::PARAM_INT);
	        $command->bindParam(":bill", $item['bill'], PDO::PARAM_INT);
	        $command->bindParam(":trackid", $item['trackid'], PDO::PARAM_INT);
	        $command->bindParam(":dat", $dat, PDO::PARAM_STR);
	        $row_count += $command->execute();

        endforeach;

		return  $row_count;		
	}
}