<?php


class CurrencySwitcherWidget extends CWidget
{
    public function run()
    {
        $currentUrl = Yii::app()->request->originalUrl;
        $links = array();
        foreach (Currencies::getArray() as $code => $symbol){
            $url =  $currentUrl . '?' . Yii::app()->currencies->get_var_name . '=' . $code ;
            $active = Yii::app()->currencies->getValue() == $code ? 1 : 0;
            $links[] = array('url' => $url, 'code' => $code, 'symbol' => $symbol, 'active' => $active, );
        }

        $this->render('currency',array('links'=>$links));
    }
}

