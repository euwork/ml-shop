<?php


class LanguageSwitcherWidget extends CWidget
{
    public function run()
    {
        $currentUrl = ltrim(Yii::app()->request->url, '/');
        $links = array();
        foreach (DMultilangHelper::langHash() as $lang => $data){
            if(Yii::app()->controller->id == 'cart'){
                $url = '/' . ($lang != Yii::app()->config->get('defaultLanguage') ? $lang . '/' . $currentUrl :  $currentUrl ) ;//. $currentUrl;
            } else {
                $url = '/' . ($lang != Yii::app()->config->get('defaultLanguage') ? $lang . '/' : '')  ;//. $currentUrl;
            }
            $active = Yii::app()->language == $lang ? 1 : 0;
            $links[] = array('url' => $url, 'name' => $data['name'], 'lang' => $lang, 'active' => $active, 'country'=>$data['country']);
        }

        // uasort($links, "cmp");
        $this->render('language',array('links'=>$links));
    }
}

function cmp($a, $b)
{

    if ((int)$a['active'] != (int)$b['active']) 
        return ((int)$a['active'] < (int)$b['active']) ? 1 : -1;
    return 0;
}