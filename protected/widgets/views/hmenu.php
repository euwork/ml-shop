<ul id="product">
    <?php foreach($model as $cat): ?>
        <li class="first_li">
            <span class="text_menu" onclick="location.href='<?php echo $cat->url; ?>'"><?php echo $cat->catname;?></span>

            <ul class="box_ul">
                <li<?php if(count($cat->drugs) > 10) { echo ' style="width: 500px;" ';} ?>>

                    <ul  class="ul_first_col" style="float:left; display: inline-block;">
                        <?php 
                            $i = 0; 
                            foreach($cat->drugs as $drug): 
                            $i++;
                            if($i > 10) {echo "</ul><ul style='float:right; display: inline-block' class='ul_other_cols'>";}
                        ?>
                            <li>
                                <span onclick="location.href='<?php echo $drug->url;?>'"><span class="li_hover_top"></span><a title="<?php echo $drug->drugname;?>" href="<?php echo $drug->url;?>"><?php echo $drug->drugname;?></a></span>
                            </li>
                        <?php endforeach;?>
                                
                    </ul>
                </li>
            </ul>
        </li>
    <?php endforeach;?> 
                                   
</ul>