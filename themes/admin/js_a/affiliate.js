// -----------------------------------------------------------------------------
$(document).ready(function() {
    var w = (document.body.clientWidth - parseInt($('#page_header').css('width')) - 10) / 2;

    if (w > 100)
    {
        $('#stl_left').css('width', w);

        $('#stl_left').hover(
            function () {
                // hover
                $('#stl_left').animate({opacity: 1}, 500);
            },
            function () {
                // out
                $('#stl_left').animate({opacity: 0}, 200);
        });
        $('#stl_left').click(function () {
            $('body,html').animate({scrollTop: 0}, 200);
        });
    }
    else
    {
        $('#stl_left').remove();
    }
});
// -----------------------------------------------------------------------------