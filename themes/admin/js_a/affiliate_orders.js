// -----------------------------------------------------------------------------
$(document).ready(function() {
    $('#day_from, #month_from, #year_from, #day_to, #month_to, #year_to').change(function () {
        $(":input[type=radio][name=period]").removeAttr('checked');
        $(":input[type=radio][name=period][value=period]").attr('checked', 'checked');
    });
});
// -----------------------------------------------------------------------------
function filter_form_submit()
{
    $('#filter_form').submit();
}
// -----------------------------------------------------------------------------