// -----------------------------------------------------------------------------
function system_show()
{
    $('#system_msg').show();
}
// -----------------------------------------------------------------------------
function system_hide()
{
    $('#system_msg').hide('slow', system_clear);
}
// -----------------------------------------------------------------------------
function system_clear()
{
    $('#system_msg').removeClass('ok');
    $('#system_msg').removeClass('error');
    $('#system_msg').removeClass('warning');
}
// -----------------------------------------------------------------------------
function system_message(message)
{
    system_clear();
    $('#system_msg').html(message);
    system_show();
    setTimeout(system_hide, 5000);
}
// -----------------------------------------------------------------------------
function system_ok(message)
{
    system_message(message);
    $('#system_msg').addClass('ok');
}
// -----------------------------------------------------------------------------
function system_error(message)
{
    system_message(message);
    $('#system_msg').addClass('error');
}
// -----------------------------------------------------------------------------
function system_warning(message)
{
    system_message(message);
    $('#system_msg').addClass('warning');
}
// -----------------------------------------------------------------------------
function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    
    if(results == null)
    {
        return '';
    }
    else
    {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
// -----------------------------------------------------------------------------
$.extend({ 
    password: function (length, special) {
        
        var iteration = 0;
        var password = "";
        var randomNumber;
        
        if(special == undefined)
        {
            var special = false;
        }
        
        while(iteration < length)
        {
            randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
            
            if(!special)
            {
                if ((randomNumber >=33) && (randomNumber <=47)) {continue;}
                if ((randomNumber >=58) && (randomNumber <=64)) {continue;}
                if ((randomNumber >=91) && (randomNumber <=96)) {continue;}
                if ((randomNumber >=123) && (randomNumber <=126)) {continue;}
            }
            
            iteration++;
            password += String.fromCharCode(randomNumber);
        }
        
        return password;
    }
});
// -----------------------------------------------------------------------------
function captcha_reload()
{
    $('#captcha').attr('src', '/captcha.php?rand=' + $.password(10));
    $('#field_captcha').val('');
    
    return false;
}
// -----------------------------------------------------------------------------
function generate_password(id)
{
    $('#' + id).val($.password(8));
}
// -----------------------------------------------------------------------------
function is_valid_email(email, strict)
{
	if ( !strict ) email = email.replace(/^\s+|\s+$/g, '');
	return (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);
}
// -----------------------------------------------------------------------------
function init_server_timer()
{
    var dateobj = new Date();
    SERVER_DX = SERVER_DATE.getTime() - dateobj.getTime();
    
    setInterval(server_timer, 1000);
}
// -----------------------------------------------------------------------------
function server_timer()
{
    var dateobj = new Date();
    var correct = dateobj.getTime() + SERVER_DX;
    
    dateobj.setTime(correct);
    
    var y = dateobj.getFullYear();
    var m = correct_zero(dateobj.getMonth()+1);
    var d = correct_zero(dateobj.getDate());
    
    $('#server_time_div').html(y + '-' + m + '-' + d + ' ' + dateobj.toLocaleTimeString());
}
// -----------------------------------------------------------------------------
function correct_zero(i)
{
    if (i < 10)
    {
        i = '0' + i;
    }
    return i;
}
// -----------------------------------------------------------------------------