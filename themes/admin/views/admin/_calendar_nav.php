
<?php


  Yii::app()->clientScript->registerScript('list_datas', "
        $('.calendar_nav a').click(function () {
          var period = $(this).attr('id');
	      var curval = $(this).attr('data');
	      $('#traff-filter-form select#'+period).val(curval);
  	      var year = $(this).attr('year');
	      if(year){
		      $('#traff-filter-form select#year1').val(year);
		      $('#traff-filter-form select#year').val(year);
	      }
	      $('#traff-filter-form').submit();
	      return false;
	    });
        ", CClientScript::POS_READY
    );


if($form->period != 'monthly'):


if($form->period == 'week'){

	$year = $form->year1;
	$cur_value = $form->week;
	$first_value = 1;
	$last_value = TraffFilterForm::getCountWeek($year);
	$data = TraffFilterForm::getWeekList();
}

if($form->period == 'month'){
	$cur_value = $form->month;
	$first_value = 1;
	$last_value = 12;
	$data = TraffFilterForm::getMonthList();
	$year = $form->year3;

}
$years = TraffFilterForm::getYearList();

$prev_data = $cur_value - 1;
$next_data = $cur_value + 1;
$prev_year = $year;
$next_year = $year;

if($cur_value == $first_value) {
	$prev_year = $year - 1;
	if(!array_search($prev_year, $years)){
		$prev_year = false;
	}
	$prev_data = $last_value;
	if($form->period == 'week' && $prev_year){
		$prev_data = TraffFilterForm::getCountWeek($prev_year);
	}
}

if($cur_value == $last_value) {
	$next_year = $year + 1;
	if(!array_search($next_year, $years)){
		$next_year = false;
	}
	$next_data = $first_value;
}
?>

<?php

elseif($form->period == 'monthly'):

$data = TraffFilterForm::getYearList();
$cur_value = $form->year2;
$first_value = 2014;
$last_value = date('Y')+1;
$prev_data = $cur_value - 1;
$next_data = $cur_value + 1;
$prev_year = '';
$next_year = '';
if($cur_value == $first_value) {
	$prev_year = false;
}
if($cur_value == $last_value) {
	$next_year = false;

}
endif;

$year = '';

?>

<ul class="calendar_nav">
	<?php if($prev_year !== false): ?>
    <li><a id='<?php echo $form->period; ?>' href="#" data='<?php echo $prev_data;?>' year='<?php echo $prev_year; ?>'>&laquo; <?php echo $data[$prev_data] . ' ' . $prev_year; ?></a>
    </li>
	<?php endif; ?>
    <li class="selected"><?php echo $data[$cur_value] . ' ' .$year; ?></li>
    <?php if($next_year !== false): ?>
    <li><a id='<?php echo $form->period; ?>' href="#" data='<?php echo $next_data;?>' year='<?php echo $next_year; ?>'><?php echo $data[$next_data] . ' ' . $next_year; ?> &raquo;</a>
    </li>
    <?php endif; ?>
</ul>




