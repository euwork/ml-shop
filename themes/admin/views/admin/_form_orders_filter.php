<div class="legend">

<?php 
$current_url = $this->createUrl( $this->getId() . '/' . $this->getAction()->getId() );


$form=$this->beginWidget('CActiveForm', array(
    'id'=>'orders-filter-form',
    'method'=>'get',
    'enableAjaxValidation'=>false,
    'action'=>$current_url,
)); ?>
     
<table class="tiny_table">
    <tr>
<td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'today', 'uncheckValue' => null)); ?>
         Today</span>
</td>
<td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'last7days', 'uncheckValue' => null)); ?>
         Last 7 days</span>
</td>
<td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'thisweek', 'uncheckValue' => null)); ?>
         This week</span>
</td>
<td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'period', 'uncheckValue' => null)); ?>
         Period:</span>
</td>
<td>
<span class="nowrap">
<?php echo $form->dropDownList($model,'day_from', OrdersFilterForm::getDayList(), array('id'=>'day_from', )); ?>
<?php echo $form->dropDownList($model,'month_from', OrdersFilterForm::getMonthList(), array('id'=>'month_from', )); ?>
<?php echo $form->dropDownList($model,'year_from', OrdersFilterForm::getYearList(), array('id'=>'year_from', )); ?>
&ndash;
<?php echo $form->dropDownList($model,'day_to', OrdersFilterForm::getDayList(), array('id'=>'day_to', )); ?>
<?php echo $form->dropDownList($model,'month_to', OrdersFilterForm::getMonthList(), array('id'=>'month_to', )); ?>
<?php echo $form->dropDownList($model,'year_to', OrdersFilterForm::getYearList(), array('id'=>'year_to', )); ?>


</span>
</td>
    </tr>
        <tr>
            <td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'yesterday', 'uncheckValue' => null)); ?>
             Yesterday</span>
            </td>
            <td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'last30days', 'uncheckValue' => null)); ?>
             Last 30 days</span>
            </td>
            <td><span class="nowrap">
<?php echo $form->radioButton($model,'period',array( 'value'=>'thismonth', 'uncheckValue' => null)); ?>
             This month</span>
            </td>
            <td class="ta_r"></td>
            <td class="ta_r">
               <?php //echo $form->checkBox($model,'approved',array(  'value'=>'1',)); ?>
               <!-- Show approved only &nbsp; -->
                <div class="button_blue">
                    <button >Show</button>
                </div>
            </td>
        </tr>
    
    <?php if(in_array($this->getAction()->getId(), array('orders', 'countries', 'products'))) $this->renderPartial('_form_pm_filter', array('model'=>$model, 'form'=>$form)); ?>
    <?php if(false && in_array($this->getAction()->getId(), array('orders', 'countries', 'products'))) $this->renderPartial('_form_orders_filter_extb', array('model'=>$model, 'form'=>$form)); ?>


    </table>
<?php $this->endWidget(); ?>
                    </div>