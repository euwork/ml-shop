<?php
$current_url = $this->createUrl( $this->getId() . '/' . $this->getAction()->getId() );

?>

<div class="legend">
<?php $form=$this->beginWidget('CActiveForm', array( 'id'=>'prices-filter-form', 'method'=>'get', 'enableAjaxValidation'=>false, 'action'=>$current_url,)); ?>

    <table class="tiny_table">
        <tr>


            <td>
                The database of products:
                <?php echo $form->dropDownList($model,'products_label', Products::getProductsBasesList(), array()); ?>
            </td>
            <td>
                Drug:
                <?php echo $form->dropDownList($model,'drug_id', Drugs::getArray(), array('empty'=> '---' )); ?>
            </td>
            <td>
                Lang:
                <?php echo $form->dropDownList($model, 'lang', DMultilangHelper::langList());?>
            </td>
            <td>
                <div class="button_blue">
                    <button >Filter</button>
                </div>
            </td>
        </tr>
    </table>
<?php $this->endWidget(); ?>
</div>