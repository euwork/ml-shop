<div class="legend">
<?php 
$current_url = $this->createUrl( $this->getId() . '/' . $this->getAction()->getId() );

$form=$this->beginWidget('CActiveForm', array( 'id'=>'traff-filter-form', 'method'=>'get', 'enableAjaxValidation'=>false, 'action'=>$current_url,)); 

?>
<table class="tiny_table">
    <tr>
        <td>
            <?php echo $form->radioButton($model,'period',array( 'value'=>'week', 'uncheckValue' => null)); ?> Daily in week</td>
        <td>
            <span class="nowrap">
                <?php echo $form->dropDownList($model,'week', TraffFilterForm::getWeekList(), array('id'=>'week', )); ?>
                <?php echo $form->dropDownList($model,'year1', TraffFilterForm::getYearList(), array('id'=>'year1', )); ?>
            </span>
        </td>
        <td>
            <?php echo $form->radioButton($model,'period',array( 'value'=>'monthly', 'uncheckValue' => null)); ?> Monthly in year</td>
        <td>
            <?php echo $form->dropDownList($model,'year2', TraffFilterForm::getYearList(), array('id'=>'monthly', )); ?>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <?php echo $form->radioButton($model,'period',array( 'value'=>'month', 'uncheckValue' => null)); ?> Daily in month</td>
        <td>
            <span class="nowrap">
                <?php echo $form->dropDownList($model,'month', TraffFilterForm::getMonthList(), array('id'=>'month', )); ?>

                <?php echo $form->dropDownList($model,'year3', TraffFilterForm::getYearList(), array('id'=>'year', )); ?>
            </span>
        </td>
        <td colspan="2">
            <span class="nowrap">

            </span>
        </td>
        <td>
            <div class="button_blue">
                <button >Show</button>
            </div>
        </td>
    </tr>

    <tr>
        <td colspan="4">
        </td>
        <td colspan="1">
                <?php echo $form->checkBox($model,'wire',array(  'value'=>'1',)); ?> Bank
                <?php echo $form->checkBox($model,'visa',array(  'value'=>'1',)); ?> Visa
                <?php echo $form->checkBox($model,'bc',array(  'value'=>'1',)); ?> BlueCard
                <?php echo $form->checkBox($model,'master',array(  'value'=>'1',)); ?> Master
                <?php echo $form->checkBox($model,'amex',array(  'value'=>'1',)); ?> Amex
                <?php echo $form->checkBox($model,'ideal',array(  'value'=>'1',)); ?> iDeal
        </td>
    </tr> 
</table>
<?php $this->endWidget(); ?>
    </div>