<?php 
$this->header = 'Article edit'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Article edit</h2>

<!-- <div class="form"> -->

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'cat-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>









	
	<div style="float:right;" class="button_blue"><button id="save_button_profile">Save changes</button></div>
	
	<div class="clean"></div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'lang'); ?> 
		<br>
		<?php echo $form->dropDownList($model,'lang', DMultilangHelper::langList(), array()); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'title'); ?> 
		<br>
		<?php echo $form->textField($model,'title',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'slug'); ?> 
		<br>/<?php echo Yii::app()->config->get('prefix.article'); ?>/
		<?php echo $form->textField($model,'slug',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 598px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'postname'); ?> 
		<br>
		<?php echo $form->textField($model,'postname',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'author'); ?> 
		<br>
		<?php echo $form->textField($model,'author',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'header'); ?> 
		<br>
		<?php echo $form->textField($model,'header',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'keywords'); ?> 
		<br>
		<?php echo $form->textField($model,'keywords',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'description'); ?> 
		<br>
		<?php echo $form->textArea($model,'description',array('rows'=>5, 'cols'=>120)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'content'); ?> 
		<br>

		<?php if(Yii::app()->config->get('textEditor') == 0) : ?>

		<?php echo $form->textArea($model,'content',array('rows'=>20, 'cols'=>120, 'id'=>'mceEditor')); ?>

		<?php elseif(Yii::app()->config->get('textEditor') == 2): ?>
		<?php 
			$this->widget('application.extensions.tinymce.ETinyMce',
				array(
					'model'=>$model,
					'attribute'=>'content',
					'editorTemplate'=>'full',
					'htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'tinymce'),
					'useSwitch' => false,
					'options' => array(
						'theme_advanced_buttons1' => 'bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,|,fontsizeselect,formatselect,|,link,unlink,anchor,image,|,code,preview,|,forecolor,backcolor,|,removeformat',
						'theme_advanced_buttons2' => '',
						'theme_advanced_buttons3' => '',
						'theme_advanced_buttons4' => '',
						'theme_advanced_toolbar_location' => 'top',
						'theme_advanced_toolbar_align' => 'left',
						'theme_advanced_statusbar_location' => 'bottom',
						'theme_advanced_resizing' => true,
						'mode' =>'specific_textareas',

					)
				)
			); 
		?>
		<?php elseif(Yii::app()->config->get('textEditor') == 1): ?>
		<?php

			Yii::import('ext.imperavi.ImperaviRedactorWidget');
			$this->widget('ImperaviRedactorWidget', array(
				'model' => $model,
				'attribute' => 'content',

				'options' => array(
					'lang' => 'en',
					'toolbar' => true,
					'iframe' => false,
					'minHeight' => 300,
					'maxHeight' => 800,
					'replaceDivs'=>false,
					// 'css' => 'wym.css',
				),
			));
		?>
		<?php endif; ?>

	</div>

	<div class="legend">
		<span class="macros-head">Macroses: </span> 
		<br> 
		<span class="macros-name">{domain}</span> <span class="macros-fields">in Title, Header, Description, Content</span>
		<br> 
	</div>





 <br><br>   
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>
<!-- </div> -->

<script type="text/javascript">

    $('#tabs').tabs();

</script>   