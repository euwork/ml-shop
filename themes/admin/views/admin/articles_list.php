<?php 
$this->header = 'Articles'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>


<?php echo CHtml::link('Add new', array('articlecreate')); ?>

                    <div>Total: <?php echo  $pages->itemCount; ?></div>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Articles:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>
                    <table>
                        <tr>
                            <th width="70">ID</th>
                            <th>Slug</th>
                            <th>Title</th>
                            <th width="140">&nbsp;</th>
                        </tr>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $item):
                            
                             $i = $i ? 0 : 1;
                             


                        ?>
                        <tr class="row<?php echo $i;?>">
                            <td class="ta_c"><?php echo $item->art_id; ?>
                            </td>
                            <td>
                                <?php echo $item->slug; ?>
                            </td>
                            <td>
                                <?php echo CHtml::link($item->title, $item->url ) ;?>
                            </td>
                            <td class="ta_c">
                                <?php echo CHtml::link('', $item->url, array('target'=>'_blank', 'class'=>'icon i_open', 'title'=>'open in new tab') ) ;?>
                                <?php echo CHtml::link('', array('articleedit', 'id'=>$item->art_id), array('class'=>'icon i_edit_super', 'title'=>'edit')); ?>
                                <?php echo CHtml::button('Delete',
                                array(
                                    'submit'=>array('articledelete', 'id'=>$item->art_id),
                                    'confirm' => 'Are you sure?',
                                )
                                ); ?>
                            </td>

                            
                        </tr>
                       <?php endforeach; ?>
                        
                    </table>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Articles:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>

                    <br />
               