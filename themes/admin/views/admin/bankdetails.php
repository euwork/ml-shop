<?php 
$this->header = 'BankDetails'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;



?>

<div id="tabs">
<ul>
<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

	<li><a href="#tabs-<?php echo $lang_id; ?>" class="entry-link" title="<?php echo $data['country']; ?>"><?php echo strtoupper($lang_id); ?></a></li>
<?php endforeach; ?>
</ul>

<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

<?php $suffix = $data['suffix']; ?>

<?php Yii::app()->language = $lang_id; ?>

<div id="tabs-<?php echo $lang_id; ?>">


	<div class="labeled" >
		<p style="line-hight:22px;">
			<b>Language:</b> <?php echo $data['name']; ?>
			<img style="vertical-align: middle;" src="<?php echo Yii::app()->theme->baseUrl;?>/lang-ico/<?php echo $lang_id; ?>.png" title="<?php echo $data['country']; ?>">
		</p>
	</div>
	<div class="clean"></div>


	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'Empfänger');?></span>
		<?php echo Config::getWireData('wire.empf'); ?>
	</div>

	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'IBAN');?></span>
		<?php echo Config::getWireData('wire.iban'); ?>
	</div>

	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'BIC');?></span>
		<?php echo Config::getWireData('wire.bic'); ?>
	</div>

<!-- 	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'Zielland');?></span>
		<?php echo CheckoutForm::getNameByCode(Config::getWireData('wire.country')); ?>
	</div> -->

	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'Bank');?></span>
		<?php echo CheckoutForm::getNameByCode(Config::getWireData('wire.name')); ?>
	</div>

	<div class="labeled">
		<span style="width: 150px; display: inline-block;"><?php echo Yii::t('thankyou', 'Adresse');?></span>
		<?php echo CheckoutForm::getNameByCode(Config::getWireData('wire.adress')); ?>
	</div>

</div>

<?php endforeach; ?>


</div>	
<br>
<?php Yii::app()->language = Yii::app()->config->get('defaultLanguage'); ?>

<div class="button_a">
    <button onclick="window.location='<?php echo Yii::app()->createUrl('/request/updatebankdetails?return=1') ?>'">Update</button>
</div>
<div class="labeled">
<br>
	<b>Last update:</b> <?php echo Yii::app()->config->get('wire.date_up'); ?>
</div>

<script type="text/javascript">

    $('#tabs').tabs();

</script>   