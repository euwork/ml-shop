<?php 
$this->header = 'Shop settings'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Options</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'profile-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>

<div class="label">Select count of steps:</div>
<div class="labeled">
    <?php echo $form->dropDownList($model,'order_form_mode', Profile::getOrderFormModeList(), array('style'=>'width:103px;')); ?>
</div>

<div class="label">Select sort order of products:</div>
<div class="labeled">
    <?php echo $form->dropDownList($model,'products_order', Profile::getProductsOrderList(), array('style'=>'width:103px;')); ?>
</div>

<br>

<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>
