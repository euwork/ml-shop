<?php 
$this->header = 'Category edit'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>

<h2>Category edit</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'cat-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>

<div id="tabs">
<ul>
<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

	<li><a href="#tabs-<?php echo $lang_id; ?>" class="entry-link" title="<?php echo $data['country']; ?>"><?php echo strtoupper($lang_id); ?></a></li>
<?php endforeach; ?>
</ul>

<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

<?php $suffix = $data['suffix']; ?>

<div id="tabs-<?php echo $lang_id; ?>">

	<div style="float:right;" class="button_blue"><button id="save_button_profile">Save changes</button></div>
	<div class="labeled" >
		<p style="line-hight:22px;">
			<b>Language:</b> <?php echo $data['name']; ?>
			<img style="vertical-align: middle;" src="<?php echo Yii::app()->theme->baseUrl;?>/lang-ico/<?php echo $lang_id; ?>.png" title="<?php echo $data['country']; ?>">
		</p>
	</div>
	<div class="clean"></div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'catname'); ?> 
		<br>
		<?php echo $form->textField($model,'catname'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'title'); ?> 
		<br>
		<?php echo $form->textField($model,'title'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'slug'); ?> 
		<?php $prefix = Yii::app()->config->get('prefix.cat'); ?>
		<br><?php echo ($prefix == '') ? '' : '/' . $prefix;  ?>/
		<?php echo $form->textField($model,'slug'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 575px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'header'); ?> 
		<br>
		<?php echo $form->textField($model,'header'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'keywords'); ?> 
		<br>
		<?php echo $form->textField($model,'keywords'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'description'); ?> 
		<br>
		<?php echo $form->textArea($model,'description'. $suffix,array('rows'=>5, 'cols'=>120)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'content'); ?> 
		<br>

		<?php if(Yii::app()->config->get('textEditor') == 0) : ?>

		<?php echo $form->textArea($model,'content'. $suffix,array('rows'=>20, 'cols'=>120, 'id'=>'mceEditor')); ?>

		<?php elseif(Yii::app()->config->get('textEditor') == 2): ?>
		<?php 
			$this->widget('application.extensions.tinymce.ETinyMce',
				array(
					'model'=>$model,
					'attribute'=>'content'. $suffix,
					'editorTemplate'=>'full',
					'htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'tinymce'),
					'useSwitch' => false,
					'options' => array(
						'theme_advanced_buttons1' => 'bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,|,fontsizeselect,formatselect,|,link,unlink,anchor,image,|,code,preview,|,forecolor,backcolor,|,removeformat',
						'theme_advanced_buttons2' => '',
						'theme_advanced_buttons3' => '',
						'theme_advanced_buttons4' => '',
						'theme_advanced_toolbar_location' => 'top',
						'theme_advanced_toolbar_align' => 'left',
						'theme_advanced_statusbar_location' => 'bottom',
						'theme_advanced_resizing' => true,
						'mode' =>'specific_textareas',

					)
				)
			); 
		?>
		<?php elseif(Yii::app()->config->get('textEditor') == 1): ?>
		<?php

			Yii::import('ext.imperavi.ImperaviRedactorWidget');
			$this->widget('ImperaviRedactorWidget', array(
				'model' => $model,
				'attribute' => 'content'. $suffix,

				'options' => array(
					'lang' => 'en',
					'toolbar' => true,
					'iframe' => false,
					'minHeight' => 300,
					'maxHeight' => 800,
					'replaceDivs'=>false,
					// 'css' => 'wym.css',
				),
			));
		?>
		<?php endif; ?>

	</div>
	<div class="legend">
		<span class="macros-head">Macroses: </span> 
		<br> 
		<span class="macros-name">{domain}</span> <span class="macros-fields">in Title, Header, Description, Content</span>
		<br> 
		<span class="macros-name">{products}</span> <span class="macros-fields">in Content</span>
		<br> 
	</div>

</div>

<?php endforeach; ?>
</div>	

 <br><br>   
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>

<script type="text/javascript">

    $('#tabs').tabs();

</script>   