<?php 
$this->header = 'Categories order'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

// $current_url = $this->createUrl( $this->getId() . '/' . $this->getAction()->getId() );
$current_url = '/' . $this->getId() . '/' . $this->getAction()->getId() ;

?>



                    <table id="sortable">
                        <thead>
                        <tr>
                            <th width="70">ID</th>
                            <th>Category Name</th>
                            <th>Products</th>
                            <th width="100">Order</th>
                            <th width="100">Active</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $item):
                            
                             $i = $i ? 0 : 1;
                             


                        ?>
                        <tr class="row<?php echo $i;?>" id="row<?php echo $item->cat_id; ?>">
                            <td class="ta_c"><?php echo $item->cat_id; ?>
                            </td>
                            <td>
                                <?php //echo $item->catname; ?>
                                <?php echo CHtml::link($item->catname, array('drugsorder', 'cat_id'=>$item->cat_id)); ?>
                            </td>
                            <td class="ta_c">
                                <?php echo count($item->drugs); ?>
                            </td>
                            <td class="ta_c">
                                <input type="text" name="order[<?php echo $item->cat_id;?>]" id="order_row<?php echo $item->cat_id;?>" value="<?php echo $item->position;?>" class="order" cid="<?php echo $item->cat_id;?>"  />
                            </td>
                            <td class="ta_c">
                                <input type="checkbox" name="active[<?php echo $item->cat_id;?>]" value="1" cid="<?php echo $item->cat_id;?>" <?php echo $item->active ? 'checked="checked"' : '';?> />
                            </td>

                            
                        </tr>
                       <?php endforeach; ?>
                        </tbody>
                    </table>

                    <br />

                    <div class="button_blue"><button onclick="category_save();">Save changes</button></div>


<script type="text/javascript">

    $('#sortable tbody tr td').css('cursor', 'move');

    $('#sortable tbody').sortable({
        cursor: 'move',
        items: 'tr',
        helper: function(event, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: handle_resort
    }).disableSelection();
    
    function handle_resort(event, ui)
    {
        var i = 1;
        $('#sortable tbody tr').each(function(i){
            var row = $(this).get()[0];
            $('#order_' + row.id).val(i+1);
            i++;
        });
    }

    function category_save()
    {
        var request = { ajax: 1, action: 'categories', method: 'save', order: [], active: [] }; 

        $(":input[type=text][name*=order]").each(function() {
            request.order[parseInt($(this).attr('cid'))] = $(this).val();
        });


        $(":input[type=checkbox][name*=active]").each(function() {
            request.active[parseInt($(this).attr('cid'))] = ($(this).is(':checked') ? 1 : 0);
        });
        
        $.post('<?php echo $current_url; ?>', request, function(data) {
            if (data.error) 
            {
                if (data.error.message)
                {
                    system_error(data.error.message);
                }
            }
            if (data.result)
            {
                system_ok('Information saved');
            }
        }, 'json');
        
        return false;
    }
</script>               