<?php 
$this->header = 'General setting'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>



<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',

	'enableAjaxValidation'=>false,
)); ?>

<?php $active_tab = Yii::app()->request->getQuery('active_tab'); ?>
<input type='hidden' name='active_tab' value='<?php echo $active_tab; ?>'>

<?php
$tabs = array(
			'Theme'=> array('defaultLanguage',  'home.show_banner', 'theme',   ),
			'URLs'=> array('use_https', 'trackid_varname', 'prefix.cat', 'prefix.drug',  'prefix.testimonials', 'prefix.page', 'prefix.article',),
			// 'Billing' => array('billing.page', ),
			//'Bank' => array('wire.empf', 'wire.country', 'wire.iban', 'wire.bic'),
			'Reviews'=> array('use_reviews', 'reviews.count.home', 'reviews.count.drug', 'reviews.count.testimonials'),
			'Counter'=> array( 'external_counters'),
			'ApiParams'=> array( 'cart.send_in_thankyou', 'api_domain', 'api_key', 'affid', 'ipill_id', ),
			'Other'=>array(),

		);

foreach($tabs as $tab_name=>$tab_value)
{
	foreach($tab_value as $param => $value)
	{
		unset($tabs[$tab_name][$param]);
		$tabs[$tab_name][$value] = '';

	}
}


?>

	<?php foreach($model as $item):

	if(in_array($item->param, 
		array(
			'price.increase', 
			'price.sort_order', 
			'price.increase_old', 
			'cart.steps', 
			#'prefix.article',
			'billing.ext_client_id', 'billing.ext_campaign_id', 'billing.ext_api_url', 'billing.ext_private_key',
			'wire.empf', 'wire.country', 'wire.iban', 'wire.bic', 'wire.date_up', 'wire.name', 'wire.adress',
			'wire.empf2', 'wire.country2', 'wire.iban2', 'wire.bic2', 'wire.date_up2', 'wire.name2', 'wire.adress2', 'wire.usefor2',
			'currency_rates.date_up',
			'billing.page',
			'ship_active_regular_de', 'ship_active_express_de', 'ship_price_regular_de', 'ship_price_express_de',
			'ship_active_regular', 'ship_active_express', 'ship_active_normal', 'ship_price_regular', 'ship_price_express', 'ship_price_normal',
			'ship_price_regular_de_less60',
			))) 

		continue;




	$out = '
		<div class="labeled">
			<span style="width: 150px; display: inline-block;">' . $item->label .'</span>';
			if ($item->type == 'checkbox') : 

				$out .= $form->checkBox($item, 'value', array('name'=>'Config['.$item->param.']')); 

			elseif ($item->type == 'string') : 

				$out .= $form->textField($item, 'value',array('size'=>30,'maxlength'=>256, 'name'=>'Config['.$item->param.']'));


			elseif ($item->type == 'select') :

				$html_params = array();

				if ($item->param == 'textEditor') : 

					//$select_list_params = array(0=>'None', 1=>'Imperavi', 2=>'ETinyMce');
					$select_list_params = array(0=>'None', 1=>'Imperavi');
				
				elseif ($item->param == 'theme') : 

					$select_list_params = Theme::getList();
					// $select_list_params = array('d2'=>'d2',);
					$html_params = array('id'=>'field_theme');
				
				elseif ($item->param == 'billing.page') : 

					$select_list_params = array(/*'external'=>'external',*/ 'internal'=>'internal',);

				elseif ($item->param == 'defaultLanguage') : 
					$select_list_params = Languages::getArray();

				elseif ($item->param == 'cart.steps') :
					$select_list_params = Config::getOrderFormModeList();
				endif;

				$out .= $form->dropDownList($item,'value', $select_list_params, array_merge(array('name'=>'Config['.$item->param.']'), $html_params) );

			elseif ($item->type == 'area') :

				$out .= $form->textArea($item, 'value',array('rows'=>10, 'cols'=>110, 'name'=>'Config['.$item->param.']'));

			endif;
		$out .='</div>';


		foreach($tabs as $tab_name=>$tab_value)
		{
			foreach($tab_value as $param => $value)
			{
				if($item->param == $param){
					$tabs[$tab_name][$param] = $out;
					continue 3;
				}
			}
		}

		$tabs['Other'][$item->param] = $out;

		// ищем имя параметра в табах, добавляем html в таб


	endforeach; 

	//print_r($tabs);
	?>




<div id="tabs">
<ul>
<?php foreach($tabs as $tab_name=>$tab_value) : ?>

	<li><a href="#tabs-<?php echo $tab_name; ?>" class="entry-link" data-tab-name="<?php echo $tab_name; ?>"><?php echo $tab_name; ?></a></li>
<?php endforeach; ?>
</ul>

<?php 
foreach($tabs as $tab_name=>$tab_value) : ?>

<div id="tabs-<?php echo $tab_name; ?>">
	<h2><?php echo $tab_name; ?></h2>
	<?php if($tab_name == 'Theme'): ?>
	<div style="float:right;">
		<img src="<?php echo Yii::app()->baseUrl; ?>/themes/<?php echo Yii::app()->config->get('theme');?>/screenshort.png" width="300" id="template_screenshort" />
	</div>

	<?php endif; ?>
	
	<?php	foreach($tab_value as $param => $value)
		{
			echo $value;
		}
		?>
	
		<?php if($tab_name == 'Theme'): ?>
			<span style="color:#9e9e9e;font-size:11px;padding: 10px 0;">
			To create your custom theme, copy the contents of the /themes/d2 folder to a new folder with the name of your theme. The new theme will automatically be added to the dropdown list below. Please do not modify the original theme! 
			</span>
		<?php endif; ?>
		<?php if($tab_name == 'URLs'): ?>
			<span style="display:block; color:#9e9e9e;font-size:11px;padding: 10px 0;">
			The prefix can be removed for all types of pages, except "testimonials".
			</span>
		<?php endif; ?>
	
	
	<br>
	<div class="clean"></div>
	<div class="button_blue"><button>Save changes</button></div>
</div>

<?php endforeach; ?>


<?php $this->endWidget(); ?>

</div><!-- form -->


<div class="clean"></div>


<script type="text/javascript">

    $('#tabs').tabs();
    <?php //$active_tab = Yii::app()->request->getQuery('active_tab'); ?>
    <?php if($active_tab) : ?>
        $('a[href="#tabs-<?php echo $active_tab; ?>"]').click();
    <?php endif; ?>

    $('a.entry-link').on( "click", function() {
		var active_tab = $(this).attr('data-tab-name');
		$('input[type=hidden][name*=active_tab]').attr('value', active_tab);
	});

	$('#field_theme').change(function() {
        $('#template_screenshort').attr('src', '<?php echo Yii::app()->baseUrl; ?>/themes/' + $(this).val() + '/screenshort.png');
    });

</script>   