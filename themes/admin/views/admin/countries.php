<?php 
$this->header = 'Statistics by countries'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


// print_r($data_traff);
// print_r($data_order);
?>
<?php $this->renderPartial('_form_orders_filter', array('model'=>$form)); ?>
<?php



// список всех стран
$countries = array();
foreach(array_merge($data_traff, $data_order) as $item)
{
    if(!in_array($item['country'], $countries)){
        $countries[] = $item['country'];
    }
}

// print_r($countries);

// Yii::app()->end();

?>


    <br />

        <?php 
       
        $total_uniq = 0;
        foreach($data_traff as $row)
        {
            $total_uniq += $row['uniq'];
        } 


        

        ?>


    <table class="nowrap">
        <tr>
            <th width="10">&nbsp;</th>
            <th width="10">&nbsp;</th>
            <th>Country</th>
            <th>%</th>
            <th>Unique</th>
            <th>Clicks</th>
            <th>Billing</th>
            <th>Ratio</th>
            <th>CPU/CPT*</th>
            <th>Sales</th>
            <th>Amount</th>
            <th>Profit</th>
        </tr>
        <?php
        $num = 0;
        $i=0;
        foreach($countries as $item_country): 
            

            $uniq = 0;
            $hit = 0;
            $bill = 0;

            

            $accept = 0;
            $reorder = 0;
            $wait = 0;
            $decline = 0;
            

            $ratio = 0;

            $amount = 0;
            $profit = 0;

            $cpu = 0;
            $cpt = 0;

            $num++;
            $i = $i ? 0 : 1;
            
            $data_traff_row = search_by_country($data_traff, $item_country);
            $data_order_row = search_by_country($data_order, $item_country);




            $country = $item_country;


            $uniq_percent = 0;

            if($data_traff_row){
                $uniq = $data_traff_row['uniq'];
                $hit = $data_traff_row['hit'];
                $bill = $data_traff_row['bill'];
                $uniq_percent = $uniq / $total_uniq * 100;
                
            }

            $count_orders = 0;

            if($data_order_row){

                $accept = $data_order_row['accept'];
                $reorder = $data_order_row['reorder'];
                $wait = $data_order_row['wait'];
                $decline = $data_order_row['decline'];  

                $count_orders = $accept + $wait + $decline ;

                $amount = $data_order_row['amount'];   
                $profit = $data_order_row['profit'];
                
            }

            if($count_orders > 0){
                $ratio =  round($uniq / $count_orders);
            }

            if($uniq > 0){
                $cpu = $profit / $uniq;
                $cpt = $cpu * 1000;
            }


            


        ?>
            <tr class="row<?php echo $i; ?>">
                <td class="ta_c"><?php echo $num; ?></td>
                <td class="ta_c"><?php echo Country::getIconByCode($country); ?></td>
                <td class="ta_c"><?php echo Country::getNameByCode($country); ?></td>
                <td class="ta_c"><?php echo Format::percent($uniq_percent); ?></td>
                <td class="ta_c"><?php echo $uniq; ?></td>
                <td class="ta_c"><?php echo $hit; ?></td>
                <td class="ta_c"><?php echo $bill; ?></td>
                <td class="ta_c">1:<?php echo $ratio; ?></td>
                <td class="ta_c">€<?php echo number_format($cpu,2); ?>/€<?php echo number_format($cpt,2); ?></td>
                <td class="ta_c">
                    <span style="color: #109618;"><b><?php echo $accept; ?></b></span> 
                    | <span style="color: #109618;"><?php echo $reorder; ?></span> 
                    | <span style="color: #3366cc;"><?php echo $wait; ?></span> 
                    | <span style="color: #dc3912;"><?php echo $decline; ?></span>
                </td>
                <td class="ta_c">€<?php echo number_format($amount,2); ?></td>
                <td class="ta_c">€<?php echo number_format($profit,2); ?></td>
            </tr>
        <?php endforeach; ?>

    </table>
    <br />  
    <div class="legend">
        <span class="calendar_square" style="background: #109618;"></span> - approved orders &nbsp; 
        <span class="calendar_square" style="background: #109618;"></span> - reorders &nbsp; 
        <span class="calendar_square" style="background: #3366cc;"></span> - waiting for approval &nbsp; 
        <span class="calendar_square" style="background: #dc3912;"></span> - not approved orders &nbsp; 
        <b>*</b> - commission per unique / commission per thousand unique
    </div>
    <br />


<?php

function search_by_country($data, $search)
{
    foreach ($data as $d) 
    {
        if($d['country'] == $search){
            return $d;
        }
    }
    return false;
}