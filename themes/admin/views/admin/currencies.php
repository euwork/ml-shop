<?php 
$this->header = 'Currencies'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

$current_url = '/' . $this->getId() . '/' . $this->getAction()->getId() ;

?>



                    <table id="sortable">
                        <thead>
                        <tr>
                            <th >Code</th>
                            <th width="100">Country</th>
                            <th width="100">Symbol</th>
                            <th width="100">Order</th>
                            <th width="100"><input id="checkAll" class="select-active" type="checkbox" checked="checked" /> Active</th>
                            <th width="100">Default</th>
                            <th width="100">Rate</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php 
                        $i=0;
               
                        foreach($model as $item):
                            
                             $i = $i ? 0 : 1;
                             


                        ?>
                        <tr class="row<?php echo $i;?>" id="row<?php echo $item->cur_id; ?>">
                            
                            <td class="ta_c">
                                <?php echo $item->cur_id; ?>
                               
                            </td>
                            <td class="">
                                <?php echo $item->description; ?>
                            </td>
                            <td class="ta_c">
                                <?php echo $item->symbol; ?>
                           
                            </td>
                            <td class="ta_c">
                                <input type="text" name="order[<?php echo $item->cur_id;?>]" id="order_row<?php echo $item->cur_id;?>" value="<?php echo $item->position;?>" class="order" cid="<?php echo $item->cur_id;?>"  />
                            </td>
                            <td class="ta_c">
                                <input class="select-active" type="checkbox" name="active[<?php echo $item->cur_id;?>]" value="1" cid="<?php echo $item->cur_id;?>" <?php echo $item->active ? 'checked="checked"' : '';?> />
                            </td>
                            <td class="ta_c">
                                <?php echo $item->default; ?>
                            </td>
                            <td class="ta_c">
                                <?php echo $item->rate; ?>
                            </td>
                            
                        </tr>
                       <?php endforeach; ?>
                        </tbody>
                    </table>

                    <br />

                    <div class="button_blue"><button onclick="item_save();">Save changes</button></div>
                    <div class="button_a">
                        <button onclick="window.location='<?php echo Yii::app()->createUrl('/request/updatecurrenciesrate?return=1') ?>'">Update Rate</button>
                    </div>
                    <div class="labeled">
                        <br>
                        <b>Last update:</b> <?php echo Yii::app()->config->get('currency_rates.date_up'); ?>
                    </div>
                    <br>




<script type="text/javascript">

    $('#sortable tbody tr td').css('cursor', 'move');

    $('#sortable tbody').sortable({
        cursor: 'move',
        items: 'tr',
        helper: function(event, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: handle_resort
    }).disableSelection();
    
    function handle_resort(event, ui)
    {
        var i = 1;
        $('#sortable tbody tr').each(function(i){
            var row = $(this).get()[0];
            $('#order_' + row.id).val(i+1);
            i++;
        });
    }

    function item_save()
    {
        var request = { ajax: 1, action: 'languages', method: 'save', order: {}, active: {} }; 

        $(":input[type=text][name*=order]").each(function() {
            request.order[$(this).attr('cid')] = $(this).val();
        });


        $(":input[type=checkbox][name*=active]").each(function() {
            request.active[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
        });
        
        $.post('<?php echo $current_url; ?>', request, function(data) {
            if (data.error) 
            {
                if (data.error.message)
                {
                    system_error(data.error.message);
                }
            }
            if (data.result)
            {
                system_ok('Information saved');
            }
        }, 'json');
        
        return false;
    }
</script>               



<?php

Yii::app()->clientScript->registerScript('checkAll',
'
$("#checkAll").click(function(){
    $(\'input:checkbox.select-active\').not(this).prop(\'checked\', this.checked);
});
', CClientScript::POS_READY);

?>