<?php 
$this->header = 'Dashboard'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;
?>
<ul class="dashboard">
    <li class="item first">
        <div class="inner">
            <div class="title dash_user">
                <h2>Account information</h2>
            </div>
            <div class="content">
                <ul>
                    <li>ID: <b><?php echo Yii::app()->config->get('affid') . ' (' . Yii::app()->config->get('ipill_id') . ')' ; ?></b>
                    </li>
                    <li>Login: <b><?php echo Yii::app()->user->name; ?></b>
                    </li>
                    <li>Commission: <b><?php echo Profile::getComission(); ?>%</b>
                    </li>
                    <li>
                        <a href="/admin/prices">Prices</a>: 
                        
                        <?php 

                        $increase = Config::getPriceIncrease(); 
                        echo 'FR Base [+'.$increase.'%]';

                        echo "<br>\n";
                        
                        $increase_old = Config::getPriceIncreaseOld(); 
                        echo '<s>Old price</s> [+'.$increase_old.'%]';


                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class="item">
        <div class="inner">
            <div class="title dash_statistics">
                <h2>Today statistics</h2>
            </div>
            <div class="content">
                <?php
                    foreach(array('uniq', 'hit', 'amount', 'profit', 'amount_wait', 'profit_wait', 'accept', 'wait', 'decline') as $key)
                    {
                        $data[$key] = isset($data[$key]) ? $data[$key] : 0;
                    }

                    $total_orders = $data['accept'] + $data['wait'] + $data['decline'];
                    
                    if($total_orders > 0){
                        $ratio = round($data['uniq'] / $total_orders);
                    } else {
                        $ratio = 0;
                    }
                        
                ?>
                <ul>
                    <li>Unique / Clicks / Ratio: <b><?php echo $data['uniq'];?> / <?php echo $data['hit'];?> / 1:<?php echo $ratio; ?></b>
                    </li>
                    <li>Sales / Amount:
                        <b><?php echo number_format($data['accept'],0);?> / €<?php echo number_format($data['amount'],2);?></b>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class="item">
        <div class="inner">
            <div class="title dash_money">
                <h2>Financial information</h2>
            </div>
            <div class="content">
                <ul>
                    <li>Balance: <b>€<?php echo number_format($data['balance'],2);?></b>
                    </li>
                    <!-- <li>BalanceExt: <b>€<?php //echo number_format($data['balance_ext'],2);?></b></li> -->
                    
                    <li>Total paid: <b>€<?php echo number_format($data['total_paid'],2);?></b> &nbsp; <small><a href="/admin/payments">History</a></small>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    
   
   
</ul>



<div class="clean"></div>

<div class="labeled" style="margin: 10px 20px 15px 10px;border: 1px solid #D9E0E7;">
    <span style="width:100px;display: inline-block;text-align: center;">Active languages:</span>
    <span style="color:green;font-weight: bold;">
    <?php

    foreach(Languages::getArrayFlip() as $langname=>$langcode)
    {
        echo strtoupper($langcode). ' ';
    }

    ?>
    </span>
</div>