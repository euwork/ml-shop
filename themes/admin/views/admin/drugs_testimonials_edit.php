<?php 
$this->header = 'Drugs testimonials edit'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>

<h2>Drug edit</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'drug-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>

<div id="tabs">
<ul>
<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

	<li><a href="#tabs-<?php echo $lang_id; ?>" class="entry-link" title="<?php echo $data['country']; ?>"><?php echo strtoupper($lang_id); ?></a></li>
<?php endforeach; ?>
</ul>

<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

<?php $suffix = $data['suffix']; ?>

<div id="tabs-<?php echo $lang_id; ?>">

	<div class="labeled" >
		<p style="line-hight:22px;">
			<b>Language:</b> <?php echo $data['name']; ?>
			<img style="vertical-align: middle;" src="<?php echo Yii::app()->theme->baseUrl;?>/lang-ico/<?php echo $lang_id; ?>.png" title="<?php echo $data['country']; ?>">
		</p>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'title'); ?> 
		<br>
		<?php echo $form->textField($model,'title'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'slug'); ?> 
		<br>/<?php echo Yii::app()->config->get('prefix.testimonials'); ?>/
		<?php echo $form->textField($model,'slug'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 598px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'header'); ?> 
		<br>
		<?php echo $form->textField($model,'header'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'subheader'); ?> 
		<br>
		<?php echo $form->textField($model,'subheader'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'keywords'); ?> 
		<br>
		<?php echo $form->textField($model,'keywords'. $suffix,array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'description'); ?> 
		<br>
		<?php echo $form->textArea($model,'description'. $suffix,array('rows'=>5, 'cols'=>120)); ?>
	</div>

	<div class="labeled">
		<?php echo $form->labelEx($model,'drug_ancor'); ?> 
		<br>
		<?php echo $form->textArea($model,'drug_ancor'. $suffix,array('rows'=>5, 'cols'=>120)); ?>
	</div>


</div>

<?php endforeach; ?>
</div>	

 <br><br>   
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>



<script type="text/javascript">

    $('#tabs').tabs();
    <?php $active_tab = Yii::app()->request->getQuery('active_tab'); ?>
    <?php if($active_tab) : ?>
        $('a[href="#tabs-<?php echo $active_tab; ?>"]').click();
    <?php endif; ?>
    
</script>   