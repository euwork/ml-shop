<?php 
$this->header = 'Pages with testimonials'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>




                    <div>Total: <?php echo  $pages->itemCount; ?></div>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Pages:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>
                    <table>
                        <tr>
                            <th width="70">ID</th>
                            <th>Reviews</th>
                            <th width="80">&nbsp;</th>
                          
                        </tr>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $item):
                            
                             $i = $i ? 0 : 1;
                             


                        ?>
                        <tr class="row<?php echo $i;?>">
                            <td class="ta_c"><?php echo $item->drug_id; ?>
                            </td>
                            <td>
                                <?php echo CHtml::link($item->drug->drugname . ' ('. count($item->drug->reviews_all) . ')' . Format::count('sup', Reviews::getCountNewByDrug($item->drug_id)), array('reviewslist', 'id'=>$item->drug_id) ) ;?> 
                            </td>
                           
                            <td class="ta_c">
                                <?php echo CHtml::link('', $item->url, array('target'=>'_blank', 'class'=>'icon i_open', 'title'=>'open in new tab') ) ;?>
                                <?php echo CHtml::link('', array('drugstestimonialsedit', 'id'=>$item->drug_id), array('class'=>'icon i_edit_super', 'title'=>'edit')); ?>
                            </td>

                            
                        </tr>
                       <?php endforeach; ?>
                        
                    </table>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Pages:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>

                    <br />
               