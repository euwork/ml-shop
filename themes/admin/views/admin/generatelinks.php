<h2>Generate settings</h2>
<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'orders-filter-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
    // 'action'=>$current_url,
)); ?>


<div class="legend">
	<table class="tiny_table">
		<tr>
			<td>
				 Language: <?php echo $form->dropDownList($model, 'language', Languages::getArray(), array('class'=>'text'));?>
			</td>
			
			<td>
				Format: <?php echo $form->dropDownList($model,'format', GenerateLinkForm::getFormatList(), array( )); ?>
				Ancors: <?php echo $form->dropDownList($model,'ancors', GenerateLinkForm::getAncorsList(), array( )); ?>
			</td>
			<td width="100"></td>
			<td>
				<div class="button_blue"><button id="generate_button" >Generate</button></div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo $form->checkBox($model,'index',array(  'value'=>'1',)); ?> Index
				<?php echo $form->checkBox($model,'drugs',array(  'value'=>'1',)); ?> Drugs
				<?php echo $form->checkBox($model,'categories',array(  'value'=>'1',)); ?> Categories
				<?php echo $form->checkBox($model,'pages',array(  'value'=>'1',)); ?> Pages
				<?php echo $form->checkBox($model,'articles',array(  'value'=>'1',)); ?> Articles
				<?php echo $form->checkBox($model,'testimonials',array(  'value'=>'1',)); ?> Testimonials
			</td>
		</tr>

</table>
</div>

<?php $this->endWidget(); ?>

<h2>Generate result</h2>
<textarea style="width: 770px; height: 400px;">
	
<?php

foreach($links as $url=>$text)
{
	if($model->format == GenerateLinkForm::FORMAT_TEXT){
		echo $url . '|' . $text . "\n";
	}
	elseif($model->format == GenerateLinkForm::FORMAT_HTML){
		echo '<a href="'. $url . '">' . $text . "</a>\n";
	}
}
?>

</textarea>