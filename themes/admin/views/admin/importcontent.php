
<?php

$this->header = 'Content import/update'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

if($this->installmode){
	$this->header = 'Installation. Choose languages. Step 2 of 3 ';
}


?>



<?php 
if (count($errors))
{
    $count = 1;
?>
        <p class="error" style="padding: 7px 10px; margin: 20px 20px;">
<?php
    foreach ($errors as $e)
    {
        echo $count . '. ' . print_r($e) . '<br />';
        $count++;
    }
?>
        </p>
<?php } ?>







<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'import_form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>




	<div class="labeled" style="margin-bottom: 20px;">
		<span style="width:250px;display: inline-block;text-align: center;">Default Language</span> <?php echo $form->dropDownList($model, 'defaultLang', Languages::getArrayAll(), array('class'=>'text'));?>
	</div>

	<?php if(!$this->installmode): ?>
		<div class="labeled" style="margin-bottom: 5px;">
			 <?php echo $form->checkBox($model, 'textcontent',  array('class'=>'text'));?>
			 <span style="display: inline-block;text-align: center;">Update text content (Drugs, Categories, Pages)</span>
		</div>
		<div class="labeled" style="margin-bottom: 5px;">
			 <?php echo $form->checkBox($model, 'products',  array('class'=>'text'));?>
			 <span style="display: inline-block;text-align: center;">Update products and prices</span>
		</div>
		<div class="labeled" style="margin-bottom: 15px;">
			 <?php echo $form->checkBox($model, 'templateimages',  array('class'=>'text'));?>
			 <span style="display: inline-block;text-align: center;">Update template images (banners and contacts)</span>
		</div>
	<?php endif; ?>

	<div class="labeled" style="margin-bottom: 15px;">
		 <?php echo $form->checkBox($model, 'dontreplace',  array('class'=>'text'));?>
		 <span style="display: inline-block;text-align: center;">Don't overwrite existing entries</span>
	</div>
	
	<div style="margin:5px 5px 25px 0; border: 1px solid #cecece;padding:15px 0;">
		<div class="labeled" style="margin-bottom: 10px;color:#45688e;">
			<input type="checkbox" id="checkAll" class="text" checked="checked"> Select All
		</div>
		<div class="languages">
			<?php foreach($languages as $item): ?>
			<div class="labeled">
				<?php echo $form->checkBox($model,'languages['.$item->lang_id.']', array('checked'=>$item->show, 'class'=>'text') ); ?>

				<?php echo $item->name . ' [' . $item->country . ']'; ?>
				<?php echo $item->imported ? '<span style="color:green;">Imported</span>' : '' ?>
			</div>
			<?php endforeach;?>
		</div>
	</div>







	<div class="button_blue button_wide">
        <button id="login_button" >Import / Update</button>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->



<?php

Yii::app()->clientScript->registerScript('checkAll',
'
$("#checkAll").click(function(){
    $(\'.languages input:checkbox\').not(this).prop(\'checked\', this.checked);
});
', CClientScript::POS_READY);

?>



