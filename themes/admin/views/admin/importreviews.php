
<?php

$this->header = 'Content reviews import'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

if($this->installmode){
	$this->header = 'Installation. Import Reviews. Step 3 of 3 ';
}

?>
<?php 
if (count($errors))
{
    $count = 1;
?>
        <p class="error" style="padding: 7px 10px; margin: 20px 20px;">
<?php
    foreach ($errors as $e)
    {
        echo $count . '. ' . print_r($e) . '<br />';
        $count++;
    }
?>
        </p>
<?php } ?>




<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'import_form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>


	<div class="labeled" style="margin-bottom: 5px;">
		 <?php echo $form->checkBox($model, 'use_reviews',  array('class'=>'text'));?>
		 <span style="display: inline-block;text-align: center;">Reviews enabled</span>
	</div>


	<div style="margin:5px 5px 25px 0; border: 1px solid #cecece;padding:15px 0;">
		<div class="labeled" style="margin-bottom: 10px;color:#45688e;">
			<input type="checkbox" id="checkAll" class="text" > Select All
		</div>
		<div class="languages">
			<?php foreach(Languages::getArrayFlip() as $langname=>$langcode): ?>
			<div class="labeled">
				<?php $count = Reviews::getCountByLang($langcode); ?>
				<?php echo $form->checkBox($model,'languages['.$langcode.']', array('checked'=> $count == 0 ? 1: 0 , 'class'=>'text') ); ?>

				<?php echo $langname; ?>
				<?php echo '<span style="color:green;">['.$count.']</span>'; ?>
			</div>
			<?php endforeach;?>
		</div>
		<span style="color:#9e9e9e;font-size:11px;padding: 10px 0 10px 110px;margin-top: 10px;display: inline-block;">For the selected languages, existing comments (if any) will be deleted and new ones loaded.  </span>
	</div>


	<div class="labeled" style="margin-bottom: 5px;">
		 <?php echo $form->checkBox($model, 'smart_random',  array('class'=>'text'));?>
		 <span style="display: inline-block;text-align: center;">Smart random <a title="The number of reviews depending on the popularity of the drug.">[?]</a></span>
	</div>

	<div class="labeled" style="margin-bottom: 15px;">
		 <span style="width:250px;display: inline-block;text-align: center;">Fake ratings <a title="The drug rating will display more reviews than it actually is.">[?]</a></span>
		 <?php echo $form->dropDownList($model, 'fake_ratings', ReviewsImportForm::fakeRatings(),  array('class'=>'text'));?>
	</div>

	<div class="labeled">
		 <span style="width:250px;display: inline-block;text-align: center;">Reviews count (real)</span> 
		 Min: <?php echo $form->textField($model,'min', array('style'=>'width:50px;', 'placeholder'=>'Min') ); ?>   
		 Max: <?php echo $form->textField($model,'max', array('style'=>'width:50px;', 'placeholder'=>'Max') ); ?>
	</div>

	<div class="labeled">
		 <span style="width:250px;display: inline-block;text-align: center;">Votes count</span> 
		 Min: <?php echo $form->textField($model,'votes_min', array('style'=>'width:50px;', 'placeholder'=>'Min') ); ?>   
		 Max: <?php echo $form->textField($model,'votes_max', array('style'=>'width:50px;', 'placeholder'=>'Max') ); ?>
	</div>
	<div class="labeled" style="margin-bottom: 15px;">
		 <span style="width:250px;display: inline-block;text-align: center;">Votes Yes, %</span> 
		 Min: <?php echo $form->textField($model,'votes_yes_prc_min', array('style'=>'width:50px;', 'placeholder'=>'Min') ); ?>   
		 Max: <?php echo $form->textField($model,'votes_yes_prc_max', array('style'=>'width:50px;', 'placeholder'=>'Max') ); ?>
	 </div>

	<div class="button_blue button_wide">
        <button id="login_button" >Import</button>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->



<?php

Yii::app()->clientScript->registerScript('checkAll',
'
$("#checkAll").click(function(){
    $(\'.languages input:checkbox\').not(this).prop(\'checked\', this.checked);
});
', CClientScript::POS_READY);

?>