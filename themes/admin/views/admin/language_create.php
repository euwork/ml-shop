<?php 
$this->header = 'Language create'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Language create</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'lang-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>

<div class="labeled">
	<?php echo $form->labelEx($model,'lang_id'); ?> 
	<br>
	<?php echo $form->textField($model,'lang_id',array('minlength'=>2, 'maxlength'=>5, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'name'); ?> 
	<br>
	<?php echo $form->textField($model,'name',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'country'); ?> 
	<br>
	<?php echo $form->textField($model,'country',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>





 <br><br>   
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>
