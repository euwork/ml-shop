<?php 
$this->header = 'Languages & Payment methods'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

// $current_url = $this->createUrl( $this->getId() . '/' . $this->getAction()->getId() );
$current_url = '/' . $this->getId() . '/' . $this->getAction()->getId() ;

#$payment_method_list = array('wire', 'visa', 'master', 'amex', 'bc', 'bitcoin');
$payment_method_list = array('wire', 'visa', 'master', 'amex', 'bc', 'ideal');

?>

<?php #echo CHtml::link('Add new', array('languagecreate')); ?>

                    <table id="sortable">
                        <thead>
                        <tr>
                            <th width="40">ID</th>
                            <th>Language</th>

                            <th width="80">Active</th>
                            <th width="80">Generic</th>
                            <th width="80">Order</th>
                            <th width="60">Bank</th>
                            <th width="60">Visa</th>
                            <th width="60">Master</th>
                            <th width="60">Amex</th>
                            <th width="60">BlueCard</th>
                            <th width="60">iDeal</th>
                            <!-- <th width="60">Bitcoin</th> -->
                        </tr>
                        </thead>
                        <tbody>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $item):
                            
                             $i = $i ? 0 : 1;
                             


                        ?>
                        <tr class="row<?php echo $i;?>" id="row<?php echo $item->lang_id; ?>">
                            <td class="ta_c"><?php echo $item->lang_id; ?>
                            </td>
                            <td>
                                <?php echo $item->name .' ['.$item->country.']'; ?>
                                <?php //echo CHtml::link($item->name .' ['.$item->country.']', array('languageedit', 'lang_id'=>$item->lang_id)); ?>
                            </td>

                            <td class="ta_c">
                                <input type="checkbox" name="active[<?php echo $item->lang_id;?>]" value="1" cid="<?php echo $item->lang_id;?>" <?php echo $item->show ? 'checked="checked"' : '';?> />
                            </td>
                            <td class="ta_c">
                                <input type="checkbox" name="show_generic[<?php echo $item->lang_id;?>]" value="1" cid="<?php echo $item->lang_id;?>" <?php echo $item->show_generic ? 'checked="checked"' : '';?> />
                            </td>
                            <td class="ta_c">
                                <input type="text" name="order[<?php echo $item->lang_id;?>]" id="order_row<?php echo $item->lang_id;?>" value="<?php echo $item->position;?>" class="order" cid="<?php echo $item->lang_id;?>"  />
                            </td>

                            <?php foreach($payment_method_list as $payment_method): ?>
                                <td class="ta_c">
                                    <input type="checkbox" name="<?php echo $payment_method;?>[<?php echo $item->lang_id;?>]" value="1" cid="<?php echo $item->lang_id;?>" <?php echo $item->{$payment_method} ? 'checked="checked"' : '';?> />
                                </td>
                            <?php endforeach; ?>
                            
                        </tr>
                       <?php endforeach; ?>
                        </tbody>
                    </table>

                    <br />

                    <div class="button_blue"><button onclick="item_save();">Save changes</button></div>


<script type="text/javascript">

    $('#sortable tbody tr td').css('cursor', 'move');

    $('#sortable tbody').sortable({
        cursor: 'move',
        items: 'tr',
        helper: function(event, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: handle_resort
    }).disableSelection();
    
    function handle_resort(event, ui)
    {
        var i = 1;
        $('#sortable tbody tr').each(function(i){
            var row = $(this).get()[0];
            $('#order_' + row.id).val(i+1);
            i++;
        });
    }

    function item_save()
    {
        var request = { ajax: 1, 
                        action: 'languages', 
                        method: 'save', 
                        order: {}, 
                        active: {} ,
                        show_generic: {}, 
                        wire: {}, 
                        visa: {}, 
                        master: {}, 
                        amex: {}, 
                        bc: {},
                        bitcoin: {},
                        ideal: {}
                    }; 

        $(":input[type=text][name*=order]").each(function() {
            request.order[$(this).attr('cid')] = $(this).val();
        });


        $(":input[type=checkbox][name*=active]").each(function() {
            request.active[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
        });

        $(":input[type=checkbox][name*=show_generic]").each(function() {
            request.show_generic[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
        });
        
        <?php foreach($payment_method_list as $payment_method): ?>
                $(":input[type=checkbox][name*=<?php echo $payment_method;?>]").each(function() {
                    request.<?php echo $payment_method;?>[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
                });
        <?php endforeach; ?>
        
        $.post('<?php echo $current_url; ?>', request, function(data) {
            if (data.error) 
            {
                if (data.error.message)
                {
                    system_error(data.error.message);
                }
            }
            if (data.result)
            {
                system_ok('Information saved');
            }
        }, 'json');
        
        return false;
    }
</script>               