<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = 'Login';

?>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login_form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>


	<div class="label">
		<?php echo $form->labelEx($model,'username'); ?>
	</div>
	<div class="labeled">
		<?php echo $form->textField($model,'username', array('class'=>'text') ); ?>
	</div>

	<div class="label">
		<?php echo $form->labelEx($model,'password'); ?>
	</div>
	<div class="labeled">
		<?php echo $form->passwordField($model,'password', array('class'=>'text')); ?>

	</div>


		
		<div class="label">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>


	<div class="button_blue button_wide">
                <button id="login_button" >Login</button>
            </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
