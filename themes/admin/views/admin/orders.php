<?php 
$this->header = 'Orders'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


if($form->currency == 'EUR'){
  $symbol = '€';
  $currency_prefix = '';
} else {
  $symbol = '$';
  $currency_prefix = '_usd';
}

?>
<?php $this->renderPartial('_form_orders_filter', array('model'=>$form)); ?>



                    <div>
                        Total orders: <?php echo  $pages->itemCount; ?>
                        <?php
                            $unsent = Orders::getUnsentCount();
                            if($unsent['count'] > 0) :
                        ?>
                            <span style="color:#FC0505;font-weight:bold;">Not send: <?php echo $unsent['count']; ?> &gt;&gt;&gt;Please contact our support team&lt;&lt;&lt;</span>
                        <?php endif; ?>
                    </div>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Pages:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>
                    <table>
                        <tr>
                            <th width="70">Date</th>
                            <th width="70">Order id</th>
                            <th width="10">&nbsp;</th>
                            <th width="10">&nbsp;</th>
                            <th>Customer</th>
                            <th width="10">&nbsp;</th>
                            <th>Order</th>
                            <th width="10">&nbsp;</th>
                            <th width="70">Amount</th>
                            <th width="70">Profit</th>
                        </tr>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $order):
                             $status = $order->statusname;
                             if($form->approved && $status != 'accept'){
                                continue;
                             }
                             $i = $i ? 0 : 1;
                             
                             $data=array();
                             foreach($order->order_products as $product):
                                 list($name, $quantity) = $product->product->pills_data_format_checkout();
                                 $data[] = '<b>'.$name.'</b> '.$quantity.' ' . $symbol.$product->getPriceinfo($currency_prefix);
                             endforeach;
                             $order_desc = implode('<br>', $data);

                             $subtotal = $order->{'subtotal'.$currency_prefix};
                             $profit = $order->getProfit($currency_prefix);
                             

                             switch ($status) {
                                 case 'accept':
                                     $status_icon = 'i_status_accept';
                                     $subtotal = Format::price($subtotal, '', $symbol);
                                     $profit = Format::price($profit, '', $symbol);

                                     break;
                                 case 'decline':
                                     $status_icon = 'i_status_decline';
                                     $subtotal = Format::price($subtotal, 's', $symbol);
                                     $profit = Format::price($profit, 's', $symbol);

                                     break;
                                 default:
                                     $status_icon = 'i_status_wait';
                                     $subtotal = Format::price($subtotal, '', $symbol);
                                     $profit = Format::price($profit, '', $symbol);

                                     break;
                             }
                             $tr_attribute = $order->processed ? '' : ' style="color: #dc3912;"';

                        ?>
                        <tr class="row<?php echo $i;?>"<?php echo $tr_attribute; ?>>
                            <td class="ta_c"><?php echo Format::datetime($order->date_cr); ?>
                            </td>
                            <td>
                                <?php echo $order->affid . $order->order_id; ?>
                            </td>
                            <td>
                                <?php echo strtoupper($order->language); ?>
                            </td>
                            <td>
                                <?php echo $order->paymenticon; ?>
                            </td>
                            <td><?php echo CHtml::encode($order->fullname); ?>
                                <br /><span class="phone"><?php echo $order->phone; ?></span></td>
                            <td>
                                <?php echo Country::getIconByCode($order->shipp_country); ?>
                            </td>
                            <td>
                                <small><?php echo $order_desc;?></small>
                            </td>
                            <td class="ta_c"><a class="icon <?php echo $status_icon;?>">#</a>
                            </td>
                            <td class="ta_c"><?php echo $subtotal; ?></td>
                            <td class="ta_c"><?php echo $profit ; ?></td>
                            
                        </tr>
                       <?php endforeach; ?>
                        <tr class="total">
                            <td class="ta_r" colspan="8">Total: &nbsp; <small><span style="color: #109618;"><?php echo $total['accept'];?></span> | <span style="color: #3366cc;"><?php echo $total['wait'];?></span> | <span style="color: #dc3912;"><?php echo $total['decline'];?></span></small>
                            &nbsp;&nbsp;/&nbsp;&nbsp;Total Pending comission: <b><?php echo Format::price($total['pending'], '', $symbol) ; ?></b>
                            &nbsp;&nbsp;/&nbsp;&nbsp;Total Profit: <b><?php echo Format::price($total['profit'], '', $symbol) ; ?></b>
                            </td>
                        </tr>
                    </table>
                    <div class="pagenavi">
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => 'Pages:',
                        'prevPageLabel' => '&laquo;',
                        'nextPageLabel' => '&raquo;',
                        'maxButtonCount' => 10,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css_a/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>
                    <div class="legend">
                        <a class="icon i_status_accept">#</a> - approved orders &nbsp; <a class="icon i_status_wait">#</a> - waiting for approval &nbsp; <a class="icon i_status_decline">#</a> - not approved orders &nbsp;
                    </div>
                    <!-- <a href="#top" style="font-size: 12px;">&Delta; Go to top</a> -->
                    <br />
               