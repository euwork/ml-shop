<?php 
$this->header = 'Payment history'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>
<div class="pagenavi">
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'header' => 'Pages:',
    'prevPageLabel' => '&laquo;',
    'nextPageLabel' => '&raquo;',
    'maxButtonCount' => 10,
    'cssFile' => '/css_a/pager.css',
))?>
</div>
<div class="clean"></div>
<table>
    <tr>
        <th width="50">ID</th>
        <th width="150">Date 
        </th>
        <th width="100">Payout
        </th>
        <th>Comment</th>
    </tr>

    <?php $i = 0; foreach($model as $row): ?>
    <?php $i = $i ? 0 : 1; ?>
    <tr class="row<?php echo $i;?>">
                            <td class="ta_c"><?php echo $row->pay_id; ?>
                            </td>
                          
                            <td><?php echo $row->date_cr; ?></td>
                            
                            <td class="ta_r">€<?php echo $row->amount; ?></td>
                            <td class="ta_c">
                                <small><?php echo $row->comment;?></small>
                            </td>
                            
                           
                            
    </tr>
    <?php endforeach; ?>
</table>
<div class="pagenavi">
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'header' => 'Pages:',
    'prevPageLabel' => '&laquo;',
    'nextPageLabel' => '&raquo;',
    'maxButtonCount' => 10,
    'cssFile' => '/css_a/pager.css',
))?>
</div>
<div class="clean"></div>