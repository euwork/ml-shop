<?php 
$this->header = 'Price & products control'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Price control</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'profile-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>

<table>
    <tr>
        <td>

<?php $item = Config::findByParam($model, 'price.increase'); ?>

<div class="label">Increase the price (only for France database): </div>
<div class="labeled">
   <?php echo $form->dropDownList($item,'value', Config::getPriceSteps(), array('name'=>'Config['.$item->param.']')); ?>
</div>
        </td>
        <td>
<?php $item = Config::findByParam($model, 'price.sort_order'); ?>
<div class="label">Select sort order of products: </div>
<div class="labeled">
   <?php echo $form->dropDownList($item,'value', Config::getProductsOrderList(), array('name'=>'Config['.$item->param.']')); ?>
</div>
        </td>

        <td>
<?php $item = Config::findByParam($model, 'price.increase_old'); ?>
<div class="label">Old price is more on: </div>
<div class="labeled">
   <?php echo $form->dropDownList($item,'value', Config::getPriceSteps(), array('name'=>'Config['.$item->param.']')); ?>
</div>
        </td>

        <td>
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>
        </td>
    </tr>
</table>

    



<?php $this->endWidget(); ?>

<br /><br />
<?php $this->renderPartial('_form_prices_filter', array('model'=>$filter_form)); ?>
<?php
// Yii::app()->language = 'fr';
?>
<table>
    <tr>
        <!-- <th width="10">&nbsp;</th> -->
        <th width="10">ID</th>
        <th width="200">drugname</th>
        <th width="100">dose</th>
        <th width="100">amount + bonus</th>
        <th>amount_in</th>
        <?php if(Config::priceOldEnabled()) : ?>
            <th width="60">old_price</th>
        <?php endif;?>
        <th width="60">price</th>
        <!-- <th width="60">priceExt</th> -->
        <th width="20">status</th>
    </tr>
    <?php
        $i = 0;
        foreach($products as $n => $item):

        $i = $i ? 0 : 1;

       // $status_url = Yii::app()->createUrl('/admin/changeProductStatus', array('id'=> $item->prod_id, 'status'=>$item->changeTo));

    ?>
        <tr class="row<?php echo $i;?><?php echo $item->a_status == 0 ? ' noactive' : ''; ?>">
            <!-- <td><?php //echo ($n+1); ?></td> -->
            <td><?php echo $item->prod_id; ?></td>
            <td><?php echo Yii::t('products', $item->drugname); ?></td>
            
            <?php if ($item->amount == 0) : //testpack ?>
               
                <td  colspan="1"><?php echo '<b>'.$item->dose . '</b> '; ?></td>
                <td  colspan="2"><?php echo  Yii::t('products', $item->getItemsDescription()); ?></td>
            <?php else: ?>
                <td class="ta_c"><?php echo $item->dose; ?></td>
                <td class="ta_c"><?php echo $item->amount; ?><?php echo $item->bonus ? ' + ' . $item->bonus : ''; ?></td>
                <td class="ta_c"><?php echo Yii::t('products', $item->amount_in); ?></td>
            <?php endif;?>
            <?php if(Config::priceOldEnabled()) : ?>
            <td class="ta_c"><?php echo '€ ' . '<s>'.$item->getPriceOld().'</s>'; ?></td>
            <?php endif;?>
            <?php
            $price = $item->getPrice();
            ?>
            <td class="ta_c"><?php echo '€ ' . $price; ?></td>
            <td><span class="changestatus"><?php echo CHtml::link( $item->statusName, '#', array('title'=>'click to change', 'class' => 'show' . $item->status, 'id'=>$item->prod_id, 'status'=>$item->changeTo)); ?></span></td>


        </tr>
    <?php endforeach; ?>
</table>



<?php

  Yii::app()->clientScript->registerScript('change_product_status',  "

        $( '.changestatus a' ).live({
          click: function() {
            var prodid = $(this).attr('id');
            var status = $(this).attr('status');
            $.post('/admin/changeproductstatus', { 'prodid': prodid, 'status': status},
              function(data){
                if(data.result == true){
                    var element = $('.changestatus a#'+prodid);
                    element.attr('status', data.change_to);
                    element.attr('class', 'show'+status);
                    element.text(data.status_name);
                } else {
                    system_error('error');
                }
            }, 'json');
            return false;
          },
        });
  ", CClientScript::POS_READY
    );
