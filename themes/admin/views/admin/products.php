<?php 
$this->header = 'Statistics by drugs'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


// print_r($data_traff);
// print_r($data_order);
?>
<?php $this->renderPartial('_form_orders_filter', array('model'=>$form)); ?>
<?php



?>


    <br />



    <table class="nowrap">
        <tr>
            <th width="10">&nbsp;</th>
            <th>DrugName</th>
            <th>Sales</th>
            <th>Amount</th>
            <th>Profit</th>
        </tr>
        <?php
        $num = 0;
        $i=0;
        foreach($data_order as $item): 
            



            

            $accept = 0;
            $reorder = 0;
            $wait = 0;
            $decline = 0;
            


            $amount = 0;
            $profit = 0;

         

            $num++;
            $i = $i ? 0 : 1;
            
            // $data_traff_row = search_by_country($data_traff, $item_country);
            // $data_order_row = search_by_country($data_order, $item_country);







            $accept = $item['accept'];
            $reorder = $item['reorder'];
            $wait = $item['wait'];
            $decline = $item['decline'];  

            $amount = isset($item['amount']) ? $item['amount'] : 0;   
            $profit = isset($item['profit']) ? $item['profit'] : 0;
                
       


        ?>
            <tr class="row<?php echo $i; ?>">
                <td class="ta_c"><?php echo $num; ?></td>
                <td class="ta_c"><?php echo $item['drugname']; ?></td>
                <td class="ta_c">
                    <span style="color: #109618;"><b><?php echo $accept; ?></b></span> 
                    | <span style="color: #109618;"><?php echo $reorder; ?></span> 
                    | <span style="color: #3366cc;"><?php echo $wait; ?></span> 
                    | <span style="color: #dc3912;"><?php echo $decline; ?></span>
                </td>
                <td class="ta_c">€<?php echo number_format($amount,2); ?></td>
                <td class="ta_c">€<?php echo number_format($profit,2); ?></td>
            </tr>
        <?php endforeach; ?>

    </table>
    <br />  
    <div class="legend">
        <span class="calendar_square" style="background: #109618;"></span> - approved orders &nbsp; 
        <span class="calendar_square" style="background: #109618;"></span> - reorders &nbsp; 
        <span class="calendar_square" style="background: #3366cc;"></span> - waiting for approval &nbsp; 
        <span class="calendar_square" style="background: #dc3912;"></span> - not approved orders &nbsp; 
        <b>*</b> - commission per unique / commission per thousand unique
    </div>
    <br />

