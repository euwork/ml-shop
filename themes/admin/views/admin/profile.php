<?php 
$this->header = 'Payment Information'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Payment methods</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'profile-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>
     
<div class="labeled">
    <table style="width: auto;">
        <tr>
            <th width="20">&nbsp;</th>
            <th width="100">Method</th>
            <th>Information</th>
            <th width="80">Commission</th>
            <th width="80">Min Amount</th>
        </tr>
        <tr>
            <td class="ta_c">
                <?php echo $form->radioButton($model,'payment_method', array('uncheckValue' => null, 'value'=>Profile::CAPITALIST)); ?>
            </td>
            <td class="ta_r"><?php echo Profile::getPaymentNameById(Profile::CAPITALIST); ?>:</td>
            <td>
                <?php echo $form->textField($model,'capitalist',array( 'maxlength'=>128, 'class'=>'text')); ?>
            </td>
            <td class="ta_c"><span class="nowrap"><?php echo Profile::getPaymentPercentById(Profile::CAPITALIST); ?></span>
            </td>
            <td class="ta_c"><?php echo Format::price(Profile::getMinPayoutById(Profile::CAPITALIST)); ?></td>
        </tr>
        <tr>
            <td class="ta_c">
                <?php echo $form->radioButton($model,'payment_method', array('uncheckValue' => null, 'value'=>Profile::WEBMONEY)); ?>
            </td>
            <td class="ta_r"><?php echo Profile::getPaymentNameById(Profile::WEBMONEY); ?>:</td>
            <td>
                <?php echo $form->textField($model,'webmoney',array( 'maxlength'=>20, 'class'=>'text')); ?>
            </td>
            <td class="ta_c"><span class="nowrap"><?php echo Profile::getPaymentPercentById(Profile::WEBMONEY); ?></span>
            </td>
            <td class="ta_c"><?php echo Format::price(Profile::getMinPayoutById(Profile::WEBMONEY)); ?></td>
        </tr>
        <tr class="row0">
            <td class="ta_c">
                <?php echo $form->radioButton($model,'payment_method', array('uncheckValue' => null, 'value'=>Profile::BITCOIN)); ?>
            </td>
            <td class="ta_r"><?php echo Profile::getPaymentNameById(Profile::BITCOIN); ?>:</td>
            <td>
                <?php echo $form->textField($model,'bitcoin',array( 'maxlength'=>128, 'class'=>'text')); ?>
            </td>
            <td class="ta_c"><span class="nowrap"><?php echo Profile::getPaymentPercentById(Profile::BITCOIN); ?></span>
            </td>
            <td class="ta_c"><?php echo Format::price(Profile::getMinPayoutById(Profile::BITCOIN)); ?></td>
        </tr>

        <tr class="row0">
            <td class="ta_c">
                <?php echo $form->radioButton($model,'payment_method', array('uncheckValue' => null, 'value'=>Profile::USDT)); ?>
            </td>
            <td class="ta_r"><?php echo Profile::getPaymentNameById(Profile::USDT); ?>:</td>
            <td>
                <?php echo $form->textField($model,'usdt',array( 'maxlength'=>128, 'class'=>'text')); ?>
            </td>
            <td class="ta_c"><span class="nowrap"><?php echo Profile::getPaymentPercentById(Profile::USDT); ?></span>
            </td>
            <td class="ta_c"><?php echo Format::price(Profile::getMinPayoutById(Profile::USDT)); ?></td>
        </tr>
        <tr >
            <td class="ta_c">
                <?php echo $form->radioButton($model,'payment_method', array('uncheckValue' => null, 'value'=>Profile::WIRE)); ?>
            </td>
            <td class="ta_r"><?php echo Profile::getPaymentNameById(Profile::WIRE); ?>:</td>
            <td>fill information below</td>
            <td class="ta_c"><span class="nowrap"><?php echo Profile::getPaymentPercentById(Profile::WIRE); ?></span>
            </td>
            <td class="ta_c"><?php echo Format::price(Profile::getMinPayoutById(Profile::WIRE)); ?></td>
        </tr>
    </table>
</div>
<h2>Wire transfer information</h2>
<div class="label">Bank account name, bank branch:</div>
<div class="labeled">
    <?php echo $form->textField($model,'bank_account_name',array('class'=>'text_big')); ?>
</div>
<div class="label">Bank address:</div>
<div class="labeled">
   <?php echo $form->textField($model,'bank_address',array('class'=>'text_big')); ?>
</div>
<div class="label">SWIFT Code:</div>
<div class="labeled">
    <?php echo $form->textField($model,'swift_code',array('class'=>'text_big')); ?>
</div>
<div class="label">Bank account number:</div>
<div class="labeled">
    <?php echo $form->textField($model,'bank_account_number',array('class'=>'text_big')); ?>
</div>
<div class="label">Account Holder Name:</div>
<div class="labeled">
    <?php echo $form->textField($model,'bank_code',array('class'=>'text_big')); ?>
</div>
<div class="label">Correspondent account:</div>
<div class="labeled">
    <?php echo $form->textArea($model,'correspondent_account',array('class'=>'text_big')); ?>
</div>




<div class="button_blue"><button id="save_button_profile">Save payment information</button></div>


<?php $this->endWidget(); ?>

