<?php 
$this->header = 'Statistics by referers'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>
<?php $this->renderPartial('_form_orders_filter', array('model'=>$form)); ?>
<?php


?>


    <br />


    <table class="nowrap">
        <tr>
            <th width="10">&nbsp;</th>
            <th>Unique</th>
            <th>Clicks</th>
            <th>Billing</th>
            <th>Orders</th>
            <th width="650">Referer</th>
        </tr>
        <?php
        $num = 0;
        $i=0;
        foreach($data_traff as $row): 
            


            $num++;
            $i = $i ? 0 : 1;
            

            

            $uniq = $row['uniq'];
            $hit = $row['hit'];
            $bill = $row['bill'];
            $ord = $row['ord'];
            $referer = $row['referer'];

			// if(strlen($referer) > 80){
	  //           $referer = substr($referer, 0, 80) . '...';
			// }

			if($referer != ''){
				$referer = '<a href="'.$referer.'">' . substr($referer, 0, 80) . '</a>';
			} else {
				$referer = '<span style="color:#cecece;">[empty]</span>';
			}
				


        ?>
            <tr class="row<?php echo $i; ?>">
                <td class="ta_c"><?php echo $num; ?></td>
                <td class="ta_c"><?php echo $uniq; ?></td>
                <td class="ta_c"><?php echo $hit; ?></td>
                <td class="ta_c"><?php echo $bill; ?></td>
                <td class="ta_c"><?php echo $ord; ?></td>
                <td class="ta_l"><?php echo $referer; ?></td>
            </tr>
        <?php endforeach; ?>

    </table>

    <br />  
    <br />
