<?php 
$this->header = 'Review create'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;


?>

<h2>Review create</h2>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'review-form',
    'method'=>'post',
    'enableAjaxValidation'=>false,
)); ?>


<div class="labeled">
	<?php echo $form->labelEx($model,'lang'); ?> 
	<br>
	<?php echo $form->dropDownList($model,'lang', Languages::getArrayExt(), array()); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'drug_id'); ?> 
	<br>
	<?php echo $form->dropDownList($model,'drug_id', Drugs::getArray(), array()); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'author'); ?> 
	<br>
	<?php echo $form->textField($model,'author',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'tit_name'); ?> 
	<br>
	<?php echo $form->textField($model,'tit_name',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'date_cr'); ?> 
	<br>
	<?php echo $form->textField($model,'date_cr',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'points'); ?> 
	<br>
	<?php echo $form->dropDownList($model,'points', Reviews::getPointsList(), array()); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'vote_yes'); ?> 
	<br>
	<?php echo $form->textField($model,'vote_yes',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'vote_all'); ?> 
	<br>
	<?php echo $form->textField($model,'vote_all',array('minlength'=>3, 'maxlength'=>256, 'class'=>'required', 'style'=>'width: 735px',)); ?>
</div>

<div class="labeled">
	<?php echo $form->labelEx($model,'content'); ?> 
	<br>
	<?php echo $form->textArea($model,'content',array('rows'=>20, 'cols'=>120, 'id'=>'mceEditor')); ?>
</div>



 <br><br>   
<div class="button_blue"><button id="save_button_profile">Save changes</button></div>


<?php $this->endWidget(); ?>
