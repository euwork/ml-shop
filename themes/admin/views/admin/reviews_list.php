<?php 
$this->header = 'Reviews ' ; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;



$drug_id = Yii::app()->request->getQuery('id');


$drugname = Drugs::getDrugNameById($drug_id) ;

?>





<div id="tabs">
<ul>
<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

<?php  
        $count = 0;
        $count_new = 0;
        foreach($model as $item):
            if($item->lang != $lang_id) continue;
            $count++;
            if($item->new != 1) continue;
            $count_new++;
        endforeach;
?>

    <li><a href="#tabs-<?php echo $lang_id; ?>" class="entry-link" title="<?php echo $data['country']; ?>"><?php echo strtoupper($lang_id)  ; ?> 
        <span style="font-size:9px;">
            <?php echo $count; ?> 
            <?php  if($count_new > 0 ):?> 
                <span style="font-size:8px;color:red;vertical-align: top;"><?php echo $count_new ?></span>
            <?php endif;?>
        </span>
        </a>
    </li>
<?php endforeach; ?>
</ul>


<?php foreach (DMultilangHelper::suffixList() as $lang_id => $data) : ?>

<?php $suffix = $data['suffix']; ?>

<div id="tabs-<?php echo $lang_id; ?>">

    <div class="labeled" >
        <p style="line-hight:22px;">
            <b>Language:</b> <?php echo $data['name']; ?>
            <img style="vertical-align: middle;" src="<?php echo Yii::app()->theme->baseUrl;?>/lang-ico/<?php echo $lang_id; ?>.png" title="<?php echo $data['country']; ?>">
            <?php 

                echo CHtml::link('Create new', array('reviewcreate', 'drug_id'=>$drug_id, 'lang_id'=>$lang_id), array('class'=>'new-review'));

            ?>
            Rating:
            <?php $rating_string = Drugs::getRatingFakeString($drug_id, $lang_id); ?>
            <span id="rating-5-<?php echo $lang_id;?>"><?php echo Drugs::getRatingByParams($drug_id, $lang_id, $rating_string); ?></span>
            <input title="Format: 5 stars| 4 stars| 3 stars | 2 stars| 1 star" type="text" name="rating" value="<?php echo $rating_string; ?>" cid="<?php echo $lang_id;?>" did="<?php echo $drug_id;?>">
            <button onclick="rating_save('<?php echo $lang_id;?>');">Update rating</button>

            &#9825;:
            <?php $count_likes = Drugs::getCountLikesByParams($drug_id, $lang_id); ?>
            <input type="text" name="likes" value="<?php echo $count_likes; ?>" cid="<?php echo $lang_id;?>" did="<?php echo $drug_id;?>" style="width:35px;">
            <button onclick="likes_save('<?php echo $lang_id;?>');">Update likes</button>
        </p>
    </div>

                    

                    <table>
                        <tr>
                            <th width="25">ID</th>
                            <th width="100">Drug</th>
                            <th>Review</th>
                            <!-- <th width="40">&nbsp;</th> -->
                            <!-- <th>date</th> -->
                            <th width="60">Active</th>
                            <th width="60">Home</th>
                            <th width="40">&nbsp;</th>
                        </tr>

                        <?php 
                        $i=0;
                        // $sm_profit=0;
                        // $sm_profit_pending=0;
                        // $total=array('a'=>0, 'w'=>0,'d'=>0);
                        foreach($model as $item):
                            
                             

                            if($item->lang != $lang_id) continue;
                            
                            $i = $i ? 0 : 1;
                            $tr_class = $item->new  ? ' noread' : '';
                        ?>
                            <tr class="row<?php echo $i.$tr_class;?>">
                                <td class="ta_c"><?php echo $item->rev_id; ?>
                                </td>
                                <td>
                                    <?php echo $drugname; ?> 
                                </td>
                                <!-- <td <?php echo $item->new ? 'class="new-unread"' : '';?> > -->
                                <td class="desc">
                                    <font style="color:#2e6e9e;font-weight:bold;"><?php echo $item->tit_name ;?></font>

                                    <?php echo $item->new ? CHtml::link('New', array('reviewread', 'id'=>$item->rev_id), array('class'=>'unread', 'title'=>'click to mark as read')) : ''; ?>
                                     <br>
                                    <font style="color:green;"><?php echo $item->author ;?></font>  
                                    <font style="font-size:0.9em;">(<?php echo date('Y-m-d', strtotime($item->date_cr)) ;?>)</font> 
                                    <font style="font-weight:bold;"><?php echo $item->points ;?> / 5</font>
                                    <span style="color:green;"><?php echo $item->vote_yes ;?></span> /
                                    <span style="color:#2e6e9e;"><?php echo $item->vote_all ;?></span>
                                    <br>
                                    <?php echo $item->content ;?> 
                                </td>


                                <td class="ta_c">
                                    <input type="checkbox" name="active[<?php echo $item->rev_id;?>]" value="1" cid="<?php echo $item->rev_id;?>" <?php echo $item->active ? 'checked="checked"' : '';?> />
                                </td>
                                <td class="ta_c">
                                    <input type="checkbox" name="home[<?php echo $item->rev_id;?>]" value="1" cid="<?php echo $item->rev_id;?>" <?php echo $item->home ? 'checked="checked"' : '';?> />
                                </td>
                                <td class="ta_c">
                                    <div style="line-height:22px;">
                                    <?php echo CHtml::link('', array('reviewedit', 'id'=>$item->rev_id), array('class'=>'icon i_edit_super', 'title'=>'edit')); ?>
                                    <?php echo CHtml::link('del', array('reviewdelete', 'id'=>$item->rev_id), array('onclick'=>'return confirm("Confirm deletion");', 'class'=>'icon i_delete_super', 'title'=>'delete')); ?>
                                    </div>
                                    

                                </td>

                                
                            </tr>
                       <?php endforeach; ?>
                        
                    </table>


                    <br />

                     <div class="button_blue"><button onclick="rev_save('<?php echo $lang_id; ?>');">Save changes</button></div>
</div>

<?php endforeach; ?>
</div>  


<script type="text/javascript">

    $('#tabs').tabs();
    <?php $active_tab = Yii::app()->request->getQuery('active_tab'); ?>
    <?php if($active_tab) : ?>
        $('a[href="#tabs-<?php echo $active_tab; ?>"]').click();
    <?php endif; ?>
    
</script>   

<?php $current_url = '/' . $this->getId() . '/' . $this->getAction()->getId() . '?id=' . Yii::app()->request->getQuery('id'); ; ?>
<?php $rating_save_url = '/admin/updaterating'; ?>
<?php $likes_save_url = '/admin/updatelikes'; ?>
<script type="text/javascript">

    function rev_save(lang_id)
    {
        var request = { ajax: 1, method: 'save', active: {}, home: {}, lang_id: lang_id }; 
        var tabs_id = 'tabs-'+lang_id;

        $("#"+tabs_id+" :input[type=checkbox][name*=active]").each(function() {
            request.active[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
        });

        $("#"+tabs_id+" :input[type=checkbox][name*=home]").each(function() {
            request.home[$(this).attr('cid')] = ($(this).is(':checked') ? 1 : 0);
        });  

        $.post('<?php echo $current_url; ?>', request, function(data) {
            if (data.error) 
            {
                if (data.error.message)
                {
                    system_error(data.error.message);
                }
            }
            if (data.result)
            {
                system_ok('Information saved');
            }
        }, 'json');
        
        return false;
    }

    function rating_save(lang_id)
    {
        var request = { ajax: 1, method: 'save', rating: {} }; 
        var tabs_id = 'tabs-'+lang_id;

        // var drug_id;
        $("#"+tabs_id+" :input[type=text][name*=rating]").each(function() {
            if($(this).attr('cid') == lang_id){
                request.rating['value'] = ($(this).val());
                request.rating['lang_id'] = ($(this).attr('cid'));
                request.rating['drug_id'] = ($(this).attr('did'));
            }
        });

        $.post('<?php echo $rating_save_url; ?>', request, function(data) {
            if (data.result == false) 
            {
                system_error('Error');
                $(":input[type=text][name*=rating]").each(function() {
                    if($(this).attr('cid') == lang_id){
                        $(this).addClass('error');
                    }
                });
                
            }
            if (data.result.success == true)
            {
                system_ok('Rating updated');
                $(":input[type=text][name*=rating]").each(function() {
                    if($(this).attr('cid') == lang_id){
                        $(this).removeClass('error');
                        $('#rating-5-'+lang_id).text(data.result.rating);
                    }
                });
            }
        }, 'json');
        
        return false;
    }

    function likes_save(lang_id)
    {
        var request = { ajax: 1, method: 'save', likes: {} }; 
        var tabs_id = 'tabs-'+lang_id;

        // var drug_id;
        $("#"+tabs_id+" :input[type=text][name*=likes]").each(function() {
            if($(this).attr('cid') == lang_id){
                request.likes['value'] = ($(this).val());
                request.likes['lang_id'] = ($(this).attr('cid'));
                request.likes['drug_id'] = ($(this).attr('did'));
            }
        });

        $.post('<?php echo $likes_save_url; ?>', request, function(data) {
            if (data.result == false) 
            {
                system_error('Error');
                $(":input[type=text][name*=likes]").each(function() {
                    if($(this).attr('cid') == lang_id){
                        $(this).addClass('error');
                    }
                });
                
            }
            if (data.result.success == true)
            {
                system_ok('Likes updated');
                $(":input[type=text][name*=likes]").each(function() {
                    if($(this).attr('cid') == lang_id){
                        $(this).removeClass('error');
                        $(this).val(data.result.likes);
                    }
                });
            }
        }, 'json');
        
        return false;
    }

</script>       

<?php
  Yii::app()->clientScript->registerScript('is_read', "

        $('a.unread').click(function() {
            var path = $(this).attr('href');
            var this_tr = $(this).closest('tr');
            var this_a = $(this);
            $.ajax({
              url: path,
              context: document.body,
              dataType: 'json',
            }).success(function(data) {
                if(data == 'success'){
                    this_tr.removeClass('noread');
                    this_a.remove();
                } else {
                    alert('error');
                }
            });
            return false;
        });

        ", CClientScript::POS_READY
    );
?>