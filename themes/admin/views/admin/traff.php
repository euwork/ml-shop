<?php 
$this->header = 'Traffic statistics'; 
$this->pageTitle = Yii::app()->name. ' - ' . $this->header;

?>
<?php $this->renderPartial('_form_traff_filter', array('model'=>$form)); ?>
<?php $this->renderPartial('_calendar_nav', array('form'=>$form)); ?>
<?php


?>


    <br />

        <?php 
        $i=0;
        $total_accept = 0;
        $total_reorder = 0;
        $total_wait = 0;
        $total_decline = 0;
        $total_uniq = 0;
        $total_hit = 0;
        $total_bill = 0;
        $total_ratio = 0;
        $total_cpu = 0;
        $total_cpt = 0;
        $total_amount = 0;
        $total_profit = 0; 


        if($form->period == 'monthly'){
            $cur_key_format = 'Y m';
        } else {
            $cur_key_format = 'Y m d';
        }
        

        ?>


    <table class="nowrap">
        <tr>
            <th width="60"><?php echo $form->period == 'monthly' ? 'Month' : 'Day';?></th>
            <th>Sales</th>
            <th>Unique</th>
            <th>Clicks</th>
            <th>Billing</th>
            <th>Ratio</th>
            <th>CPU/CPT*</th>
            <th>Amount</th>
            <th>Profit</th>
        </tr>
        <?php
        foreach($rows_array as $row): 
        
            $i = $i ? 0 : 1;
            $cur_key = date($cur_key_format, $row);

            switch ($form->period) {
                case 'week':
                    $row_name = date('Y-m-d', $row) . '<br />'.date('l', $row);
                    break;
                
                case 'month':
                    $row_name = date('j', $row);
                    break;
                case 'monthly':
                    $row_name = date('F', $row);
                    break;
            }
            

            $accept = 0;
            $reorder = 0;
            $wait = 0;
            $decline = 0;
            
            $uniqs = 0;
            $hits = 0;
            $bills = 0;

            $ratio = 0;

            $amount = 0;
            $profit = 0;

            $cpu = 0;
            $cpt = 0;

            if(isset($data_order[$cur_key])){
                $accept = $data_order[$cur_key]['accept'];
                $reorder = $data_order[$cur_key]['reorder'];
                $wait = $data_order[$cur_key]['wait'];
                $decline = $data_order[$cur_key]['decline'];  

                $amount = $data_order[$cur_key]['amount'];   
                $profit = $data_order[$cur_key]['profit'];
            }
            $count_orders = $accept + $wait + $decline ;
            if(isset($data_traff[$cur_key])){
                $uniqs = $data_traff[$cur_key]['uniq'];
                $hits = $data_traff[$cur_key]['hit'];
                $bills = $data_traff[$cur_key]['bill'];

            }
            if($count_orders > 0){
                $ratio =  round($uniqs / $count_orders);
            }
            if($uniqs > 0){
                $cpu = $profit / $uniqs;
                $cpt = $cpu * 1000;
            }
            $total_accept += $accept;
            $total_reorder += $reorder;
            $total_wait += $wait;
            $total_decline += $decline;
            $total_uniq += $uniqs;
            $total_hit += $hits;
            $total_bill += $bills;

            $total_amount += $amount;
            $total_profit += $profit;

        ?>
            <tr class="row<?php echo $i; ?>">
                <td class="ta_c"><span style="font-size:11px;"><?php echo $row_name; ?></span>
                </td>
                <td class="ta_c">
                    <span style="color: #109618;"><b><?php echo $accept; ?></b></span> 
                    | <span style="color: #109618;"><?php echo $reorder; ?></span>
                    | <span style="color: #3366cc;"><?php echo $wait; ?></span> 
                    | <span style="color: #dc3912;"><?php echo $decline; ?></span>
                </td>
                <td class="ta_c"><?php echo $uniqs; ?></td>
                <td class="ta_c"><?php echo $hits; ?></td>
                <td class="ta_c"><?php echo $bills; ?></td>
                <td class="ta_c">1:<?php echo $ratio; ?></td>
                <td class="ta_c">€<?php echo number_format($cpu,2); ?>/€<?php echo number_format($cpt,2); ?></td>
                <td class="ta_c">€<?php echo number_format($amount,2); ?></td>
                <td class="ta_c">€<?php echo number_format($profit,2); ?></td>
            </tr>
        <?php endforeach; ?>
        <?php

            $total_orders = $total_accept + $total_wait + $total_decline;
            if($total_orders > 0){
                 $total_ratio =  round($total_uniq / $total_orders);
            }
            if($total_uniq > 0){
                $total_cpu = $total_profit / $total_uniq;
                $total_cpt = $total_cpu * 1000;
            }



        ?>
        <tr class="total">
            <td class="ta_r">Total:</td>
            <td class="ta_c"><span style="color: #109618;"><b><?php echo $total_accept;?></b></span> | <span style="color: #109618;"><?php echo $total_reorder;?></span> | <span style="color: #3366cc;"><?php echo $total_wait;?></span> | <span style="color: #dc3912;"><?php echo $total_decline;?></span>
            </td>
            <td class="ta_c"><?php echo $total_uniq;?></td>
            <td class="ta_c"><?php echo $total_hit;?></td>
            <td class="ta_c"><?php echo $total_bill;?></td>
            <td class="ta_c">1:<?php echo $total_ratio;?></td>
            <td class="ta_c">€<?php echo number_format($total_cpu,2); ?>/€<?php echo number_format($total_cpt,2); ?></td>
            <td class="ta_c">€<?php echo number_format($total_amount,2); ?></td>
            <td class="ta_c">€<?php echo number_format($total_profit,2); ?></td>
        </tr>
    </table>
    <div class="legend">
        <span class="calendar_square" style="background: #109618;"></span> - approved orders &nbsp; 
        <span class="calendar_square" style="background: #109618;"></span> - reorders &nbsp; 
        <span class="calendar_square" style="background: #3366cc;"></span> - waiting for approval &nbsp; 
        <span class="calendar_square" style="background: #dc3912;"></span> - not approved orders &nbsp; 
        <b>*</b> - commission per unique / commission per thousand unique
    </div>
    <!-- <br /><a href="#top" style="font-size: 12px;">&Delta; Go to top</a> -->
    <br />
