
<?php

$this->pageTitle = '[ML]PharmaShop Install';

?>
<?php 
if (count($errors))
{
    $count = 1;
?>
        <p class="error" style="padding: 7px 10px; margin: 20px 20px;">
<?php
    foreach ($errors as $e)
    {
        echo $count . '. ' . $e . '<br />';
        $count++;
    }
?>
        </p>
<?php } ?>







<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'install_form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>


	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'aff_id'); ?></span>
		<?php echo $form->textField($model,'aff_id', array('class'=>'text') ); ?>
	</div>


	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'api_key'); ?></span>
		<?php echo $form->textField($model,'api_key', array('class'=>'text')); ?>

	</div>

	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'api_domain'); ?></span>
		<?php echo $form->textField($model,'api_domain', array('class'=>'text')); ?>

	</div>

	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'login'); ?></span>
		<?php echo $form->textField($model,'login', array('class'=>'text') ); ?>
	</div>

	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'password'); ?></span>
		<?php echo $form->passwordField($model,'password', array('class'=>'text')); ?>

	</div>

	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'db_host'); ?></span>
		<?php echo $form->textField($model,'db_host', array('class'=>'text') ); ?>
	</div>


	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'db_port'); ?></span>
		<?php echo $form->textField($model,'db_port', array('class'=>'text') ); ?>
	</div>


	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'db_name'); ?></span>
		<?php echo $form->textField($model,'db_name', array('class'=>'text') ); ?>
	</div>


	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'db_login'); ?></span>
		<?php echo $form->textField($model,'db_login', array('class'=>'text') ); ?>
	</div>

	<div class="labeled">
		<span class="label"><?php echo $form->labelEx($model,'db_password'); ?></span>
		<?php echo $form->passwordField($model,'db_password', array('class'=>'text')); ?>

	</div>
	

	<div class="button_blue button_wide">
        <button id="login_button" >Install</button>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->