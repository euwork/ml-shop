<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>
        <?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/favicon.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/favicon.ico" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/common.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/flags.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/affiliate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/affiliate_layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/affiliate_menu.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/affiliate_dashboard.css" type="text/css" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css_a/jquery.ui.css');?>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js_a/functions.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js_a/affiliate.js" type="text/javascript"></script>
    <?php $datetime_for_js = str_replace('-', (date('m')-1), date('Y, -, d, H, i, s, 0') ); ?>
    <?php Yii::app()->clientScript->registerScript('ajax', " var SERVER_DATE = new Date(".$datetime_for_js."); var SERVER_DX = 0; ", CClientScript::POS_HEAD); ?>
    <?php
    $js_code = '';
    if(Yii::app()->user->hasFlash('success')){
        $js_code =  'system_ok(" ' . Yii::app()->user->getFlash('success') . '");';
    }elseif(Yii::app()->user->hasFlash('error')){
        $js_code =  'system_error(" ' . Yii::app()->user->getFlash('error') . '");';
    }
    Yii::app()->clientScript->registerScript('flash-message',$js_code, CClientScript::POS_END);
    ?>
</head>
<body onload="init_server_timer();">
    <div id="system_msg"></div>
    <div id="stl_left" class="fixed" style="left: 0px; opacity: 0;">
        <div id="stl_bg"><a href="#top" id="stl_text">&Delta; Go to top</a>
        </div>
    </div>
    <div id="page_dashboard">
        <?php if($this->installmode == false): ?>
        <div id="page_header">
            <div class="fl_l">
                <h1>IpillShop<span style="color:#fff;">[</span>EU<span style="color:#fff;">]</span></h1> 
            </div>
            <div class="headerinfo fl_r">
                <a href="<?php echo Yii::app()->createUrl('/admin/cacheflush?return=1'); ?>">Clear Cache</a> / 
                Default Language : <?php echo strtoupper(Yii::app()->language); ?> /
                <?php echo Yii::app()->params['login'];?> (aff_id: <?php echo Yii::app()->config->get('affid');?> ) <span id="server_time_div"><?php echo date('Y-m-d H:i:s'); ?></span> (EDT)</div>
        </div>
        <?php endif;?>
        <div id="page_wrapper">
            <div id="page_sidebar" class="fl_l">
                <?php if($this->installmode == false): ?>
                <ul class="menu">
                    <li class="icon_dashboard"><a href="/admin">Dashboard</a></li>
                    
                    <li class="separate">Statistics</li>

                    <li class="icon_orders"><a href="/admin/orders">Orders</a></li>
                    <li class="icon_statistics"><a href="/admin/traff">Traffic statistics</a></li>
                    <li class="icon_country"><a href="/admin/countries">Countries</a></li>
                    <li class="icon_referrer"><a href="/admin/referers">Referers</a></li>
                    <li class="icon_products"><a href="/admin/products">Products</a></li>
                    <li class="icon_trackids"><a href="/admin/trackids">Trackids</a></li>




                    <li class="separate">Content</li>
                    
                    <li class="icon_cats"><a href="/admin/catslist">Categories</a></li>
                    <li class="icon_pills"><a href="/admin/drugslist">Drugs</a></li>
                    <li class="icon_testimonials"><a href="/admin/drugstestimonialslist">Testimonials <?php echo Format::count('sup', Reviews::getCountNew()); ?></a></li>
                    <li class="icon_invoices"><a href="/admin/pageslist">Pages</a></li>
                    <li class="icon_articles"><a href="/admin/articleslist">Articles</a></li>

                    <li class="separate">Tools</li>

                    <li class="icon_link"><a href="/admin/generatelinks">Generate links</a></li>
                    
                    <li class="separate">Settings</li>

                    <li class="icon_config"><a href="/admin/config">General</a></li>
                    <li class="icon_languages"><a href="/admin/languages" title="Languages & Payment methods">Languages & PM</a></li>
                    <li class="icon_currencies"><a href="/admin/currencies">Currencies</a></li>
                    <li class="icon_price"><a href="/admin/prices">Price & Products</a></li>
                    <li class="icon_bank"><a href="/admin/bankdetails">Bankdetails</a></li>

                    <li class="separate">Import/Update</li>
                    <li class="icon_import"><a href="/admin/importcontent">Content</a></li>
                    <li class="icon_import_reviews"><a href="/admin/importreviews">Reviews</a></li>
                    <li class="separate">Payments</li>
                    <li class="icon_money"><a href="/admin/payments">Payment history</a></li>
                    <li class="icon_vcard"><a href="/admin/profile">Payment Info</a></li>

                    <li class="separate"></li>
                    <li class="icon_logout"><a href="/admin/logout">Exit</a></li>
                </ul>
                <?php endif;?>
            </div>
            <!-- page_sidebar -->
            <div id="page_body" class="fl_r">
                <div id="header_wrap2">
                    <div id="header_wrap1">
                        <div id="header">
                            <h1 id="title"><?php echo $this->header; ?></h1>
                        </div>
                        <!-- header -->
                    </div>
                    <!-- header_wrap1 -->
                </div>
                <!-- header_wrap2 -->
                <div id="wrap_between"></div>
                <div id="content">
                    <?php echo $content; ?>
                </div>
                <!-- content -->
                <div id="footer_wrap">
                    <div id="footer">Copyright &copy; 2014 - <?php echo date('Y'); ?> </div>
                </div>
                <!-- footer_wrap -->
            </div>
            <!-- page_body -->
        </div>
        <!-- page_wrapper -->
    </div>
    <!-- page_dash -->
</body>

</html>