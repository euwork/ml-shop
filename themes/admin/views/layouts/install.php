
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>
        <?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/favicon.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/favicon.ico" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/common.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css_a/install.css" type="text/css" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div class="indexlogo">
                <img src="<?php echo Yii::app()->theme->baseUrl;?>/img/index_logo.gif">
            </div>
        </div>
        <div id="content">
            <?php echo $content; ?>
            
        </div>

    </div>
</body>

</html>