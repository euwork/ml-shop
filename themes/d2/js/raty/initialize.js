$(document).ready(function() {
    var raty = $.fn.raty.defaults;
    raty.path = window.ratyPath;
    raty.cancel = true;
    raty.targetKeep = true;
    raty.targetType = 'number';
    raty.cancelHint = 'Cancel this rating';
    raty.hintList = ['1', '2', '3', '4', '5'];
    raty.cancelOff = 'cancel-off-big.png';
    raty.cancelOn = 'cancel-on-big.png';
    raty.size = 24;
    raty.starOff = 'star-off-big.png';
    raty.starOn = 'star-on-big.png';

    $('#star_recommandation').raty({
        target: '#rev_recommandation'
    });

    $('#star_website_friendly').raty({
        target: '#rev_website_friendly'
    });

    $('#star_shipping').raty({
        target: '#rev_shipping'
    });

    $('#star_price_performance').raty({
        target: '#rev_price_performance'
    });

    $('.review_stars').each(function() {
        var val = $(this).prev().val();
        reviewsStars(this, val);
    });
    $('#rewiews_raty').raty({
        cancel: false,
        readOnly: true,
        start: window.ratyStart,
        starOff: 'star-off.png',
        starOn: 'star-on.png'
    });

    $('#rev_submit').click(function() {
        if ($('#rev_name').val() == "" || $('#rev_comment').val() == "" || $('#rev_captcha').val() == "") {
            alert(window.emptyFieldMess);
            return false;
        }
        else {
            $('#reviews_form').submit();
        }
    });

});
function reviewsStars(obj, val) {
    $(obj).raty({
        cancel: false,
        readOnly: true,
        start: val,
        starOff: 'star-off.png',
        starOn: 'star-on.png'
    });
}