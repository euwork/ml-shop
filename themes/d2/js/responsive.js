function init_navigation () {
	var width = $(window).width();
	if(width>=1000) {
		$('body').removeClass('mobile');
	}
	else {
		$('body').addClass('mobile');
	}
}


$(window).on('resize', init_navigation);
$(window).on('load', function(){
  init_navigation();
});
$(function(){
	/*$('#headerWrap').append('<div id="header-mobile" class="headNav"><ul>\n\
		<li><a href="'+href_home+'" id="menu-homepage">'+tr_homepage+'</a></li>\n\
		<li><a href="#" id="menu-menuopen">'+tr_menu+'</a></li>\n\
		<li><a href="'+href_cart+'" id="menu-cart">'+tr_cart+'</a></li>\n\
		<li><a href="'+href_contact+'" id="menu-contacts">'+tr_contact+'</a></li>\n\
	</ul></div>');*/
	if($('#col-1:has(.wrapper_category_a) > #products').size()>0) {
		$('#headerWrap #header-mobile li:has(a#menu-menuopen)').after('<li><a href="#" id="menu-pills">'+tr_pills+'</a></li>');
		$('#menu-homepage').parent().remove();
	}
	$('#menu-cart').html($('#menu-cart').html()+" <small>("+$('#cart-block strong').html().replace(' ','')+")</small>");
	// menu pills 
	$(document).on('click','li:not(.act) #menu-pills', function(){
		$('body').addClass('modal-open');
		$('#header-mobile > ul > li').removeClass('act');
		$(this).parent().addClass('act');
		$('.modal').remove();
		$('body').append('<div class="mobile-menu modal"><div class="modal-content"><a href="#" class="close img">x</a></div></div>');
		$('#col-1:has(.wrapper_category_a) > #products').clone().appendTo('div.mobile-menu .modal-content');
		//$('.modal-content').append('<a href="#" class="close">AbschlieÃŸen</a>');
		return false;
	});
	
	// menu mainmenu
	$(document).on('click','li:not(.act) #menu-menuopen', function(){
		$('body').addClass('modal-open');
		$('#header-mobile > ul > li').removeClass('act');
		$(this).parent().addClass('act');
		$('.modal').remove();
		$('body').append('<div class="mobile-menu modal"><div class="modal-content big"><a href="#" class="close img">x</a><div class="section"><b>'+tr_menu+'</b></div></div></div>');
		$('#headNav > ul').clone().appendTo('div.mobile-menu .modal-content .section');
		
		$('#col-1 > .section:last').clone().appendTo('div.mobile-menu .modal-content');
		
		// langs && currency
		$('div.mobile-menu .modal-content').append('<div class="section mobile-langs"><b>'+$('.a-lang-list a.active').html()+'</b></div>'); 
		$('.a-lang-list > ul').clone().appendTo('div.mobile-menu .modal-content .mobile-langs'); 
		
		$('div.mobile-menu .modal-content').append('<div class="section mobile-currency"><b>'+$('.a-curr-list a.active').html()+'</b></div>'); 
		$('.a-curr-list > ul').clone().appendTo('div.mobile-menu .modal-content .mobile-currency');
		// 

		return false;
	});
	
	// menu cart
	$(document).on('click','li:not(.act) #menu-cart', function(){
		$('body').addClass('modal-open');
		$('#header-mobile > ul > li').removeClass('act');
		$(this).parent().addClass('act');
		$('.modal').remove();
		$('body').append('<div class="mobile-menu modal"><div class="modal-content big"><a href="#" class="close img">x</a><div class="section"><b>'+tr_cart+'</b></div></div></div>');
		$('#cart-block > p').clone().appendTo('div.mobile-menu .modal-content .section');
		$('div.mobile-menu .modal-content .section a').html(tr_cart);
		return false;
	});
	
	// menu contacts
	$(document).on('click','li:not(.act) #menu-contacts', function(){
		$('body').addClass('modal-open');
		$('#header-mobile > ul > li').removeClass('act');
		$(this).parent().addClass('act');
		$('.modal').remove();
		$('body').append('<div class="mobile-menu modal"><div class="modal-content big"><a href="#" class="close img">x</a><div class="section"><b>'+tr_contact+'</b></div></div></div>');
		$('#col-3 > img.rban:eq(0)').clone().css({margin: '10px auto', display: 'block'}).appendTo('div.mobile-menu .modal-content .section');
		$('div.mobile-menu .modal-content .section').append('<a class="btn_blank" style="margin: 5px auto;color: white;text-decoration: none;" href="'+href_contact+'">'+tr_contact+'</a>');
		return false;
	});
	
	$(document).on('click','li.act #menu-pills, li.act #menu-menuopen, li.act #menu-cart, li.act #menu-contacts, .modal .close',function(){
		$('#header-mobile > ul > li').removeClass('act');
		$('.modal').remove();
		$('body').removeClass('modal-open');
		return false;
	});
	
	// client menu 
	$('.clientmenu > .section').clone().addClass('onlymob').addClass('clientmenusection').css({'margin': '0 auto 30px'}).prependTo('#col-2-3');
	
	init_navigation();
});

jQuery(document).ready(function($){
	function bg(){
		if($('.over_bg').length > 0 && !$('body').hasClass('open-cart') && !$('body').hasClass('open-m')){
			$('.over_bg').remove();
		}else if($('.over_bg').length < 1){
			$('#footerWrap').after('<div class="over_bg"></div>');
		}
	}
	
	$('.mob-m__btn').click(function(){
		$('.menu-mobile').css('max-height', $(window).height()+'px')
		$('body').removeClass('open-cart');
		$('.mob-m button').toggleClass('openm');
		$('body').toggleClass('open-m');
		bg();
	});
	
	$('.cart-mobile').click(function(){
		$('body').removeClass('open-m');
		$('.mob-m button').removeClass('openm');
		$('body').toggleClass('open-cart');
		bg();
	});

	$('body').on('click', '.over_bg', function(){
		$(this).remove();
		$('body').removeClass('open-m open-cart');
		$('.mob-m button').removeClass('openm');
	});
	
	$('.product-mobile__item.last .product-mobile__title').click(function(){
		$(this).toggleClass('active');
		$(this).next().slideToggle();
	});
	
	$('.omenu-top select').on('change', function(){
		var url = window.location.protocol+'//'+window.location.hostname+$(this).val();
		document.location.href = url;
	});
	
	console.log(document.location.href);
	
	$('#products ul li a,.side-section a').each(function(){
		if($(this).attr('href') == document.location.href){
			$(this).addClass('active');
		}
	});
	
});