


<?php if(count($links) > 1) : ?>


	<div class="a-curr-list">
	<?php foreach($links as $link): ?>
		<?php if($link['active'] == 1) : ?>
		    <a href="" class="active"><span><?php echo $link['symbol']; ?></span> <?php echo $link['code']; ?></a>
	    <?php endif; ?>
	<?php endforeach; ?>
        <ul>
    	<?php foreach($links as $link): ?>
	        <li><a href="<?php echo $link['url']; ?>"><span><?php echo $link['symbol']; ?></span> <?php echo $link['code']; ?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>

<?php endif; ?>                