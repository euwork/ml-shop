


<?php if(count($links) > 1) : ?>

<div class="a-lang-list">

	<?php foreach($links as $link): ?>
		<?php if($link['active'] == 1) : ?>
			<a href="" class="active"><img src="<?php echo Yii::app()->theme->baseUrl;?>/images-lang/ico/<?php echo $link['lang']; ?>.png" alt=""><?php echo $link['name']; ?></a>
		<?php endif; ?>
	<?php endforeach; ?>
    <ul>
	<?php foreach($links as $link): ?>
        <li><a href="<?php echo $link['url']; ?>" title="<?php echo $link['country']; ?>"><img src="<?php echo Yii::app()->theme->baseUrl;?>/images-lang/ico/<?php echo $link['lang']; ?>.png" alt=""/><?php echo $link['name']; ?></a></li>
    <?php endforeach; ?>
    </ul>
</div>

<?php endif; ?>