








<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'affiliates',
	'enableAjaxValidation'=>false,
    'action' => Yii::app()->getRequest()->getOriginalUrl(),
)); ?>
<?php //echo $form->errorSummary($model); ?>
    <fieldset>
    	<div class="affiliat_block">
                <div class="affiliat_img">
                    <img src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/affilate.png" />
                </div>
        <div class="line">
        	<?php echo $form->labelEx($model,'name'); ?>

            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
        </div>
        <div class="line">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256, )); ?>
        </div>
        <div class="line affiliat_block">
			<?php echo $form->labelEx($model,'website'); ?>
			<?php echo $form->textField($model,'website',array('size'=>60,'maxlength'=>256)); ?>
			<div style="clear: both; overflow: hidden;">&nbsp;</div>
        </div>
        <div class="line mesedg">
			<?php echo $form->labelEx($model,'content', array('class' => 'textarea_label')); ?>
			<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        </div>
        <div class="line"> <span class="span"><?php echo Yii::t('affiliates', 'Geben Sie die Zeichen in dem Bild ein'); ?></span> 
        </div>
		<?php if(CCaptcha::checkRequirements() ):?>
        <div class="line" style="float: left; margin-left: 0px;">
            <label class="capcha" for="contact_captcha">
                <div style="float:left;"><?php $this->widget('CCaptcha', array('showRefreshButton'=>false, 'clickableImage'=>true)); ?>
                </div>
                <span class="errow"></span> 
            </label>
            <?php echo $form->textField($model,'code',array('size'=>20,'maxlength'=>256)); ?>
        </div> 
		<?php endif; ?>
        <div class="line" style="float: left; margin-left: 0px;">
            <a class="btn" href="#" onclick="document.getElementById('affiliates').submit(); return false;"><?php echo Yii::t('contacts', 'Senden'); ?><span></span></a> 
        </div>
        <div style="clear: both; overflow: hidden;">&nbsp;</div>
	    </div>
    </fieldset>
    <div style="clear: both; overflow: hidden;">&nbsp;</div>
<?php $this->endWidget(); ?>


