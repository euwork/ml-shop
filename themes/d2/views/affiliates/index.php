
<h2 class="title_prod"><span></span><?php echo $text->header; ?></h2>



<div class="box_text">
    <p>
        <?php echo $text->content; ?>    
    </p>
</div>

<div id="identifyingDetails">
    <h3><?php echo Yii::t('affiliates', 'Persönliche Details'); ?></h3>
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash flash-' . $key . ' flash-affiliate">' . $message . "</div>\n";
    }
    ?>
    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    
</div>
<div style="clear: both; overflow: hidden;">&nbsp;</div>