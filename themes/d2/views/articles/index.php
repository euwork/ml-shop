<?php
/* @var $this ArticlesController */
/* @var $dataProvider CActiveDataProvider */


?>
<?php if(count($model) > 0) : ?>
    <p class="page_name"><?php echo Yii::t('articles', 'Interessante Artikel'); ?></p>
<?php else: ?>
    <p class="page_name"><?php echo Yii::t('articles', 'nicht gefunden'); ?></p>
<?php endif; ?>

<?php foreach($model as $item): ?>
<div class="article">
    <p class="title">
    	<?php echo $item->postname; ?>
        <span><?php echo date('Y/m/d', strtotime($item->date_cr)); ?></span>
    </p>

    <div class="article_info">
        <div class="categories">
            <!-- <strong>Kategorie:</strong> -->
            <!-- <p><?php #echo $item->cat->name; ?></p> -->
            <a href="<?php echo $item->url; ?>" title="" class="read_more"><?php echo $item->postname; ?></a>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php endforeach; ?>