<?php
/* @var $this ArticlesController */
/* @var $model Articles */

?>

<div id="full_article">
        <p class="title"><?php echo $model->header; ?></p>
        <div class="article_info">
            <div class="categories">
                <strong>Autor:</strong>
                <p><?php echo $model->author; ?></p>
                <p class="date"><?php echo date('Y/m/d', strtotime($model->date_cr)); ?></p>
            </div>
            <!-- <div class="clear"></div> -->
        </div>

        <?php echo $model->content; ?>
</div>