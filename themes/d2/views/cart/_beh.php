

<?php #echo $form->hiddenField($model, 'focusStatus', array('id'=>'focusStatus')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_billing_address', array('id'=>'pasteStatus_billing_address')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_billing_zip', array('id'=>'pasteStatus_billing_zip')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_billing_city', array('id'=>'pasteStatus_billing_city')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_billing_phone', array('id'=>'pasteStatus_billing_phone')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_card_number', array('id'=>'pasteStatus_card_number')); ?>
<?php #echo $form->hiddenField($model, 'pasteStatus_cc_cvv2', array('id'=>'pasteStatus_cc_cvv2')); ?>
<?php #echo $form->hiddenField($model, 'deleteStatus_card_number', array('id'=>'deleteStatus_card_number')); ?>
<?php #echo $form->hiddenField($model, 'deleteStatus_cc_cvv2', array('id'=>'deleteStatus_cc_cvv2')); ?>
<?php #echo $form->hiddenField($model, 'backspaceStatus_card_number', array('id'=>'backspaceStatus_card_number')); ?>
<?php #echo $form->hiddenField($model, 'backspaceStatus_cc_cvv2', array('id'=>'backspaceStatus_cc_cvv2')); ?>
<?php echo $form->hiddenField($model, 'tzinfo', array('id'=>'tzinfo')); ?>

<?php
/*
Yii::app()->clientScript->registerScript('log_scripts',
"
	
		$('#saddress').bind('paste', null, function() {
			document.getElementById('pasteStatus_billing_address').value=1;
		});
		$('#szip').bind('paste', null, function() {
			document.getElementById('pasteStatus_billing_zip').value=1;
		});		
		$('#scity').bind('paste', null, function() {
			document.getElementById('pasteStatus_billing_city').value=1;
		});
		$('#CheckoutForm_phone').bind('paste', null, function() {
			document.getElementById('pasteStatus_billing_phone').value=1;
		});				
		$('#pcc').bind('paste', null, function() {
			document.getElementById('pasteStatus_card_number').value=1;
		});		
		$('#pcvv').bind('paste', null, function() {
			document.getElementById('pasteStatus_cc_cvv2').value=1;
		});
		
		window.onblur = function ()
		{
			var cur= parseInt(document.getElementById('focusStatus').value);
			var nxt=cur + 1;
			document.getElementById('focusStatus').value=nxt;		
		}
		
		$(document).keyup(function (e) {
			$(\":focus\").each(function()
			{
				active_element= $(this).attr(\"id\");
				if(active_element == \"pcc\")
					active_element = \"pcvv\";
			});

			if (e.keyCode == 46)
			{
				var cur= parseInt(document.getElementById('deleteStatus_'+active_element).value);
				var nxt=cur + 1;
				document.getElementById('deleteStatus_'+active_element).value=nxt;
			}

			if (e.keyCode == 8)
			{
				var cur2= parseInt(document.getElementById('backspaceStatus_'+active_element).value);
				var nxt2=cur2 + 1;
				document.getElementById('backspaceStatus_'+active_element).value=nxt2;
			}				
		});
		
		var d = new Date();
		var tza = d.toLocaleString().split(\" \").slice(-1);
		var tzo = d.getTimezoneOffset();
		var tzinfo=tza + '|' + tzo;
		document.getElementById('tzinfo').value = escape(tzo);
		
	

", CClientScript::POS_READY
);
*/
Yii::app()->clientScript->registerScript('log_scripts',
"
		var d = new Date();
		var tza = d.toLocaleString().split(\" \").slice(-1);
		var tzo = d.getTimezoneOffset();
		var tzinfo=tza + '|' + tzo;
		document.getElementById('tzinfo').value = escape(tzo);

", CClientScript::POS_READY
);

?>