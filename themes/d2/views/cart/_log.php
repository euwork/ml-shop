

<?php echo $form->hiddenField($model, 'log_browser_lang', array('id'=>'log_browser_lang', 'value'=>$_SERVER['HTTP_ACCEPT_LANGUAGE'])); ?>   
<?php echo $form->hiddenField($model, 'log_browser_reso', array('id'=>'log_browser_reso')); ?>   
<?php echo $form->hiddenField($model, 'log_colordepth', array('id'=>'log_colordepth')); ?>   
<?php echo $form->hiddenField($model, 'log_machine_type', array('id'=>'log_machine_type')); ?>   
<?php echo $form->hiddenField($model, 'log_sockip', array('id'=>'log_sockip')); ?>

<?php echo $form->hiddenField($model, 'log_useragent', array('id'=>'log_useragent')); ?>
<?php echo $form->hiddenField($model, 'log_plugins', array('id'=>'log_plugins')); ?>
<?php echo $form->hiddenField($model, 'log_fonts', array('id'=>'log_fonts')); ?>
<?php echo $form->hiddenField($model, 'log_mtime', array('id'=>'log_mtime')); ?>
<?php echo $form->hiddenField($model, 'log_os', array('id'=>'log_os')); ?>
<?php echo $form->hiddenField($model, 'log_jsversion', array('id'=>'log_jsversion')); ?>




<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/device/device.min.js', CClientScript::POS_HEAD);


Yii::app()->clientScript->registerScript('log_scripts',
'
	function get_js_version()
	{
	    this.jsv = {
		    versions: [
			"1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0"
		    ],
		    version: ""
		};

	    var d = document;

	    for (i = 0; i < jsv.versions.length; i++) {
		var g = d.createElement(\'script\'),
		    s = d.getElementsByTagName(\'script\')[0];

		    g.setAttribute("language", "JavaScript" + jsv.versions[i]);
		    g.text = "this.jsv.version=\'" + jsv.versions[i] + "\';";
		    s.parentNode.insertBefore(g, s);
	    }

	    return jsv.version;
	}
	
    var mtime = Date();
    
    function plug(name)
    {
	    this.name = name;
    }
    
    var plugins = new Array();
    var x=navigator.plugins.length;
    for(var i=0;i<x;i++)
    {
	    plugins.push(new plug(navigator.plugins[i].name));
    }
    plugins = JSON.stringify(plugins);
    
    function font(name)
    {
	    this.name = name;
    }
    function populateFontList(fontArr)
    {
	    fonts = JSON.stringify(fontArr);		
	    var fonts = new Array();
	    var x=fontArr.length;
	    for(var i=0;i<x;i++)
	    {
		    fonts.push(new font(fontArr[i]));
	    }
	    fonts = JSON.stringify(fonts);
	    document.getElementById(\'log_fonts\').value = fonts;		
    } 
    
  
	
    var mtype = \'desktop\';
    if(device.tablet() == true)mtype = \'tablet\';
    if(device.mobile() == true)mtype = \'mobile\';
    document.getElementById(\'log_browser_reso\').value = screen.width+" x "+screen.height;
    document.getElementById(\'log_colordepth\').value = screen.colorDepth;
    document.getElementById(\'log_machine_type\').value = mtype;
    
    document.getElementById(\'log_useragent\').value = navigator.userAgent;
    document.getElementById(\'log_plugins\').value = plugins;

    document.getElementById(\'log_mtime\').value = mtime;
    document.getElementById(\'log_os\').value = get_os();
    document.getElementById(\'log_jsversion\').value = "javascript "+get_js_version();
    
     function getFIP(str) 
      {   
        document.getElementById(\'log_sockip\').value = str;
      }
      
        function clientString(s, r)
	{
		this.s = s;
		this.r = r;
	}
	
function get_os()    {
var nAgt = navigator.userAgent;
         var os = "";
         var clientStrings = [
             {s:\'Windows 3.11\', r:/Win16/},
             {s:\'Windows 95\', r:/(Windows 95|Win95|Windows_95)/},
             {s:\'Windows ME\', r:/(Win 9x 4.90|Windows ME)/},
             {s:\'Windows 98\', r:/(Windows 98|Win98)/},
             {s:\'Windows CE\', r:/Windows CE/},
             {s:\'Windows 2000\', r:/(Windows NT 5.0|Windows 2000)/},
             {s:\'Windows XP\', r:/(Windows NT 5.1|Windows XP)/},
             {s:\'Windows Server 2003\', r:/Windows NT 5.2/},
             {s:\'Windows Vista\', r:/Windows NT 6.0/},
             {s:\'Windows 7\', r:/(Windows 7|Windows NT 6.1)/},
             {s:\'Windows 8.1\', r:/(Windows 8.1|Windows NT 6.3)/},
             {s:\'Windows 8\', r:/(Windows 8|Windows NT 6.2)/},
             {s:\'Windows NT 4.0\', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
             {s:\'Windows ME\', r:/Windows ME/},
             {s:\'Android\', r:/Android/},
             {s:\'Open BSD\', r:/OpenBSD/},
             {s:\'Sun OS\', r:/SunOS/},
             {s:\'Linux\', r:/(Linux|X11)/},
             {s:\'iOS\', r:/(iPhone|iPad|iPod)/},
             {s:\'Mac OS X\', r:/Mac OS X/},
             {s:\'Mac OS\', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
             {s:\'QNX\', r:/QNX/},
             {s:\'UNIX\', r:/UNIX/},
             {s:\'BeOS\', r:/BeOS/},
             {s:\'OS/2\', r:/OS\/2/},
             {s:\'Search Bot\', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
         ];
         for (var id in clientStrings) {
             var cs = clientStrings[id];
             if (cs.r.test(nAgt)) {
                 os = cs.s;
                 break;
             }
         }
         var osVersion = "";
         if (/Windows/.test(os)) {
             osVersion = /Windows (.*)/.exec(os)[1];
             os = \'Windows\';
         }
         switch (os) {
             case \'Mac OS X\':
                 osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                 break;

             case \'Android\':
                 osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                 break;

             case \'iOS\':
                 osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nAgt);
                 osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                 break;
         }

         return os+osVersion;
     }
', CClientScript::POS_END
);

?>


<?php if(false) : ?>
<object
      classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
      codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab"
      width=0
      height=0
  >
      <param name="movie" value="<?php echo  Yii::app()->theme->baseUrl.'/js/device/'; ?>myip.swf" />
      <embed src="<?php echo Yii::app()->theme->baseUrl.'/js/device/'; ?>myip.swf"
              play="true"
              loop="false"
              allowScriptAccess="always"
              type="application/x-shockwave-flash"
              pluginspage="http://www.macromedia.com/go/getflashplayer"
              width=0
              height=0
              >

      </embed>
</object>



<object 
		classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
		codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" 
		width=0 
		height=0
	>
      <param name="movie" value="<?php echo  Yii::app()->theme->baseUrl.'/js/device/'; ?>FontList1.swf" />
      <embed src="<?php echo  Yii::app()->theme->baseUrl.'/js/device/'; ?>FontList1.swf" 
		      play="true" 
		      loop="false" 
		      allowScriptAccess="always" 
		      type="application/x-shockwave-flash" 
		      pluginspage="http://www.macromedia.com/go/getflashplayer" 
		      width=0 
		      height=0
		      >
      </embed>
</object>
<?php endif; ?>