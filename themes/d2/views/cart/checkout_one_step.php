<?php

$this->pageTitle = Yii::t('checkout', 'Ihre Bestellung');
$this->keywords = Yii::t('checkout', 'Ihre Bestellung');
$this->description = Yii::t('checkout', 'Ihre Bestellung');
?>



   <div id="checkout-one-step">


        <h3 class="orange first"><?php echo Yii::t('checkout', 'Ihre Bestellung'); ?></h3>
    

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'orderform',
            'enableAjaxValidation'=>false,
            'action' => Yii::app()->getRequest()->getOriginalUrl(),
        )); ?>
    
            <?php echo $form->hiddenField($model, 'step', array('value'=>2)); ?>

            <?php echo $form->hiddenField($model, 'js_time_string', array()); ?>  

			<?php $this->renderPartial('_log', array('form'=>$form, 'model' => $model)); ?>
			<?php $this->renderPartial('_beh', array('form'=>$form, 'model' => $model)); ?>


            <div id="cntr_order_details">


				<div id="cart">
		          <div class="shopping_cart">
		            <table width="100%">
		              <thead>
		                <tr>
		                  <th class="col-1"><?php echo Yii::t('cart', 'Produkt'); ?></th>
		                  <th nowrap="nowrap" class="col-3" colspan="2"><?php echo Yii::t('drugs', 'Menge'); ?></th>
		                  <th nowrap="nowrap" class="col-5"><?php echo Yii::t('drugs', 'Preis'); ?></th>
		                </tr>
		              </thead>
		              <tbody>

		                <?php $positions = Yii::app()->shoppingCart->getPositions(); ?>
		                <?php $even=' even';foreach($positions as $prod): ?>
		                <?php
		                //$image = $prod->image;
		                $price = $prod->getSumPrice();
		                list($name, $quantity) = $prod->pills_data_format_checkout(); 

					    // $name = $prod->getDrugname() . ' ' . $prod->dose;
						// if($prod->isTestpacken()){
						// 	$quantity = $prod->getItemsDescription();
						// } else {
						// 	if($prod->bonus) {
						// 		$quantity = $prod->amount . ' + '. $prod->bonus . ' ' . $prod->getAmountIn();					
						// 	} else {
						// 		$quantity = $prod->amount . ' ' . $prod->getAmountIn();
						// 	}
						// }

		                ?>

		                <tr class="<?php echo $even;$even=($even==' even')?'':' even'; ?>">
		                  <td class="col-title">
		                    <span><?php echo $name; ?></span>
		                  </td>
		                  <td class="col-title" colspan="2">
		                    <?php echo $quantity; ?>
		                    <br>
		                    <font style="color: #ccc;"></font>
		                  </td>
		                  <td class="price">
		                    <strong><?php echo Currencies::symbol();?> <?php echo Currencies::price($price); ?></strong>
		                  </td>
		                </tr>
		                <?php endforeach; ?>
		              </tbody>

		              <tbody id="current_shipping_method"></tbody>

		              <tfoot>
		                <tr class="even">
		                  <td class="title">
		                    <span><?php echo Yii::t('checkout' , 'Gesamtbetrag'); ?>:</span>
		                  </td>
		                  <td class="dosage"></td>
		                  <td class="quantity"></td>
		                  <td class="price">
		                    <strong>
		                      <span id="order_amount"><?php echo Currencies::symbol();?> <?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?></span>
		                    </strong>
		                  </td>
		                </tr>
		              </tfoot>
		            </table>
		          </div>
		        </div>

		        
		        <input type="hidden" id="products_amount" value="<?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?>" />
				
				<div id="order" style="margin-right: 0px;">
					<h3 class="stepTitle title_prod orange"><span></span><?php echo Yii::t('checkout' , 'Versandart'); ?></h3>
		            <div id="stepBody3">
		                <div class="productTable">

		                    <table id="cart" style="margin-top:10px;">
		                        <tbody>
		                        <tr>
		                            <td colspan="3" id="toleft" style="border:none;" class="versandart">
										
										<?php $is_original = false; ?>
										<?php $shippings_list = Orders::getListShippingMethodPrices(); ?>
										<?php foreach($shippings_list as $shipping_method_id => $shipping_method_price) : ?>

		                                    <?php $checked =  false; ?>
		                                    <?php if($shipping_method_id == 2 ) $checked = true; ?>
		                                    <?php if(count($shippings_list) == 1) $checked = true; ?>
		                                   
											<div>

			                                    <?php echo $form->radioButton($model,'shipping_method',array('id'=>"shipping_method" . $shipping_method_id, 'class'=>'shipping_method validate-one-required', 'value'=>$shipping_method_id, 'checked'=> $checked,'uncheckValue' => null, 'onclick'=>"change_ship_method('shipping_method'+".$shipping_method_id.")")); ?>

			                                    <label id="lbl_shipping_method<?php echo $shipping_method_id; ?>" for="shipping_method<?php echo $shipping_method_id; ?>"><?php echo Yii::t('cart', Orders::getShippingNameById($shipping_method_id)); ?></label> - <?php echo Currencies::symbol();?><?php echo Currencies::price(Orders::getShippingPriceById($shipping_method_id, $is_original));?>&nbsp; 

			                                    <input id="sm_shipping_method<?php echo $shipping_method_id; ?>" type="hidden" value="<?php echo Currencies::price(Orders::getShippingPriceById($shipping_method_id, $is_original));?>" />

											</div>
		                                <?php endforeach; ?>


		                               
										<div>
			                                <?php echo $form->checkBox($model,'insurance',array('id'=>'insurance', 'checked'=>"checked", 'class'=>'',)); ?>
			                                
			                                <label id="l_insurance" for="insurance"><?php echo Yii::t('checkout' , 'Versicherung der Lieferung - {symbol}{price} (Rückverbringung garantiert, wenn die Ware nicht geliefert ist)', array('{symbol}'=>Currencies::symbol(), '{price}'=>Currencies::price(Orders::getInsurancePrice()))); ?></label>
			                                <input id="sm_insurance" type="hidden" value="<?php echo Currencies::price(Orders::getInsurancePrice()); ?>" />
										</div>
		                            </td>
		                        </tr>
		                        
		                        </tbody>
		                    </table>
		                    

		                </div>




		                

		            </div>

		        </div>



	            <h3 class="stepTitle orange"><span><?php echo Yii::t('checkout', 'Lieferadresse & Rechnungsadresse'); ?></span></h3>

                <div id="order" class="productTable">

                    <fieldset id="shipping_info">

                        <h3 class="shipping_title"><span><?php echo Yii::t('checkout', 'Lieferadresse'); ?></span></h3>


                        <div id="stepBody1">

                            <div class="line email">
                                <?php echo $form->labelEx($model,'email'); ?>
                                <?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>256, 'autocomplete'=>'off', 'class'=>'email required')); ?>
                                <span></span>
                            </div>

                        </div>

                        <div id="stepBody2">

                            <div id="shipping">
                                <div class="line rowColor1">
                                    <?php echo $form->labelEx($model,'shipp_fname'); ?>
                                    <?php echo $form->textField($model,'shipp_fname',array('id'=>'sfirst_name', 'minlength'=>2,'maxlength'=>256, 'class'=>'required')); ?>
                                    <span></span>
                                </div>

                                <div class="line rowColor0">
                                    <?php echo $form->labelEx($model,'shipp_lname'); ?>
                                    <?php echo $form->textField($model,'shipp_lname',array('id'=>'slast_name', 'minlength'=>2, 'maxlength'=>256, 'class'=>'required')); ?>
                                    <span></span>
                                </div>

                                <div class="line rowColor1">
                                    <?php echo $form->labelEx($model,'shipp_address'); ?>
                                    <?php echo $form->textField($model,'shipp_address',array('id'=>'saddress', 'minlength'=>3,'maxlength'=>256, 'class'=>'required')); ?>
                                    <span></span>
                                </div>

                                <div class="line rowColor0">
                                    <?php echo $form->labelEx($model,'shipp_zip'); ?>
                                    <?php echo $form->textField($model,'shipp_zip',array('id'=>'szip', 'maxlength'=>256, 'class'=>'required')); ?>
                                    <span></span> 
                                </div>

                                <div class="line rowColor1">
                                    <?php echo $form->labelEx($model,'shipp_city'); ?>
                                    <?php echo $form->textField($model,'shipp_city',array('id'=>'scity', 'maxlength'=>256, 'class'=>'required')); ?>
                                    <span></span>
                                </div>

                                <div class="line rowColor0">

                                    <?php echo $form->labelEx($model,'shipp_country'); ?>
                                    <?php echo $form->dropDownList($model,'shipp_country', CheckoutForm::getCountryList(), array('id'=>'scountry', 'options' => array(Languages::getDefaultCountry()=>array('selected'=>true)),)); ?>
                                    <span></span>
                                </div>

                                <div class="line shipp_state">
                                    <?php echo $form->labelEx($model,'shipp_state'); ?>
                                    <?php echo $form->dropDownList($model,'shipp_state', CheckoutForm::getStateList(), array( 'id'=>'sstate', 'options' => array(),)); ?>
                                    <span></span>
                                </div>
                                
                            </div>

                            <div class="line rowColor1">
                                <?php echo $form->labelEx($model,'phone'); ?>
                                <?php echo $form->textField($model,'phone',array('maxlength'=>256, 'class'=>'required')); ?>
                                <span></span> 
                            </div>

                        </div>


                    </fieldset>

                    <fieldset id="billing_info">
                        <h3 class="billing_title"><span><?php echo Yii::t('checkout', 'Rechnungsadresse'); ?></span></h3>
                        <div id="stepBody4">
                            <div class="line billing_as_shipping">
                                <input style="width: 20px;border: 0;" id="billing_as_shipping" type="checkbox" value="1" />
                                <label id="lbl_billing_as_shipping" for="billing_as_shipping">
                                    <?php echo Yii::t('checkout', 'Wenn Rechnungsadresse und Lieferadresse gleich sind, klicken Sie bitte hier'); ?></label>
                            </div>

                            
							<div id="billing">
                                <div class="line">
                                    <?php echo $form->labelEx($model,'bill_fname'); ?>
                                    <?php echo $form->textField($model,'bill_fname',array('id'=>'bfirst_name', 'minlength'=>2, 'maxlength'=>256, 'class'=>'required')); ?>
                                </div>

                                <div class="line">
                                    <?php echo $form->labelEx($model,'bill_lname'); ?>
                                    <?php echo $form->textField($model,'bill_lname',array('id'=>'blast_name', 'minlength'=>2, 'maxlength'=>256, 'class'=>'required')); ?>
                                
                                </div>

                                <div class="line">
                                    <?php echo $form->labelEx($model,'bill_address'); ?>
                                    <?php echo $form->textField($model,'bill_address',array('id'=>'baddress', 'minlength'=>3, 'maxlength'=>256, 'class'=>'required')); ?>
                                </div>

                                <div class="line">
                                    <?php echo $form->labelEx($model,'bill_zip'); ?>
                                    <?php echo $form->textField($model,'bill_zip',array('id'=>'bzip',  'maxlength'=>256, 'class'=>'required')); ?>
                                </div>

                                <div class="line">
                                    <?php echo $form->labelEx($model,'bill_city'); ?>
                                    <?php echo $form->textField($model,'bill_city',array('id'=>'bcity', 'maxlength'=>256, 'class'=>'required')); ?>
                                </div>

                                <div class="line rowColor0">
                                    <?php echo $form->labelEx($model,'bill_country'); ?>
                                    <?php echo $form->dropDownList($model,'bill_country', CheckoutForm::getCountryList(), array( 'id'=>'bcountry', 'options' => array(Languages::getDefaultCountry()=>array('selected'=>true)),)); ?>
                                    <span></span>
                                </div>

                                <div class="line bill_state">
                                    <?php echo $form->labelEx($model,'bill_state'); ?>
                                    <?php echo $form->dropDownList($model,'bill_state', CheckoutForm::getStateList(), array( 'id'=>'bstate', 'options' => array(),)); ?>
                                    <span></span>
                                </div>
                                

                            </div>
                            
                           
                        </div>
                    </fieldset>
                    <br clear="all" />
                </div>
				<!-- <br clear="all"/> -->

				<h3 class="stepTitle orange"><?php echo Yii::t('checkout' , 'Bitte Zahlungsart auswählen'); ?></h3>
				<div id="order" class="productTable">


		            
	                <?php

	                // if($is_original){
	                //     $list_payment_methods = Orders::getListPaymentMethodOriginal();
	                // } else {
	                    $list_payment_methods = Orders::getCurrentListPaymentMethodInternal();
	                // }

	                ?>

	                
	                	
	                		
            		<table style="width:100%;height:100%;" class="pm-options">
            			<tr>
            				<td class="pm-first-td" align="center" width="200" style="font-size:0.8em;">
			                    <?php echo Yii::t('checkout', 'Zahlungsart'); ?>
			                </td>
			       

			                
				                
						
							<td class="pm-second-td">
			                    <?php foreach($list_payment_methods as $value => $method_name): ?>
				                    <div class="line">
										<?php if(!isset($default_pm)) $default_pm = $value; ?>
				                        <?php echo $form->radioButton($model,'payment_method',array('id'=>"payment_type", 'class'=>'validate-one-required', 'value'=>$value, 'checked'=> ($value == $default_pm ? true : false),'uncheckValue' => null, )); ?>
					                    <?php if($value == 'ideal'): ?>
					                    	<?php echo "<img src='".Yii::app()->theme->baseUrl."/images/payment_method_icon/ideal-logo.svg' style='vertical-align: middle;width:37px;'>" ?>
				                    	<?php else: ?>
						                    <?php echo "<img src='".Yii::app()->theme->baseUrl."/images/payment_method_icon/".Yii::app()->language."/{$value}.jpg' style='vertical-align: middle;'>" ?>
										<?php endif; ?>
										<?php echo $method_name; ?>
										</br>
									</div>
				                <?php endforeach; ?>
				            </td>
                        </tr>
	                </table>             
	                
	                <div id="wire_info2">
				
	                </div>
	                

	                <div id="cc_info">
						<br>
	                    <div class="line">
	                        <label for="pcc"><?php echo Yii::t('checkout', 'Kreditkartennummer'); ?></label>
	                        <?php echo $form->numberField($model,'card_number',array('id'=>"pcc", 'maxlength'=>16, 'class'=>'required', 'pattern'=>'\d*', 'oninput'=>'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);')); ?>
	                        <?php if (false && $model->getError('card_number')) : ?>
	                            <span><?php  echo $model->getError('card_number'); ?></span>
	                        <?php endif; ?>
	                    </div>

	                    <div class="line">
	                        <label for="payment_exp_m"><?php echo Yii::t('checkout', 'Gültig bis'); ?></label>
	                        <?php echo $form->dropDownList($model,'card_month', CheckoutForm::getMonthList(), array('class'=>'required', )); ?>
	                        <?php echo $form->dropDownList($model,'card_year', CheckoutForm::getYearList(), array('class'=>'required', )); ?>
	                      
	                    </div>

	                    <div class="line" style="margin-bottom: 10px;">
	                        <label for="pcvv"><?php echo Yii::t('checkout', 'CVV2'); ?>
	                        <!-- <br> -->
	                        <!-- <span class="tooltip"><span class="question"><?php echo Yii::t('checkout', 'Was ist es?'); ?></span><em><i></i><span id="cvv-tooltip"></span></em></span> -->
	                        </label>
	                        <?php echo $form->numberField($model,'card_cvv',array('id'=>"pcvv", 'maxlength'=>4,'minlength'=>3, 'class'=>'required number', 'pattern'=>'\d*', 'onfocus'=>"jQuery('#what_is_cvv_code').show();", 'oninput'=>'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);')); ?>
	                        
	                    </div>

	                    <div class="line">
	                        <label for="what_is_cvv_code" style="display:none; ">&nbsp;</label>
	                        <div id="what_is_cvv_code" style="display:none; ">
	                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img-europe/cvv/cvv.jpg" />
	                            <div class="clear"></div>
	                        </div>

	                    </div>



	                    <div class="line" id="bc_birth" style="margin-bottom: 5px;">
	                            <label for="bc_birth"><?php echo Yii::t('checkout', 'Date of birth'); ?>
	                                <br>
	                                <span class="tooltip"><span class="question">Pourquoi ?</span><em><i></i>Vous devez avoir au moins 18 ans.</em></span>
	                            </label>
	                            
	                            <?php echo $form->textField($model,'bc_day',array('id'=>"bc_day", 'maxlength'=>2, 'class'=>'required number', 'placeholder' => 'jj', )); ?> <span class="slash">/</span>
	                            <?php echo $form->textField($model,'bc_month',array('id'=>"bc_month", 'maxlength'=>2, 'class'=>'required number', 'placeholder' => 'mm', )); ?> <span class="slash">/</span>
	                            <?php echo $form->textField($model,'bc_year',array('id'=>"bc_year", 'maxlength'=>4, 'class'=>'required number', 'placeholder' => 'aaaa', )); ?>
	                            
	                    </div>

	                </div>

	                

	                <div id="wire_info"></div>

				</div>



            </div>
			<?php if(Yii::app()->language == 'fr') : ?>
	            <p style="color:#666666;font-size:0.85em;padding-left:10px;">Cliquer sur <b>Payer</b> pour effectuer votre achat. Vérifiez que vos informations sont correctes.</p>
	        <?php endif; ?>
            
            <br />
		    <div id="checkout">
		        <input id="checkout" type="submit" value="<?php echo Yii::t('checkout', 'Bestellung abschließen'); ?>" class="btn_blank" style="font-size:14px;font-weight:bold;cursor: pointer;border: 0;margin: 5px auto;float:right;width:251px;text-align:center;" />
		    </div>
		    <br clear="all" /><br />
		    <fieldset id="agreement_info">
		        <h3 class="stepTitle orange"><?php echo Yii::t('checkout', 'Allg. Geschäftsbedingungen'); ?></h3>
		        <div id="stepBody6" class="productTable">
		            <?php echo $form->checkBox($model,'agreement',array('checked'=>"checked", 'class'=>'required', 'id'=>'agb')); ?>
		                    
		            <a style="color: #666666;font-size: 12px;" href="<?php echo Pages::getUrlById(6); ?>" target="_blank"><?php echo Yii::t('checkout', 'Allg. Geschäftsbedingungen: Ich erkläre hiermit, dass alle meine Angaben richtig sind und und ich folgende Erklärung gelesen habe und die Bedingungen verstehe.'); ?></a>
		        </div>
		    </fieldset>
		    <br clear="all" /><br />
        <?php $this->endWidget(); ?>
        <div class="clear"></div>
    

<br />
<br />


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/validation.js', CClientScript::POS_END);?>
<?php
Yii::app()->clientScript->registerScript('billing_as_shipping', "
    jQuery('#billing_as_shipping').click(function () {
        if (jQuery('input#billing_as_shipping:checkbox').attr(\"checked\") == \"checked\") {
			state_procedure($('select#scountry').attr('value'), 'bstate', 'div.line.bill_state');
            jQuery('div#shipping input,div#shipping select').each(function () {
                var sh_id = jQuery(this).attr('id');
                if (typeof sh_id != 'undefined') {
                    var sh_val = jQuery(this).val();
                    var bl_id = sh_id.slice(1);
                    jQuery('#b' + bl_id).val(sh_val);
                }
            });
        } else {
            jQuery('div#stepBody4 input').each(function () {
                jQuery(this).val('');
            });
        }
    });
", CClientScript::POS_END
);

$card_number_regex = $is_original ? '^[4].{15,16}$' : '^[345].{15,16}$';
$card_number_message = $is_original ? 'Nur Visa!' : 'Visa, Master oder Amex!';


Yii::app()->clientScript->registerScript('validation',
'
    jQuery.validator.addMethod(
        \'regexp\',
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );

    jQuery.validator.addMethod(
        \'nozeros\',
        function(value) {
            var re = new RegExp(\'[0]{5}\');
            return !re.test(value);
        },
        "Please check your input."
    );

    jQuery.validator.addMethod(
        \'nohyphen\',
        function(value) {
            var re = new RegExp(\'[\-]{2}\');
            return !re.test(value);
        },
        "Please check your input."
    );

    jQuery.validator.addMethod(
        "cardmonth", 
        function (value) {
            var today = new Date();
            var year = $("#CheckoutForm_card_year").val();
            if(year != today.getFullYear()) {
                return true;
            }
            if(value >= today.getMonth()+1) {
                return true;
            }
            return false;
        }, 
        "year required"
    );

   jQuery.validator.addMethod(
        "bc_birth", 
        function (value) {
            var today = new Date();
            var year = $("#bc_year").val();
            var month = $("#bc_month").val();
            var day = $("#bc_day").val();

            if(year < 1900){ return false;}
            if(month > 12 || month < 1) {return false;}
            
            count_days = new Date(year, month, 0).getDate();
            if(day > count_days || day < 1){return false;}
            

            var bc_date = new Date(year, month-1, day);

            var age = today.getFullYear() - bc_date.getFullYear();
            age = today.setFullYear(1972) < bc_date.setFullYear(1972) ? age - 1 : age;
            
            if(age >= 18) {
                return true;
            }
            return false;
        }, 
        "must be greater than 18"
    );
	jQuery.validator.addMethod(
		"valueNotEquals", 
		function(value, element, arg){
		  return arg !== value;
		 }, 
		 "Value must not equal arg."
	 );

    $("#orderform").validate({
        rules: {
            "CheckoutForm[phone]":{
                minlength: 7, regexp: \'^[-\+\(\) 0-9]+$\', nozeros: true, nohyphen: true,
            },
            "CheckoutForm[shipp_zip]":{
                minlength: 3, maxlength: 16 //, regexp: \'^[0-9]+$\'
            },
            "CheckoutForm[bill_zip]":{
                minlength: 3, maxlength: 16 //, regexp: \'^[0-9]+$\'
            },
            // "CheckoutForm[card_number]":{
            //     regexp: "'.$card_number_regex.'"
            // },
            "CheckoutForm[card_month]":{
                cardmonth: true
            },
            "CheckoutForm[card_cvv]": {
                regexp: "^[0-9]{3}$"
            },
            "CheckoutForm[shipp_country]":{
                valueNotEquals: "empty"
            },
            "CheckoutForm[bill_country]":{
                valueNotEquals: "empty"
            },
            // "CheckoutForm[bc_year]":{
            //     bc_birth: true
            // },
            // "CheckoutForm[bc_month]":{
            //     bc_birth: true
            // },
            // "CheckoutForm[bc_day]":{
            //     bc_birth: true
            // },
        },
        messages: {
            "CheckoutForm[email]": null,
            "CheckoutForm[shipp_fname]": null,
            "CheckoutForm[shipp_lname]": null,
            "CheckoutForm[shipp_address]": null,
            "CheckoutForm[shipp_zip]": null,
            "CheckoutForm[shipp_city]": null,
            "CheckoutForm[shipp_country]": null,
            "CheckoutForm[phone]": null,
            "CheckoutForm[bill_fname]": null,
            "CheckoutForm[bill_lname]": null,
            "CheckoutForm[bill_address]": null,
            "CheckoutForm[bill_zip]": null,
            "CheckoutForm[bill_city]": null,
            "CheckoutForm[bill_country]": null,
            "CheckoutForm[shipping_method]": null,
            "CheckoutForm[payment_method]": null,
            "CheckoutForm[agreement]": null,
            "CheckoutForm[card_number]": null,
            "CheckoutForm[card_cvv]": null,
            "CheckoutForm[card_month]": null,
            "CheckoutForm[bc_day]": null,
            "CheckoutForm[bc_month]": null,
            "CheckoutForm[bc_year]": null,
            "CheckoutForm[shipp_state]": null,
            "CheckoutForm[bill_state]": null
        }
    });

    $("#CheckoutForm_card_year").change(function(){
        $("#orderform").valid();
    });
', CClientScript::POS_END
);


?>


<?php


Yii::app()->clientScript->registerScript('payments', 
'
    jQuery(function () {
        var objPaymentSelect = jQuery("input#payment_type:checked");

        payment_type(objPaymentSelect.val());
        // objPaymentSelect.change(function () {
        //     payment_type(this.value);
        // });

		$(\'input:radio[id=payment_type]\').on(\'change\', function () {
		    payment_type(this.value);
		});

        var objInsuranceCheckbox = jQuery("#insurance");
        objInsuranceCheckbox.change(function () {
            shopping_cart();
        });

        shopping_cart();
    });

    function shopping_cart() {

        var curr_ship = jQuery(".shipping_method:checked");
        var curr_ship_id = curr_ship.attr("id");

        jQuery("#current_shipping_method").html(\'<tr><td class="title" colspan="3" style="text-indent: 0px;line-height: 17px;">\' + jQuery("#lbl_" + curr_ship_id).html() + \'</td><td class="price"><strong>'.Currencies::symbol().' \' + roundNumber(jQuery("#sm_" + curr_ship_id).val() * 1, 2) + \'</strong></td></tr>\');
        if(jQuery("#insurance").prop("checked")){
            jQuery("#order_amount").html("'.Currencies::symbol().' " + (roundNumber(jQuery("#products_amount").val() * 1 + jQuery("#sm_" + curr_ship_id).val() * 1 + jQuery("#sm_insurance").val() * 1, 2)));
        } else {
            jQuery("#order_amount").html("'.Currencies::symbol().' " + (roundNumber(jQuery("#products_amount").val() * 1 + jQuery("#sm_" + curr_ship_id).val() * 1, 2)));
        }

    }

    function payment_type(payment_type) {
        switch (payment_type) {
        case \'amex\':
        case \'visa\':
        case \'master\':
        case \'jcb\':
        case \'diners\':
        case \'bc\':
            jQuery(\'#wire_info\').hide();
            jQuery(\'#wire_info2\').hide();
            jQuery(\'#cc_info\').show();

            if (payment_type == \'bc\') {
                //jQuery(\'#bc_birth\').show();
            } else {
                jQuery(\'#bc_birth\').hide();
            }

            $(\'#pcc\').rules(\'remove\', \'regexp, creditcard\');
            $(\'#pcvv\').rules(\'remove\', \'regexp\');

            if (payment_type == \'amex\') {

                $(\'#pcvv\').rules(\'add\', {regexp: "^[0-9]{4}$"});
                $(\'#pcc\').rules(\'add\', { regexp: "^[3][0-9]{14}$"});
                $(\'#cvv-tooltip\').html("<span class=\"cardIconAmer\"></span>'. Yii::t('checkout', 'Für American Express, ist es die vierstellige Zahl auf der Vorderseite der Karte.') .'");


            } else {

                
                $(\'#pcvv\').rules(\'add\', {regexp: "^[0-9]{3}$"});
                $(\'#pcc\').rules(\'add\', { creditcard: true});
                $(\'#cvv-tooltip\').html("<span class=\"cardIcon\"></span>'. Yii::t('checkout', 'Für Mastercard, Visa oder Discover, sind dies die letzten drei Ziffern im Unterschriftsfeld auf der Rückseite der Karte.') .'");


                if(payment_type == \'visa\'){
                    $(\'#pcc\').rules(\'add\', { regexp: "^[4][0-9]{15}$"});
                } else if(payment_type == \'master\') {
                    $(\'#pcc\').rules(\'add\', { regexp: "^[5][0-9]{15}$"});
                } else if(payment_type == \'bc\') {
                    $(\'#pcc\').rules(\'add\', { regexp: "^[4][0-9]{15}$"});
                } else {
                    $(\'#pcc\').rules(\'add\', { regexp: "^[0-9]{16}$"});
                }
            } 

            break;



        case \'wire\':
            jQuery(\'#cc_info\').hide();
            jQuery(\'#wire_info\').show();
            if("'.Yii::app()->currencies->getValue().'" != "EUR"){
	            // jQuery(\'#wire_info2\').show();
            }
            break;

        default:
            jQuery(\'#wire_info\').hide();
            jQuery(\'#cc_info\').hide();
            break;
        }
    }

    function roundNumber(num, dec) {
        var result = (Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec)).toString();
        if (result.indexOf(".") == -1) result = result + ".";
        var num = dec - result.split(".")[1].toString().length;
        for (i = 0; i < num; i++) result = result + "0";
        return result;
    }

    function change_ship_method(el_id) {
        jQuery(\'#current_shipping_method\').html(\'<tr><td style="text-indent: 0px;line-height: 17px;" colspan="3" class="title">\' + jQuery("#lbl_" + el_id).html() + \'</td><td class="price"><strong>'.Currencies::symbol().' \' + roundNumber(jQuery("#sm_" + el_id).val() * 1, 2) + \'</strong></td></tr>\');
        if(jQuery("#insurance").prop("checked")){
            jQuery("#order_amount").html("'.Currencies::symbol().' " + (roundNumber(jQuery("#products_amount").val() * 1 + jQuery("#sm_" + el_id).val() * 1 + jQuery("#sm_insurance").val() * 1, 2)));
        } else {
            jQuery("#order_amount").html("'.Currencies::symbol().' " + (roundNumber(jQuery("#products_amount").val() * 1 + jQuery("#sm_" + el_id).val() * 1, 2)));
        }
    }

	

', CClientScript::POS_END
);


Yii::app()->clientScript->registerScript('js_time_string',
'
    $("#orderform").submit(function(){
        $("#orderform input[name=\"CheckoutForm[js_time_string]\"]").val(Date());
    });
', CClientScript::POS_END
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/const.states.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('state',
'
    state_procedure($("select#scountry").attr("value"), "sstate", "div.line.shipp_state");
    state_procedure($("select#bcountry").attr("value"), "bstate", "div.line.bill_state");
	$("select#scountry").on("change", function () {
		state_procedure(this.value, "sstate", "div.line.shipp_state");
	});
	$("select#bcountry").on("change", function () {
		state_procedure(this.value, "bstate", "div.line.bill_state");
	});

	function state_procedure(country, state_select, state_div)
	{
		state_clean(state_select);
		if (FORM_STATES[country] != undefined)
        {
            state_load(state_select, country);
            $("#"+state_select).rules("add", {required: true});
            $(state_div).show();
        } else {
        	$("#"+state_select).rules("remove", "required");
        	$(state_div).hide();
        }		
	}

	function state_clean(id)
	{
	    $("#"+ id + " option").each(function () {
	        $(this).remove();
	    });
	    $("#"+ id).append("<option value=\"\" selected=\"selected\"></option>");
	}

	function state_load(id, c)
	{
	    for (var item in FORM_STATES[c])
	    {
	        $("#"+ id).append("<option value=\"" + item + "\">" + FORM_STATES[c][item] + "</option>");
	    }
	}

', CClientScript::POS_END
);


?>




<div style="clear: both; overflow: hidden;">&nbsp;</div>


</div>