<?php

$this->pageTitle = Yii::t('cart', 'Warenkorb');
$this->keywords = Yii::t('cart', 'Warenkorb');
$this->description = Yii::t('cart', 'Warenkorb');
?>




    <h1><span></span><?php echo Yii::t('cart', 'Warenkorb'); ?></h1>


    <div class="productTable order">
        <div class="table">
            <table id="cart">
                <thead>
                    <tr>
                        <th nowrap="nowrap" class="col-1"><?php echo Yii::t('cart', 'Produkt');?> </th>
                        <th nowrap="nowrap" class="col-1 menge"><?php echo Yii::t('drugs', 'Menge');?> </th>
                        <th nowrap="nowrap" class="col-5"><?php echo Yii::t('drugs', 'Preis');?> </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php //print_r(Yii::app()->shoppingCart->toArray()); ?>
                <?php $positions = Yii::app()->shoppingCart->getPositions();?>
                <?php $i=1;foreach($positions as $prod): ?>
	                <?php
					$image = $prod->image;
					$price = $prod->getSumPrice();
                    
                    // list($name, $dose, $quantity) = $prod->pills_data_format_cart();
                    if($prod->isTestpacken()){
                        $name = $prod->getDrugname() . ' ' . $prod->dose;
                        $dose = '<p style="font-size: 12px;">' . $prod->getItemsDescription() . '</p>';
                        $quantity = '1 ' . Yii::t('products', 'Packung');
                    } else {
                        $name = $prod->getDrugname();
                        $dose = $prod->dose;
                        if($prod->bonus) {
                            $quantity = $prod->amount . ' + '. $prod->bonus . ' ' . $prod->getAmountIn();                   
                        } else {
                            $quantity = $prod->amount . ' ' . $prod->getAmountIn();
                        }
                    }
					


	                ?>


                    <tr class="">
                        <td class="col-title"><span><?php echo $name; ?> <?php echo $dose; ?></span></td>
                        <td class="col-title">
                            <?php echo $quantity; ?>                        <br />
                            <font style="color: #555;"></font>
                        </td>
                        <td class="price"><strong><?php echo Currencies::symbol();?> <?php echo Currencies::price($price); ?></strong></td>
                        <td class="addToCart">
                            <a href="<?php echo Yii::app()->createUrl("/cart/remove/".$prod->id); ?>" class="remove">
                                <?php echo Yii::t('cart', 'Zum Warenkorb hinzufügen'); ?>
                            </a>
                        </td>
                    </tr>
                                                


                    <?php if(!$prod->use_discount): ?>
                        <?php if($nextpack = Products::getNextPack($prod)) : ?>
                        <?php
                                 $tabl_count = $nextpack->amount + $nextpack->bonus - $prod->amount - $prod->bonus;
                                 $tabl_price = number_format($nextpack->price - $nextpack->price * $nextpack->discount / 100 - $prod->price);
                                 $tabl_price_one = number_format($tabl_price / $tabl_count, 2);
                        ?>

                        <tr>
                            <td colspan="4" style="padding-left: 0;">
                                <div class="orderExtra">
                                    <div class="supersize">  
                                    <?php if(!in_array( Yii::app()->language, array() )): ?>                              
                                        <div class="h4"><?php echo Yii::t('cart', 'Jetzt Rabatt sichern!'); ?></div>
                                    <?php endif; ?>
                                        <div class="h5">
                                            <?php echo Yii::t('cart', 
                                                    'Wähle {tabl_count} extra Pillen für {symbol} {tabl_price} ({symbol}{tabl_price_one} per Pille)', 
                                                array(  '{tabl_count}'=>$tabl_count, 
                                                        '{tabl_price}'=>Currencies::price($tabl_price), 
                                                        '{tabl_price_one}'=>Currencies::price($tabl_price_one),
                                                        '{symbol}'=>Currencies::symbol(),

                                                        )); ?>
                                        </div>
                                        <a class="submit" href="<?php echo Yii::app()->createUrl("/cart/upgrade/".$prod->id); ?>"><?php echo Yii::t('cart', 'Aktualisieren'); ?></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endif; ?>
                    <?php endif; ?>
				<?php endforeach; ?>
               
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="4">
                            <p>
                                <span style="float:left;"><?php echo Yii::t('cart', 'Gesamtbetrag'); ?>:</span>
                                <strong style="float:right;">&nbsp;<?php echo Currencies::symbol();?> <?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?></strong>
                            </p>
                            <br style="clear: both;"/>
                            <a class="btn_b cart_btn_first" style="float:left;" href="<?php echo Yii::app()->createUrl("/"); ?>">
                                <?php echo Yii::t('cart', 'Mit Einkauf fortfahren'); ?> </a>
                            <a class="btn cart_btn_second" style="float:right;" href="<?php echo $this->billing_internal_url; ?>">
                                <?php echo Yii::t('cart', 'Zur Kasse'); ?> </a>
                        </td>
                    </tr>
                </tfoot>

            </table>
           
        </div>


     
        
    </div>
