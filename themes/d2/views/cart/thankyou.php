<?php

$this->pageTitle = Yii::t('thankyou', 'Vielen Dank für Ihre Bestellung');
$this->keywords = '';
$this->description = '';
?>
<?php if(Config::getOrderFormInOneStep() != true) : ?>
<div id="div-orderpage-levels">
    <ul class="orderpage-levels level-3">
        <li><?php echo Yii::t('checkout', 'Versand & Rechnungsanschrift'); ?></li>
        <li><?php echo Yii::t('checkout', 'Versand & Zahlungsart wählen'); ?></li>
        <li class="active"><?php echo Yii::t('checkout', 'Zusammenfassung'); ?></li>
    </ul>
</div>
<?php endif; ?>
<br />

<span class="h3"><?php echo Yii::t('thankyou', 'Vielen Dank für Ihre Bestellung'); ?></span> 





<div id="toPrint">
 

    <div id="productTable">
        <p class="invoice">
            <label><?php echo Yii::t('thankyou', 'Bestellnummer'); ?></label> <span><?php echo $model->partid;?></span></p>
        <?php $i =0; foreach($model->order_products as $prod): ?>
        <p class="invoice">
            <label><?php echo $i === 0 ? Yii::t('thankyou', 'Bestellte Produkte') : '&nbsp;'; $i++; ?></label>
            <?php $price = number_format($prod->price - $prod->price * $prod->discount / 100, 2); ?>
            <span>1 <b>x</b> <?php echo $prod->product->getDrugname() . ' ' . Currencies::price($price);?> <?php echo Currencies::code();?></span></p>
        <?php endforeach; ?>
        <p class="invoice">
            <label><?php echo Yii::t('thankyou', 'Versendungsart'); ?></label> <span><?php echo Yii::t('checkout', $model->getItemShippingName())  .' '. Currencies::price($model->shipping_price); ?> <?php echo Currencies::code();?></span></p>
        <?php if($model->insurance > 0): ?>
        <p class="invoice">
            <label><?php echo Yii::t('thankyou', 'Versicherung'); ?></label> <span><?php echo Currencies::price($model->insurance); ?> <?php echo Currencies::code();?></span></p>
        <?php endif; ?>
        <p class="invoice">
            <?php $total = number_format($model->subtotal + $model->shipping_price + $model->insurance, 2); ?>
            <label><?php echo Yii::t('checkout', 'Gesamtbetrag'); ?></label> <span><?php echo Currencies::price($total); ?> <?php echo Currencies::code();?> </span></p>
        <p class="invoice">
            <label><?php echo Yii::t('checkout', 'Name'); ?></label> <span><?php echo CHtml::encode($model->shipp_fname . ' ' . $model->shipp_lname); ?></span></p>
        <p class="invoice">
            <label><?php echo Yii::t('checkout', 'E-Mail'); ?></label> <span><?php echo CHtml::encode($model->email); ?></span></p>
        <p class="invoice">
            <label><?php echo Yii::t('thankyou', 'Telefonnummer'); ?></label> <span><?php echo $model->phone; ?></span></p>
        <p class="invoice">
            <label><?php echo Yii::t('checkout', 'Land'); ?></label> <span><?php echo CheckoutForm::getNameByCode($model->shipp_country); ?></span></p>
        <?php if($model->shipp_state != false): ?>
            <p class="invoice">
            <label><?php echo Yii::t('checkout', 'State'); ?></label> <span><?php echo Country::getNameByCode($model->shipp_state); ?></span></p>
        <?php endif;?>
        <p class="invoice">
            <label><?php echo Yii::t('checkout', 'Ort'); ?></label> <span><?php echo CHtml::encode($model->shipp_city); ?></span></p>
        <p class="invoice">
            <label><?php echo Yii::t('checkout', 'Strasse / Hausnr'); ?></label> <span><?php echo CHtml::encode($model->shipp_address); ?></span></p>

    </div>


<?php
if($model->payment_method == Orders::PAYMENT_WIRE): 

    if(in_array(Yii::app()->language, array('de', 'fi', 'da', 'nl', 'el', 'pl', 'et', 'no'))) {
        $bank_name_and_adress = '';
    } else {
        $bank_name_and_adress = '
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Bank').'</label> <span>'.Config::getWireData('wire.name').' </span></p>
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Adresse').'</label> <span>'.Config::getWireData('wire.adress').'</span></p>
        ';
    }

    $ziiland = '
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Zielland').'</label> <span>'.CheckoutForm::getNameByCode(Config::getWireData('wire.country')).'</span></p>
    ';
    $ziiland = '';

    $wire = '
                <div class="bankdetails" id="productTable">
                    <p id="bankdetail-header">'.Yii::t('thankyou', 'Bankverbindung').'</p>
                    '.$ziiland.'
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Empfänger').'</label> <span>'.Config::getWireData('wire.empf').'</span></p>
                    '.$bank_name_and_adress.'
                    <p class="invoice"><label>'.Yii::t('thankyou', 'BIC').'</label> <span>'.Config::getWireData('wire.bic').'</span></p>
                    <p class="invoice"><label>'.Yii::t('thankyou', 'IBAN').'</label> <span>'.Config::getWireData('wire.iban').' </span></p>
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Verwendungszweck').'</label> <span>'.Yii::t('thankyou', 'Bestellnummer').' <b>'.$model->partid.'</b></span></p>
                    <p class="invoice"><label>'.Yii::t('thankyou', 'Betrag').'</label> <span>'.Currencies::price($total).' '.Currencies::code().' </span></p>

                 </div>
    ';

    $text->content = str_replace('{wire_details}', $wire, $text->content);

endif;

?>    
    


<?php echo $text->content; ?>
    



</div>

<div id="print">
    <input type="button" onclick="printBlock('toPrint');" value="<?php echo Yii::t('thankyou', 'Drucken');?>" class="btn" style="border: 0;" />
</div>

<?php
Yii::app()->clientScript->registerScript('drucken', '
    function printBlock(id_block) {
        var printContent = document.getElementById(id_block);
        var sFeatures = "height=200,width=300,status=no,toolbar=no,menubar=no,location=no";
        win = window.open("", "Print", sFeatures);
        win.document.write(printContent.innerHTML);
        win.document.close();
        win.focus();
        win.print();
        win.close();
    }
', CClientScript::POS_END
);?>
<div style="clear: both; overflow: hidden;">&nbsp;</div>

