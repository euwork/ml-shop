

<?php

// все таблы в переменную, в контенте макрос {products}
$products = array();
foreach(Drugs::atCategory($model->cat_id) as $item):

    $products[] = 
    '<div class="product" onclick="document.location.href=\'' . $item['url'] . '\';">
	    <img src="'. $item['images'][DrugsImages::TYPE_TEXT] . '" alt="" />
	    <img src="'. $item['images'][DrugsImages::TYPE_CAT] . '" alt="" />
        <a href="' . $item['url'] . '" title="' . Yii::t('categories', 'Jetzt kaufen') . ' ' . $item['name'] . '" class="btn_blank" style="margin: 0 auto;">' . Yii::t('categories', 'Jetzt kaufen') . '</a>
    </div>';

endforeach; 

$products_html = implode("\n", $products);
$products_html .= "\n<div class=\"clear\"></div>\n";

?>


<div id="topProducts">
	<h1><?php echo $model->header; ?></h1>
	<?php $content = str_replace('{products}', $products_html, $model->content); ?>
	<?php echo $content; ?>
	<div class="clear"></div>
</div>
