<?php
/* @var $this CategoriesArticlesController */
/* @var $data CategoriesArticles */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('acat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->acat_id), array('view', 'id'=>$data->acat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>