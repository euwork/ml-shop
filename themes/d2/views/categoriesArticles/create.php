<?php
/* @var $this CategoriesArticlesController */
/* @var $model CategoriesArticles */

$this->breadcrumbs=array(
	'Categories Articles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CategoriesArticles', 'url'=>array('index')),
	array('label'=>'Manage CategoriesArticles', 'url'=>array('admin')),
);
?>

<h1>Create CategoriesArticles</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>