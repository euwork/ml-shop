<?php
/* @var $this CategoriesArticlesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Categories Articles',
);

$this->menu=array(
	array('label'=>'Create CategoriesArticles', 'url'=>array('create')),
	array('label'=>'Manage CategoriesArticles', 'url'=>array('admin')),
);
?>

<h1>Categories Articles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
