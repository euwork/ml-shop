<?php
/* @var $this CategoriesArticlesController */
/* @var $model CategoriesArticles */

$this->breadcrumbs=array(
	'Categories Articles'=>array('index'),
	$model->name=>array('view','id'=>$model->acat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CategoriesArticles', 'url'=>array('index')),
	array('label'=>'Create CategoriesArticles', 'url'=>array('create')),
	array('label'=>'View CategoriesArticles', 'url'=>array('view', 'id'=>$model->acat_id)),
	array('label'=>'Manage CategoriesArticles', 'url'=>array('admin')),
);
?>

<h1>Update CategoriesArticles <?php echo $model->acat_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>