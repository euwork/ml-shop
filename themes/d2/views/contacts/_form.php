<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacts-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
    'action' => Yii::app()->getRequest()->getOriginalUrl(),
	'enableAjaxValidation'=>false,
)); ?>
<?php //echo $form->errorSummary($model); ?>
    <fieldset>
        <div class="line">
        	<?php echo $form->labelEx($model,'name'); ?>

            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
        </div>
        <div class="line">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256, )); ?>
        </div>
        <div class="line">
			<?php echo $form->labelEx($model,'thema'); ?>
			<?php echo $form->textField($model,'thema',array('size'=>60,'maxlength'=>256)); ?>
        </div>
        <div class="line">
			<?php echo $form->labelEx($model,'content', array('class' => 'textarea_label')); ?>
			<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        </div>

		<?php if(CCaptcha::checkRequirements() ):?>
        <div class="line captcha-line">
            <label for="contact_captcha" class="hidden">&nbsp;</label>
            <div class="captcha-line__title"><?php echo Yii::t('contacts', 'Geben Sie die Zeichen in dem Bild ein'); ?></div>
            <?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>256)); ?>
            <?php $this->widget('CCaptcha', array('showRefreshButton'=>false, 'clickableImage'=>true)); ?>
            <span class="errow"></span> 
        </div> 

        
		<?php endif?>
        <a class="btn" href="#" onclick="document.getElementById('contacts-form').submit(); return false;"><span><?php echo Yii::t('contacts', 'Senden'); ?></span></a> 
    </fieldset>
<?php $this->endWidget(); ?>