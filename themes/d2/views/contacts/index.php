<?php
$this->pageTitle = Yii::t('contacts', 'Kontakt');
$this->keywords = Yii::t('contacts', 'Kontakt');
$this->description = Yii::t('contacts', 'Kontakt');

$mediasuppExt = 'png';

?>

<div class="mob-rban">
    <img src="<?php echo Theme::getImgUrlTemplate();?>/languages/<?php echo Yii::app()->language; ?>/media-supp.<?php echo $mediasuppExt; ?>" style="padding:0;"  class="rban" />
</div>

<div class="contact">
    <h2 class="title_prod"><span></span><?php echo Yii::t('contacts', 'Kontakt'); ?></h2> 
    <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash flash-' . $key . '">' . $message . "</div>\n";
        }
    ?>
	<div id="identifyingDetails">
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
    <br />
    <br />
    <p></p>

</div>
<div style="clear: both; overflow: hidden;">&nbsp;</div>