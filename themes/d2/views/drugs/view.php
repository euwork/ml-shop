<?php


$domain = $this->domain;


Yii::app()->clientScript->registerScript('rating-info-mobile',

'
    $("a.txt_star").click(function() {
        $(".rating_info").show();
        return false;
    });
    $(\'.stars_box+div+div\').click(function(e){
     $(\'.rating_info\').hide();
    });

    // $(\'.rating_info\').click(function(e){
    //  e.stopPropagation();
    // });
    // $(document).click(function(e){
    // if ($(e.target).closest(".rating_info").length) return;
    //     $(\'.rating_info\').hide();
    //     e.stopPropagation();
    // });

', CClientScript::POS_END);

?>


<h1 class="prod_title"><?php echo $model->header; ?></h1>

<?php if (Yii::app()->config->get('use_reviews') &&  $model->rating): ?>
    <div class="stars_box">
            <div id="main_avg_rate_div" class="stars_img_box">
                <span class="stars_img" style="width:<?php echo $model->getRatingPrc();?>%;">&nbsp;</span> 
            </div>
          <a class="txt_star" href="<?php echo $model->dt->url; ?>">
            <span class="block_txt_rate">
                <span class="text_star"><?php echo $model->getRating();?></span> /
                <span class="text_star"><?php echo $model->getReviewsCount();?></span> 
            </span>
            <?php echo Yii::t('drugs', 'Kundenrezensionen'); ?></a>
           
            <div class="rating_info">
                <?php $count_drug_reviews = $model->getReviewsCount(); ?>
                <b><?php echo $count_drug_reviews;?>  <?php echo Yii::t('drugs', 'Rezensionen'); ?></b>
                <?php $i = 5; $j =1; while($j < 6) : ?>
                    <div class="line_rating">
                        <span> 
                            <?php echo $i . ' '; 
                            if($i == 1) : 
                                echo Yii::t('drugs', 'Stern:');
                            elseif($i==2 && in_array(Yii::app()->language, array('sl'))):
                                echo Yii::t('drugs', '2Sterne:');
                            elseif($i==5 && in_array(Yii::app()->language, array('pl','sr', 'sk', 'sl', 'hr', 'cs'))):
                                echo Yii::t('drugs', '5Sterne:');
                            else: 
                                echo Yii::t('drugs', 'Sterne:');
                            endif; ?>
                        </span> 
                        <div class="box_rating_grey">
                            <div class="box_rating_yellow" style="width: <?php echo $model->getRatingStarPrc( $i) ;  ?>%;">  &nbsp;</div>
                        </div>
                      <span>(<?php echo $model->getRatingStar($i);$j++;  ?>)</span>
                    </div>
                    <?php $i--;?>
                <?php endwhile; ?>
                <a href="<?php echo $model->dt->url; ?>"> <?php echo Yii::t('drugs', 'Alle {count} Bewertungen anzeigen...', array('{count}'=>$model->getReviewsCount())); ?></a> 
            </div>
    </div>
<?php endif;?>


<div style="clear:both; overflow: hidden;">&nbsp;</div>

<div>
    <img align="right" class="product_image_border" src="<?php echo  $model->getImage(); ?>" alt="<?php echo $model->drugname; ?>" />
    <?php echo $model->content;?>
</div>

<div style="clear:both; overflow: hidden;">&nbsp;</div>


<?php 
$data = array();

foreach($model->products as $prod)
{
	$data[] = $prod->dose;
}
$tabs_doses = array_unique($data);

if($model->cat_id == 2) {
	$tabs_doses = array('PRODUCTS');
}
?>

<?php foreach($tabs_doses as $dose): ?>

        <?php if($model->cat_id == 2): ?>
            <!-- <h2 class="title"><?php echo $dose; ?></h2> -->
        <?php else: ?>
            <h2 class="title"><?php echo $model->getDrugname() .' '.$dose; ?></h2>
        <?php endif; ?>

		<?php if($model->cat_id == 2): ?>
		<?php $i='even'; foreach($model->products as $prod): ?>
		<h2 class="title"><?php echo $prod->getDrugname() . ' ' . $prod->dose; ?></h2> 
        <br />
        <div class="productTable">
            <table id="products">
                <thead>
                    <tr>
                        <th class="col-1 testpack" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Produkt');?> </b></th>
                        <th class="col-1-testpack" nowrap="nowrap"><b>&nbsp;</b></th>
                        <?php if(Config::priceOldEnabled()) : ?>
                        <th class="col-2-price-old testpack" nowrap="nowrap"><b></b></th>
                        <?php endif; ?>
                        <th class="col-2-price" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Preis');?> </b></th>
                        <th class="col-2" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Jetzt kaufen');?> </b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="<?php echo $i;$i=($i=='even')?'':'even'; ?>">
                        <td class="col-title"><span><?php echo $model->getDrugname();?></span>
                        </td>
                        <td class="col-title-testpack"><?php echo $prod->getItemsDescription();?></td>
                        <?php if(Config::priceOldEnabled()) : ?>
                            <td class="col-price-old testpack"><del><?php echo Currencies::symbol();?><?php echo Currencies::price($prod->getPriceOld());?></del></td>
                        <?php endif; ?>
                        <td class="col-price"><strong><?php echo Currencies::symbol();?><?php echo Currencies::price($prod->getPrice());?></strong>
                        </td>
                        <td class="addToCart" nowrap="nowrap"> 
                            <a class="buy" style="cursor:pointer;" 
                                onclick="document.location.href ='<?php echo Yii::app()->createUrl('/cart/add/' . $prod->prod_id); ?>';return false;">
                                <?php echo Yii::t('drugs', 'kaufen'); ?>
                            </a> 
                        </td>
                        
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>
        <?php endforeach;?>
		<?php else: ?> 
        
        <div class="productTable">
            <table id="products">
                <thead>
                    <tr>
                        <th class="col-1" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Produkt');?></b></th>
                        <th class="col-2-dosage" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Dosierung');?></b></th>
                        <th class="col-2-menge" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Menge + Bonus');?></b></th>
                        <?php if(Config::priceOldEnabled()) : ?>
                        <th class="col-2-price-old" nowrap="nowrap"><b></b></th>
                        <?php endif; ?>
                        <th class="col-2-price" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Preis');?></b></th>
                        <th class="col-2" nowrap="nowrap"><b><?php echo Yii::t('drugs', 'Jetzt kaufen');?></b></th>
                    </tr>
                </thead>
                <tbody>
            	<?php $i='even'; foreach($model->products as $prod): ?>
            	<?php if($prod->dose != $dose) continue; ?>
                    <tr class="<?php echo $i;$i=($i=='even')?'':'even'; ?>">
                        <td class="col-title"><span><?php echo $model->getDrugname();?></span></td>
                        <td class="col-dosage"><span><?php echo $prod->dose;?></span></td>
                        <?php 

                            $bonus = $prod->bonus ? '+ '.$prod->bonus . ' '   : ''; 
                            $t_amount_in = Yii::t('products', $prod->amount_in);
                            if(in_array(Yii::app()->language, array('sl','sr','hr'))  && $prod->amount_in == 'Pillen'  ){
                                if($prod->bonus == 2 or $prod->bonus == 4){
                                    $t_amount_in = Yii::t('products', $prod->bonus.$prod->amount_in);
                                }
                            }

                        ?>
                        <td class="col-quantity"><?php echo $prod->amount . ' ' . $bonus . $t_amount_in;?></td>
                        
                        <?php if(Config::priceOldEnabled()) : ?>
                            <td class="col-price-old"><del><?php echo Currencies::symbol();?><?php echo Currencies::price($prod->getPriceOld());?></del></td>
                        <?php endif; ?>
                        <td class="col-price"><strong><?php echo Currencies::symbol();?><?php echo Currencies::price($prod->getPrice());?></strong></td>
                        <td class="addToCart" nowrap="nowrap"> 
                            <a class="buy" style="cursor:pointer;" 
                                onclick="document.location.href ='<?php echo Yii::app()->createUrl('/cart/add/' . $prod->prod_id); ?>';return false;">
                                    <?php echo Yii::t('drugs', 'kaufen'); ?>
                            </a> 
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
  
<?php endforeach;?>
    <?php if($model->instruction): ?>
	<div id="med">
        <?php echo $model->instruction;?>
	</div>
	<?php endif; ?>

<?php $reviews = $model->getReviewsForDrugPage(); ?>
<?php if(Yii::app()->config->get('use_reviews') && count($reviews) > 0): ?>
<div class="domtab">
    <div class="top_box">&nbsp;</div>
    <div class="box_txt">
        <h2><a><?php echo $model->dt->subheader; ?></a></h2>

        <?php foreach($reviews as $item) : ?>

        <?php if($item->content == '') continue; ?> 

        <div class="txt_commentsbox">
            <span class="helpfull_info" id="rev_<?php echo $item->rev_id; ?>">
                <?php echo Yii::t('drugs', 
                    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich', 
                    array('{vote_yes}'=> '<span id="vote_yes">' . $item->vote_yes . '</span>', '{vote_all}'=> '<span id="vote_all">' . $item->vote_all . '</span>')); ?>
            </span>
            <div class="stars_img_box">
                <?php $item_rating = number_format($item->points / 5 * 100, 2); ?>
                <span class="stars_img" style="width:<?php echo $item_rating;?>%;"> &nbsp;</span>
            </div>
            <div class="current_box">

                <span class="current_review_points"><?php echo $item->points; ?></span>/<span>5</span> 
            </div>
            <div class="txt_box_date">
                <?php if($item->tit_name): ?>
                    <span class="tit_name">"<?php echo str_replace('{domain}', $domain, $item->tit_name); ?>"</span>
                <?php endif; ?>
                <span><?php //echo date('d-m-Y', strtotime($item->date_cr)); ?></span>
            </div>
            <div class="author_box">
                <?php if(in_array(Yii::app()->language, array('hu', 'et', 'tr'))): ?>
                    <span><?php echo $item->author; ?></span> <?php echo Yii::t('drugs', 'von'); ?>
                <?php elseif(in_array(Yii::app()->language, array('fi'))): ?>
                    <span><?php echo $item->author; ?><?php #echo Yii::t('drugs', 'von'); ?></span> 
                <?php else: ?>
                    <?php echo Yii::t('drugs', 'von'); ?> <span><?php echo $item->author; ?></span>
                <?php endif;?>
            </div>

            <span class="txt_comment"><?php echo str_replace('{domain}', $domain, $item->content); ?></span>
            <div class="info_commentsbox" rev-id="<?php echo $item->rev_id; ?>">
                <a href="#" rel="nofollow" class="red" onclick="js: return false;"><?php echo Yii::t('drugs', 'nein'); ?></a>
                <a href="#" rel="nofollow" class="green" onclick="js: return false;"><?php echo Yii::t('drugs', 'ja'); ?></a>&nbsp;&nbsp;
                <span><?php echo Yii::t('drugs', 'Hat Ihnen diese Bewertung geholfen?'); ?>&nbsp;&nbsp;</span>
            </div>
        </div>

        <?php endforeach; ?>
    </div>



</div>
<?php endif; ?>

<?php
Yii::app()->clientScript->registerScript('vote',
"
$('.info_commentsbox a').click(function(){

    var rev = $(this).closest('div.info_commentsbox').attr('rev-id');
    var vote = $(this).attr('class') == 'green' ? 1 : 0;
    var url = '" . Yii::app()->createUrl('/testimonials/vote') . "';

    $.ajax({
      type: \"POST\",
      url: url,
      data: {'rev': rev, 'vote': vote},
      success: function(data){       
        $('.helpfull_info#rev_'+rev+'>span#vote_yes').html(data.vote_yes);
        $('.helpfull_info#rev_'+rev+'>span#vote_all').html(data.vote_all);
      },
      dataType: 'json'
    });


    return false;
});


", CClientScript::POS_END);

?>