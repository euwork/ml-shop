<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'review-form',
    'enableAjaxValidation'=>false,
    'action' => Yii::app()->getRequest()->getOriginalUrl() . '#newtestimonial',
)); ?>
        <ul class="commentsbox">

            <li class="form_first">
                <fieldset>
                    <legend><?php echo Yii::t('drugs', 'Rezension abgeben'); ?></legend>
                    <div class="name_li div_li">
                        <label><?php echo Yii::t('drugs', 'Name'); ?>:</label>
                        <?php echo $form->textField($model,'author',array('id'=>'rev_name', 'maxlength'=>256,)); ?>
                    </div>
                    <div class="div_li">
                        <label><?php echo Yii::t('drugs', 'Email'); ?>:</label>
                        <input type="text" name="testimonials_cemail" id="testimonials_cemail" />
                    </div>
                    <div class="title_li div_li">
                        <label><?php echo Yii::t('drugs', 'Thema'); ?>:</label>
                        <?php echo $form->textField($model,'tit_name',array('id'=>'tit_name', 'maxlength'=>256,)); ?>
                    </div>
                </fieldset>
            </li>
            <li class="li_radio first_li_radio">
                <fieldset>
                    <legend><?php echo Yii::t('drugs', 'Bewertung abgeben'); ?>:</legend>
                    <div class="bx_form">
                        <span><?php echo Yii::t('drugs', 'Webseitenfreundlichkeit'); ?></span>
                        <div id="stars_rate_feature"></div>
                    </div>
                    <div class="bx_form bx_form_2">
                        <span><?php echo Yii::t('drugs', 'Versand Zufriedenheit'); ?></span>
                        <div id="stars_rate_performance"></div>
                    </div>
                    <div class="bx_form bx_form_2">
                        <span><?php echo Yii::t('drugs', 'Preis / Leistung'); ?></span>
                        <div id="stars_rate_value"></div>
                    </div>
                    <div class="bx_form bx_form_2">
                        <span><?php echo Yii::t('drugs', 'Empfehlung'); ?></span>
                        <div id="stars_rate_overall"></div>
                    </div>
                </fieldset>
            </li>

            <li class="text_area_li">
                <label><?php echo Yii::t('drugs', 'Kundenkommentar'); ?>:</label>
                <?php echo $form->textArea($model,'content',array('id'=>'rev_comment', 'rows'=>5, 'cols'=>40)); ?>
            </li>

	        <?php if(CCaptcha::checkRequirements() ):?>
	            <li class="captcha_li">
	                <label><?php echo Yii::t('drugs', 'Geben Sie die Zeichen in dem Bild ein'); ?></label>
	                <label class="img_captcha" for="review_captcha">
	                    <?php $this->widget('CCaptcha', array('showRefreshButton'=>false, 'clickableImage'=>true)); ?>
	                </label>
	                <?php echo $form->textField($model,'code',array('id'=>'rev_captcha', 'size'=>20,'maxlength'=>256)); ?>
	            </li>
			<?php endif; ?>

            <li>
                <button class="button-css-blue" type="submit" id="rev_submit">
                    <span> <?php echo Yii::t('drugs', 'Rezension abschicken'); ?> </span>
                </button>
                <div style="clear:both; overflow: hidden;">&nbsp;</div>
            </li>
        </ul>
<?php $this->endWidget(); ?>



