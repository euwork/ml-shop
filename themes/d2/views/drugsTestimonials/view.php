<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/raty/jquery.raty.min.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('raty',

"

    $('#rev_submit').click(function() {
        if (($('#rev_name').val() == '' || $('#rev_comment').val() == '') && $('#rev_captcha').val() != '') {
            if(confirm(\"".Yii::t('testimonials', 'Sind Sie sicher, dass Sie eine leere Bewertung hinterlassen möchten?')."\")){
                $('#review-form').submit();
            } else {
                return false;
            }
        }
        if ( $('#rev_captcha').val() == '') {
            alert(\"".Yii::t('testimonials', 'Geben Sie die Zeichen in dem Bild ein!')."\");
            return false;
        }
        else {
            $('#review-form').submit();
        }
    });
    $('.stars_img_box').mouseover(function() {
        $(this).siblings('.block_star_rat').toggle();
    }).mouseout(function(){
        $(this).siblings('.block_star_rat').toggle();
    });
	$.fn.raty.defaults.path = '".Yii::app()->theme->baseUrl."/js/raty/img/';
    $.fn.raty.defaults.cancel    = true;
    $.fn.raty.defaults.cancelOff = 'cancel-off-big.png';
    $.fn.raty.defaults.cancelOn  = 'cancel-on-big.png';
    $.fn.raty.defaults.size      = 24;
    $.fn.raty.defaults.starHalf  = 'star-half-big.png';
    $.fn.raty.defaults.starOff   = 'star-off-big.png';
    $.fn.raty.defaults.starOn    = 'star-on-big.png';

    $('#stars_rate_feature').raty({ scoreName:'ratefeature', start: 5.0 });
    $('#stars_rate_performance').raty({ scoreName:'rateperformance', start: 5.0  });
    $('#stars_rate_value').raty({ scoreName:'ratevalue', start: 5.0  });
    $('#stars_rate_overall').raty({ scoreName:'rateoverall', start: 5.0  });

", CClientScript::POS_END);




Yii::app()->clientScript->registerScript('vote',
"
$('.info_commentsbox a').click(function(){

    var rev = $(this).closest('div.info_commentsbox').attr('rev-id');
    var vote = $(this).attr('class') == 'green' ? 1 : 0;
    var url = '" . Yii::app()->createUrl('/testimonials/vote') . "';

    $.ajax({
      type: \"POST\",
      url: url,
      data: {'rev': rev, 'vote': vote},
      success: function(data){       
        $('.helpfull_info#rev_'+rev+'>span#vote_yes').html(data.vote_yes);
        $('.helpfull_info#rev_'+rev+'>span#vote_all').html(data.vote_all);
      },
      dataType: 'json'
    });


    return false;
});


", CClientScript::POS_END);

$domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
?>




<h1 class="testimonial_title"><?php echo $model->header; ?></h1>
<?php echo CHtml::link($model->drug_ancor, $model->drug->url, array('class'=>'testimonial_a')); ?>
<p>
        
</p>
<?php if ($model->drug->rating): ?>
    <?php //$rating = explode('|', $model->drug->rating);?>
    <?php //$line_ratings = array_slice($rating, 2); ?>

            <div class="testimonial_rating_info">
                <?php $count_drug_reviews = $model->drug->getReviewsCount(); ?>
                <b><?php echo $count_drug_reviews ?>  <?php echo Yii::t('drugs', 'Rezensionen'); ?></b>
                <?php $i = 5; $j =1; while($j < 6) : ?>
                    <div class="line_rating">
                        <span> 
                            <?php echo $i . ' '; 
                            if($i == 1) : 
                                echo Yii::t('drugs', 'Stern:');
                            elseif($i==2 && in_array(Yii::app()->language, array('sl'))):
                                echo Yii::t('drugs', '2Sterne:');
                            elseif($i==5 && in_array(Yii::app()->language, array('pl','sr', 'sk', 'sl', 'hr', 'cs'))):
                                echo Yii::t('drugs', '5Sterne:');
                            else: 
                                echo Yii::t('drugs', 'Sterne:');
                            endif; ?> 
                        </span> 
                        <div class="box_rating_grey">
                            <div class="box_rating_yellow" style="width: <?php echo $model->drug->getRatingStarPrc( $i);  ?>%;">  &nbsp;</div>
                        </div>
                      <span>(<?php echo $model->drug->getRatingStar( $i);$j++;  ?>)</span>
                    </div>
                    <?php $i--;?>
                <?php endwhile; ?>

            </div>


		    <div class="stars_box box_testimonial_stars">
		            <div id="main_avg_rate_div" class="stars_img_box">
		                <span class="stars_img" style="width:<?php echo $model->drug->getRatingPrc();?>%;">&nbsp;</span> 
		            </div>
		            <span class="block_txt_rate">
		                <span class="text_star"><?php echo $model->drug->getRating();?></span> <span>/</span>
		                <span><?php echo $count_drug_reviews;?></span> 
		            </span>
		            <span><?php echo Yii::t('drugs', 'Kundenrezensionen'); ?></span>
                     <div id="div_domtabs" class="domtabs">
                            <div class="left_li">
		                        <a class="menu_bomtabs" href="#newtestimonial"><?php echo Yii::t('drugs', 'Eigene Bewertung erstellen'); ?></a>                 
		                    </div>
                     </div>
		           
		    </div>




		  
    

<?php endif;?>


<div style="clear:both; overflow: hidden;">&nbsp;</div>

<div class="domtab">
    <div class="top_box">&nbsp;</div>
    <div class="box_txt">
        <h2><a><?php echo $model->subheader; ?></a></h2>


        <?php //foreach($model->drug->reviews as $item) : ?>
		<?php foreach($reviews as $item) : ?>

        <?php if($item->content == '') continue; ?>

		<div class="txt_commentsbox">
            <span class="helpfull_info" id="rev_<?php echo $item->rev_id; ?>">
                <?php echo Yii::t('drugs', 
                    '{vote_yes} von {vote_all} fanden diese Bewertung hilfreich', 
                    array('{vote_yes}'=> '<span id="vote_yes">' . $item->vote_yes . '</span>', '{vote_all}'=> '<span id="vote_all">' . $item->vote_all . '</span>')); ?>
            </span>
            <div class="stars_img_box">
                <?php $item_rating = number_format($item->points / 5 * 100, 2); ?>
                <span class="stars_img" style="width:<?php echo $item_rating;?>%;"> &nbsp;</span>
            </div>
            <div class="current_box">

                <span class="current_review_points"><?php echo $item->points; ?></span>/<span>5</span> 
            </div>
            <div class="txt_box_date">
                <?php if($item->tit_name): ?>
                    <span class="tit_name">"<?php echo str_replace('{domain}', $domain, $item->tit_name); ?>"</span>
                <?php endif; ?>
                <span><?php //echo date('d-m-Y', strtotime($item->date_cr)); ?></span>
            </div>
            <div class="author_box">
                <?php if(in_array(Yii::app()->language, array('hu', 'et', 'tr',))): ?>
                    <span><?php echo $item->author; ?></span> <?php echo Yii::t('drugs', 'von'); ?>
                <?php elseif(in_array(Yii::app()->language, array('fi'))): ?>
                    <span><?php echo $item->author; ?><?php #echo Yii::t('drugs', 'von'); ?></span>
                <?php else: ?>
                    <?php echo Yii::t('drugs', 'von'); ?> <span><?php echo $item->author; ?></span>
                <?php endif;?>
            </div>

            <span class="txt_comment"><?php echo str_replace('{domain}', $domain, $item->content); ?></span>
            <div class="info_commentsbox" rev-id="<?php echo $item->rev_id; ?>">
                <a href="#" rel="nofollow" class="red" onclick="js: return false;"><?php echo Yii::t('drugs', 'nein'); ?></a>
                <a href="#" rel="nofollow" class="green" onclick="js: return false;"><?php echo Yii::t('drugs', 'ja'); ?></a>&nbsp;&nbsp;
                <span><?php echo Yii::t('drugs', 'Hat Ihnen diese Bewertung geholfen?'); ?>&nbsp;&nbsp;</span>
            </div>
        </div>

		<?php endforeach; ?>
    </div>


                    <div class="pagenavi">
                    <?php $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'header' => false,
                        'prevPageLabel' => '&#8592;',
                        'nextPageLabel' => '&#8594;',
                        'maxButtonCount' => 4,
                        'cssFile' => Yii::app()->theme->baseUrl.'/css-europe/pager.css',
                    ))?>
                    </div>
                    <div class="clean"></div>



	<div class="box_txt">
	    <h2><a name="newtestimonial" id="eigenebewertungerstellen"><?php echo Yii::t('drugs', 'Eigene Bewertung erstellen'); ?></a></h2>
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash flash-' . $key . '">' . $message . "</div>\n";
            }
        ?>
	    <?php $this->renderPartial('_form_review', array('model'=>$form)); ?>
	    
	</div>




</div>
