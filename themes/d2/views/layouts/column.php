<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>   
        <div id="col-1">

            
            <div id="products">
                <?php $menu = Categories::getMenu(); ?>
                <?php foreach($menu as $cat): ?>
                        <div class="section">
                                <b><a class="wrapper_category_a" href="<?php echo $cat['url']; ?>" title="<?php echo $cat['name'];?>"><?php echo $cat['name'];?>&nbsp;</a></b>
                                <ul>
                                    <?php foreach($cat['drugs'] as $drug): ?>
                                            <li> 
                                                <a href="<?php echo $drug['url']; ?>" title="<?php echo $drug['name']; ?>"><?php echo $drug['name']; ?></a> 
                                            </li>
                                    <?php endforeach;?>
                                </ul>
                        </div>
                <?php endforeach;?>
            </div>


            <?php if(false): ?>
            <!-- <div class="section">
                <b><?php echo Yii::t('column3', 'Artikel'); ?></b>
                <ul>
                    <li><a href="<?php echo Yii::app()->createUrl('/articles'); ?>"><?php echo Yii::t('column3', 'Artikel'); ?></a>
                    </li>
                </ul>
            </div> -->
            <?php endif; ?>

            

            <div class="section side-section">
                <b><?php echo Yii::t('column3', 'Nützliche Informationen'); ?></b>
                <ol>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(4); ?>"><?php echo Yii::t('column3', 'Lieferkonditionen'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(5); ?>"><?php echo Yii::t('column3', 'Rückerstattung'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(6); ?>"><?php echo Yii::t('column3', 'AGB'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(7); ?>"><?php echo Yii::t('column3', 'Datenschutz'); ?></a>
                    </li>
                    <?php if(!in_array(Yii::app()->language, array('pt', 'br'))) : ?>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(8); ?>"><?php echo Yii::t('column3', 'Impressum'); ?></a>
                    </li>
                    <?php endif; ?>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(9); ?>"><?php echo Yii::t('column3', 'Garantie'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Yii::app()->createUrl('/affiliates'); ?>"><?php echo Yii::t('column3', 'Affiliates'); ?></a>
                    </li>
                </ol>
            </div>


            
            <img src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/secure.png" />
            


        </div>



        <div id="col-2">
            <?php echo $content; ?>
        </div>
        <!--/col-2-->
        <?php if(in_array(Yii::app()->language, array( 'uk'))) : ?>
        <div id="col-3">
        <?php else: ?>
        <div id="col-3"  class="col-3-block">
        <?php endif; ?>

            <div class="section" id="cart-block">
                <?php $count = Yii::app()->shoppingCart->getCount(); ?>
                <span class="h3"><?php echo Yii::t('column3', 'Warenkorb'); ?> </span>
                <p>
                    
                    <?php if($count == 0): ?>
                        <?php echo Yii::t('column3', 'Kein Produkt im Warenkorb'); ?>
                    <?php elseif($count == 1): ?>
                        <?php echo Yii::t('column3', '1 Produkt im Warenkorb'); ?>
                    <?php elseif(Yii::app()->language == 'tr' && $count > 1 ):?>
                        <?php echo Yii::t('column3', '{count} Produkt im Warenkorb', array('{count}'=>$count)); ?>
                    <?php elseif(Yii::app()->language == 'sl' && $count == 2 ):?>
                        <?php echo Yii::t('column3', '2 Produkt im Warenkorb'); ?>
                    <?php elseif(Yii::app()->language == 'sl' && ($count == 3 or $count == 4) ):?>
                        <?php echo Yii::t('column3', '{count} Produkt im Warenkorb', array('{count}'=>$count)); ?>
                    <?php else: ?>
                        <span><?php echo $count; ?></span> <?php echo Yii::t('column3', 'Produkte im Warenkorb'); ?>
                    <?php endif; ?>
                    
                    <br /><span><?php echo Yii::t('column3', 'Gesamtbetrag'); ?>:</span>
                    <!--Gesamtbestellung-->
                    <br /><strong><?php echo Currencies::symbol();?> <?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?></strong>
                    <?php if($count > 0): ?>
                        <br /><br /><a class="btn_blank" style="margin: 5px auto;color: white;text-decoration: none;" href="<?php echo Yii::app()->createUrl('/cart'); ?>"><?php echo Yii::t('column3', 'Warenkorb'); ?> </a>
                    <?php endif; ?>
                    
                </p>

            </div>


            <?php

            $mediasuppExt = 'png';

            ?>
            <img src="<?php echo Theme::getImgUrlTemplate();?>/languages/<?php echo Yii::app()->language; ?>/media-supp.<?php echo $mediasuppExt; ?>" style="padding:0;" alt="" class="rban" />

            <?php

            $offerExt = 'jpg';
            if(!in_array(Yii::app()->language, array('de','fr','es','it','nl', 'uk', 'pt', 'se', 'be'))) $offerExt = 'png';
            ?>
            <img src="<?php echo Theme::getImgUrlTemplate();?>/languages/<?php echo Yii::app()->language; ?>/offer.<?php echo $offerExt; ?>" class="rban" />

            <div class="section narrow section-x2">
                <span class="h3"><?php echo Yii::t('column3', 'ZAHLUNGSMÖGLICHKEITEN'); ?></span>
                <?php 
                if(in_array(Yii::app()->language, array(  'fr', 'be' ))) {
                    
                    $pm_img = 'fr_bank_pm.jpg';
                     
                } else {
                    $pm_img = 'ideal_bank_pm.png';
                } 
                       
                 ?>
                <img src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/<?php echo $pm_img; ?>" alt="" width="190" />
            </div>
            <div class="section narrow section-x3">
                <span class="h3"><?php echo Yii::t('column3', 'VERSANDPARTNER'); ?></span>
                <img src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/delivery.png" alt="" />
            </div>

            <?php if(in_array(Yii::app()->language, array( 'uk'))) : ?>
                <div class="section narrow x5">
                    <span class="h3" id="sertified"><?php echo Yii::t('column3', "WE'RE CERTIFIED"); ?></span>
                    <img src="<?php echo Theme::getImgUrlTemplate();?>/languages/<?php echo Yii::app()->language; ?>/sidebar-logos.jpg" alt="" />
                </div>
            <?php endif; ?>
            
        </div>
        <!--/col-3-->
<?php $this->endContent(); ?>