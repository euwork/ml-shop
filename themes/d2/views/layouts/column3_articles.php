<?php $this->beginContent('//layouts/main'); ?> 
<div id="col-1">
    <div class="section blog_menu" id="specials">
        <?php $menu = CategoriesArticles::getMenu(); ?>
        <?php foreach($menu as $cat): ?>
            <b><a style="color: #000;" href="<?php echo $cat->url; ?>"><?php echo $cat->name;?></a></b>
            <ul style="display: block;">
                <?php foreach($cat->articles as $article): ?>
                    <li>
                        <a href="<?php echo $article->url; ?>"><?php echo $article->postname; ?></a>
                    </li>
                <?php endforeach;?>
            </ul>
        <?php endforeach;?>
    </div>
</div>
<div id="col-2">
    <?php echo $content; ?>
</div>
<div id="col-3">
    <div class="section narrow">
        <img style="padding: 0 0 15px 0;" src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/articles/banner_viagra.jpg" alt="" />
    </div>
    <div class="section narrow">
        <img style="padding: 0 0 15px 0;" src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/articles/banner_lovegra.jpg" alt="" />
    </div>
    <div class="section narrow">
        <img style="padding: 0 0 15px 0;" src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/articles/banner_slimming.jpg" alt="" />
    </div>
    <div class="section narrow">
        <img style="padding: 0 0 15px 0;" src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/articles/banner_zyban.jpg" alt="" />
    </div>
    <div class="section narrow">
        <img style="padding: 0 0 15px 0;" src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/articles/banner_propecia.jpg" alt="" />
    </div>
    <div class="section narrow">
        <span class="h3">Vorteile</span>
        <a href="<?php echo Yii::app()->createUrl('/pages/online-apotheke-vorteile.html'); ?>">
            <img src="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/languages/<?php echo Yii::app()->language; ?>/advantage_small.png" alt="" />
        </a>
    </div>
</div>

<?php $this->endContent(); ?>