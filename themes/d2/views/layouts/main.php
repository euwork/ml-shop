<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo Yii::app()->language;?>">

<head>
	<meta content="True" name="HandheldFriendly">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="keywords" content="<?php echo CHtml::encode($this->keywords); ?>" />
    <meta name="description" content="<?php echo CHtml::encode($this->description); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl;?>/css-europe/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl;?>/css-europe/swiper.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl;?>/css-europe/responsive.css?v=<?php echo time(); ?>" />
    <link rel="canonical" href="<?php echo Yii::app()->createUrl(  '/' . Yii::app()->request->getPathInfo() ); ?>" />
    <?php Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END; ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/swiper.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/responsive.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScript('responsive_translate','
                tr_homepage = "'.Yii::t('mobile', 'Startseite').'";
                tr_menu = "'.Yii::t('mobile', 'Menü').'";
                tr_cart = "'.Yii::t('mobile', 'Warenkorb').'";
                tr_contact = "'.Yii::t('mobile', 'Kontakt').'";
                tr_pills = "'.Yii::t('mobile', 'Pillen').'";
                href_contact = "'.Yii::app()->createUrl('/contacts').'";
                href_cart = "'.Yii::app()->createUrl('/cart').'";
                href_home = "'.Yii::app()->createUrl('/').'";
            ', CClientScript::POS_HEAD
        );
    ?>
    <?php $this->widget('CounterWidget'); ?>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img-europe/favicon.ico" />
</head>
<body>
    <div id="headerWrap">
        <?php
        $europeheaderExt = 'jpg';
        if(!in_array(Yii::app()->language, array('de','fr','es','it','nl', 'uk', 'pt', 'se'))) $europeheaderExt = 'png';
        ?>
        <div id="header" style="background-image: url('<?php echo Theme::getImgUrlTemplate();?>/languages/<?php echo Yii::app()->language; ?>/pharmatheke-europeheader.<?php echo $europeheaderExt;?>')">
        </div>
        <div id="headNav" class="headNav">
            <ul id="topNav"<?php if(Yii::app()->language == 'de') echo ' class="de-topnav"'; ?>>
                <li><a href="<?php echo Yii::app()->createUrl('/'); ?>"><?php echo Yii::t('main', 'Startseite'); ?></a>
                </li>
                <li><a href="<?php echo Pages::getUrlById(1); ?>"><?php echo Yii::t('main', 'Über uns'); ?> </a>
                </li>
                <li><a href="<?php echo Pages::getUrlById(2); ?>"><?php echo Yii::t('main', 'F.A.Q.'); ?></a>
                </li>
                <li><a rel="nofollow" href="<?php echo Yii::app()->createUrl('/contacts'); ?>"><?php echo Yii::t('main', 'Kontakt'); ?></a>
                </li>
                <li><a href="<?php echo Pages::getUrlById(3); ?>"><?php echo Yii::t('main', 'So bestellen Sie'); ?></a>
                </li>
            </ul>

            
            <div class="a-curr">
                <?php $this->widget('CurrencySwitcherWidget'); ?>
            </div>
            <div class="a-langs">
                <?php $this->widget('LanguageSwitcherWidget'); ?>
            </div>
			
        </div>
    </div>
	
	<?php include 'mobile-menu.php'; ?>
	
    <div id="mainWrap">
        <div id="to_top">
            <i></i>
            <span><?php echo Yii::t('main', 'To top'); ?></span>
        </div>
        <?php echo $content; ?>
    </div>


    <div id="footerWrap">
        <div id="footer">
            
            <div class="h6"><?php echo Yii::t('main', 'Urheberrecht'); ?> &copy; <?php echo date('Y'); ?> <?php echo $this->domain; ?> &nbsp;&nbsp; <?php echo Yii::t('main', 'Alle Rechte vorbehalten'); ?></div>
        </div>
    </div>

<?php echo Yii::app()->config->get('external_counters'); ?>
</body>
</html>
<?php 
Yii::app()->clientScript->registerScript('to-top',
"
$(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('#to_top').fadeIn();
        } else {
            $('#to_top').fadeOut();
        }
    });
    $('#to_top').click(function () {
        $('body,html').animate( { scrollTop: 0 }, 400 );
    });
", CClientScript::POS_READY);
