<div class="menu-mobile">
	<div class="mob-m">
		<div class="mob-m__btn">
			<button><span></span><span></span><span></span></button>
			<div class="mob-m__title"><?php echo Yii::t('mobile', 'Menü'); ?></div>
		</div>
		
		<div class="cart-mobile">
		<?php $count = Yii::app()->shoppingCart->getCount(); ?>
			<div class="cart-mobile__icon">
				<span><?php echo $count; ?></span>
			</div>
			
			<div class="cart-mobile__title">
				<?php echo Yii::t('mobile', 'Warenkorb'); ?> (<span><?php echo Currencies::symbol();?> <?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?></span>)
			</div>
		</div>
	</div>
	
	<div class="item-cart">
		<div class="item-cart__row">
		<?php if($count == 0): ?>
			<?php echo Yii::t('column3', 'Kein Produkt im Warenkorb'); ?>
		<?php elseif($count == 1): ?>
			<?php echo Yii::t('column3', '1 Produkt im Warenkorb'); ?>
		<?php else: ?>
			<span><?php echo $count; ?></span> <?php echo Yii::t('column3', 'Produkte im Warenkorb'); ?>
		<?php endif; ?>
		</div>
		
		<div class="item-cart__row">
		<span><?php echo Yii::t('column3', 'Gesamtbetrag'); ?>:</span>
		<strong><?php echo Currencies::symbol();?> <?php echo Currencies::price(Yii::app()->shoppingCart->getCost()); ?></strong>
		</div>
		
		<?php if($count > 0): ?>
		<a class="btn_blank" style="margin: 5px auto;color: white;text-decoration: none;" href="<?php echo Yii::app()->createUrl('/cart'); ?>"><?php echo Yii::t('column3', 'Warenkorb'); ?> </a>
		<?php endif; ?>
	</div>
	
	
	<div class="omenu">
	
		<div class="omenu-top">
			<div class="omenu-langs">
                <?php
				$currentUrl = ltrim(Yii::app()->request->url, '/');
				$links = array();
				foreach (DMultilangHelper::langHash() as $lang => $data){
					if(Yii::app()->controller->id == 'cart'){
						$url = '/' . ($lang != Yii::app()->config->get('defaultLanguage') ? $lang . '/' . $currentUrl :  $currentUrl ) ;//. $currentUrl;
					} else {
						$url = '/' . ($lang != Yii::app()->config->get('defaultLanguage') ? $lang . '/' : '')  ;//. $currentUrl;
					}
					$active = Yii::app()->language == $lang ? 1 : 0;
					$links[] = array('url' => $url, 'name' => $data['name'], 'lang' => $lang, 'active' => $active, 'country'=>$data['country']);
				}
				
				?>
				<div class="omenu-langs__item">
				<?php foreach($links as $link): ?>
					<?php if($link['active'] == 1) : ?>
					<img src="<?php echo Yii::app()->theme->baseUrl;?>/images-lang/ico/<?php echo $link['lang']; ?>.png" alt="lang">
					<span><?php echo $link['name']; ?></span>
					<?php endif; ?>
				<?php endforeach; ?>
					
				</div>
				<select class="lang-select">
				<?php foreach($links as $link): ?>
					<option value="<?php echo $link['url']; ?>"<?php if($link['active'] == 1){echo ' selected';} ?>><?php echo $link['name']; ?></option>
				<?php endforeach; ?>
				</select>
            </div>
			
			<div class="a-curr">
                <?php
				$currentUrl = Yii::app()->request->originalUrl;
				$links = array();
				foreach (Currencies::getArray() as $code => $symbol){
					$url =  $currentUrl . '?' . Yii::app()->currencies->get_var_name . '=' . $code ;
					$active = Yii::app()->currencies->getValue() == $code ? 1 : 0;
					$links[] = array('url' => $url, 'code' => $code, 'symbol' => $symbol, 'active' => $active, );
				}
				?>
				<div class="a-curr__item">
				<?php foreach($links as $link): ?>
					<?php if($link['active'] == 1) : ?>
					<span><?php echo $link['symbol']; ?></span> <?php echo $link['code']; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
				
				<select class="cur-select">
				<?php foreach($links as $link): ?>
					<option value="<?php echo $link['url']; ?>"<?php if($link['active'] == 1){echo ' selected';} ?>><?php echo $link['symbol']; ?> <?php echo $link['code']; ?></option>
				<?php endforeach; ?>
				</select>
            </div>
		</div>
	
		<div class="product-mobile">
		<?php $menu = Categories::getMenu(); ?>
		<?php foreach($menu as $cat): ?>
				<div class="product-mobile__item">
					<a class="product-mobile__title" href="<?php echo $cat['url']; ?>" title="<?php echo $cat['name'];?>"><?php echo $cat['name'];?></a>
					<ul>
						<?php foreach($cat['drugs'] as $drug): ?>
								<li> 
									<a href="<?php echo $drug['url']; ?>" title="<?php echo $drug['name']; ?>"><?php echo $drug['name']; ?></a> 
								</li>
						<?php endforeach;?>
					</ul>
				</div>
		<?php endforeach;?>
		
			<div class="product-mobile__item last">
				<div class="product-mobile__title"><?php echo Yii::t('column3', 'Nützliche Informationen'); ?></div>
				<ul class="user">
					 <li><a rel="nofollow" href="<?php echo Pages::getUrlById(4); ?>"><?php echo Yii::t('column3', 'Lieferkonditionen'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(5); ?>"><?php echo Yii::t('column3', 'Rückerstattung'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(6); ?>"><?php echo Yii::t('column3', 'AGB'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(7); ?>"><?php echo Yii::t('column3', 'Datenschutz'); ?></a>
                    </li>
                    <?php if(!in_array(Yii::app()->language, array('pt', 'br'))) : ?>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(8); ?>"><?php echo Yii::t('column3', 'Impressum'); ?></a>
                    </li>
                    <?php endif; ?>
                    <li><a rel="nofollow" href="<?php echo Pages::getUrlById(9); ?>"><?php echo Yii::t('column3', 'Garantie'); ?> </a>
                    </li>
                    <li><a rel="nofollow" href="<?php echo Yii::app()->createUrl('/affiliates'); ?>"><?php echo Yii::t('column3', 'Affiliates'); ?></a>
                    </li>
				</ul>
			</div>
		
			<div class="product-mobile__item dop">
				<ul class="user">
					<li><a href="<?php echo Yii::app()->createUrl('/'); ?>"><?php echo Yii::t('main', 'Startseite'); ?></a>
					</li>
					<li><a href="<?php echo Pages::getUrlById(1); ?>"><?php echo Yii::t('main', 'Über uns'); ?> </a>
					</li>
					<li><a href="<?php echo Pages::getUrlById(2); ?>"><?php echo Yii::t('main', 'F.A.Q.'); ?></a>
					</li>
					<li><a rel="nofollow" href="<?php echo Yii::app()->createUrl('/contacts'); ?>"><?php echo Yii::t('main', 'Kontakt'); ?></a>
					</li>
					<li><a href="<?php echo Pages::getUrlById(3); ?>"><?php echo Yii::t('main', 'So bestellen Sie'); ?></a>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
</div>