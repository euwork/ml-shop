<?php 

$imgExt = 'png';
if(in_array(Yii::app()->language,array('de','fr','es','it','nl', 'uk', 'pt', 'se', 'be', 'sg', 'ie', ))) {
	$imgExt = 'jpg';
} 

$srcPath = Yii::app()->theme->baseUrl . '/images/front_bunner/' . Yii::app()->language;

?>

<div class="top-bunner" id="top-bunner">
		<a href="<?php echo Drugs::getUrlById(10); ?>"><img src="<?php echo $srcPath; ?>/kamagra.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Kamagra Brausetabletten bestellen'); ?>"/></a>
		<a href="<?php echo Drugs::getUrlById(14); ?>"><img src="<?php echo $srcPath; ?>/priligy.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Priligy bestellen'); ?>"/></a>
		<a href="<?php echo Drugs::getUrlById(11); ?>"><img src="<?php echo $srcPath; ?>/kamagraOralJelly.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Kamagra Oral Jelly bestellen'); ?>"/></a>
</div>

<div class="bunner-mobile">
	<div class="bunner-mobile__slider swiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<a href="<?php echo Drugs::getUrlById(10); ?>"><img src="<?php echo $srcPath; ?>/kamagra.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Kamagra Brausetabletten bestellen'); ?>"/></a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo Drugs::getUrlById(14); ?>"><img src="<?php echo $srcPath; ?>/priligy.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Priligy bestellen'); ?>"/></a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo Drugs::getUrlById(11); ?>"><img src="<?php echo $srcPath; ?>/kamagraOralJelly.<?php echo $imgExt;?>" alt="<?php echo Yii::t('index', 'Kamagra Oral Jelly bestellen'); ?>"/></a>
			</div>
		</div>
	</div>
</div>

<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.carouFredSel-6.2.1.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('carusel',

'
var mySwiper = new Swiper (".bunner-mobile__slider", {
	loop: true,
	slidesPerView: 1,
	effect: "fade",
	speed: 1000,
	autoplay: {
		delay: 5000,
	},
})
$("#top-bunner").carouFredSel({
			width: 581,
			height: 210,
			items               : 1,
			direction           : "left",
			align			: "left",
			scroll : {
				items           : 1,
				easing          : "linear",
				fx			: "cover-fade",
				duration        : 1000,                        
				pauseOnHover    : true
			}  
		});
    

', CClientScript::POS_READY);
