<?php 

if(Yii::app()->config->get('home.show_banner') == true) {
	 echo $this->renderPartial('_banner', array()); 
} 

?>

<?php if($text->header): ?>
<h1><?php echo $text->header; ?></h1>
<?php endif;?>

<?php
    $products = array();
    foreach($drugs as $item):

	    $products[] = 
	    '<div class="product" onclick="document.location.href=\'' . $item['url'] . '\';">
		    <img src="'. $item['images'][DrugsImages::TYPE_TEXT] . '" alt="" />
		    <img src="'. $item['images'][DrugsImages::TYPE_CAT] . '" alt="" />
	        <a href="' . $item['url'] . '" title="' . Yii::t('categories', 'Jetzt kaufen') . ' ' . $item['name'] . '" class="btn_blank" style="margin: 0 auto;">' . Yii::t('categories', 'Jetzt kaufen') . '</a>
	    </div>';
    endforeach; 

    $products = implode("\n", $products);


    $content = $text->content;
    $content = str_replace('{products}', $products, $content);
    echo $content;

?>

<?php foreach($reviews as $item): ?>
<div class="box_txt">
	<div class="txt_commentsbox txt_commentsbox_homepage">
		<div class="author_box">
			<span class="tit_name">"<?php echo str_replace('{domain}', $this->domain, $item->tit_name); ?>"</span>
		</div>
		<span class="txt_comment">
			<?php echo str_replace('{domain}', $this->domain, $item->content); ?>                     
		</span>
		<div class="info_commentsbox">
		</div>
	</div>
</div>
<?php endforeach; ?>